variables:
   BUILD_ARTIFACT_DIR: "./gitlab-build" # Shared jobs build data
   DEFAULT_IMAGE: "docker@sha256:c8bb6fa5388b56304dd770c4bc0478de81ce18540173b1a589178c0d31bfce90"

# docker:latest (23.0) issues : https://docs.docker.com/engine/release-notes/23.0/
# Workarounds : https://forum.gitlab.com/t/the-ci-cd-pipeline-suddenly-fails-without-any-changes-done-to-the-repository/81117
default:
   image: $DEFAULT_IMAGE
   services:
      - docker:dind@sha256:c8bb6fa5388b56304dd770c4bc0478de81ce18540173b1a589178c0d31bfce90

stages:
   - build
   - post-build

# Base configuration.
.common:base:
   retry:
      max: 2
      when:
         - runner_system_failure
   artifacts:
      name: base
      expire_in: 1 day
      paths:
         - $BUILD_ARTIFACT_DIR

# Common build.
.common:build:
   extends:
      - .common:base
      - .common:docker
   variables:
      BASE_VERSION: "${CI_COMMIT_REF_SLUG}"
      VERSION: "$${BASE_VERSION}-SNAPSHOT"
      SONAR: "false"
      SITE: "false"
      SITE_EXPORT: "/export/site.tar.gz"
      TESTS_EXPORT: "/export/tests.tar.gz"
      SKIP_TESTS: "false"
      METADATA_CI_PIPELINE_URL: "${CI_PIPELINE_URL}"
      METADATA_SCM_BRANCH: "${CI_COMMIT_REF_NAME}"
      METADATA_SCM_COMMIT: "${CI_COMMIT_SHORT_SHA}"
      DOCKER_BUILD_ARGS: "INSTANCE VERSION CI_JOB_TOKEN METADATA_CI_PIPELINE_URL METADATA_SCM_BRANCH METADATA_SCM_COMMIT SONAR SONAR_USERNAME SONAR_PASSWORD SITE SITE_EXPORT SKIP_TESTS TESTS_EXPORT"
   before_script:
      - cp .docker/buildsdk /bin
      - mkdir -p "$BUILD_ARTIFACT_DIR"
      - test -f "$BUILD_ARTIFACT_DIR/context-general" && source "$BUILD_ARTIFACT_DIR/context-general"
      - eval $(BASE_VERSION="$BASE_VERSION" VERSION="$VERSION" buildsdk sh_eval BASE_VERSION VERSION)
      - buildsdk export_env "$BUILD_ARTIFACT_DIR/context-general" BASE_VERSION VERSION SONAR SITE SITE_EXPORT SKIP_TESTS TESTS_EXPORT
      - buildsdk title "Build version $(buildsdk color 34 $VERSION) from GIT $(buildsdk color 34 $CI_COMMIT_REF_NAME):${CI_COMMIT_SHORT_SHA}:$(buildsdk color '34;4' $CI_COMMIT_TITLE)"
   after_script:
      - cp .docker/buildsdk /bin
      - test -f "$BUILD_ARTIFACT_DIR/context-general" && source "$BUILD_ARTIFACT_DIR/context-general"
      - test -f "$BUILD_ARTIFACT_DIR/context-docker" && source "$BUILD_ARTIFACT_DIR/context-docker"
      - if test "$DOCKER_TARGET" = "build"; then
      - if $SITE; then
      - mkdir -p "$BUILD_ARTIFACT_DIR/site"
      - docker run --rm --entrypoint sh "${DOCKER_IMAGE}:${DOCKER_IMAGE_TAG}" -c "cat $SITE_EXPORT" | tar xzf - -C "$BUILD_ARTIFACT_DIR/site"
      - fi
      - if ! $SKIP_TESTS; then
      - docker run --rm --entrypoint sh "${DOCKER_IMAGE}:${DOCKER_IMAGE_TAG}" -c "cat $TESTS_EXPORT" | tar xzf - -C .
      - fi
      - fi

   coverage: /JaCoCo instructions coverage=([0-9.]+)%/
   artifacts:
      name: build
      reports:
         junit:
            - "*/target/*-reports/TEST-*.xml"
            - "*/*/target/*-reports/TEST-*.xml"
            - "*/*/*/target/*-reports/TEST-*.xml"
      paths:
         - $BUILD_ARTIFACT_DIR

# Common merge request build.
.common:build:merge-request:
   extends:
      - .common:build
   variables:
      MAVEN_PHASE: verify # Maven target phase in Dockerfile, if supported, for this build
   rules:
      -  if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

# Common branch build.
# Site generation can be provided in ./pages directory if any.
.common:build:branch:
   extends: .common:build

# Common Docker image.
.common:docker:
   extends: .common:base
   variables:
      DOCKER_FILE: "Dockerfile" # Dockerfile to use
      DOCKER_BUILD_OPTS: "" # Optional docker options for docker build command
      DOCKER_TARGET: "" # Build stage target
      DOCKER_BASE_IMAGE: "${CI_PROJECT_NAME}" # Docker base image name
      QUALIFIER: "" # Optional qualifier to add to image name
      INSTANCE: "" # Optional instance name
      VERSION: "" # Maven artifacts version.
      DOCKER_IMAGE_TAG: "$${VERSION}" # Image tag.
      DOCKER_IMAGE_ALIAS_TAGS: "" # Optional alias tag.
      DOCKER_CACHE_IMAGES: "" # Optional image to cache from (can be comma separated list by priority order)
      DOCKER_PUSH: "false" # Whether to push image or not
      DOCKER_BUILD_ARGS: "INSTANCE VERSION"
      DOCKER_IGNORE_GIT: "false" # Whether to add .git to .dockerignore
   script:
      - cp .docker/buildsdk /bin
      - test -f "$BUILD_ARTIFACT_DIR/context-docker" && source "$BUILD_ARTIFACT_DIR/context-docker"
      - eval $(BASE_VERSION="$BASE_VERSION" VERSION="$VERSION" QUALIFIER="$QUALIFIER" INSTANCE="$INSTANCE" DOCKER_IMAGE_TAG="$DOCKER_IMAGE_TAG" DOCKER_IMAGE_ALIAS_TAGS="$DOCKER_IMAGE_ALIAS_TAGS" DOCKER_CACHE_IMAGES="$DOCKER_CACHE_IMAGES" buildsdk sh_eval QUALIFIER DOCKER_IMAGE_TAG DOCKER_IMAGE_ALIAS_TAGS DOCKER_CACHE_IMAGES)
      - export DOCKER_IMAGE="${DOCKER_BASE_IMAGE}${QUALIFIER:+-$QUALIFIER}${INSTANCE:+-$INSTANCE}"
      - if grep -qE "^FROM\s+${CI_REGISTRY}.+" "$DOCKER_FILE" || echo "$DOCKER_IMAGE" | grep -qE "^${CI_REGISTRY}.+"; then buildsdk retry_command 2 5 docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "${CI_REGISTRY}"; fi
      - buildsdk docker_build $DOCKER_BUILD_OPTS
      - buildsdk export_env "$BUILD_ARTIFACT_DIR/context-docker" DOCKER_IMAGE DOCKER_IMAGE_TAG DOCKER_IMAGE_ALIAS_TAGS

# Common documentation site deployment.
.common:pages:
   extends: .common:base
   stage: post-build
   rules:
      -  if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
   script:
      - mv "$BUILD_ARTIFACT_DIR/site" public
   allow_failure: true
   dependencies:
   artifacts:
      paths:
         - public

# Common build for merge requests.
.build:merge-request:
   extends:
      - .common:build:merge-request
   stage: build
   variables:
      QUALIFIER: "build"
      DOCKER_TARGET: "build"
      SITE: "true"
      DOCKER_CACHE_IMAGES: "$${DOCKER_BASE_IMAGE}-build:master-latest" # FIXME : utiliser if: variables en comparant les ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME} pour adapter master -> team-archi, etc ..

# Build branch runtime instance image.
.build:
   extends:
      - .common:build:branch
   stage: build
   variables:
      QUALIFIER: "build"
      VERSION: "$${BASE_VERSION}-${CI_PIPELINE_IID}-${CI_COMMIT_SHORT_SHA}"
      DOCKER_IMAGE_TAG: "$${BASE_VERSION}-${CI_PIPELINE_IID}-${CI_COMMIT_SHORT_SHA}"
      DOCKER_IMAGE_ALIAS_TAGS: "$${BASE_VERSION}-latest"
      DOCKER_TARGET: "build"
      DOCKER_CACHE_IMAGES: "$${DOCKER_BASE_IMAGE}-build:$${BASE_VERSION}-latest"
   rules:
      if: "$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH"
      variables:
         SONAR: "true"
         SITE: "true"


# SonarQube analysis (external to build).
.analysis:sonarqube:
   extends:
      - .common:build
   stage: post-build
   variables:
      DOCKER_TARGET: "build"
      QUALIFIER: "sonarqube"
      SONAR: "true"
      SKIP_TESTS: "false"
      SITE: "false"
      DOCKER_CACHE_IMAGES: "$${DOCKER_BASE_IMAGE}-build:$${BASE_VERSION}-latest"
      GIT_STRATEGY: "clone" # Disable shallow clone, required by the analysis task
      GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
