/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.document;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;

import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.fs.FsDocumentRepository;
import com.tinubu.commons.ports.spring.autoconfigure.CommonsPortsDocumentClasspathAutoConfiguration;
import com.tinubu.commons.ports.spring.autoconfigure.CommonsPortsDocumentFsAutoConfiguration;
import com.tinubu.commons.ports.spring.autoconfigure.CommonsPortsDocumentSftpAutoConfiguration;
import com.tinubu.commons.ports.spring.document.MultipleDocumentRepositoryDependencyTest.TestConfig;

/**
 * Demonstrates how to inject a {@link DocumentRepository} in an application when a multiple repository
 * implementations are available on classpath.
 * In this case, repository injection must be typed, or qualified.
 */
@SpringBootTest(classes = TestConfig.class)
@DirtiesContext
@TestPropertySource(properties = { "commons-ports.document.sftp.enabled=false" })
public class MultipleDocumentRepositoryDependencyTest {

   /**
    * Simulates a real application Spring context with multiple repositories on classpath.
    */
   @ImportAutoConfiguration({
         CommonsPortsDocumentFsAutoConfiguration.class,
         CommonsPortsDocumentSftpAutoConfiguration.class,
         CommonsPortsDocumentClasspathAutoConfiguration.class })
   public static class TestConfig {}

   @Autowired
   FsDocumentRepository typedDocumentRepository;

   @Autowired
   @Qualifier("commons-ports.document.fsDocumentRepository")
   DocumentRepository qualifiedDocumentRepository;

   @Autowired
   ApplicationContext applicationContext;

   @Test
   public void testTypedDocumentRepository() {
      assertThat(typedDocumentRepository).isNotNull();
      assertThat(typedDocumentRepository).isInstanceOf(FsDocumentRepository.class);
   }

   @Test
   public void testQualifiedDocumentRepository() {
      assertThat(qualifiedDocumentRepository).isNotNull();
      assertThat(qualifiedDocumentRepository).isInstanceOf(FsDocumentRepository.class);
   }

   @Test
   public void testDisabledBeansNotLoaded() {
      assertThat(applicationContext.containsBean("commons-ports.document.sftpDocumentRepository")).isFalse();
      assertThat(applicationContext.containsBean("commons-ports.document.sftpDocumentConfig")).isFalse();

      assertThat(applicationContext.containsBean("commons-ports.document.fsDocumentRepository")).isTrue();
      assertThat(applicationContext.containsBean("commons-ports.document.fsDocumentConfig")).isTrue();

      assertThat(applicationContext.containsBean("commons-ports.document.classpathDocumentRepository")).isTrue();
      assertThat(applicationContext.containsBean("commons-ports.document.classpathDocumentConfig")).isTrue();
   }

   @Test
   public void testDefaultConfigurationPropertiesNotLoaded() {
      assertThat(applicationContext.containsBean(
            "commons-ports.document.fs-com.tinubu.commons.ports.spring.document.fs.FsDocumentConfig")).isFalse();
      assertThat(applicationContext.containsBean(
            "commons-ports.document.sftp-com.tinubu.commons.ports.spring.document.sftp.SftpDocumentConfig")).isFalse();
   }

}
