/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.document;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.fs.FsDocumentRepository;
import com.tinubu.commons.ports.spring.autoconfigure.CommonsPortsDocumentFsAutoConfiguration;
import com.tinubu.commons.ports.spring.document.SingleDocumentRepositoryDependencyTest.TestConfig;

/**
 * Demonstrates how to inject a {@link DocumentRepository} in an application when a single repository
 * implementation is available on classpath.
 * In this case, repository injection can be typed or not, qualified or not.
 */
@SpringBootTest(classes = TestConfig.class)
@DirtiesContext
public class SingleDocumentRepositoryDependencyTest {

   /**
    * Simulates a real application Spring context with a single repository on classpath.
    */
   @ImportAutoConfiguration({ CommonsPortsDocumentFsAutoConfiguration.class })
   public static class TestConfig {}

   @Autowired
   FsDocumentRepository typedDocumentRepository;

   @Autowired
   DocumentRepository unqualifiedDocumentRepository;

   @Autowired
   @Qualifier("commons-ports.document.fsDocumentRepository")
   DocumentRepository qualifiedDocumentRepository;

   @Test
   public void testTypedDocumentRepository() {
      assertThat(typedDocumentRepository).isNotNull();
      assertThat(typedDocumentRepository).isInstanceOf(FsDocumentRepository.class);
   }

   @Test
   public void testUnqualifiedDocumentRepository() {
      assertThat(unqualifiedDocumentRepository).isNotNull();
      assertThat(unqualifiedDocumentRepository).isInstanceOf(FsDocumentRepository.class);
   }

   @Test
   public void testQualifiedDocumentRepository() {
      assertThat(qualifiedDocumentRepository).isNotNull();
      assertThat(qualifiedDocumentRepository).isInstanceOf(FsDocumentRepository.class);
   }

}
