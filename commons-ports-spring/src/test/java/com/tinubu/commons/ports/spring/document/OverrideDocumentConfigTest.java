/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.document;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.Paths;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;

import com.tinubu.commons.ports.document.classpath.ClasspathDocumentRepository;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.spring.autoconfigure.CommonsPortsDocumentClasspathAutoConfiguration;
import com.tinubu.commons.ports.spring.document.OverrideDocumentConfigTest.TestConfig;
import com.tinubu.commons.ports.spring.document.classpath.ClasspathDocumentConfig;

/**
 * Demonstrates how to override default provided config configuration.
 * In this case, we want to redefine a repository configuration, with original repository bean.
 * <p>
 * Note that since a bean, or a configuration of a given type, is overridden, the default bean/configuration
 * is no more loaded.
 */
@SpringBootTest(classes = TestConfig.class)
@DirtiesContext
@TestPropertySource(properties = {
      "test.document.feature1.classpath.enabled=true",
      "test.document.feature1.classpath.classpath-prefix=/test/feature1" })
@SuppressWarnings("ConfigurationProperties")
public class OverrideDocumentConfigTest {

   /**
    * Simulates a real application Spring context with a default configuration,
    * then, override classpath repository configuration with a custom prefix, and a custom bean name.
    */
   @ImportAutoConfiguration({
         CommonsPortsDocumentClasspathAutoConfiguration.class })
   public static class TestConfig {

      @Bean("commons-ports.document.classpathDocumentConfig")
      @ConditionalOnProperty(name = "test.document.feature1.classpath.enabled")
      @ConfigurationProperties(prefix = "test.document.feature1.classpath", ignoreUnknownFields = false)
      public ClasspathDocumentConfig feature1ClasspathDocumentConfig() {
         return new ClasspathDocumentConfig();
      }
   }

   @Autowired
   DocumentRepository unqualifiedDocumentRepository;

   @Autowired
   @Qualifier("commons-ports.document.classpathDocumentConfig")
   ClasspathDocumentConfig feature1DocumentConfig;

   @Test
   public void testUnqualifiedDocumentRepository() {
      assertThat(unqualifiedDocumentRepository).isNotNull();
      assertThat(unqualifiedDocumentRepository).isInstanceOf(ClasspathDocumentRepository.class);
      assertThat(feature1DocumentConfig.getClasspathPrefix()).isEqualTo(Paths.get("/test/feature1"));
   }

}
