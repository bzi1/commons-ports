/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.autoconfigure;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;

import com.tinubu.commons.ports.document.sftp.SftpDocumentRepository;
import com.tinubu.commons.ports.spring.document.sftp.SftpDocumentConfig;

/**
 * SFTP document adapter auto-configuration.
 * <p>
 * This repository is configured only if {@value ENABLED_PROPERTY} property is set to {@code true}.
 * Default bean names for this repository are :
 * <ul>
 *    <li>SftpDocumentConfigPoint : {@value CONFIG_BEAN_NAME}</li>
 *    <li>SftpDocumentRepository : {@value REPOSITORY_BEAN_NAME}</li>
 * </ul>
 */
@ConditionalOnClass(SftpDocumentRepository.class)
public class CommonsPortsDocumentSftpAutoConfiguration extends CommonsPortsDocumentAutoConfiguration {

   private static final Logger log = LoggerFactory.getLogger(CommonsPortsDocumentSftpAutoConfiguration.class);

   private static final String ENABLED_PROPERTY = "commons-ports.document.sftp.enabled";
   private static final String REPOSITORY_BEAN_NAME = "commons-ports.document.sftpDocumentRepository";
   private static final String CONFIG_BEAN_NAME = "commons-ports.document.sftpDocumentConfig";

   @Bean(CONFIG_BEAN_NAME)
   @ConditionalOnProperty(name = { GLOBAL_ENABLED_PROPERTY, ENABLED_PROPERTY }, matchIfMissing = true)
   @ConditionalOnMissingBean(SftpDocumentConfig.class)
   public SftpDocumentConfig sftpDocumentConfig() {
      return new SftpDocumentConfig();
   }

   @Bean(REPOSITORY_BEAN_NAME)
   @ConditionalOnProperty(name = { GLOBAL_ENABLED_PROPERTY, ENABLED_PROPERTY }, matchIfMissing = true)
   @ConditionalOnMissingBean(SftpDocumentRepository.class)
   public SftpDocumentRepository sftpDocumentRepository(
         @Qualifier(CONFIG_BEAN_NAME) SftpDocumentConfig sftpDocumentConfig) {

      com.tinubu.commons.ports.document.sftp.SftpDocumentConfig config =
            sftpDocumentConfig.toSftpDocumentConfig();

      log.info("Using 'sftp' document repository configuration {}", config);

      return new SftpDocumentRepository(config);
   }

}
