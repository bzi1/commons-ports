/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.document.awss3;

import static com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.ListingContentMode.FAST;

import java.net.URL;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.ListingContentMode;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.spring.document.azureblob.AzureBlobDocumentConfig.ValidConnection;

/**
 * AWS S3 document repository configuration.
 */
@Validated
@ConfigurationProperties(prefix = "commons-ports.document.aws-s3", ignoreUnknownFields = false)
@ValidConnection
public class AwsS3DocumentConfig {

   /**
    * Enable flag for the default repository instance.
    */
   private boolean enabled = true;

   /**
    * S3 region.
    */
   @NotBlank
   private String region;

   /**
    * Optional S3 endpoint override.
    */
   private URL endpoint;

   /**
    * S3 bucket name.
    */
   @NotBlank
   private String bucketName;

   /**
    * Optional flag to automatically create missing bucket.
    */
   private boolean createBucketIfMissing = false;

   /**
    * Optional flag to use path-style access.
    */
   private boolean pathStyleAccess = false;

   /**
    * Optional max keys (i.e. page size) in objects listing operations.
    */
   private int listingMaxKeys = 1000;

   /**
    * Optional listing content mode to workaround S3 limitations.
    * By default ({@link ListingContentMode#FAST}, S3 does not return all metadata for objects, so that
    * {@link DocumentEntry} metadata are  incomplete for both filtering and returned entries. The library can
    * complete the metadata at the cost of one extra API call per object.
    */
   @NotNull
   private ListingContentMode listingContentMode = FAST;

   public boolean isEnabled() {
      return enabled;
   }

   public void setEnabled(boolean enabled) {
      this.enabled = enabled;
   }

   public String getRegion() {
      return region;
   }

   public void setRegion(String region) {
      this.region = region;
   }

   public URL getEndpoint() {
      return endpoint;
   }

   public void setEndpoint(URL endpoint) {
      this.endpoint = endpoint;
   }

   public String getBucketName() {
      return bucketName;
   }

   public void setBucketName(String bucketName) {
      this.bucketName = bucketName;
   }

   public boolean isCreateBucketIfMissing() {
      return createBucketIfMissing;
   }

   public void setCreateBucketIfMissing(boolean createBucketIfMissing) {
      this.createBucketIfMissing = createBucketIfMissing;
   }

   public boolean isPathStyleAccess() {
      return pathStyleAccess;
   }

   public void setPathStyleAccess(boolean pathStyleAccess) {
      this.pathStyleAccess = pathStyleAccess;
   }

   public int getListingMaxKeys() {
      return listingMaxKeys;
   }

   public void setListingMaxKeys(int listingMaxKeys) {
      this.listingMaxKeys = listingMaxKeys;
   }

   public ListingContentMode getListingContentMode() {
      return listingContentMode;
   }

   public void setListingContentMode(ListingContentMode listingContentMode) {
      this.listingContentMode = listingContentMode;
   }

   public com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig toAwsS3DocumentConfig() {
      return new com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.AwsS3DocumentConfigBuilder()
            .region(region)
            .endpoint(endpoint)
            .bucketName(bucketName)
            .pathStyleAccess(pathStyleAccess)
            .createIfMissingBucket(createBucketIfMissing)
            .listingMaxKeys(listingMaxKeys)
            .listingContentMode(listingContentMode)
            .build();
   }

}
