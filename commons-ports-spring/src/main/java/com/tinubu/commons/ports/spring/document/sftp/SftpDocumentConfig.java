/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.document.sftp;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.apache.commons.io.IOUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.Resource;
import org.springframework.validation.annotation.Validated;

/**
 * SFTP document repository configuration.
 */
@Validated
@ConfigurationProperties(prefix = "commons-ports.document.sftp", ignoreUnknownFields = false)
public class SftpDocumentConfig {

   /**
    * Default SFTP caching session pool size. Value must be > 0.
    */
   private static final int DEFAULT_SESSION_POOL_SIZE = 10;

   /**
    * Enable flag for the default repository instance.
    */
   private boolean enabled = true;

   /**
    * SFTP server host.
    */
   @NotBlank
   private String host = "localhost";

   /**
    * SFTP server port.
    */
   @NotNull
   private Integer port = 22;

   /**
    * Optional SFTP server authentication username.
    * Use {@code ${user.name}} by default.
    */
   private String username;

   /**
    * Optional SFTP server authentication password.
    * Use either privateKey or password for authentication. Use password authentication if both set.
    */
   private String password;

   /**
    * Optional SFTP server authentication private key.
    * Use either privateKey or password for authentication. Use password authentication if both set.
    * Use {@code ${user.home}/.ssh/id_rsa} by default.
    */
   private Resource privateKey;

   /**
    * Optional base path to store documents in SFTP server. If omitted, the SFTP server default directory will
    * be used.
    */
   @NotNull
   private Path basePath = Paths.get("");

   /**
    * Optional SFTP caching session pool size. Value must be > 0. Default to
    * {@link #DEFAULT_SESSION_POOL_SIZE}.
    */
   @Min(1)
   private int sessionPoolSize = DEFAULT_SESSION_POOL_SIZE;

   public boolean isEnabled() {
      return enabled;
   }

   public void setEnabled(boolean enabled) {
      this.enabled = enabled;
   }

   public String getHost() {
      return host;
   }

   public void setHost(String host) {
      this.host = host;
   }

   public Integer getPort() {
      return port;
   }

   public void setPort(Integer port) {
      this.port = port;
   }

   public String getUsername() {
      return username;
   }

   public void setUsername(String username) {
      this.username = username;
   }

   public String getPassword() {
      return password;
   }

   public void setPassword(String password) {
      this.password = password;
   }

   public Resource getPrivateKey() {
      return privateKey;
   }

   public void setPrivateKey(Resource privateKey) {
      this.privateKey = privateKey;
   }

   public Path getBasePath() {
      return basePath;
   }

   public void setBasePath(Path basePath) {
      this.basePath = basePath;
   }

   public int getSessionPoolSize() {
      return sessionPoolSize;
   }

   public SftpDocumentConfig setSessionPoolSize(int sessionPoolSize) {
      this.sessionPoolSize = sessionPoolSize;
      return this;
   }

   public com.tinubu.commons.ports.document.sftp.SftpDocumentConfig toSftpDocumentConfig() {
      return new com.tinubu.commons.ports.document.sftp.SftpDocumentConfig.SftpDocumentConfigBuilder()
            .host(host)
            .port(port)
            .basePath(basePath)
            .username(username)
            .password(password)
            .privateKey(nullable(privateKey).map(resource -> resourceToArray(resource, false)).orElse(null))
            .sessionPoolSize(sessionPoolSize)
            .build();
   }

   private static byte[] resourceToArray(Resource resource, boolean ignoreIfNotFound) {
      if (resource.exists()) {
         try (InputStream sshPrivateKeyStream = resource.getInputStream()) {
            return IOUtils.toByteArray(sshPrivateKeyStream);
         } catch (Exception e) {
            throw new IllegalStateException(e);
         }
      } else {
         if (!ignoreIfNotFound) {
            throw new IllegalStateException(String.format("Unknown '%s' resource", resource.getFilename()));
         } else {
            return null;
         }
      }
   }

}
