/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.gzip;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.mimeType;
import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;

class GzipTransformerTest {

   private static final ZonedDateTime now = ZonedDateTime.now();

   @BeforeAll
   public static void initializeApplicationClock() {
      ApplicationClock.setFixedClock(now);
   }

   @Test
   public void testSupportsWhenNominal() {
      GzipTransformer gzipTransformer = new GzipTransformer();

      assertThat(gzipTransformer.supports(stubDocument(DocumentPath.of("test.txt")).build())).isTrue();
   }

   @Test
   public void testTransformWhenNominal() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("test.txt"))
            .loadedContent("content", StandardCharsets.UTF_8)
            .contentType(parseMimeType("text/plain"))
            .build();

      Document gzip = new GzipTransformer().compress(document);

      assertThat(gzip.documentId()).isEqualTo(DocumentPath.of("test.txt.gz"));
      assertThat(gzip.metadata().simpleContentType()).isEqualTo(parseMimeType("application/gzip"));
   }

   @Test
   public void testTransformWhenInvalidParameters() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new GzipTransformer().transform(null))
            .withMessage("Invariant validation error : 'document' must not be null");
   }

   @Test
   public void testTransformWhenEmptyDocuments() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("test.txt"))
            .loadedContent("", StandardCharsets.UTF_8)
            .contentType(parseMimeType("text/plain"))
            .build();

      Document gzip = new GzipTransformer().compress(document);

      assertThat(gzip.documentId()).isEqualTo(DocumentPath.of("test.txt.gz"));
      assertThat(gzip.metadata().simpleContentType()).isEqualTo(parseMimeType("application/gzip"));
   }

   private DocumentBuilder stubDocument(DocumentPath documentId, String content) {
      DocumentBuilder documentBuilder =
            new DocumentBuilder().documentId(documentId).loadedContent(content, StandardCharsets.UTF_8);

      if (documentId.stringValue().endsWith(".gz") || documentId.stringValue().endsWith(".gzip")) {
         documentBuilder = documentBuilder.contentType(mimeType("application", "gzip"));
      }

      return documentBuilder;
   }

   private DocumentBuilder stubDocument(DocumentPath documentId) {
      return stubDocument(documentId, documentId.stringValue());
   }

}