/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.zip;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.mimeType;
import static com.tinubu.commons.ports.document.transformer.transformers.DocumentNameStrategy.DOCUMENT_LOGICAL_NAME_STRATEGY;
import static com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipArchiveFactory.ZipLibrary.JDK;
import static com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipArchiveFactory.ZipLibrary.ZIP4J;
import static com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipConfiguration.CompressionLevel.HIGH;
import static com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipConfiguration.CompressionLevel.LOW;
import static com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipConfiguration.CompressionLevel.MAXIMUM;
import static com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipConfiguration.CompressionLevel.MINIMUM;
import static com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipConfiguration.CompressionLevel.NONE;
import static com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipConfiguration.CompressionLevel.NORMAL;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipArchiveFactory;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipArchiveFactory.ZipLibrary;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipConfiguration;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipConfiguration.EncryptionMethod;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipOptions;

class UnzipTransformerTest extends AbstractTransformerTest {

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testSupportsWhenNominal(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      UnzipTransformer unzipTransformer = new UnzipTransformer();

      assertThat(unzipTransformer.supports(stubDocument(DocumentPath.of("test.zip")).build())).isTrue();
      assertThat(unzipTransformer.supports(stubDocument(DocumentPath.of("test.txt"))
                                                 .<DocumentBuilder>chain(m -> m.contentType(mimeType(
                                                       "application",
                                                       "zip")))
                                                 .build())).isTrue();
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testSupportsWhenBadContentType(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      UnzipTransformer unzipTransformer = new UnzipTransformer();

      assertThat(unzipTransformer.supports(stubDocument(DocumentPath.of("test.txt")).build())).isFalse();
      assertThat(unzipTransformer.supports(stubDocument(DocumentPath.of("test.zip"))
                                                 .<DocumentBuilder>chain(m -> m.contentType(mimeType("text",
                                                                                                     "plain")))
                                                 .build())).isFalse();
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testSupportsWhenJavaArchive(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      UnzipTransformer unzipTransformer = new UnzipTransformer();

      assertThat(unzipTransformer.supports(stubDocument(DocumentPath.of("test.jar")).build())).isTrue();
      assertThat(unzipTransformer.supports(stubDocument(DocumentPath.of("test.war")).build())).isTrue();
      assertThat(unzipTransformer.supports(stubDocument(DocumentPath.of("test.ear")).build())).isTrue();
      assertThat(unzipTransformer.supports(stubDocument(DocumentPath.of("test.txt"))
                                                 .<DocumentBuilder>chain(m -> m.contentType(mimeType(
                                                       "application",
                                                       "java-archive")))
                                                 .build())).isTrue();
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testTransformWhenNominal(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      List<Document> documents = Arrays.asList(stubDocument(DocumentPath.of("test.txt")).build(),
                                               stubDocument(DocumentPath.of("path/test2.txt")).build());
      Document zip = new ZipTransformer(DocumentPath.of("archive.zip")).transform(documents);

      List<Document> unzippedDocuments = new UnzipTransformer().transform(zip);

      assertThat(unzippedDocuments).hasSize(2);
      assertThat(unzippedDocuments.get(0)).satisfies(test -> {
         assertThat(test.documentId()).isEqualTo(DocumentPath.of("test.txt"));
         assertThat(test.metadata().documentPath()).isEqualTo(Paths.get("test.txt"));
         assertThat(test.metadata().contentType()).isEqualTo(mimeType("text", "plain"));
         assertThat(test.metadata().contentSize()).hasValue(8L);
         assertThatValidEntryMetadataDates(library, test);
         assertThat(test.content().stringContent(StandardCharsets.UTF_8)).isEqualTo("test.txt");
      });
      assertThat(unzippedDocuments.get(1)).satisfies(test -> {
         assertThat(test.documentId()).isEqualTo(DocumentPath.of("path/test2.txt"));
         assertThat(test.metadata().documentPath()).isEqualTo(Paths.get("path/test2.txt"));
         assertThat(test.metadata().contentType()).isEqualTo(mimeType("text", "plain"));
         assertThat(test.metadata().contentSize()).hasValue(14L);
         assertThatValidEntryMetadataDates(library, test);
         assertThat(test.content().stringContent(StandardCharsets.UTF_8)).isEqualTo("path/test2.txt");

      });
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testTransformWhenJar(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      List<Document> documents = Arrays.asList(stubDocument(DocumentPath.of("test.txt")).build(),
                                               stubDocument(DocumentPath.of("path/test2.txt")).build());
      Document jar = new ZipTransformer(DocumentPath.of("archive.jar")).transform(documents);

      List<Document> unzippedDocuments = new UnzipTransformer().transform(jar);

      assertThat(unzippedDocuments).hasSize(2);
      assertThat(unzippedDocuments.get(0)).satisfies(test -> {
         assertThat(test.documentId()).isEqualTo(DocumentPath.of("test.txt"));
         assertThat(test.metadata().documentPath()).isEqualTo(Paths.get("test.txt"));
         assertThat(test.metadata().contentType()).isEqualTo(mimeType("text", "plain"));
         assertThat(test.metadata().contentSize()).hasValue(8L);
         assertThatValidEntryMetadataDates(library, test);
         assertThat(test.content().stringContent(StandardCharsets.UTF_8)).isEqualTo("test.txt");
      });
      assertThat(unzippedDocuments.get(1)).satisfies(test -> {
         assertThat(test.documentId()).isEqualTo(DocumentPath.of("path/test2.txt"));
         assertThat(test.metadata().documentPath()).isEqualTo(Paths.get("path/test2.txt"));
         assertThat(test.metadata().contentType()).isEqualTo(mimeType("text", "plain"));
         assertThat(test.metadata().contentSize()).hasValue(14L);
         assertThatValidEntryMetadataDates(library, test);
         assertThat(test.content().stringContent(StandardCharsets.UTF_8)).isEqualTo("path/test2.txt");
      });
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testTransformWhenUnknownEncoding(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      List<Document> documents = Arrays.asList(stubDocument(DocumentPath.of("test.txt"))
                                                     .loadedContent("test.txt".getBytes(StandardCharsets.UTF_8))
                                                     .build(),
                                               stubDocument(DocumentPath.of("test2.txt"))
                                                     .loadedContent("test2.txt".getBytes(StandardCharsets.UTF_8))
                                                     .build());
      Document zip = new ZipTransformer(DocumentPath.of("archive.zip")).transform(documents);

      List<Document> unzippedDocuments = new UnzipTransformer().transform(zip);

      assertThat(unzippedDocuments).hasSize(2);
      assertThat(unzippedDocuments.get(0)).satisfies(test -> {
         assertThat(test.documentId()).isEqualTo(DocumentPath.of("test.txt"));
         assertThat(test.metadata().documentPath()).isEqualTo(Paths.get("test.txt"));
         assertThat(test.metadata().contentType()).isEqualTo(mimeType("text", "plain"));
         assertThat(test.metadata().contentSize()).hasValue(8L);
         assertThatValidEntryMetadataDates(library, test);
         assertThat(test.content().stringContent(StandardCharsets.UTF_8)).isEqualTo("test.txt");
      });
      assertThat(unzippedDocuments.get(1)).satisfies(test -> {
         assertThat(test.documentId()).isEqualTo(DocumentPath.of("test2.txt"));
         assertThat(test.metadata().documentPath()).isEqualTo(Paths.get("test2.txt"));
         assertThat(test.metadata().contentType()).isEqualTo(mimeType("text", "plain"));
         assertThat(test.metadata().contentSize()).hasValue(9L);
         assertThatValidEntryMetadataDates(library, test);
         assertThat(test.content().stringContent(StandardCharsets.UTF_8)).isEqualTo("test2.txt");

      });
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testTransformWhenInvalidParameters(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new UnzipTransformer().transform(null))
            .withMessage("Invariant validation error : 'document' must not be null");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new UnzipTransformer().transform(stubDocument(DocumentPath.of("test.txt")).build()))
            .withMessage(
                  "Invariant validation error : 'document.contentType=text/plain' must be in [application/zip,application/java-archive]");
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testTransformWhenNoDocuments(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      List<Document> documents = emptyList();
      Document zip = new ZipTransformer(DocumentPath.of("archive.zip")).transform(documents);

      assertThat(zip.documentId()).isEqualTo(DocumentPath.of("archive.zip"));
      assertThat(zip.metadata().contentType()).isEqualTo(mimeType("application", "zip"));
      assertThat(zip.metadata().contentSize()).hasValue(22L);
      assertThat(zip.metadata().creationDate()).hasValue(now.toInstant());
      assertThat(zip.metadata().lastUpdateDate()).hasValue(now.toInstant());

      List<Document> unzippedDocuments = new UnzipTransformer().transform(zip);

      assertThat(unzippedDocuments).hasSize(0);
   }

   @ParameterizedTest
   @EnumSource(ZipLibrary.class)
   public void testTransformWhenEmptyDocuments(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      List<Document> documents = Arrays.asList(stubDocument(DocumentPath.of("test.txt"), "").build(),
                                               stubDocument(DocumentPath.of("test2.txt"), "").build());
      Document zip = new ZipTransformer(DocumentPath.of("archive.zip")).transform(documents);

      assertThat(zip.documentId()).isEqualTo(DocumentPath.of("archive.zip"));
      assertThat(zip.metadata().contentType()).isEqualTo(mimeType("application", "zip"));
      if (library != ZIP4J) {
         assertThat(zip.metadata().contentSize()).hasValue(288L);
      } else {
         assertThat(zip.metadata().contentSize()).hasValue(244L);
      }
      assertThat(zip.metadata().creationDate()).hasValue(now.toInstant());
      assertThat(zip.metadata().lastUpdateDate()).hasValue(now.toInstant());

      List<Document> unzippedDocuments = new UnzipTransformer().transform(zip);

      assertThat(unzippedDocuments).hasSize(2);
      assertThat(unzippedDocuments.get(0)).satisfies(test -> {
         assertThat(test.metadata().contentSize()).hasValue(0L);
      });
      assertThat(unzippedDocuments.get(1)).satisfies(test -> {
         assertThat(test.metadata().contentSize()).hasValue(0L);

      });
   }

   @ParameterizedTest
   @EnumSource(value = ZipLibrary.class)
   public void testTransformWhenPassword(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      if (library != JDK) {
         List<Document> documents = singletonList(stubDocument(DocumentPath.of("path/test.txt")).build());
         Document zip = new ZipTransformer(DocumentPath.of("archive.zip"),
                                           DOCUMENT_LOGICAL_NAME_STRATEGY,
                                           new ZipOptions().password("changeit")).transform(documents);

         Document loadedZip = zip.loadContent();

         List<Document> unzippedDocuments =
               new UnzipTransformer(new ZipOptions().password("changeit")).transform(loadedZip);

         assertThat(unzippedDocuments).hasSize(1);
         assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Paths.get("test.txt"));

         assertThatExceptionOfType(DocumentAccessException.class).isThrownBy(() -> new UnzipTransformer(new ZipOptions().password(
               "wrong password")).transform(loadedZip));
         assertThatExceptionOfType(DocumentAccessException.class).isThrownBy(() -> new UnzipTransformer(new ZipOptions()).transform(
               loadedZip));
      }
   }

   @ParameterizedTest
   @EnumSource(value = ZipLibrary.class)
   public void testTransformWhenStandardEncryption(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      if (library != JDK) {
         List<Document> documents = singletonList(stubDocument(DocumentPath.of("path/test.txt")).build());
         Document zip = new ZipTransformer(DocumentPath.of("archive.zip"),
                                           DOCUMENT_LOGICAL_NAME_STRATEGY,
                                           new ZipOptions().password("changeit"),
                                           new ZipConfiguration().encryptionMethod(EncryptionMethod.ZIP_STANDARD)).transform(
               documents);

         List<Document> unzippedDocuments =
               new UnzipTransformer(new ZipOptions().password("changeit")).transform(zip);

         assertThat(unzippedDocuments).hasSize(1);
         assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Paths.get("test.txt"));
      }
   }

   @ParameterizedTest
   @EnumSource(value = ZipLibrary.class)
   public void testTransformWhenAes128Encryption(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      if (library != JDK) {
         List<Document> documents = singletonList(stubDocument(DocumentPath.of("path/test.txt")).build());
         Document zip = new ZipTransformer(DocumentPath.of("archive.zip"),
                                           DOCUMENT_LOGICAL_NAME_STRATEGY,
                                           new ZipOptions().password("changeit"),
                                           new ZipConfiguration().encryptionMethod(EncryptionMethod.AES_128)).transform(
               documents);

         List<Document> unzippedDocuments =
               new UnzipTransformer(new ZipOptions().password("changeit")).transform(zip);

         assertThat(unzippedDocuments).hasSize(1);
         assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Paths.get("test.txt"));
      }
   }

   @ParameterizedTest
   @EnumSource(value = ZipLibrary.class)
   public void testTransformWhenAes256Encryption(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      if (library != JDK) {
         List<Document> documents = singletonList(stubDocument(DocumentPath.of("path/test.txt")).build());
         Document zip = new ZipTransformer(DocumentPath.of("archive.zip"),
                                           DOCUMENT_LOGICAL_NAME_STRATEGY,
                                           new ZipOptions().password("changeit"),
                                           new ZipConfiguration().encryptionMethod(EncryptionMethod.AES_256)).transform(
               documents);

         List<Document> unzippedDocuments =
               new UnzipTransformer(new ZipOptions().password("changeit")).transform(zip);

         assertThat(unzippedDocuments).hasSize(1);
         assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Paths.get("test.txt"));
      }
   }

   @ParameterizedTest
   @EnumSource(value = ZipLibrary.class)
   public void testTransformWhenNoCompressionLevel(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      List<Document> documents = singletonList(stubDocument(DocumentPath.of("path/test.txt")).build());
      Document zip = new ZipTransformer(DocumentPath.of("archive.zip"),
                                        DOCUMENT_LOGICAL_NAME_STRATEGY,
                                        new ZipOptions(),
                                        new ZipConfiguration().compressionLevel(NONE)).transform(documents);

      List<Document> unzippedDocuments = new UnzipTransformer().transform(zip);

      assertThat(unzippedDocuments).hasSize(1);
      assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Paths.get("test.txt"));
   }

   @ParameterizedTest
   @EnumSource(value = ZipLibrary.class)
   public void testTransformWhenMinimumCompressionLevel(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      List<Document> documents = singletonList(stubDocument(DocumentPath.of("path/test.txt")).build());
      Document zip = new ZipTransformer(DocumentPath.of("archive.zip"),
                                        DOCUMENT_LOGICAL_NAME_STRATEGY,
                                        new ZipOptions(),
                                        new ZipConfiguration().compressionLevel(MINIMUM)).transform(documents);

      List<Document> unzippedDocuments = new UnzipTransformer().transform(zip);

      assertThat(unzippedDocuments).hasSize(1);
      assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Paths.get("test.txt"));
   }

   @ParameterizedTest
   @EnumSource(value = ZipLibrary.class)
   public void testTransformWhenLowCompressionLevel(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      List<Document> documents = singletonList(stubDocument(DocumentPath.of("path/test.txt")).build());
      Document zip = new ZipTransformer(DocumentPath.of("archive.zip"),
                                        DOCUMENT_LOGICAL_NAME_STRATEGY,
                                        new ZipOptions(),
                                        new ZipConfiguration().compressionLevel(LOW)).transform(documents);

      List<Document> unzippedDocuments = new UnzipTransformer().transform(zip);

      assertThat(unzippedDocuments).hasSize(1);
      assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Paths.get("test.txt"));
   }

   @ParameterizedTest
   @EnumSource(value = ZipLibrary.class)
   public void testTransformWhenNormalCompressionLevel(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      List<Document> documents = singletonList(stubDocument(DocumentPath.of("path/test.txt")).build());
      Document zip = new ZipTransformer(DocumentPath.of("archive.zip"),
                                        DOCUMENT_LOGICAL_NAME_STRATEGY,
                                        new ZipOptions(),
                                        new ZipConfiguration().compressionLevel(NORMAL)).transform(documents);

      List<Document> unzippedDocuments = new UnzipTransformer().transform(zip);

      assertThat(unzippedDocuments).hasSize(1);
      assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Paths.get("test.txt"));
   }

   @ParameterizedTest
   @EnumSource(value = ZipLibrary.class)
   public void testTransformWhenHighCompressionLevel(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      List<Document> documents = singletonList(stubDocument(DocumentPath.of("path/test.txt")).build());
      Document zip = new ZipTransformer(DocumentPath.of("archive.zip"),
                                        DOCUMENT_LOGICAL_NAME_STRATEGY,
                                        new ZipOptions(),
                                        new ZipConfiguration().compressionLevel(HIGH)).transform(documents);

      List<Document> unzippedDocuments = new UnzipTransformer().transform(zip);

      assertThat(unzippedDocuments).hasSize(1);
      assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Paths.get("test.txt"));
   }

   @ParameterizedTest
   @EnumSource(value = ZipLibrary.class)
   public void testTransformWhenMaximumCompressionLevel(ZipLibrary library) {
      ZipArchiveFactory.defaultLibrary().state(library);

      List<Document> documents = singletonList(stubDocument(DocumentPath.of("path/test.txt")).build());
      Document zip = new ZipTransformer(DocumentPath.of("archive.zip"),
                                        DOCUMENT_LOGICAL_NAME_STRATEGY,
                                        new ZipOptions(),
                                        new ZipConfiguration().compressionLevel(MAXIMUM)).transform(documents);

      List<Document> unzippedDocuments = new UnzipTransformer().transform(zip);

      assertThat(unzippedDocuments).hasSize(1);
      assertThat(unzippedDocuments.get(0).metadata().documentPath()).isEqualTo(Paths.get("test.txt"));
   }

   private DocumentBuilder stubDocument(DocumentPath documentId, String content) {
      return new DocumentBuilder().documentId(documentId).loadedContent(content, StandardCharsets.UTF_8);
   }

   private DocumentBuilder stubDocument(DocumentPath documentId) {
      return stubDocument(documentId, documentId.stringValue());
   }

}