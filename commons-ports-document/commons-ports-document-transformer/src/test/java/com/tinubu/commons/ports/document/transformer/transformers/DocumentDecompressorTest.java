/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.transformer.transformers.gzip.GzipTransformer;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ZipTransformer;

class DocumentDecompressorTest {

   @Test
   public void testDecompressWhenNominal() {
      DocumentDecompressor decompressor = new DocumentDecompressor();

      List<Document> documents = Arrays.asList(stubDocument(DocumentPath.of("test.txt")).build(),
                                               stubDocument(DocumentPath.of("test2.txt")).build());
      Document zip = new ZipTransformer(DocumentPath.of("archive.zip")).transform(documents);

      List<Document> decompressedDocuments = decompressor.decompress(zip);

      assertThat(decompressedDocuments).hasSize(2);
   }

   @Test
   public void testDecompressWhenZip() {
      DocumentDecompressor decompressor = new DocumentDecompressor();

      List<Document> documents = Arrays.asList(stubDocument(DocumentPath.of("test.txt")).build(),
                                               stubDocument(DocumentPath.of("test2.txt")).build());
      Document zip = new ZipTransformer(DocumentPath.of("archive.zip")).transform(documents);

      List<Document> decompressedDocuments = decompressor.decompress(zip);

      assertThat(decompressedDocuments).hasSize(2);
   }

   @Test
   public void testDecompressWhenGZip() {
      DocumentDecompressor decompressor = new DocumentDecompressor();

      Document document = stubDocument(DocumentPath.of("test.txt")).build();
      Document gzip = new GzipTransformer().transform(document);

      List<Document> decompressedDocuments = decompressor.decompress(gzip);

      assertThat(decompressedDocuments).hasSize(1);
   }

   private DocumentBuilder stubDocument(DocumentPath documentId, String content) {
      return new DocumentBuilder().documentId(documentId).loadedContent(content, StandardCharsets.UTF_8);
   }

   private DocumentBuilder stubDocument(DocumentPath documentId) {
      return stubDocument(documentId, documentId.stringValue());
   }

}