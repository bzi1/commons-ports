/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.zip;

import static com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipArchiveFactory.ZipLibrary.ZIP4J;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeAll;

import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipArchiveFactory.ZipLibrary;

public class AbstractTransformerTest {

   protected static final ZonedDateTime now = ZonedDateTime.now();

   @BeforeAll
   public static void initializeApplicationClock() {
      ApplicationClock.setFixedClock(now);
   }

   /**
    * Zip entry metadata checks.
    *
    * @implNote Dates are stored in ZIP with a low resolution for compaction, there are -1,+1s rounding
    *       issues.
    */
   protected void assertThatValidEntryMetadataDates(ZipLibrary library, Document document) {
      Instant expected = now.toInstant().truncatedTo(ChronoUnit.SECONDS);

      if (library != ZIP4J) {
         assertThat(document.metadata().creationDate()).hasValueSatisfying(lud -> assertThat(lud).isBetween(
               expected.minusSeconds(1),
               expected.plusSeconds(1)));
      }

      assertThat(document.metadata().lastUpdateDate()).hasValueSatisfying(lud -> assertThat(lud).isBetween(
            expected.minusSeconds(1),
            expected.plusSeconds(1)));
   }

   protected void debugZipFile(Document zip, File zipFile) {
      try {
         FileOutputStream out = new FileOutputStream(zipFile);
         IOUtils.copy(zip.content().inputStreamContent(), out);

         System.out.println(String.format("'%s' document content written to '%s'",
                                          zip.documentId().stringValue(),
                                          zipFile.getAbsolutePath()));
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   protected void debugZipFile(Document zip) {
      try {
         debugZipFile(zip, Files.createTempFile("ziptest", null).toFile());
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

}
