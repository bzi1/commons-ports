/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.or;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import com.tinubu.commons.ddd2.invariant.rules.UriRules;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.UriAdapter;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.commons.ports.document.transformer.transformers.zip.UnzipTransformer;

/**
 * {@link UriAdapter} that wraps a delegated adapter, and supports special transformer URIs.
 * Supported URIs are :
 * <ul>
 *    <li>{@code zip URI} : {@code zip:<url>!/[<entry-path>]}</li>
 *    <li>{@code jar URI} : {@code jar:<url>!/[<entry-path>]}</li>
 *    <li>{@code war URI} : {@code war:<url>!/[<entry-path>]}</li>
 *    <li>{@code ear URI} : {@code ear:<url>!/[<entry-path>]}</li>
 * </ul>
 * <p>
 * If an {@code <entry-path>} is specified, the entry path will be extracted from url archive and returned,
 * otherwise, the archive itself is returned as a document.
 * <p>
 * The delegated URI adapter must support the specified URI {@code <url>}.
 */
public class DocumentTransformerUriAdapter implements UriAdapter {

   private static final List<String> JAVA_ARCHIVE_SCHEMES = list("jar", "war", "ear");

   private final UriAdapter uriAdapter;

   public DocumentTransformerUriAdapter(UriAdapter uriAdapter) {
      this.uriAdapter = notNull(uriAdapter, "uriAdapter");
   }

   @Override
   public URI toUri(DocumentPath documentId) {
      return uriAdapter.toUri(documentId);
   }

   @Override
   public boolean supportsUri(URI documentUri) {
      validate(documentUri, "documentUri", UriRules.hasNoTraversal()).orThrow();

      return or(parseZipUri(documentUri), () -> parseJavaArchiveUri(documentUri))
            .map(uriParts -> uriAdapter.supportsUri(uriParts.getKey()))
            .orElseGet(() -> uriAdapter.supportsUri(documentUri));
   }

   @Override
   public Optional<Document> findDocumentByUri(URI documentUri) {
      satisfies(documentUri, this::supportsUri, "documentUri", "must be supported : " + documentUri);

      return findTransformedDocument(documentUri, uriAdapter);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
      satisfies(documentUri, this::supportsUri, "documentUri", "must be supported : " + documentUri);

      return findTransformedDocumentEntry(documentUri, uriAdapter);
   }

   /**
    * Parses a ZIP URI.
    *
    * @param zipUri ZIP URI
    *
    * @return parsed URL and entry parts, or {@link Optional#empty} if specified URI is not a ZIP URI
    */
   public static Optional<Pair<URI, Path>> parseZipUri(URI zipUri) {
      if (zipUri.isAbsolute() && zipUri.getScheme().equals("zip")) {
         return parseZipOrJavaArchiveUri(zipUri);
      } else {
         return optional();
      }
   }

   /**
    * Parses a Java archive (jar, war, ear) URI.
    *
    * @param jarUri Java archive URI
    *
    * @return parsed URL and entry parts, or {@link Optional#empty} if specified URI is not a Java archive URI
    */
   public static Optional<Pair<URI, Path>> parseJavaArchiveUri(URI jarUri) {
      if (jarUri.isAbsolute() && JAVA_ARCHIVE_SCHEMES.contains(jarUri.getScheme())) {
         return parseZipOrJavaArchiveUri(jarUri);
      } else {
         return optional();
      }
   }

   public static <T> Optional<Document> findTransformedDocument(URI documentUri, UriAdapter uriAdapter) {
      return or(parseZipUri(documentUri), () -> parseJavaArchiveUri(documentUri))
            .map(uriParts -> uriAdapter.findDocumentByUri(uriParts.getKey()).map(entryDocument -> {
               if (!uriParts.getValue().toString().equals("")) {
                  entryDocument = entryDocument.process(unzipEntry(uriParts.getValue()));
               }
               return entryDocument;
            }))
            .orElseGet(() -> uriAdapter.findDocumentByUri(documentUri));
   }

   public static <T> Optional<DocumentEntry> findTransformedDocumentEntry(URI documentUri,
                                                                          UriAdapter uriAdapter) {
      return or(parseZipUri(documentUri), () -> parseJavaArchiveUri(documentUri))
            .map(uriParts -> uriAdapter.findDocumentByUri(uriParts.getKey()).map(entryDocument -> {
               if (!uriParts.getValue().toString().equals("")) {
                  entryDocument = entryDocument.process(unzipEntry(uriParts.getValue()));
               }
               return entryDocument.documentEntry();
            }))
            .orElseGet(() -> uriAdapter.findDocumentEntryByUri(documentUri));
   }

   private static DocumentProcessor unzipEntry(Path entryPath) {
      return new UnzipTransformer().asSingleEntryTransformer(DocumentPath.of(entryPath));
   }

   private static Optional<Pair<URI, Path>> parseZipOrJavaArchiveUri(URI uri) {
      if (uri.isAbsolute() && (uri.getScheme().equals("zip")
                               || JAVA_ARCHIVE_SCHEMES.contains(uri.getScheme())) && uri.isOpaque()) {
         String schemeSpecificPart = uri.getSchemeSpecificPart();
         int separator = schemeSpecificPart.indexOf("!/");

         if (separator == -1) {
            throw new IllegalArgumentException("Malformed " + uri.getScheme() + " URI : " + uri);
         }

         try {
            URI url = new URI(schemeSpecificPart.substring(0, separator++));
            Path entry = Paths.get(schemeSpecificPart.substring(separator + 1));

            return optional(Pair.of(url, entry));
         } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Malformed " + uri.getScheme() + " URI : " + uri);
         }
      }

      return optional();
   }
}
