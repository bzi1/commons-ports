/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Function;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;

public interface DocumentNameStrategy extends Function<Document, Path> {

   /**
    * Naming strategy based on {@link Document#documentId() document identifier}, with complete path.
    */
   DocumentNameStrategy DOCUMENT_ID_PATH_STRATEGY = document -> document.documentId().value();

   /**
    * Naming strategy based on {@link Document#documentId() document identifier}, without path (only path last
    * element).
    */
   DocumentNameStrategy DOCUMENT_ID_NAME_STRATEGY = document -> document.documentId().value().getFileName();

   /**
    * Naming strategy based on document {@link DocumentMetadata#documentPath() metadata logical path}, with
    * complete path.
    */
   DocumentNameStrategy DOCUMENT_LOGICAL_PATH_STRATEGY = document -> document.metadata().documentPath();

   /**
    * Naming strategy based on document
    * {@link DocumentMetadata#documentName() metadata logical name}, without path (only path last element).
    */
   DocumentNameStrategy DOCUMENT_LOGICAL_NAME_STRATEGY =
         document -> Paths.get(document.metadata().documentName());

   /**
    * Returns document name for specified document.
    *
    * @param document document
    *
    * @return document name for specified document. Never null
    */
   default Path name(Document document) {
      Path documentEntryName = apply(document);

      if (documentEntryName == null) {
         throw new IllegalStateException("document name must not be null");
      }

      return documentEntryName;
   }
}
