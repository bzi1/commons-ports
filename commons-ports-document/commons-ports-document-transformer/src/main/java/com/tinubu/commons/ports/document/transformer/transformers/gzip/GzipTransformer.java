/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.gzip;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_GZIP;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPOutputStream;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.processor.common.ContentTypeDocumentRenamer;
import com.tinubu.commons.ports.document.transformer.DocumentTransformer;
import com.tinubu.commons.ports.document.transformer.DocumentTransformer.OneToOneDocumentTransformer;

/**
 * Gzip compressor.
 */
public class GzipTransformer implements OneToOneDocumentTransformer {

   /** Read/write buffer size. */
   private static final int BUFFER_SIZE = 8192;
   /** Charset used to encode gzip archive. */
   private static final Charset GZIP_CHARSET = StandardCharsets.UTF_8;

   /**
    * Supports any document.
    *
    * @param document document to check
    *
    * @return {@code true} if specified document is supported
    */
   @Override
   public boolean supports(Document document) {
      return true;
   }

   /**
    * Gzip specified document.
    *
    * @param document document
    *
    * @return compressed document
    */
   @Override
   public Document transform(Document document) throws DocumentAccessException {
      validate(document, "document", isNotNull()).orThrow();

      try (ByteArrayOutputStream os = new ByteArrayOutputStream(); GZIPOutputStream zis = new GZIPOutputStream(os, BUFFER_SIZE)) {
         int count;
         byte[] data = new byte[BUFFER_SIZE];

         try (InputStream src = document.content().inputStreamContent()) {
            while ((count = src.read(data, 0, BUFFER_SIZE)) != -1) {
               zis.write(data, 0, count);
            }
            zis.finish();

            return new DocumentBuilder()
                  .documentId(document.documentId())
                  .contentType(APPLICATION_GZIP)
                  .streamContent(os.toByteArray(), GZIP_CHARSET)
                  .build()
                  .process(new ContentTypeDocumentRenamer(true));
         }
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   /**
    * Alias for {@link DocumentTransformer#transform(Object)}.
    *
    * @param document document to compress
    *
    * @return compressed document
    *
    * @throws DocumentAccessException if an I/O error occurs
    */
   public Document compress(Document document) {
      return transform(document);
   }

}
