/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive;

import static com.tinubu.commons.lang.validation.Validate.notNull;

/**
 * ZIP archive creation configuration (encrypt, ...).
 */
public class ZipConfiguration {

   /**
    * Compression level.
    */
   private CompressionLevel compressionLevel = CompressionLevel.NORMAL;

   /**
    * Encryption method to use of ZIP have to be encrypted.
    */
   private EncryptionMethod encryptionMethod = EncryptionMethod.ZIP_STANDARD;

   public CompressionLevel compressionLevel() {
      return compressionLevel;
   }

   public ZipConfiguration compressionLevel(CompressionLevel compressionLevel) {
      this.compressionLevel = notNull(compressionLevel, "compressionLevel");
      return this;
   }

   public EncryptionMethod encryptionMethod() {
      return encryptionMethod;
   }

   public ZipConfiguration encryptionMethod(EncryptionMethod encryptionMethod) {
      this.encryptionMethod = notNull(encryptionMethod, "encryptionMethod");
      return this;
   }

   public enum CompressionLevel {

      /** No compression. */
      NONE,
      /** Minimum compression. */
      MINIMUM,
      /** Low compression. */
      LOW,
      /** Normal compression. */
      NORMAL,
      /** High compression. */
      HIGH,
      /** Maximum compression. */
      MAXIMUM
   }

   public enum EncryptionMethod {
      /** Standard ZIP encryption method. */
      ZIP_STANDARD,
      /** AES 128 bits key length encryption method. */
      AES_128,
      /** AES 256 bits key length encryption method. */
      AES_256
   }

}
