/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.jdk;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipArchive;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipArchiveEntry;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipConfiguration;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipException;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipOptions;

/**
 * JDK-native {@link ZipArchive} implementation.
 */
public class JdkZipArchive implements ZipArchive {

   /** Read/write buffer size. */
   private static final int BUFFER_SIZE = 8192;

   private final InputStream content;
   private final Long contentSize;
   private final ZipOptions options;

   private JdkZipArchive(InputStream content, Long contentSize, ZipOptions options) {
      this.content = notNull(content, "content");
      this.contentSize = contentSize;
      this.options = notNull(options, "options");

      if (options.password() != null) {
         throw new IllegalArgumentException("Password not supported");
      }
   }

   /**
    * Creates a low-level zip archive from raw zip content.
    *
    * @param content raw content
    * @param options zip options
    *
    * @return zip archive
    */
   public static JdkZipArchive fromContent(InputStream content, Long contentSize, ZipOptions options) {
      return new JdkZipArchive(content, contentSize, options);
   }

   /**
    * Creates a low-level zip archive from specified entries.
    *
    * @param entries entries to add to zip archive
    * @param options zip options
    * @param config zip configuration
    *
    * @return zip archive
    *
    * @throws IOException if an I/O error occurs
    */
   public static JdkZipArchive fromEntries(List<ZipArchiveEntry> entries,
                                           ZipOptions options,
                                           ZipConfiguration config) throws IOException {
      noNullElements(entries, "entries");
      notNull(options, "options");
      notNull(config, "config");

      try (ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
           ZipOutputStream zipOutput = new ZipOutputStream(byteOutput, options.metadataEncoding())) {

         configCompressionLevel(zipOutput, config);

         byte[] readBuffer = new byte[BUFFER_SIZE];
         for (ZipArchiveEntry entry : entries) {
            try (InputStream bis = entry.content()) {
               ZipEntry zipEntry = new ZipEntry(entry.name().toString());

               if (entry.creationDate() != null) {
                  zipEntry.setCreationTime(FileTime.from(entry.creationDate()));
               }
               if (entry.lastUpdateDate() != null) {
                  zipEntry.setLastModifiedTime(FileTime.from(entry.lastUpdateDate()));
               }
               zipOutput.putNextEntry(zipEntry);
               int bytesRead;
               while ((bytesRead = bis.read(readBuffer)) != -1) {
                  zipOutput.write(readBuffer, 0, bytesRead);
               }
            } finally {
               zipOutput.closeEntry();
            }
         }
         zipOutput.finish();

         byte[] zipContent = byteOutput.toByteArray();
         return new JdkZipArchive(new ByteArrayInputStream(zipContent), (long) zipContent.length, options);
      } catch (IOException e) {
         throw e;
      } catch (Exception e) {
         throw new ZipException(e);
      }
   }

   private static void configCompressionLevel(ZipOutputStream zipOutput, ZipConfiguration config) {
      switch (config.compressionLevel()) {
         case NONE:
            zipOutput.setLevel(Deflater.NO_COMPRESSION);
            break;
         case MINIMUM:
            zipOutput.setLevel(Deflater.BEST_SPEED);
            break;
         case LOW:
            zipOutput.setLevel(3);
            break;
         case NORMAL:
            zipOutput.setLevel(5);
            break;
         case HIGH:
            zipOutput.setLevel(7);
            break;
         case MAXIMUM:
            zipOutput.setLevel(Deflater.BEST_COMPRESSION);
            break;
      }
   }

   @Override
   public InputStream content() {
      return content;
   }

   @Override
   public Optional<Long> contentSize() {
      return nullable(contentSize);
   }

   @Override
   public ZipOptions options() {
      return options;
   }

   @Override
   public List<ZipArchiveEntry> unzipEntries() throws IOException {
      return unzipEntries(__ -> true);
   }

   @Override
   public List<ZipArchiveEntry> unzipEntries(Predicate<? super Path> entryFilter) throws IOException {
      List<ZipArchiveEntry> entries = new ArrayList<>();

      try (ZipInputStream zis = new ZipInputStream(content, options.metadataEncoding())) {
         ZipEntry zipEntry = zis.getNextEntry();

         byte[] buffer = new byte[BUFFER_SIZE];
         while (zipEntry != null) {
            String name = zipEntry.getName();

            if (entryFilter.test(Paths.get(name))) {
               try (ByteArrayOutputStream fos = new ByteArrayOutputStream()) {
                  int len;
                  while ((len = zis.read(buffer)) > 0) {
                     fos.write(buffer, 0, len);
                  }

                  Instant creationDate =
                        nullable(zipEntry.getCreationTime()).map(FileTime::toInstant).orElse(null);
                  Instant lastUpdateDate =
                        nullable(zipEntry.getLastModifiedTime()).map(FileTime::toInstant).orElse(null);

                  entries.add(new ZipArchiveEntry() {
                     @Override
                     public Path name() {
                        return Paths.get(name);
                     }

                     @Override
                     public InputStream content() {
                        return new ByteArrayInputStream(fos.toByteArray());
                     }

                     @Override
                     public Long contentSize() {
                        return (long) fos.size();
                     }

                     @Override
                     public Instant creationDate() {
                        return creationDate;
                     }

                     @Override
                     public Instant lastUpdateDate() {
                        return lastUpdateDate;
                     }
                  });

               }
            }
            zipEntry = zis.getNextEntry();
         }
         zis.closeEntry();
      } catch (IOException e) {
         throw e;
      } catch (Exception e) {
         throw new ZipException(e);
      }

      return entries;
   }
}
