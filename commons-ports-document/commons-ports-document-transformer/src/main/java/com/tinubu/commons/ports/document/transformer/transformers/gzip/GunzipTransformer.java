/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.gzip;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isIn;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.withStrippedExperimental;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.withStrippedParameters;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_GZIP;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMainRules.metadata;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMetadataRules.contentType;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.FilenameUtils;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.transformer.DocumentTransformer;
import com.tinubu.commons.ports.document.transformer.DocumentTransformer.OneToOneDocumentTransformer;

/**
 * Gzip decompressor.
 */
public class GunzipTransformer implements OneToOneDocumentTransformer {

   /** Read/write buffer size. */
   private static final int BUFFER_SIZE = 8192;
   /** Charset used to encode gzip archive. */
   private static final Charset GZIP_CHARSET = StandardCharsets.UTF_8;

   public GunzipTransformer() {}

   /**
    * Checks if specified document represent a gzip archive.
    * The check relies on content type that must match any {@link PresetMimeTypeRegistry#APPLICATION_GZIP}.
    *
    * @param document document to check
    *
    * @return {@code true} if specified document is a zip archive
    */
   @Override
   public boolean supports(Document document) {
      return validate(document, isGzipDocument()).success();
   }

   /**
    * Gunzip specified document.
    *
    * @param document gzip document
    *
    * @return uncompressed document
    */
   @Override
   public Document transform(Document document) throws DocumentAccessException {
      validate(document, "document", isGzipDocument()).orThrow();

      try (GZIPInputStream zis = new GZIPInputStream(document.content().inputStreamContent(), BUFFER_SIZE)) {
         int count;
         byte[] data = new byte[BUFFER_SIZE];

         try (ByteArrayOutputStream dest = new ByteArrayOutputStream()) {
            while ((count = zis.read(data, 0, BUFFER_SIZE)) != -1) {
               dest.write(data, 0, count);
            }
            dest.flush();

            DocumentPath uncompressedDocumentId = uncompressedDocumentId(document.documentId());

            return new DocumentBuilder()
                  .documentId(uncompressedDocumentId)
                  .streamContent(dest.toByteArray(), GZIP_CHARSET)
                  .build();
         }
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   /**
    * Generates the uncompressed document id from original document by removing extension if present.
    *
    * @param documentId original document id
    *
    * @return uncompressed document id
    */
   private DocumentPath uncompressedDocumentId(DocumentPath documentId) {
      String gzipDocumentName = documentId.value().toString();

      return DocumentPath.of(FilenameUtils.removeExtension(gzipDocumentName));
   }

   /**
    * Alias for {@link DocumentTransformer#transform(Object)}.
    *
    * @param gzipDocument Gzip document to uncompress
    *
    * @return uncompressed document
    *
    * @throws DocumentAccessException if an I/O error occurs
    */
   public Document uncompress(Document gzipDocument) {
      return transform(gzipDocument);
   }

   public InvariantRule<Document> isGzipDocument() {
      return metadata(contentType(withStrippedParameters(withStrippedExperimental(isIn(value(list(
            APPLICATION_GZIP)))))));
   }

}
