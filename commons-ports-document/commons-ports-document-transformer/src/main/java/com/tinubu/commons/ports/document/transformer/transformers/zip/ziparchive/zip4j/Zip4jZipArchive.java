/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.zip4j;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static net.lingala.zip4j.model.enums.CompressionLevel.FAST;
import static net.lingala.zip4j.model.enums.CompressionLevel.FASTEST;
import static net.lingala.zip4j.model.enums.CompressionLevel.MAXIMUM;
import static net.lingala.zip4j.model.enums.CompressionLevel.NORMAL;
import static net.lingala.zip4j.model.enums.CompressionLevel.NO_COMPRESSION;
import static net.lingala.zip4j.model.enums.CompressionLevel.ULTRA;
import static net.lingala.zip4j.model.enums.EncryptionMethod.AES;
import static net.lingala.zip4j.model.enums.EncryptionMethod.ZIP_STANDARD;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipArchive;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipArchiveEntry;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipConfiguration;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipException;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipOptions;

import net.lingala.zip4j.io.inputstream.ZipInputStream;
import net.lingala.zip4j.io.outputstream.ZipOutputStream;
import net.lingala.zip4j.model.LocalFileHeader;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.model.enums.AesKeyStrength;

/**
 * JDK-native {@link ZipArchive} implementation.
 */
public class Zip4jZipArchive implements ZipArchive {

   /** Read/write buffer size. */
   private static final int BUFFER_SIZE = 8192;

   private final InputStream content;
   private final Long contentSize;
   private final ZipOptions options;

   private Zip4jZipArchive(InputStream content, Long contentSize, ZipOptions options) {
      this.content = notNull(content, "content");
      this.contentSize = contentSize;
      this.options = notNull(options, "options");
   }

   /**
    * Creates a low-level zip archive from raw zip content.
    *
    * @param content raw content
    * @param contentSize raw content size
    * @param options zip options
    *
    * @return zip archive
    */
   public static Zip4jZipArchive fromContent(InputStream content, Long contentSize, ZipOptions options) {
      return new Zip4jZipArchive(content, contentSize, options);
   }

   /**
    * Creates a low-level zip archive from specified entries.
    *
    * @param entries entries to add to zip archive
    * @param options zip options
    * @param config zip configuration
    *
    * @return zip archive
    *
    * @throws IOException if an I/O error occurs
    */
   public static Zip4jZipArchive fromEntries(List<ZipArchiveEntry> entries,
                                             ZipOptions options,
                                             ZipConfiguration config) throws IOException {
      noNullElements(entries, "entries");
      notNull(options, "options");
      notNull(config, "config");

      ZipParameters zipParameters =
            configEncryptionMethod(configCompressionLevel(new ZipParameters(), config), config);
      zipParameters.setEncryptFiles(options.password() != null);

      try (ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
           ZipOutputStream zipOutput = new ZipOutputStream(byteOutput,
                                                           nullable(options.password())
                                                                 .map(String::toCharArray)
                                                                 .orElse(null),
                                                           options.metadataEncoding())) {
         byte[] readBuffer = new byte[BUFFER_SIZE];
         for (ZipArchiveEntry entry : entries) {
            try (InputStream bis = entry.content()) {
               zipParameters.setEntrySize(entry.contentSize());
               zipParameters.setFileNameInZip(entry.name().toString());
               zipParameters.setLastModifiedFileTime(entry.lastUpdateDate().toEpochMilli());

               zipOutput.putNextEntry(zipParameters);

               int bytesRead;
               while ((bytesRead = bis.read(readBuffer)) != -1) {
                  zipOutput.write(readBuffer, 0, bytesRead);
               }
            } finally {
               zipOutput.closeEntry();
            }
         }

         zipOutput.close();

         byte[] zipContent = byteOutput.toByteArray();
         return new Zip4jZipArchive(new ByteArrayInputStream(zipContent), (long) zipContent.length, options);
      } catch (net.lingala.zip4j.exception.ZipException e) {
         throw new ZipException(e.getMessage(), e);
      } catch (IOException e) {
         throw e;
      } catch (Exception e) {
         throw new ZipException(e);
      }

   }

   @Override
   public InputStream content() {
      return content;
   }

   @Override
   public Optional<Long> contentSize() {
      return nullable(contentSize);
   }

   @Override
   public ZipOptions options() {
      return options;
   }

   @Override
   public List<ZipArchiveEntry> unzipEntries() throws IOException {
      return unzipEntries(__ -> true);
   }

   @Override
   public List<ZipArchiveEntry> unzipEntries(Predicate<? super Path> entryFilter) throws IOException {
      List<ZipArchiveEntry> entries = new ArrayList<>();

      try (ZipInputStream zis = new ZipInputStream(content,
                                                   nullable(options.password())
                                                         .map(String::toCharArray)
                                                         .orElse(null),
                                                   options.metadataEncoding())) {
         LocalFileHeader zipEntry = zis.getNextEntry();

         byte[] buffer = new byte[BUFFER_SIZE];
         while (zipEntry != null) {
            String name = zipEntry.getFileName();

            if (entryFilter.test(Paths.get(name))) {
               try (ByteArrayOutputStream fos = new ByteArrayOutputStream()) {
                  int len;
                  while ((len = zis.read(buffer)) > 0) {
                     fos.write(buffer, 0, len);
                  }

                  Instant lastUpdateDate =
                        nullable(zipEntry.getLastModifiedTimeEpoch()).map(Instant::ofEpochMilli).orElse(null);

                  entries.add(new ZipArchiveEntry() {
                     @Override
                     public Path name() {
                        return Paths.get(name);
                     }

                     @Override
                     public InputStream content() {
                        return new ByteArrayInputStream(fos.toByteArray());
                     }

                     @Override
                     public Long contentSize() {
                        return (long) fos.size();
                     }

                     @Override
                     public Instant creationDate() {
                        return null;
                     }

                     @Override
                     public Instant lastUpdateDate() {
                        return lastUpdateDate;
                     }
                  });

               }
            }
            zipEntry = zis.getNextEntry();
         }
      } catch (net.lingala.zip4j.exception.ZipException e) {
         throw new ZipException(e.getMessage(), e);
      } catch (IOException e) {
         throw e;
      } catch (Exception e) {
         throw new ZipException(e);
      }

      return entries;
   }

   private static ZipParameters configCompressionLevel(ZipParameters zipParameters, ZipConfiguration config) {
      switch (config.compressionLevel()) {
         case NONE:
            zipParameters.setCompressionLevel(NO_COMPRESSION);
            break;
         case MINIMUM:
            zipParameters.setCompressionLevel(FASTEST);
            break;
         case LOW:
            zipParameters.setCompressionLevel(FAST);
            break;
         case NORMAL:
            zipParameters.setCompressionLevel(NORMAL);
            break;
         case HIGH:
            zipParameters.setCompressionLevel(MAXIMUM);
            break;
         case MAXIMUM:
            zipParameters.setCompressionLevel(ULTRA);
            break;
      }
      return zipParameters;
   }

   private static ZipParameters configEncryptionMethod(ZipParameters zipParameters, ZipConfiguration config) {
      switch (config.encryptionMethod()) {
         case ZIP_STANDARD:
            zipParameters.setEncryptionMethod(ZIP_STANDARD);
            break;
         case AES_128:
            zipParameters.setEncryptionMethod(AES);
            zipParameters.setAesKeyStrength(AesKeyStrength.KEY_STRENGTH_128);
            break;
         case AES_256:
            zipParameters.setEncryptionMethod(AES);
            zipParameters.setAesKeyStrength(AesKeyStrength.KEY_STRENGTH_256);
            break;
      }
      return zipParameters;
   }

}
