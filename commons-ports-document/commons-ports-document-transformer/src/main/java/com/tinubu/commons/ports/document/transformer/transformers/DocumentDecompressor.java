/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers;

import static com.tinubu.commons.lang.util.CollectionUtils.list;

import java.util.List;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.transformer.DocumentTransformer;
import com.tinubu.commons.ports.document.transformer.DocumentTransformer.OneToManyDocumentTransformer;
import com.tinubu.commons.ports.document.transformer.DocumentTransformerChain;
import com.tinubu.commons.ports.document.transformer.transformers.gzip.GunzipTransformer;
import com.tinubu.commons.ports.document.transformer.transformers.zip.UnzipTransformer;

/**
 * Decompresses specified document using any of pre-registered decompressors.
 * The first decompressor supporting this document will be used.
 */
public class DocumentDecompressor extends DocumentTransformerChain<Document, List<Document>>
      implements OneToManyDocumentTransformer {

   public static final List<DocumentTransformer<Document, List<Document>>> DECOMPRESSORS =
         list(new UnzipTransformer(), new GunzipTransformer().asOneToManyTransformer());

   public DocumentDecompressor() {
      super(new DocumentTransformerChainBuilder<Document, List<Document>>().transformers(DECOMPRESSORS));
   }

   /**
    * Alias for {@link DocumentTransformer#transform(Object)}.
    *
    * @param source document to uncompress
    *
    * @return uncompressed documents
    *
    * @throws DocumentAccessException if an I/O error occurs
    */
   public List<Document> decompress(Document source) throws DocumentAccessException {
      return transform(source);
   }
}
