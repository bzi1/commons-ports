/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive;

import static com.tinubu.commons.lang.validation.Validate.notBlank;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.tinubu.commons.lang.libraryselector.LibrarySelector;
import com.tinubu.commons.lang.libraryselector.LibrarySelector.LibraryDefinition;
import com.tinubu.commons.lang.util.GlobalState;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.jdk.JdkZipArchive;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.zip4j.Zip4jZipArchive;

public class ZipArchiveFactory {

   private static final LibrarySelector<ZipLibrary> librarySelector = ZipLibrary.librarySelector();

   /**
    * Creates a low-level zip archive from raw zip content.
    *
    * @param content raw content
    * @param contentSize content size in bytes
    *
    * @return zip archive
    */
   public static ZipArchive fromContent(InputStream content, Long contentSize, ZipOptions options) {
      switch (librarySelector.checkLibrary()) {
         case ZIP4J:
            return Zip4jZipArchive.fromContent(content, contentSize, options);
         case JDK:
            return JdkZipArchive.fromContent(content, contentSize, options);
         default:
            throw new IllegalStateException();
      }
   }

   /**
    * Creates a low-level zip archive from specified entries.
    *
    * @param entries entries to add to zip archive
    *
    * @return zip archive
    *
    * @throws IOException if an I/O error occurs
    */
   public static ZipArchive fromEntries(List<ZipArchiveEntry> entries,
                                        ZipOptions options,
                                        ZipConfiguration config) throws IOException {
      switch (librarySelector.checkLibrary()) {
         case ZIP4J:
            return Zip4jZipArchive.fromEntries(entries, options, config);
         case JDK:
            return JdkZipArchive.fromEntries(entries, options, config);
         default:
            throw new IllegalStateException();
      }
   }

   public static GlobalState<ZipLibrary> defaultLibrary() {
      return librarySelector.defaultLibrary();
   }

   public enum ZipLibrary implements LibraryDefinition {
      ZIP4J("net.lingala.zip4j:zip4j", "net.lingala.zip4j.ZipFile"), JDK("JDK", "java.util.zip.ZipFile");

      private final String libraryName;
      private final String classSample;

      ZipLibrary(String libraryName, String classSample) {
         this.libraryName = notBlank(libraryName, "libraryName");
         this.classSample = notBlank(classSample, "classSample");
      }

      @Override
      public String libraryName() {
         return libraryName;
      }

      @Override
      public String classSample() {
         return classSample;
      }

      public static LibrarySelector<ZipLibrary> librarySelector() {
         return new LibrarySelector<>(values());
      }

   }

}
