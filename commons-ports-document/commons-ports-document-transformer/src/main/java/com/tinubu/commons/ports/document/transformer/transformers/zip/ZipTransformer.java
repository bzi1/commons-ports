/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.zip;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.allSatisfies;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_JAVA_ARCHIVE;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_ZIP;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.time.Instant;
import java.util.List;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.transformer.DocumentTransformer;
import com.tinubu.commons.ports.document.transformer.DocumentTransformer.ManyToOneDocumentTransformer;
import com.tinubu.commons.ports.document.transformer.transformers.DocumentNameStrategy;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipArchive;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipArchiveEntry;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipArchiveFactory;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipConfiguration;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipException;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipOptions;

/**
 * ZIP archive compressor.
 */
public class ZipTransformer implements ManyToOneDocumentTransformer {

   /** Supported mime-types for zip files. */
   private static final List<MimeType> SUPPORTED_MIME_TYPES = list(APPLICATION_ZIP, APPLICATION_JAVA_ARCHIVE);
   /** Default zip entry naming strategy when not specified. */
   private static final DocumentNameStrategy DEFAULT_ENTRY_NAMING_STRATEGY = DocumentNameStrategy.DOCUMENT_ID_PATH_STRATEGY;
   public static final PresetMimeTypeRegistry MIME_TYPE_REGISTRY = PresetMimeTypeRegistry.ofPresets();

   private final DocumentPath zipDocumentId;
   private final DocumentNameStrategy zipEntryNameStrategy;
   private final ZipOptions options;
   private final ZipConfiguration config;

   public ZipTransformer(DocumentPath zipDocumentId,
                         DocumentNameStrategy zipEntryNameStrategy,
                         ZipOptions options,
                         ZipConfiguration config) {
      this.zipDocumentId = validate(zipDocumentId, "zipDocumentId", isNotNull()).orThrow();
      this.zipEntryNameStrategy =
            validate(zipEntryNameStrategy, "zipEntryNameStrategy", isNotNull()).orThrow();
      this.options = validate(options, "options", isNotNull()).orThrow();
      this.config = validate(config, "config", isNotNull()).orThrow();
   }

   public ZipTransformer(DocumentPath zipDocumentId,
                         DocumentNameStrategy zipEntryNameStrategy,
                         ZipOptions options) {
      this(zipDocumentId, zipEntryNameStrategy, options, new ZipConfiguration());
   }

   public ZipTransformer(DocumentPath zipDocumentId, DocumentNameStrategy zipEntryNameStrategy) {
      this(zipDocumentId, zipEntryNameStrategy, new ZipOptions());
   }

   public ZipTransformer(DocumentPath zipDocumentId) {
      this(zipDocumentId, DEFAULT_ENTRY_NAMING_STRATEGY);
   }

   /** Zip document id. */
   public DocumentPath zipDocumentId() {
      return zipDocumentId;
   }

   /**
    * Zip entry naming strategy.
    *
    * @see DocumentNameStrategy DocumentNameStrategy for preset strategies
    */
   public DocumentNameStrategy zipEntryNameStrategy() {
      return zipEntryNameStrategy;
   }

   /**
    * Checks if specified documents can be added to zip archive.
    *
    * @param documents documents to check
    *
    * @return {@code true} if all specified documents are supported
    */
   @Override
   public boolean supports(List<Document> documents) {
      return validate(documents, "documents", isValidDocuments()).success();
   }

   /**
    * Creates a new zip document containing all specified documents.
    * An empty zip document is returned if there are no documents.
    *
    * @param documents documents to archive
    *
    * @return zip document
    */
   @Override
   public Document transform(List<Document> documents) throws DocumentAccessException {
      validate(documents, "documents", isValidDocuments()).orThrow();

      return zipDocuments(documents);
   }

   /**
    * Alias for {@link DocumentTransformer#transform(Object)}.
    *
    * @param documents documents to compress
    *
    * @return Zip compressed document
    *
    * @throws DocumentAccessException if a transformation error occurs
    */
   public Document compress(List<Document> documents) {
      return transform(documents);
   }

   public InvariantRule<List<Document>> isValidDocuments() {
      return allSatisfies(isValidDocument());
   }

   public InvariantRule<Document> isValidDocument() {
      return isNotNull();
   }

   /**
    * Zip single document {@link OneToOneDocumentTransformer} adapter.
    *
    * @return one to one transformer from the specified document
    */
   public OneToOneDocumentTransformer asSingleEntryTransformer() {
      return new OneToOneDocumentTransformer() {

         @Override
         public boolean supports(Document document) {
            return ZipTransformer.this.supports(list(document));
         }

         @Override
         public Document transform(Document document) throws DocumentAccessException {
            validate(document, "document", isValidDocument()).orThrow();

            return ZipTransformer.this.transform(list(document));
         }
      };
   }

   protected Document zipDocuments(List<Document> documents) {
      try {
         ZipArchive zipArchive = ZipArchiveFactory.fromEntries(documents
                                                                     .stream()
                                                                     .map(document -> documentEntry(document,
                                                                                                    zipEntryNameStrategy))
                                                                     .collect(toList()), options, config);
         return new DocumentBuilder()
               .documentId(zipDocumentId)
               .contentType(zipContentType(zipDocumentId))
               .streamContent(zipArchive.content(), zipArchive.contentSize().orElse(null))
               .build();
      } catch (ZipException | IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   /**
    * Selects archive content-type depending on extension.
    * This enables different mime-types/extensions for ZIP archive.
    *
    * @param documentId zip document id
    *
    * @return zip content type
    *
    * @see #SUPPORTED_MIME_TYPES
    */
   private MimeType zipContentType(DocumentPath documentId) {
      return MIME_TYPE_REGISTRY.mimeType(documentId.value()).filter(SUPPORTED_MIME_TYPES::contains).orElse(APPLICATION_ZIP);
   }

   protected static ZipArchiveEntry documentEntry(Document document,
                                                  DocumentNameStrategy documentEntryNameStrategy) {
      return new ZipArchiveEntry() {
         @Override
         public Path name() {
            return documentEntryNameStrategy.name(document);
         }

         @Override
         public InputStream content() {
            return document.content().inputStreamContent();
         }

         @Override
         public Long contentSize() {
            return document.metadata().contentSize().orElse(null);
         }

         @Override
         public Instant creationDate() {
            return document.metadata().creationDate().orElse(null);
         }

         @Override
         public Instant lastUpdateDate() {
            return document.metadata().lastUpdateDate().orElse(null);
         }
      };
   }

}
