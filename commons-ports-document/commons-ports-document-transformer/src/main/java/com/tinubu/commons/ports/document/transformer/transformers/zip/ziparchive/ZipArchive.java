/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * Represents a low-level zip archive.
 */
public interface ZipArchive {

   /**
    * Returns zip raw content.
    *
    * @return zip raw content
    */
   InputStream content();

   /**
    * Returns zip raw content size in bytes.
    *
    * @return zip raw content size
    */
   Optional<Long> contentSize();

   /**
    * Returns zip options.
    *
    * @return zip options
    */
   ZipOptions options();

   /**
    * Unzip archive.
    *
    * @return uncompressed zip entries
    *
    * @throws ZipException if an error occur in zip library
    * @throws IOException if an I/O error occurs
    */
   List<ZipArchiveEntry> unzipEntries() throws IOException;

   /**
    * Unzip archive.
    *
    * @param entryFilter ZIP archive entry path filter
    *
    * @return uncompressed zip entries
    *
    * @throws ZipException if an error occur in zip library
    * @throws IOException if an I/O error occurs
    */
   List<ZipArchiveEntry> unzipEntries(Predicate<? super Path> entryFilter) throws IOException;

}
