/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.memory;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.optionalPredicate;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.invariant.rules.UriRules;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.UnsupportedUriException;
import com.tinubu.commons.ports.document.domain.UriAdapter;
import com.tinubu.commons.ports.document.transformer.DocumentTransformerUriAdapter;

/**
 * In-memory {@link DocumentRepository} adapter implementation.
 */
public class MemoryDocumentRepository extends AbstractDocumentRepository {

   private final Map<DocumentPath, Document> documentStorage = new ConcurrentHashMap<>();

   private final boolean caseInsensitive;
   private final DocumentTransformerUriAdapter transformerUriAdapter;

   public MemoryDocumentRepository(boolean caseInsensitive) {
      this.caseInsensitive = caseInsensitive;
      this.transformerUriAdapter = new DocumentTransformerUriAdapter(new MemoryUriAdapter());
   }

   public MemoryDocumentRepository() {
      this(false);
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      return Objects.equals(this, documentRepository);
   }

   @Override
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {

      startWatch();

      return handleDocumentEvent(() -> {
         if (!overwrite && !append && documentStorage.containsKey(documentId)) {
            return optional();
         }

         Document documentToSave = new DocumentBuilder()
               .documentId(documentId)
               .content(MemoryOutputStreamDocumentContent.create(metadata.contentEncoding().orElse(null)))
               .chain(metadata.chainDocumentBuilder())
               .build();

         if (append && documentStorage.containsKey(documentId)) {
            try {
               transferTo(documentStorage.get(documentId).content().inputStreamContent(),
                          documentToSave.content().outputStreamContent());
            } catch (IOException e) {
               throw new DocumentAccessException(e);
            }
         }

         Document savedDocument = documentStorage.merge(adaptCase(documentId),
                                                        documentToSave,
                                                        (pv, nv) -> overwrite || append
                                                                    ? documentToSave
                                                                    : pv);

         return optionalPredicate(savedDocument, sd -> sd == documentToSave);
      }, d -> documentSaved(d.documentEntry()));
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      notNull(documentId, "documentId");

      startWatch();

      return handleDocumentEvent(() -> {
         return nullable(documentStorage.get(adaptCase(documentId))).map(d -> {
            DocumentContent adaptedContent = adaptContent(d.content());
            DocumentMetadata adaptedMetadata = DocumentMetadataBuilder
                  .from(d.metadata())
                  .contentSize(adaptedContent.contentSize().orElse(null))
                  .build();

            return DocumentBuilder
                  .from(d)
                  .<DocumentBuilder>conditionalChain(__ -> caseInsensitive, b -> b.documentId(documentId))
                  .metadata(adaptedMetadata)
                  .content(adaptedContent)
                  .build();
         });
      }, d -> documentAccessed(d.documentEntry()));
   }

   /**
    * Potentially converts an {@link OutputStreamDocumentContent} to an
    * {@link InputStreamDocumentContentBuilder}.
    */
   private DocumentContent adaptContent(DocumentContent content) {
      if (content instanceof MemoryOutputStreamDocumentContent) {
         return new InputStreamDocumentContentBuilder()
               .content(new ByteArrayInputStream(content.content()),
                        content.contentEncoding().orElse(null),
                        (long) content.content().length)
               .build();
      } else {
         return content;
      }
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      notNull(documentId, "documentId");

      return findDocumentById(documentId).map(Document::documentEntry);
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();
      notNull(specification, "specification");

      startWatch();

      return handleDocumentStreamEvent(() -> stream(documentStorage.values())
                                             .filter(d -> specification.satisfiedBy(d.documentEntry()))
                                             .map(d -> DocumentBuilder.from(d).content(adaptContent(d.content())).build()),
                                       d -> documentAccessed(d.documentEntry()));
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();
      notNull(specification, "specification");

      startWatch();

      return handleDocumentEntryStreamEvent(() -> stream(documentStorage.values())
            .map(Document::documentEntry)
            .filter(specification::satisfiedBy), this::documentAccessed);
   }

   @Override
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      notNull(document, "document");

      if (document.content() instanceof OutputStreamDocumentContent) {
         throw new IllegalArgumentException(
               "Unsupported operation for this document content implementation : " + document
                     .content()
                     .getClass()
                     .getName());
      }

      startWatch();

      return handleDocumentEntryEvent(() -> {

         Document documentToSave = updateDocumentBeforeSave(document);
         Document savedDocument = documentStorage.merge(adaptCase(document.documentId()),
                                                        documentToSave,
                                                        (pv, nv) -> overwrite ? documentToSave : pv);

         return optionalPredicate(savedDocument, sd -> sd == documentToSave).map(Document::documentEntry);
      }, this::documentSaved);
   }

   @Override
   public Optional<DocumentEntry> deleteDocument(DocumentPath documentId) {
      notNull(documentId, "documentId");

      startWatch();

      return handleDocumentEntryEvent(() -> nullable(documentStorage.remove(adaptCase(documentId))).map(
            Document::documentEntry), this::documentDeleted);
   }

   @Override
   public boolean supportsUri(URI documentUri) {
      return transformerUriAdapter.supportsUri(documentUri);
   }

   @Override
   public URI toUri(DocumentPath documentId) {
      return transformerUriAdapter.toUri(documentId);
   }

   @Override
   public Optional<Document> findDocumentByUri(URI documentUri) {
      return transformerUriAdapter.findDocumentByUri(documentUri);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
      return transformerUriAdapter.findDocumentEntryByUri(documentUri);
   }

   /** Java < 10 InputStream::transferTo implementation. */
   private static long transferTo(InputStream in, OutputStream out) throws IOException {
      Objects.requireNonNull(in, "in");
      Objects.requireNonNull(out, "out");

      final int TRANSFER_BUFFER_SIZE = 4096 * 16;

      long transferred = 0;
      byte[] buffer = new byte[TRANSFER_BUFFER_SIZE];
      int read;
      while ((read = in.read(buffer, 0, TRANSFER_BUFFER_SIZE)) >= 0) {
         out.write(buffer, 0, read);
         transferred += read;
      }
      return transferred;
   }

   /**
    * Real memory URI adapter to be used by {@link DocumentTransformerUriAdapter}.
    */
   public class MemoryUriAdapter implements UriAdapter {
      private static final String URI_SCHEME = "memory";

      @Override
      public boolean supportsUri(URI documentUri) {
         validate(documentUri, "documentUri", UriRules.hasNoTraversal()).orThrow();

         return documentUri.isAbsolute()
                && documentUri.getScheme().equals(URI_SCHEME)
                && documentUri.getAuthority() == null
                && documentUri.getFragment() == null
                && documentUri.getQuery() == null
                && documentUri.getPath() != null;
      }

      @Override
      public URI toUri(DocumentPath documentId) {
         notNull(documentId, "documentId");

         try {
            return new URI(URI_SCHEME, null, "/" + documentId.stringValue(), null);
         } catch (URISyntaxException e) {
            throw new UnsupportedUriException(e);
         }
      }

      @Override
      public Optional<Document> findDocumentByUri(URI documentUri) {
         satisfies(documentUri, this::supportsUri, "documentUri", "must be supported : " + documentUri);

         return findDocumentById(documentId(documentUri));
      }

      @Override
      public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
         satisfies(documentUri, this::supportsUri, "documentUri", "must be supported : " + documentUri);

         return findDocumentEntryById(documentId(documentUri));
      }

      /**
       * Extracts document identifier from specified document URI.
       *
       * @param documentUri document URI
       *
       * @return document identifier
       */
      protected DocumentPath documentId(URI documentUri) {
         return DocumentPath.of(Paths.get("/").relativize(Paths.get(documentUri.getPath())));
      }
   }

   private DocumentPath adaptCase(DocumentPath documentId) {
      if (caseInsensitive) {
         return DocumentPath.of(documentId.stringValue().toUpperCase().toLowerCase());
      } else {
         return documentId;
      }
   }

   /**
    * Simulates a physical save.
    *
    * @param document document to save
    *
    * @return document updated for save
    *
    * @implNote Content must be loaded into memory in the case it's a stream content. A stream must be
    *       consumed by a save operation, and safely closed after that without compromising the saved content
    */
   private Document updateDocumentBeforeSave(Document document) {
      return DocumentBuilder
            .from(document)
            .metadata(DocumentMetadataBuilder
                            .from(document.metadata())
                            .chain((DocumentMetadataBuilder b) -> b.lastUpdateDate(ApplicationClock.nowAsInstant()))
                            .build())
            .build()
            .loadContent();
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", MemoryDocumentRepository.class.getSimpleName() + "[", "]")
            .add("caseInsensitive=" + caseInsensitive)
            .toString();
   }

   /**
    * Special {@link OutputStreamDocumentContent} that store data in a {@link ByteArrayOutputStream} and
    * provide access to internal byte array.
    */
   public static class MemoryOutputStreamDocumentContent extends OutputStreamDocumentContent {

      private final ByteArrayOutputStream outputStream;

      protected MemoryOutputStreamDocumentContent(ByteArrayOutputStream outputStream,
                                                  Charset contentEncoding) {
         super(new OutputStreamDocumentContentBuilder().content(outputStream, contentEncoding));
         this.outputStream = outputStream;
      }

      public static MemoryOutputStreamDocumentContent create(Charset contentEncoding) {
         return new MemoryOutputStreamDocumentContent(new ByteArrayOutputStream(), contentEncoding);
      }

      public ByteArrayOutputStream outputStream() {
         return outputStream;
      }

      @Override
      public byte[] content() {
         return outputStream.toByteArray();
      }
   }

}
