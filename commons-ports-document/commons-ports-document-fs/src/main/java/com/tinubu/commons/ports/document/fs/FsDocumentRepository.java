/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.CREATE_NEW;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.nio.file.StandardOpenOption.WRITE;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.StringJoiner;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.AbstractFileFilter;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;

import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.invariant.rules.UriRules;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.UnsupportedUriException;
import com.tinubu.commons.ports.document.domain.UriAdapter;
import com.tinubu.commons.ports.document.fs.FsDocumentConfig.FsDocumentConfigBuilder;
import com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy;
import com.tinubu.commons.ports.document.fs.storagestrategy.FsStorageStrategy;
import com.tinubu.commons.ports.document.fs.storagestrategy.FsStorageStrategyFactory;
import com.tinubu.commons.ports.document.transformer.DocumentTransformerUriAdapter;

/**
 * FS {@link DocumentRepository} adapter implementation.
 * <p>
 * If configured storage path is missing, the storage path will be created only at write time, or the write
 * operation will fail if repository configuration does not instruct to do so.
 */
// FIXME partial written files : create tmp and rename it at the end of the transfer ? (config tmp prefix + disable feature completely)
// FIXME create storage path permissions
// FIXME Create document file permissions
public class FsDocumentRepository extends AbstractDocumentRepository {

   private final FsStorageStrategy fsStorageStrategy;
   private final DocumentTransformerUriAdapter transformerUriAdapter;
   private final boolean createStoragePathIfMissing;

   public FsDocumentRepository(FsDocumentConfig fsDocumentConfig) {
      this.fsStorageStrategy = FsStorageStrategyFactory.fsStorageStrategy(fsDocumentConfig);
      this.transformerUriAdapter = new DocumentTransformerUriAdapter(new FsUriAdapter());
      this.createStoragePathIfMissing = fsDocumentConfig.createStoragePathIfMissing();
   }

   /**
    * Creates an instance from real filesystem directory. Storage path must exist.
    *
    * @param storagePath filesystem storage path
    *
    * @return new instance
    *
    * @throws NoSuchFileException if specified storage path is not an existing directory
    */
   public static FsDocumentRepository ofFilesystem(Path storagePath) throws NoSuchFileException {
      notNull(storagePath, "storagePath");

      File directory = storagePath.toFile();
      if (directory.exists() && directory.isDirectory()) {
         return new FsDocumentRepository(new FsDocumentConfigBuilder()
                                               .storageStrategy(DirectFsStorageStrategy.class)
                                               .storagePath(storagePath)
                                               .build());
      } else {
         throw new NoSuchFileException(directory.getAbsolutePath(),
                                       null,
                                       String.format("'%s' is not an existing directory",
                                                     directory.getAbsolutePath()));
      }
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      return documentRepository instanceof FsDocumentRepository
             && Objects.equals(((FsDocumentRepository) documentRepository).fsStorageStrategy,
                               fsStorageStrategy);
   }

   @Override
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      notNull(documentId, "documentId");
      notNull(metadata, "metadata");

      startWatch();

      return handleDocumentEvent(() -> {
         File documentFile = storageFile(documentId);

         if (!overwrite && !append && documentFile.exists()) {
            return optional();
         }

         mkdirStorageFile(documentFile);

         try {
            OutputStream documentOutputStream = Files.newOutputStream(documentFile.toPath(),
                                                                      WRITE,
                                                                      overwrite || append
                                                                      ? CREATE
                                                                      : StandardOpenOption.CREATE_NEW,
                                                                      append
                                                                      ? StandardOpenOption.APPEND
                                                                      : StandardOpenOption.TRUNCATE_EXISTING);

            return optional(new DocumentBuilder()
                                  .documentId(documentId)
                                  .content(new OutputStreamDocumentContentBuilder()
                                                 .content(documentOutputStream,
                                                          metadata.contentEncoding().orElse(null))
                                                 .build())
                                  .chain(metadata.chainDocumentBuilder())
                                  .build());
         } catch (FileAlreadyExistsException e) {
            return optional();
         } catch (IOException e) {
            throw new DocumentAccessException(e);
         }
      }, d -> documentSaved(d.documentEntry()));
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      notNull(documentId, "documentId");

      startWatch();

      return handleDocumentEvent(() -> {
         File documentFile = storageFile(documentId);

         return fsDocumentEntry(documentFile,
                                documentId).flatMap(entry -> fsDocumentContent(documentFile).map(content -> new DocumentBuilder()
               .<DocumentBuilder>reconstitute()
               .documentEntry(entry)
               .content(content)
               .build()));
      }, e -> documentAccessed(e.documentEntry()));
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      return findDocumentEntriesBySpecification(basePath, specification).flatMap(entry -> stream(
            fsDocumentContent(storageFile(entry.documentId())).map(content -> new DocumentBuilder()
                  .<DocumentBuilder>reconstitute()
                  .documentEntry(entry)
                  .content(content)
                  .build())));
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      notNull(documentId, "documentId");

      startWatch();

      return handleDocumentEntryEvent(() -> {
         File documentFile = storageFile(documentId);

         return fsDocumentEntry(documentFile, documentId);
      }, this::documentAccessed);
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNull().orValue(isNotAbsolute().andValue(hasNoTraversal()))).orThrow();
      notNull(specification, "specification");

      startWatch();

      return handleDocumentEntryStreamEvent(() -> {
         Stream<DocumentEntry> documentEntries;

         if (fsStorageStrategy instanceof DirectFsStorageStrategy) {
            documentEntries = listDocumentEntries(storageDirectory(basePath), directoryFilter(specification));
         } else {
            documentEntries = listAllDocumentEntries();
         }

         return documentEntries.filter(specification);
      }, this::documentAccessed);
   }

   @Override
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      notNull(document, "document");

      if (document.content() instanceof OutputStreamDocumentContent) {
         throw new IllegalArgumentException(
               "Unsupported operation for this document content implementation : " + document
                     .content()
                     .getClass()
                     .getName());
      }

      startWatch();

      return handleDocumentEntryEvent(() -> {

         File documentFile = storageFile(document.documentId());
         Optional<DocumentEntry> savedDocument = optional();

         if (overwrite || !documentFile.exists()) {

            mkdirStorageFile(documentFile);

            try (InputStream documentInputStream = document.content().inputStreamContent();
                 OutputStream documentOutputStream = Files.newOutputStream(documentFile.toPath(),
                                                                           WRITE,
                                                                           overwrite ? CREATE : CREATE_NEW,
                                                                           TRUNCATE_EXISTING)) {
               transferTo(documentInputStream, documentOutputStream);

               savedDocument = fsDocumentEntry(documentFile, document.documentId());
            } catch (FileAlreadyExistsException e) {
               savedDocument = optional();
            } catch (IOException e) {
               throw new DocumentAccessException(e);
            }
         }

         return savedDocument;
      }, this::documentSaved);
   }

   @Override
   public Optional<DocumentEntry> deleteDocument(DocumentPath documentId) {
      notNull(documentId, "documentId");

      startWatch();

      return handleDocumentEntryEvent(() -> {
         File documentFile = storageFile(documentId);
         Optional<DocumentEntry> documentEntry = fsDocumentEntry(documentFile, documentId);

         boolean removed = documentEntry.isPresent() && documentFile.delete();

         return documentEntry.filter(__ -> removed);
      }, this::documentDeleted);
   }

   @Override
   public boolean supportsUri(URI documentUri) {
      return transformerUriAdapter.supportsUri(documentUri);
   }

   @Override
   public URI toUri(DocumentPath documentId) {
      return transformerUriAdapter.toUri(documentId);
   }

   @Override
   public Optional<Document> findDocumentByUri(URI documentUri) {
      return transformerUriAdapter.findDocumentByUri(documentUri);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
      return transformerUriAdapter.findDocumentEntryByUri(documentUri);
   }

   /**
    * Real FS URI adapter to be used by {@link DocumentTransformerUriAdapter}.
    */
   public class FsUriAdapter implements UriAdapter {
      private static final String URI_SCHEME = "file";

      @Override
      public boolean supportsUri(URI documentUri) {
         validate(documentUri, "documentUri", UriRules.hasNoTraversal()).orThrow();

         if (!(documentUri.isAbsolute()
               && documentUri.getScheme().equals(URI_SCHEME)
               && (documentUri.getAuthority() == null || (documentUri.getHost().equals("localhost")
                                                          && documentUri.getPort() == -1
                                                          && documentUri.getUserInfo() == null))
               && documentUri.getFragment() == null
               && documentUri.getQuery() == null
               && documentUri.getPath() != null)) {
            return false;
         }

         try {
            fsStorageStrategy.documentId(Paths.get(documentUri.getPath()));
         } catch (IllegalArgumentException e) {
            return false;
         }

         return true;
      }

      @Override
      public URI toUri(DocumentPath documentId) {
         notNull(documentId, "documentId");

         try {
            return new URI(URI_SCHEME, null, fsStorageStrategy.storageFile(documentId).toString(), null);
         } catch (URISyntaxException e) {
            throw new UnsupportedUriException(e);
         }
      }

      @Override
      public Optional<Document> findDocumentByUri(URI documentUri) {
         satisfies(documentUri, this::supportsUri, "documentUri", "must be supported : " + documentUri);

         return findDocumentById(documentId(documentUri));
      }

      @Override
      public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
         satisfies(documentUri, this::supportsUri, "documentUri", "must be supported : " + documentUri);

         return findDocumentEntryById(documentId(documentUri));
      }

      /**
       * Extracts document identifier from specified document URI.
       *
       * @param documentUri document URI
       *
       * @return document identifier
       */
      protected DocumentPath documentId(URI documentUri) {
         return fsStorageStrategy.documentId(Paths.get(documentUri.getPath()));
      }

   }

   private Predicate<Path> directoryFilter(Specification<DocumentEntry> specification) {
      if (specification instanceof DocumentEntrySpecification) {
         DocumentEntrySpecification documentEntrySpecification = (DocumentEntrySpecification) specification;
         return documentEntrySpecification::satisfiedBySubPath;
      } else {
         return __ -> true;
      }
   }

   /**
    * List recursively all document entries.
    */
   private Stream<DocumentEntry> listAllDocumentEntries() {
      return listDocumentEntries(storageDirectory(Paths.get("")), __ -> true);
   }

   /**
    * List document entries from specified base directory. Base directory represents a logical relative path.
    *
    * @param baseDirectory backend-space directory to search from
    * @param directoryFilter user-space directory filter. Use {@code __ -> false} to disable recursive
    *       search
    *
    * @apiNote Specified base directory is directly resolved against configured storage path without
    *       taking {@link FsStorageStrategy strategies} into account. This operation will ignore base
    *       directory for {@link FsStorageStrategy strategies} other than {@link DirectFsStorageStrategy}.
    */
   private Stream<DocumentEntry> listDocumentEntries(Path baseDirectory, Predicate<Path> directoryFilter) {
      notNull(baseDirectory, "baseDirectory");
      notNull(directoryFilter, "directoryFilter");

      return StreamSupport
            .stream(Spliterators.spliteratorUnknownSize(FileUtils.iterateFiles(baseDirectory.toFile(),
                                                                               TrueFileFilter.INSTANCE,
                                                                               filterDirectory(directoryFilter)),
                                                        Spliterator.ORDERED), false)
            .filter(this::isRegularFile)
            .flatMap(storageFile -> stream(this.fsDocumentEntry(storageFile, null)));
   }

   private IOFileFilter filterDirectory(Predicate<Path> directoryFilter) {
      return new AbstractFileFilter() {
         @Override
         public boolean accept(File file) {
            return directoryFilter.test(documentDirectory(file));
         }
      };
   }

   private boolean isRegularFile(File file) {
      try {
         return Files.readAttributes(file.toPath(), BasicFileAttributes.class).isRegularFile();
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   /**
    * Low-level document entry generation from storage file.
    *
    * @param storageFile storage file
    * @param documentId optional document identifier, when known, to optimize reverse document id
    *       mapping
    *
    * @return document entry or {@link Optional#empty} if file not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   private Optional<DocumentEntry> fsDocumentEntry(File storageFile, DocumentPath documentId) {
      return fsDocumentMetadata(storageFile).map(metadata -> new DocumentEntryBuilder()
            .<DocumentEntryBuilder>reconstitute()
            .documentId(documentId != null ? documentId : documentId(storageFile))
            .metadata(metadata)
            .build());
   }

   /**
    * Low-level document content generation from storage file.
    *
    * @param storageFile storage file
    *
    * @return document content or {@link Optional#empty} if file not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   private Optional<DocumentContent> fsDocumentContent(File storageFile) {
      try {
         return optional(new InputStreamDocumentContentBuilder()
                               .<InputStreamDocumentContentBuilder>reconstitute()
                               .content(new FileInputStream(storageFile), storageFile.length())
                               .build());
      } catch (FileNotFoundException e) {
         return optional();
      } catch (Exception e) {
         throw new DocumentAccessException(e);
      }
   }

   /**
    * Low-level document metadata generation from storage file.
    *
    * @param storageFile storage file
    *
    * @return document metadata or {@link Optional#empty} if file not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   private Optional<DocumentMetadata> fsDocumentMetadata(File storageFile) {
      try {
         BasicFileAttributes basicFileAttributes =
               Files.readAttributes(storageFile.toPath(), BasicFileAttributes.class);
         Instant creationDate = basicFileAttributes.creationTime().toInstant();
         Instant lastUpdateDate = basicFileAttributes.lastModifiedTime().toInstant();

         return optional(new DocumentMetadataBuilder()
                               .<DocumentMetadataBuilder>reconstitute()
                               .documentPath(documentId(storageFile).value())
                               .contentSize(storageFile.length())
                               .creationDate(creationDate)
                               .lastUpdateDate(lastUpdateDate)
                               .build());
      } catch (NoSuchFileException e) {
         return optional();
      } catch (Exception e) {
         throw new DocumentAccessException(e);
      }
   }

   /**
    * Creates parent directories of specified storageFile. Does nothing if storageFile has no parent
    * directory.
    *
    * @param storageFile storageFile
    */
   private void mkdirStorageFile(File storageFile) {
      notNull(storageFile, "storageFile");

      File storagePathDirectory = fsStorageStrategy.storagePath().toFile();
      if (!storagePathDirectory.exists() && !createStoragePathIfMissing) {
         throw new DocumentAccessException(String.format("Missing '%s' filesystem storage directory",
                                                         storagePathDirectory));
      }

      File directory = storageFile.getParentFile();
      if (directory != null) {
         if (!(directory.mkdirs() || directory.isDirectory())) {
            throw new DocumentAccessException(String.format("Can't create '%s' filesystem storage directory",
                                                            directory));
         }
      }
   }

   /**
    * Generate real local storage path for specified document.
    *
    * @param documentId document id
    *
    * @return real document local storage path
    *
    * @apiNote The returned file is protected against path traversal attacks.
    */
   private File storageFile(DocumentPath documentId) {
      return fsStorageStrategy.storageFile(documentId).toFile();
   }

   private Path storageDirectory(Path documentDirectory) {
      return fsStorageStrategy.storageDirectory(documentDirectory);
   }

   /**
    * Returns document identifier from storage document file. Reverses path creation logic to generate a
    * logical document path.
    *
    * @param storageFile storage document file
    *
    * @return document identifier from storage file
    */
   private DocumentPath documentId(File storageFile) {
      return fsStorageStrategy.documentId(storageFile.toPath());
   }

   private Path documentDirectory(File storageDirectory) {
      return fsStorageStrategy.documentDirectory(storageDirectory.toPath());
   }

   /** Java < 10 InputStream::transferTo implementation. */
   private static long transferTo(InputStream in, OutputStream out) throws IOException {
      Objects.requireNonNull(in, "in");
      Objects.requireNonNull(out, "out");

      final int TRANSFER_BUFFER_SIZE = 4096 * 16;

      long transferred = 0;
      byte[] buffer = new byte[TRANSFER_BUFFER_SIZE];
      int read;
      while ((read = in.read(buffer, 0, TRANSFER_BUFFER_SIZE)) >= 0) {
         out.write(buffer, 0, read);
         transferred += read;
      }
      return transferred;
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", FsDocumentRepository.class.getSimpleName() + "[", "]")
            .add("fsStorageStrategy=" + fsStorageStrategy)
            .toString();
   }
}
