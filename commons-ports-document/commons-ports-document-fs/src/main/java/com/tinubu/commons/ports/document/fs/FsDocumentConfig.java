/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs;

import static com.tinubu.commons.ddd2.domain.type.support.TypeSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isLessThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isStrictlyPositive;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isAbsolute;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.nio.file.Path;
import java.nio.file.Paths;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy;
import com.tinubu.commons.ports.document.fs.storagestrategy.FsStorageStrategy;
import com.tinubu.commons.ports.document.fs.storagestrategy.HexTreeFsStorageStrategy;

/**
 * FS document repository configuration.
 */
// FIXME createIfMissingStoragePath + failFastIfMissingStoragePath
public class FsDocumentConfig extends AbstractValue {

   public static final int DEFAULT_HEX_TREE_DEPTH = 2;
   public static final Path DEFAULT_STORAGE_PATH =
         Paths.get(System.getProperty("java.io.tmpdir", "/tmp"), "document-fs");
   private static final int MAX_TREE_DEPTH = 16;

   private final Path storagePath;
   private boolean createStoragePathIfMissing;
   private final Class<? extends FsStorageStrategy> storageStrategy;
   private final HexTreeStorageStrategyConfig hexTreeStorageStrategy;

   public FsDocumentConfig(FsDocumentConfigBuilder builder) {
      this.storagePath = nullable(builder.storagePath, DEFAULT_STORAGE_PATH);
      this.createStoragePathIfMissing = nullable(builder.createStoragePathIfMissing, false);
      this.storageStrategy = nullable(builder.storageStrategy, DirectFsStorageStrategy.class);
      this.hexTreeStorageStrategy =
            nullable(builder.hexTreeStorageStrategy, HexTreeStorageStrategyConfig.ofDefaults());
   }

   @Override
   public Fields<? extends FsDocumentConfig> defineDomainFields() {
      return Fields
            .<FsDocumentConfig>builder()
            .field("storagePath",
                   v -> v.storagePath,
                   isNotNull().andValue(isAbsolute().andValue(hasNoTraversal())))
            .field("storageStrategy", v -> v.storageStrategy, isNotNull())
            .field("hexTreeStorageStrategy", v -> v.hexTreeStorageStrategy, isNotNull())
            .build();
   }

   /**
    * Document filesystem storage root path. This path must be absolute so that storage path does not depend
    * on current application default directory.
    */
   public Path storagePath() {
      return storagePath;
   }

   /**
    * Whether to automatically create missing storage path. Default to {@code false}.
    */
   public boolean createStoragePathIfMissing() {
      return createStoragePathIfMissing;
   }

   public FsDocumentConfig createStoragePathIfMissing(boolean createStoragePathIfMissing) {
      this.createStoragePathIfMissing = createStoragePathIfMissing;
      return checkInvariants(this);
   }

   /**
    * Document filesystem storage strategy to use. You must not change this configuration if documents already
    * exist, as it changes the way documents are searched.
    */
   public Class<? extends FsStorageStrategy> storageStrategy() {
      return storageStrategy;
   }

   /**
    * {@link HexTreeFsStorageStrategy} configuration.
    *
    * @return {@link HexTreeFsStorageStrategy} configuration
    */
   public HexTreeStorageStrategyConfig hexTreeStorageStrategy() {
      return hexTreeStorageStrategy;
   }

   public static class HexTreeStorageStrategyConfig extends AbstractValue {

      private final int treeDepth;

      private HexTreeStorageStrategyConfig(int treeDepth) {
         this.treeDepth = treeDepth;
      }

      @Override
      public Fields<? extends HexTreeStorageStrategyConfig> defineDomainFields() {
         return Fields
               .<HexTreeStorageStrategyConfig>builder()
               .field("treeDepth",
                      v -> v.treeDepth,
                      isStrictlyPositive().andValue(isLessThanOrEqualTo(value(MAX_TREE_DEPTH))))
               .build();
      }

      public static HexTreeStorageStrategyConfig of(int treeDepth) {
         return checkInvariants(new HexTreeStorageStrategyConfig(treeDepth));
      }

      public static HexTreeStorageStrategyConfig ofDefaults() {
         return of(DEFAULT_HEX_TREE_DEPTH);
      }

      /**
       * Generated hex tree depth for files, e.g.: a depth of 3 will generate directory tree like {@code
       * 6a/fd/ac}. You must not change this configuration if documents already exist, as it changes the way
       * documents are searched.
       */
      public int treeDepth() {
         return treeDepth;
      }

   }

   public static class FsDocumentConfigBuilder extends DomainBuilder<FsDocumentConfig> {

      private Path storagePath;
      private boolean createStoragePathIfMissing;
      private Class<? extends FsStorageStrategy> storageStrategy;
      private HexTreeStorageStrategyConfig hexTreeStorageStrategy;

      public FsDocumentConfigBuilder storagePath(Path storagePath) {
         this.storagePath = storagePath;
         return this;
      }

      public FsDocumentConfigBuilder createStoragePathIfMissing(boolean createStoragePathIfMissing) {
         this.createStoragePathIfMissing = createStoragePathIfMissing;
         return this;
      }

      public FsDocumentConfigBuilder storageStrategy(Class<? extends FsStorageStrategy> storageStrategy) {
         this.storageStrategy = storageStrategy;
         return this;
      }

      public FsDocumentConfigBuilder hexTreeStorageStrategy(HexTreeStorageStrategyConfig hexTreeStorageStrategy) {
         this.hexTreeStorageStrategy = hexTreeStorageStrategy;
         return this;
      }

      @Override
      protected FsDocumentConfig buildDomainObject() {
         return new FsDocumentConfig(this);
      }
   }
}
