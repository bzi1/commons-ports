/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs.storagestrategy;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;

import java.nio.file.Path;
import java.util.Objects;
import java.util.StringJoiner;

import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.fs.FsDocumentConfig;

/**
 * Generates a storage path that directly and exactly matches document identifier path.
 */
public class DirectFsStorageStrategy extends AbstractFsStorageStrategy {

   public DirectFsStorageStrategy(FsDocumentConfig fsDocumentConfig) {
      super(fsDocumentConfig);
   }

   @Override
   public Path storageFile(DocumentPath documentId) {
      notNull(documentId, "documentId");

      Path storageFile = storagePath.resolve(documentId.value());

      checkPathTraversal(storageFile);

      return storageFile;
   }

   @Override
   public DocumentPath documentId(Path storageFile) {
      notNull(storageFile, "storageFile");
      satisfies(storageFile,
                sf -> sf.startsWith(storagePath),
                "storageFile",
                String.format("must be contained in '%s' storage path", storageFile));

      return DocumentPath.of(storagePath.relativize(storageFile));
   }

   @Override
   public Path storageDirectory(Path documentDirectory) {
      notNull(documentDirectory, "documentDirectory");

      Path storageFile = storagePath.resolve(documentDirectory);

      checkPathTraversal(storageFile);

      return storageFile;
   }

   @Override
   public Path documentDirectory(Path storageDirectory) {
      notNull(storageDirectory, "storageDirectory");
      satisfies(storageDirectory,
                sd -> sd.startsWith(storagePath),
                "storageDirectory",
                String.format("must be contained in '%s' storage path", storageDirectory));

      return storagePath.relativize(storageDirectory);
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      DirectFsStorageStrategy that = (DirectFsStorageStrategy) o;
      return Objects.equals(storagePath, that.storagePath);
   }

   @Override
   public int hashCode() {
      return Objects.hash(storagePath);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", DirectFsStorageStrategy.class.getSimpleName() + "[", "]")
            .add("storagePath=" + storagePath)
            .toString();
   }
}
