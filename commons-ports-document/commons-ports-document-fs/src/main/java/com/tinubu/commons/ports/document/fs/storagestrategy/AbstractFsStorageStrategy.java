/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs.storagestrategy;

import java.io.IOException;
import java.nio.file.Path;

import com.tinubu.commons.ports.document.fs.FsDocumentConfig;

/**
 * Generates a storage path that dispatch files in a hexadecimal directory tree. Hexadecimal directories are
 * 2 hexadecimal digits in lowercase. The directory tree depth is configurable. Directory tree is
 * deterministic as it is based on {@code MD5(documentId::path)}.
 */
public abstract class AbstractFsStorageStrategy implements FsStorageStrategy {

   protected final Path storagePath;

   public AbstractFsStorageStrategy(FsDocumentConfig fsDocumentConfig) {
      this.storagePath = fsDocumentConfig.storagePath().toAbsolutePath();
   }

   @Override
   public Path storagePath() {
      return this.storagePath;
   }

   /**
    * Systematic security check for path traversal. Storage file must never reference a file out of
    * configured storage path. This operation will detect any path traversal issue due to bad validation or
    * coding mistake.
    *
    * @param storageFile storage file to check for path traversal
    */
   protected void checkPathTraversal(Path storageFile) {
      try {
         if (!storageFile.toFile().getCanonicalPath().startsWith(storagePath.toFile().getCanonicalPath())) {
            throw new IllegalArgumentException(String.format(
                  "'%s' storage file must not reference a file out of '%s' storage root path",
                  storageFile,
                  storagePath));
         }
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }
}
