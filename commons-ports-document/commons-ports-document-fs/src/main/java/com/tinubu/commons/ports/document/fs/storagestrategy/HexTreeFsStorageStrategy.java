/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs.storagestrategy;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import java.util.StringJoiner;

import org.apache.commons.codec.binary.Hex;

import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.fs.FsDocumentConfig;

/**
 * Generates a storage path that dispatch files in a hexadecimal directory tree. Hexadecimal directories are
 * 2 hexadecimal digits in lowercase. The directory tree depth is configurable. Directory tree is
 * deterministic as it is based on {@code MD5(documentId::path)}.
 */
public class HexTreeFsStorageStrategy extends AbstractFsStorageStrategy {

   /**
    * Thread safe MD5 message digest instance.
    */
   private static final ThreadLocal<MessageDigest> md5 = ThreadLocal.withInitial(() -> {
      try {
         return MessageDigest.getInstance("MD5");
      } catch (NoSuchAlgorithmException e) {
         throw new IllegalStateException(e);
      }
   });

   private final int hexTreeDepth;

   public HexTreeFsStorageStrategy(FsDocumentConfig fsDocumentConfig) {
      super(fsDocumentConfig);
      hexTreeDepth = fsDocumentConfig.hexTreeStorageStrategy().treeDepth();
   }

   @Override
   public Path storageFile(DocumentPath documentId) {
      notNull(documentId, "documentId");

      Path storageFile =
            storagePath.resolve(pathHexa(documentId.value(), hexTreeDepth)).resolve(documentId.value());

      checkPathTraversal(storageFile);

      return storageFile;
   }

   @Override
   public DocumentPath documentId(Path storageFile) {
      notNull(storageFile, "storageFile");
      satisfies(storageFile,
                sf -> sf.startsWith(storagePath),
                "storageFile",
                String.format("must be contained in '%s' storage path", storagePath));

      int treeDepth = hexTreeDepth;

      Path relativizedDocumentPath = storagePath.relativize(storageFile);

      if (!(relativizedDocumentPath.getNameCount() > treeDepth)) {
         throw new IllegalArgumentException(String.format("Unsupported '%s' storage file path", storageFile));
      }

      Path documentPath = Paths.get(".");
      for (int i = treeDepth; i < relativizedDocumentPath.getNameCount(); i++) {
         documentPath = documentPath.resolve(relativizedDocumentPath.getName(i));
      }

      return DocumentPath.of(documentPath);
   }

   @Override
   public Path storageDirectory(Path documentDirectory) {
      throw new UnsupportedOperationException();
   }

   @Override
   public Path documentDirectory(Path storageDirectory) {
      throw new UnsupportedOperationException();
   }

   static String pathHexaString(Path documentPath) {
      MessageDigest md = md5.get();

      md.reset();
      md.update(documentPath.toString().getBytes());
      return Hex.encodeHexString(md.digest());
   }

   /**
    * Generates an hexadecimal directory path, of specified depth, from hash of specified document path.
    *
    * @param documentPath document path to use for hashing
    * @param treeDepth hexadecimal directory tree depth
    *
    * @return a directory path composed of specified number of hexadecimal names
    */
   private Path pathHexa(Path documentPath, int treeDepth) {
      notNull(documentPath, "documentPath");

      String pathHexa = pathHexaString(documentPath);

      if (treeDepth > 0 && treeDepth <= pathHexa.length() / 2) {
         Path path = null;
         for (int i = 0; i < treeDepth; i++) {
            String hexDirectory = pathHexa.substring(i * 2, (i + 1) * 2);
            path = path == null ? Paths.get(hexDirectory) : path.resolve(hexDirectory);
         }
         return path;
      } else {
         throw new IllegalStateException(String.format("Bad '%d' tree depth for '%s' document hash : %s",
                                                       treeDepth,
                                                       documentPath,
                                                       pathHexa));
      }

   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      HexTreeFsStorageStrategy that = (HexTreeFsStorageStrategy) o;
      return Objects.equals(storagePath, that.storagePath) && Objects.equals(hexTreeDepth, that.hexTreeDepth);
   }

   @Override
   public int hashCode() {
      return Objects.hash(storagePath, hexTreeDepth);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", HexTreeFsStorageStrategy.class.getSimpleName() + "[", "]")
            .add("storagePath=" + storagePath)
            .add("hexTreeDepth=" + hexTreeDepth)
            .toString();
   }
}
