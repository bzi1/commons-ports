/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs.storagestrategy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.nio.file.Paths;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.fs.FsDocumentConfig.FsDocumentConfigBuilder;

class DirectFsStorageStrategyTest {

   @Test
   public void testDocumentPathWhenNominal() {
      assertThat(newStorageStrategy().storageFile(DocumentPath.of("path", "file.ext"))).isEqualTo(Paths.get(
            "/tmp",
            "test",
            "path",
            "file.ext"));
   }

   @Test
   public void testDocumentPathWhenSimpleFile() {
      assertThat(newStorageStrategy().storageFile(DocumentPath.of("file.ext"))).isEqualTo(Paths.get("/tmp",
                                                                                                    "test",
                                                                                                    "file.ext"));
   }

   @Test
   public void testDocumentPathWhenBadParameters() {
      assertThatNullPointerException().isThrownBy(() -> newStorageStrategy().storageFile(null));
   }

   private FsStorageStrategy newStorageStrategy() {
      return FsStorageStrategyFactory.fsStorageStrategy(new FsDocumentConfigBuilder()
                                                              .storagePath(Paths.get("/tmp", "test"))
                                                              .storageStrategy(DirectFsStorageStrategy.class)
                                                              .build());
   }

}