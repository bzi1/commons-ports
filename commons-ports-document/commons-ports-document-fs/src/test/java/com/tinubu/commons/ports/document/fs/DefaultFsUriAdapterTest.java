/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.UriAdapter;

class DefaultFsUriAdapterTest {

   @Test
   public void testUriAdapterWhenNominal() throws IOException {
      UriAdapter uriAdapter = new DefaultFsUriAdapter();

      File testFile = Files.createTempFile("fs-uri-adapter-test", ".txt").toFile();
      testFile.deleteOnExit();

      Optional<Document> document = uriAdapter.findDocumentByUri(testFile.toURI());

      Path expectedTestFilePath = Paths.get("/").relativize(testFile.toPath());
      assertThat(document).hasValueSatisfying(d -> {
         assertThat(d.documentId()).isEqualTo(DocumentPath.of(expectedTestFilePath));
         assertThat(d.metadata().documentPath()).isEqualTo(expectedTestFilePath);
      });
   }

}