/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.UnaryOperator;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import com.tinubu.commons.ports.document.domain.CommonDocumentRepositoryTest;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent.LoadedDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata.OpenDocumentMetadataBuilder;
import com.tinubu.commons.ports.document.fs.FsDocumentConfig.FsDocumentConfigBuilder;
import com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy;
import com.tinubu.commons.ports.document.fs.storagestrategy.FsStorageStrategy;
import com.tinubu.commons.ports.document.fs.storagestrategy.HexTreeFsStorageStrategy;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ZipTransformer;

public class FsDocumentRepositoryTest extends CommonDocumentRepositoryTest {

   private static Path TEST_STORAGE_PATH;

   private FsDocumentRepository documentRepository;

   /**
    * {@inheritDoc}
    * At least the Linux filesystem updates the creation date on overwrite.
    */
   @Override
   public boolean isUpdatingCreationDateOnOverwrite() {
      return true;
   }

   @BeforeAll
   public static void createStoragePath() {
      try {
         TEST_STORAGE_PATH = Files.createTempDirectory("document-fs-test");

         System.out.printf("Creating '%s' filesystem test storage path%n", TEST_STORAGE_PATH);
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   @AfterAll
   public static void deleteStoragePath() {
      try {
         System.out.printf("Deleting '%s' filesystem test storage path%n", TEST_STORAGE_PATH);

         FileUtils.deleteDirectory(TEST_STORAGE_PATH.toFile());
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   @BeforeEach
   public void configureDocumentRepository() {
      this.documentRepository = newFsDocumentRepository();
   }

   @Override
   protected DocumentRepository documentRepository() {
      return documentRepository;
   }

   @Override
   protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
      return new ZipTransformer(zipPath).compress(documents);
   }

   private FsDocumentRepository newFsDocumentRepository() {
      return new FsDocumentRepository(new FsDocumentConfigBuilder()
                                            .storagePath(TEST_STORAGE_PATH)
                                            .storageStrategy(DirectFsStorageStrategy.class)
                                            .build());
   }

   @Override
   protected UnaryOperator<DocumentMetadataBuilder> synchronizeExpectedMetadata(DocumentMetadata actual) {
      return builder -> builder
            .chain(super.synchronizeExpectedMetadata(actual))
            .attributes(actual.attributes())
            .contentType(actual.contentEncoding().orElse(null));
   }

   @Override
   protected UnaryOperator<LoadedDocumentContentBuilder> synchronizeExpectedContent(DocumentContent actual) {
      return builder -> builder
            .chain(super.synchronizeExpectedContent(actual))
            .contentEncoding(actual.contentEncoding().orElse(null));
   }

   @Nested
   public class WhenNotExistingStoragePath {

      @ParameterizedTest
      @ValueSource(classes = { DirectFsStorageStrategy.class, HexTreeFsStorageStrategy.class })
      public void testFindDocumentByIdWhenNotExistingBaseStoragePath(Class<FsStorageStrategy> storageStrategy)
            throws IOException {
         Path storagePath = Paths.get(System.getProperty("java.io.tmpdir", "/tmp"), "test-document-fs");

         try {
            DocumentRepository repository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                           .storagePath(storagePath)
                                                                           .createStoragePathIfMissing(false)
                                                                           .storageStrategy(storageStrategy)
                                                                           .build());

            assertThat(repository.findDocumentById(DocumentPath.of("file.pdf"))).isEmpty();

            DocumentRepository createStorageIfMissingRepository =
                  new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                 .storagePath(storagePath)
                                                 .createStoragePathIfMissing(true)
                                                 .storageStrategy(storageStrategy)
                                                 .build());

            assertThat(createStorageIfMissingRepository.findDocumentById(DocumentPath.of("file.pdf"))).isEmpty();
         } finally {
            FileUtils.deleteDirectory(storagePath.toFile());
         }
      }

      @ParameterizedTest
      @ValueSource(classes = { DirectFsStorageStrategy.class, HexTreeFsStorageStrategy.class })
      public void testSaveDocumentWhenNotExistingBaseStoragePath(Class<FsStorageStrategy> storageStrategy)
            throws IOException {
         Path storagePath = Paths.get(System.getProperty("java.io.tmpdir", "/tmp"), "test-document-fs");

         try {
            DocumentRepository repository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                           .storagePath(storagePath)
                                                                           .createStoragePathIfMissing(false)
                                                                           .storageStrategy(storageStrategy)
                                                                           .build());

            assertThatExceptionOfType(DocumentAccessException.class).isThrownBy(() -> repository.saveDocument(
                  stubDocument(DocumentPath.of("file.pdf")).build(),
                  false));

            DocumentRepository createStorageIfMissingRepository =
                  new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                 .storagePath(storagePath)
                                                 .createStoragePathIfMissing(true)
                                                 .storageStrategy(storageStrategy)
                                                 .build());

            assertThat(createStorageIfMissingRepository.saveDocument(stubDocument(DocumentPath.of("file.pdf")).build(),
                                                                     false)).isPresent();
         } finally {
            FileUtils.deleteDirectory(storagePath.toFile());
         }
      }

      @ParameterizedTest
      @ValueSource(classes = { DirectFsStorageStrategy.class, HexTreeFsStorageStrategy.class })
      public void testOpenDocumentWhenNotExistingBaseStoragePath(Class<FsStorageStrategy> storageStrategy)
            throws IOException {
         Path storagePath = Paths.get(System.getProperty("java.io.tmpdir", "/tmp"), "test-document-fs");

         try {
            DocumentRepository repository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                           .storagePath(storagePath)
                                                                           .createStoragePathIfMissing(false)
                                                                           .storageStrategy(storageStrategy)
                                                                           .build());

            assertThatExceptionOfType(DocumentAccessException.class).isThrownBy(() -> repository.openDocument(
                  DocumentPath.of("file.pdf"),
                  false,
                  false,
                  OpenDocumentMetadataBuilder.empty()));

            DocumentRepository createStorageIfMissingRepository =
                  new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                 .storagePath(storagePath)
                                                 .createStoragePathIfMissing(true)
                                                 .storageStrategy(storageStrategy)
                                                 .build());

            assertThat(createStorageIfMissingRepository.openDocument(DocumentPath.of("file.pdf"),
                                                                     false,
                                                                     false,
                                                                     OpenDocumentMetadataBuilder.empty())).isPresent();
         } finally {
            FileUtils.deleteDirectory(storagePath.toFile());
         }
      }

   }

   @Nested
   public class UriAdapter {

      @Test
      public void testToUriWhenNominal() {
         assertThat(documentRepository().toUri(DocumentPath.of("file.txt"))).isEqualTo(uri("file:"
                                                                                           + TEST_STORAGE_PATH
                                                                                           + "/file.txt"));
         assertThat(documentRepository().toUri(DocumentPath.of("path/file.txt"))).isEqualTo(uri("file:"
                                                                                                + TEST_STORAGE_PATH
                                                                                                + "/path/file.txt"));
      }

      @Test
      public void testSupportsUriWhenNominal() {
         assertThat(documentRepository().supportsUri(uri("file:"
                                                         + TEST_STORAGE_PATH
                                                         + "/path/test.txt"))).isTrue();
      }

      @Test
      public void testSupportsUriWhenAlternativeForms() {
         assertThat(documentRepository().supportsUri(uri("file://"
                                                         + TEST_STORAGE_PATH
                                                         + "/path/test.txt"))).isTrue();
         assertThat(documentRepository().supportsUri(uri("file:"
                                                         + TEST_STORAGE_PATH.toString().substring(1)
                                                         + "/path/test.txt"))).isFalse();
         assertThat(documentRepository().supportsUri(uri("file://host"
                                                         + TEST_STORAGE_PATH
                                                         + "/path/test.txt"))).isFalse();
         assertThat(documentRepository().supportsUri(uri("file:"
                                                         + TEST_STORAGE_PATH
                                                         + "/path/test.txt#fragment"))).isFalse();
         assertThat(documentRepository().supportsUri(uri("file:"
                                                         + TEST_STORAGE_PATH
                                                         + "/path/test.txt?query"))).isFalse();
      }

   }
}