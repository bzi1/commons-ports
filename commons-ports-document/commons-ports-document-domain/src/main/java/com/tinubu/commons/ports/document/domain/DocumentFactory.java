/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.ServiceLoader;

import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;

/**
 * Simple document factory, in complement to more complex {@link DocumentRepository} implementations.
 */
// FIXME tests
public class DocumentFactory {

   private static final List<UriAdapter> defaultUriAdapters = defaultUriAdapters();

   /**
    * Generates a document from local filesystem's {@link File}.
    * Metadata document name is set to the original file name (not the full path).
    *
    * @param file file to generate this document from
    * @param documentId generated document identifier
    * @param documentEncoding document encoding
    *
    * @return document builder
    *
    * @throws NoSuchFileException if specified file does not exist
    * @throws IOException if an I/O error occurs while reading file
    */
   public Document document(File file, DocumentPath documentId, Charset documentEncoding) throws IOException {
      notNull(file, "file");
      notNull(documentId, "documentId");

      BasicFileAttributes basicFileAttributes =
            Files.readAttributes(file.toPath(), BasicFileAttributes.class);
      Instant creationDate = basicFileAttributes.creationTime().toInstant();
      Instant lastUpdateDate = basicFileAttributes.lastModifiedTime().toInstant();

      return new DocumentBuilder().<DocumentBuilder>reconstitute()
            .documentId(documentId)
            .metadata(new DocumentMetadataBuilder()
                            .<DocumentMetadataBuilder>reconstitute()
                            .documentPath(file.toPath().getFileName())
                            .creationDate(creationDate)
                            .lastUpdateDate(lastUpdateDate)
                            .contentType(documentEncoding)
                            .contentSize(file.length())
                            .build())
            .streamContent(Files.newInputStream(file.toPath()), documentEncoding, file.length())
            .build();
   }

   /**
    * Generates a document from local filesystem's {@link File} with
    * {@link Charset#defaultCharset() default JVM encoding}.
    * Metadata document name keeps the original baseline file name (not the full path).
    *
    * @param file file to generate this document from
    * @param documentId generated document identifier
    *
    * @return document builder
    *
    * @throws NoSuchFileException if specified file does not exist
    * @throws IOException if an I/O error occurs while reading file
    */
   public Document document(File file, DocumentPath documentId) throws IOException {
      return document(file, documentId, Charset.defaultCharset());
   }

   /**
    * Finds document by URI in specified adapters. First adapter
    * {@link UriAdapter#supportsUri(URI) supporting} the specified URI will be used.
    *
    * @param documentUri document URI
    *
    * @return found document or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   public Optional<Document> document(URI documentUri, List<? extends UriAdapter> uriAdapters) {
      notNull(documentUri, "documentUri");
      noNullElements(uriAdapters, "uriAdapters");

      return stream(uriAdapters)
            .filter(repository -> repository.supportsUri(documentUri))
            .findFirst()
            .flatMap(documentRepository -> documentRepository.findDocumentByUri(documentUri));
   }

   /**
    * Finds document by URI in specified adapters. First adapter
    * {@link UriAdapter#supportsUri(URI) supporting} the specified URI will be used.
    *
    * @param documentUri document URI
    *
    * @return found document or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   public Optional<Document> document(URI documentUri, UriAdapter... uriAdapters) {
      return document(documentUri, list(notNull(uriAdapters, "uriAdapters")));
   }

   /**
    * Finds document by URI in default provided adapters. First adapter
    * {@link UriAdapter#supportsUri(URI) supporting} the specified URI will be used.
    * Default provided adapters are dynamically loaded using {@link ServiceLoader} registered
    * {@link UriAdapter}s.
    * <p>
    * Out-of-the-box registered adapters :
    * <ul>
    * <li>{@code file}</li>
    * <li>{@code classpath}</li>
    * </ul>
    *
    * @param documentUri document URI
    *
    * @return found document or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   public Optional<Document> document(URI documentUri) {
      return document(documentUri, defaultUriAdapters);
   }

   /**
    * Finds document entry by URI in specified adapters. First adapter
    * {@link UriAdapter#supportsUri(URI) supporting} the specified URI will be used.
    *
    * @param documentUri document URI
    *
    * @return found document entry or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   public Optional<DocumentEntry> documentEntry(URI documentUri, List<? extends UriAdapter> uriAdapters) {
      notNull(documentUri, "documentUri");
      noNullElements(uriAdapters, "uriAdapters");

      return stream(uriAdapters)
            .filter(repository -> repository.supportsUri(documentUri))
            .findFirst()
            .flatMap(documentRepository -> documentRepository.findDocumentEntryByUri(documentUri));
   }

   /**
    * Finds document entry by URI in specified adapters. First adapter
    * {@link UriAdapter#supportsUri(URI) supporting} the specified URI will be used.
    *
    * @param documentUri document URI
    *
    * @return found document entry or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   public Optional<DocumentEntry> documentEntry(URI documentUri, UriAdapter... uriAdapters) {
      return documentEntry(documentUri, list(notNull(uriAdapters, "uriAdapters")));
   }

   /**
    * Finds document entry by URI in default provided adapters. First adapter
    * {@link UriAdapter#supportsUri(URI) supporting} the specified URI will be used.
    * Default provided adapters are dynamically loaded using {@link ServiceLoader} registered
    * {@link UriAdapter}s.
    * <p>
    * Out-of-the-box registered adapters :
    * <ul>
    * <li>{@code file}</li>
    * <li>{@code classpath}</li>
    * </ul>
    *
    * @param documentUri document URI
    *
    * @return found document entry or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   public Optional<DocumentEntry> documentEntry(URI documentUri) {
      return documentEntry(documentUri, defaultUriAdapters);
   }

   private static List<UriAdapter> defaultUriAdapters() {
      ServiceLoader<UriAdapter> uriAdapters = ServiceLoader.loadInstalled(UriAdapter.class);

      return list(stream(uriAdapters));
   }

}
