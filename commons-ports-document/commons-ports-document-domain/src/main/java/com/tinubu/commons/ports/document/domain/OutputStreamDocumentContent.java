/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Optional;
import java.util.function.Consumer;

import org.apache.commons.io.output.WriterOutputStream;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.valueformatter.HiddenValueFormatter;
import com.tinubu.commons.lang.beans.Getter;

/**
 * Document content stream implementation for write access.
 * This content is represented with an {@link OutputStream}.
 * This implementation should be used only by
 * {@link DocumentRepository#openDocument(DocumentPath, boolean, boolean, OpenDocumentMetadata)}
 * implementations.
 */
public class OutputStreamDocumentContent extends AbstractValue implements DocumentContent {
   protected static final int DEFAULT_BUFFER_SIZE = 4096 * 16;

   /** Document content as {@link InputStream}. Content can be read only one time. */
   protected final OutputStream content;
   /** Optional document content encoding. */
   protected final Charset contentEncoding;

   protected OutputStreamDocumentContent(OutputStreamDocumentContentBuilder builder) {
      this.content = nullable(builder.content,
                              content -> new AutoFinalizingOutputStream(content,
                                                                        nullable(builder.finalize,
                                                                                 __ -> {})));
      this.contentEncoding = builder.contentEncoding;
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends OutputStreamDocumentContent> defineDomainFields() {
      return Fields
            .<OutputStreamDocumentContent>builder()
            .superFields((Fields<OutputStreamDocumentContent>) super.defineDomainFields())
            .technicalField("content",
                            v -> v.content,
                            new HiddenValueFormatter().compose(v -> v.content),
                            isNotNull())
            .field("contentEncoding", v -> v.contentEncoding)
            .build();
   }

   @Override
   public String stringContent(Charset defaultContentEncoding) {
      throw new IllegalStateException("Unsupported operation for this content implementation");
   }

   @Override
   public String stringContent() {
      return stringContent(contentEncoding().orElseThrow(() -> new IllegalStateException(
            "No content encoding set, you should specify one to encode this content")));
   }

   @Override
   public InputStream inputStreamContent() {
      throw new IllegalStateException("Unsupported operation for this content implementation");
   }

   @Override
   public OutputStream outputStreamContent() {
      return content;
   }

   @Override
   public InputStreamReader readerContent(Charset defaultContentEncoding) {
      throw new IllegalStateException("Unsupported operation for this content implementation");
   }

   @Override
   public InputStreamReader readerContent() {
      return readerContent(contentEncoding().orElseThrow(() -> new IllegalStateException(
            "No content encoding set, you should specify one to encode this content")));
   }

   @Override
   public Writer writerContent() {
      return new OutputStreamWriter(outputStreamContent());
   }

   @Override
   public Optional<Long> contentSize() {
      return optional();
   }

   @Override
   @Getter
   public byte[] content() {
      throw new IllegalStateException("Unsupported operation for this content implementation");
   }

   @Override
   @Getter
   public Optional<Charset> contentEncoding() {
      return nullable(contentEncoding);
   }

   public static OutputStreamDocumentContentBuilder reconstituteBuilder() {
      return new OutputStreamDocumentContentBuilder().reconstitute();
   }

   public static class OutputStreamDocumentContentBuilder extends DomainBuilder<OutputStreamDocumentContent> {
      private OutputStream content;
      private Charset contentEncoding;
      private Consumer<OutputStream> finalize;

      public OutputStreamDocumentContentBuilder content(OutputStream content, Charset contentEncoding) {
         this.content = content;
         this.contentEncoding = contentEncoding;
         return this;
      }

      public OutputStreamDocumentContentBuilder content(OutputStream content) {
         return content(content, null);
      }

      public OutputStreamDocumentContentBuilder content(Writer content, Charset contentEncoding) {
         return content(new WriterOutputStream(notNull(content, "content"),
                                               notNull(contentEncoding, "contentEncoding"),
                                               DEFAULT_BUFFER_SIZE,
                                               false), contentEncoding);
      }

      public OutputStreamDocumentContentBuilder finalize(Consumer<OutputStream> finalize) {
         this.finalize = finalize;
         return this;
      }

      @Override
      public OutputStreamDocumentContent buildDomainObject() {
         return new OutputStreamDocumentContent(this);
      }

   }

   /**
    * {@link OutputStream} that automatically calls an arbitrary finalize operation at close.
    */
   private static class AutoFinalizingOutputStream extends OutputStream {

      private final OutputStream outputStream;
      private final Consumer<OutputStream> finalize;

      public AutoFinalizingOutputStream(OutputStream outputStream, Consumer<OutputStream> finalize) {
         this.outputStream = notNull(outputStream, "outputStream");
         this.finalize = notNull(finalize, "finalize");
      }

      @Override
      public void write(byte[] b) throws IOException {
         outputStream.write(b);
      }

      @Override
      public void write(byte[] b, int off, int len) throws IOException {
         outputStream.write(b, off, len);
      }

      @Override
      public void flush() throws IOException {
         outputStream.flush();
      }

      @Override
      public void write(int b) throws IOException {
         outputStream.write(b);
      }

      @Override
      public void close() throws IOException {
         try {
            finalize.accept(outputStream);
         } finally {
            outputStream.close();
         }
      }
   }

}

