/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd2.domain.type.support.TypeSupport.checkInvariants;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.net.URI;
import java.nio.file.Paths;

/**
 * Uniquely identify a document using a logical, simple key representation.
 * The resulting document path is a 0-level path with key value as path filename.
 * Document key length is restricted to {@value #MAX_PATH_ELEMENT_LENGTH} bytes.
 */
public class DocumentKey extends DocumentPath {

   protected static final String URN_ID_TYPE = "key";

   protected DocumentKey(String documentKey, boolean newObject) {
      super(Paths.get(documentKey), newObject);
   }

   protected DocumentKey(String documentKey) {
      super(Paths.get(documentKey));
   }

   public static DocumentKey of(String documentKey) {
      return checkInvariants(new DocumentKey(documentKey));
   }

   public static DocumentKey of(URI urn) {
      return of(nullable(urn).map(u -> documentUrnValue(u, URN_ID_TYPE)).orElse(null));
   }

   public static DocumentKey ofNewObject(String documentKey) {
      return checkInvariants(new DocumentKey(documentKey, true));
   }

   public static DocumentKey ofNewObject(URI urn) {
      return ofNewObject(nullable(urn).map(u -> documentUrnValue(u, URN_ID_TYPE)).orElse(null));
   }

   /**
    * Creates a URN from this document key.
    * URN format is {@code urn:document:key:<key>}.
    *
    * @return URN of this document key
    */
   @Override
   public URI urnValue() {
      return documentUrn(URN_ID_TYPE, stringValue());
   }

}
