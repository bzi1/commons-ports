/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.allSatisfies;
import static com.tinubu.commons.ddd2.invariant.rules.CriterionRules.operands;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.doesNotStartWith;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FilenameUtils;

import com.tinubu.commons.ddd2.criterion.Criteria;
import com.tinubu.commons.ddd2.criterion.CriteriaSatisfier;
import com.tinubu.commons.ddd2.criterion.Criterion;
import com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder;
import com.tinubu.commons.ddd2.criterion.specializations.PathCriterion;
import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.rules.NumberRules;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;

/**
 * A {@link DocumentEntry} criteria specification.
 */
public class DocumentEntryCriteria extends AbstractValue
      implements Criteria<DocumentEntry>, DocumentEntrySpecification {

   protected final Criterion<Path> documentPath;
   protected final Criterion<Integer> documentPathDepth;
   protected final Criterion<Path> documentParentPath;
   protected final Criterion<String> documentName;
   protected final Criterion<String> documentExtension;
   protected final Criterion<MimeType> contentType;
   protected final Criterion<Long> contentSize;
   protected final Criterion<Instant> creationDate;
   protected final Criterion<Instant> lastUpdateDate;

   public DocumentEntryCriteria(DocumentEntryCriteriaBuilder builder) {
      this.documentPath = nullable(builder.documentPath, PathCriterion::of);
      this.documentPathDepth = builder.documentPathDepth;
      this.documentParentPath = nullable(builder.documentParentPath, PathCriterion::of);
      this.documentName = builder.documentName;
      this.documentExtension = builder.documentExtension;
      this.contentType = builder.contentType;
      this.contentSize = builder.contentSize;
      this.creationDate = builder.creationDate;
      this.lastUpdateDate = builder.lastUpdateDate;
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends DocumentEntryCriteria> defineDomainFields() {
      InvariantRule<String> validExtension = doesNotStartWith(value("."));

      return Fields
            .<DocumentEntryCriteria>builder()
            .superFields((Fields<DocumentEntryCriteria>) super.defineDomainFields())
            .field("documentPath",
                   v -> v.documentPath,
                   isNull().orValue(operands(allSatisfies(isValidPath()))))
            .field("documentPathDepth",
                   v -> v.documentPathDepth,
                   isNull().orValue(operands(allSatisfies(NumberRules.<Integer>isPositive()))))
            .field("documentParentPath",
                   v -> v.documentParentPath,
                   isNull().orValue(operands(allSatisfies(isValidPath()))))
            .field("documentName", v -> v.documentName)
            .field("documentExtension",
                   v -> v.documentExtension,
                   isNull().orValue(operands(allSatisfies(validExtension))))
            .field("contentType", v -> v.contentType)
            .field("contentSize",
                   v -> v.contentSize,
                   isNull().orValue(operands(allSatisfies(NumberRules.<Long>isPositive()))))
            .field("creationDate", v -> v.creationDate)
            .field("lastUpdateDate", v -> v.lastUpdateDate)
            .build();
   }

   @Override
   public boolean satisfiedBy(DocumentEntry entry) {
      notNull(entry, "entry");

      return new CriteriaSatisfier()
            .satisfiedBy(documentPathDepth, () -> documentPathDepth(entry))
            .satisfiedBy(documentExtension, () -> documentExtension(entry))
            .satisfiedBy(documentName, () -> documentName(entry))
            .satisfiedBy(documentPath, () -> documentPath(entry))
            .satisfiedBy(documentParentPath, () -> documentParentPath(entry))
            .satisfiedBy(contentType, () -> contentType(entry))
            .satisfiedByOptional(contentSize, () -> contentSize(entry))
            .satisfiedByOptional(creationDate, () -> creationDate(entry))
            .satisfiedByOptional(lastUpdateDate, () -> lastUpdateDate(entry))
            .isSatisfied();
   }

   @Override
   public boolean satisfiedBySubPath(Path documentSubPath) {
      Path normalizedDocumentPath =
            validate(normalizePath(documentSubPath), "documentSubPath", isValidPath()).orThrow();

      if (documentPath != null && !documentSubPath.toString().isEmpty()) {
         switch (documentPath.operator()) {
            case TRUE:
            case DEFINED:
               return true;
            case FALSE:
            case UNDEFINED:
               return false;
            case EQUAL:
            case IN:
            case START_WITH:
               return stream(documentPath.operands()).anyMatch(p -> PathCriterion
                     .of(CriterionBuilder.startWith(documentPath.flags(), normalizedDocumentPath))
                     .satisfiedBy(p));
            case NOT_EQUAL:
            case NOT_IN:
            case NOT_START_WITH:
               return stream(documentPath.operands()).noneMatch(p -> PathCriterion
                     .of(CriterionBuilder.equal(documentPath.flags(), normalizedDocumentPath))
                     .satisfiedBy(p));
            case CONTAIN:
            case MATCH:
               if (documentPath.operand().startsWith("/")) {
                  return PathCriterion
                        .of(CriterionBuilder.startWith(documentPath.flags(), normalizedDocumentPath))
                        .satisfiedBy(documentPath.operand());
               } else {
                  return true;
               }
            default:
               return true;
         }
      }

      return true;
   }

   /**
    * A criterion matching the document id whole path, including document name (last path element). e.g. :
    * {@code path/subpath/file.ext} will be matched against {@code path/subpath/file.ext}.
    *
    * @return document id path criterion
    */
   @Getter
   public Criterion<Path> documentPath() {
      return documentPath;
   }

   /**
    * A criterion matching the document id path depth (depth >= 0). e.g.:
    * <ul>
    *    <li>{@code file.ext} : 0</li>
    *    <li>{@code path/file.ext} : 1</li>
    *    <li>{@code path/subpath/file.ext} : 2</li>
    * </ul>
    *
    * @return document id path depth criterion
    */
   @Getter
   public Criterion<Integer> pathDepth() {
      return documentPathDepth;
   }

   /**
    * A criterion matching the document id path, without document name (last path element), that can be
    * separately matched using {@link #documentName()}. e.g. : {@code path/subpath/file.ext} will be matched
    * against
    * {@code path/subpath}.
    *
    * @return document id parent path criterion
    */
   @Getter
   public Criterion<Path> documentParentPath() {
      return documentParentPath;
   }

   /**
    * A criterion matching the document name (last path element). e.g. : {@code path/subpath/file.ext} will
    * be matched against {@code file.ext}.
    *
    * @return document name criterion
    */
   @Getter
   public Criterion<String> documentName() {
      return documentName;
   }

   /**
    * A criterion matching the document id last path element extension (without '.'). e.g. :
    * {@code path/subpath/file.ext} will be matched against {@code ext}.
    *
    * @return document name extension criterion
    */
   @Getter
   public Criterion<String> documentExtension() {
      return documentExtension;
   }

   /**
    * A criterion matching the document content-type. e.g. :
    * {@code path/subpath/file.html} will be matched against {@code text/html}.
    * Mime-type is matched with type and subtype only (e.g.: not charset parameter).
    *
    * @return document content-type criterion
    */
   @Getter
   public Criterion<MimeType> contentType() {
      return contentType;
   }

   /**
    * A criterion matching the document content size.
    * If document content size is not known, criterion won't match.
    *
    * @return document content size criterion
    */
   @Getter
   public Criterion<Long> contentSize() {
      return contentSize;
   }

   /**
    * A criterion matching the document creation date.
    * If document creation date is not known, criterion won't match.
    *
    * @return document creation date criterion
    */
   @Getter
   public Criterion<Instant> creationDate() {
      return creationDate;
   }

   /**
    * A criterion matching the document last update date.
    * If document last update date is not known, criterion won't match.
    *
    * @return document last update date criterion
    */
   @Getter
   public Criterion<Instant> lastUpdateDate() {
      return lastUpdateDate;
   }

   /**
    * Normalize path for implementation usage.
    *
    * @param path path normalize
    *
    * @return normalized path
    */
   protected Path normalizePath(Path path) {
      return nullable(path, Path::normalize);
   }

   protected static InvariantRule<Path> isValidPath() {
      return isNotNull().andValue(isNotAbsolute()).andValue(hasNoTraversal());
   }

   protected Path documentPath(DocumentEntry entry) {
      return entry.documentId().value();
   }

   protected int documentPathDepth(DocumentEntry entry) {
      return entry.documentId().value().getNameCount() - 1;
   }

   protected Path documentParentPath(DocumentEntry entry) {
      return nullable(entry.documentId().value().getParent(), Path::normalize, Paths.get(""));
   }

   protected String documentExtension(DocumentEntry entry) {
      return FilenameUtils.getExtension(entry.documentId().value().getFileName().toString());
   }

   protected MimeType contentType(DocumentEntry entry) {
      return entry.metadata().simpleContentType();
   }

   private Optional<Long> contentSize(DocumentEntry entry) {
      return entry.metadata().contentSize();
   }

   private Optional<Instant> creationDate(DocumentEntry entry) {
      return entry.metadata().creationDate();
   }

   private Optional<Instant> lastUpdateDate(DocumentEntry entry) {
      return entry.metadata().lastUpdateDate();
   }

   protected String documentName(DocumentEntry entry) {
      return entry.documentId().value().getFileName().toString();
   }

   public static DocumentEntryCriteriaBuilder reconstituteBuilder() {
      return new DocumentEntryCriteriaBuilder().reconstitute();
   }

   public static class DocumentEntryCriteriaBuilder extends DomainBuilder<DocumentEntryCriteria> {

      private Criterion<Path> documentPath;
      private Criterion<Integer> documentPathDepth;
      private Criterion<Path> documentParentPath;
      private Criterion<String> documentName;
      private Criterion<String> documentExtension;
      private Criterion<MimeType> contentType;
      private Criterion<Long> contentSize;
      private Criterion<Instant> creationDate;
      private Criterion<Instant> lastUpdateDate;

      /**
       * A pre-set specification to return all documents from a repository.
       *
       * @return document entry specification to match all documents
       */
      public static DocumentEntryCriteria allDocuments() {
         return new DocumentEntryCriteriaBuilder().build();
      }

      public static DocumentEntryCriteria ofDocumentPaths(List<Path> paths) {
         return new DocumentEntryCriteriaBuilder().documentPath(CriterionBuilder.in(list(paths))).build();
      }

      public static DocumentEntryCriteria ofDocumentPaths(Path... paths) {
         return ofDocumentPaths(list(paths));
      }

      public static DocumentEntryCriteria ofDocumentExtensions(List<String> extensions) {
         return new DocumentEntryCriteriaBuilder()
               .documentExtension(CriterionBuilder.in(list(extensions)))
               .build();
      }

      public static DocumentEntryCriteria ofDocumentExtensions(String... extensions) {
         return ofDocumentExtensions(list(extensions));
      }

      public static DocumentEntryCriteria ofContentTypes(List<MimeType> contentTypes) {
         return new DocumentEntryCriteriaBuilder()
               .contentType(CriterionBuilder.in(list(contentTypes)))
               .build();
      }

      public static DocumentEntryCriteria ofContentTypes(MimeType... contentTypes) {
         return ofContentTypes(list(contentTypes));
      }

      public static DocumentEntryCriteria ofDocumentIds(List<DocumentPath> documentIds) {
         return ofDocumentPaths(list(stream(documentIds).map(DocumentPath::value)));
      }

      public static DocumentEntryCriteria ofDocumentIds(DocumentPath... documentIds) {
         return ofDocumentIds(list(documentIds));
      }

      @Setter
      public DocumentEntryCriteriaBuilder documentPath(Criterion<Path> documentPath) {
         this.documentPath = documentPath;
         return this;
      }

      @Setter
      public DocumentEntryCriteriaBuilder documentPathDepth(Criterion<Integer> documentPathDepth) {
         this.documentPathDepth = documentPathDepth;
         return this;
      }

      @Setter
      public DocumentEntryCriteriaBuilder documentParentPath(Criterion<Path> documentParentPath) {
         this.documentParentPath = documentParentPath;
         return this;
      }

      @Setter
      public DocumentEntryCriteriaBuilder documentName(Criterion<String> documentName) {
         this.documentName = documentName;
         return this;
      }

      @Setter
      public DocumentEntryCriteriaBuilder documentExtension(Criterion<String> documentExtension) {
         this.documentExtension = documentExtension;
         return this;
      }

      @Setter
      public DocumentEntryCriteriaBuilder contentType(Criterion<MimeType> contentType) {
         this.contentType = contentType;
         return this;
      }

      @Setter
      public DocumentEntryCriteriaBuilder contentSize(Criterion<Long> contentSize) {
         this.contentSize = contentSize;
         return this;
      }

      @Setter
      public DocumentEntryCriteriaBuilder creationDate(Criterion<Instant> creationDate) {
         this.creationDate = creationDate;
         return this;
      }

      @Setter
      public DocumentEntryCriteriaBuilder lastUpdateDate(Criterion<Instant> lastUpdateDate) {
         this.lastUpdateDate = lastUpdateDate;
         return this;
      }

      @Override
      public DocumentEntryCriteria buildDomainObject() {
         return new DocumentEntryCriteria(this);
      }
   }

}
