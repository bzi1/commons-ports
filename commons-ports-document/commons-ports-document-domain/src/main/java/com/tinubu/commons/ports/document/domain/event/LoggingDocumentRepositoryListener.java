/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.event;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ports.document.domain.DocumentRepository;

/**
 * Logging listener for {@link DocumentRepository} events.
 * Change {@link LoggingDocumentRepositoryListener} log level to 'info' to see these logs.
 */
public class LoggingDocumentRepositoryListener<E extends DocumentRepositoryEvent>
      implements DomainEventListener<E> {

   private static final Logger log = LoggerFactory.getLogger(LoggingDocumentRepositoryListener.class);

   @Override
   public void accept(E event) {
      if (event instanceof DocumentSaved) {
         log((DocumentSaved) event);
      } else if (event instanceof DocumentDeleted) {
         log((DocumentDeleted) event);
      } else if (event instanceof DocumentAccessed) {
         log((DocumentAccessed) event);
      }
   }

   public void log(DocumentSaved event) {
      log.info("Document '{}' -> '{}' saved in {} ms",
               event.documentId().stringValue(),
               event.documentUri().map(URI::toString).orElse("<unset>"),
               event.duration().toMillis());
   }

   public void log(DocumentDeleted event) {
      log.info("Document '{}' -> '{}' deleted in {} ms",
               event.documentId().stringValue(),
               event.documentUri().map(URI::toString).orElse("<unset>"),
               event.duration().toMillis());
   }

   public void log(DocumentAccessed event) {
      log.info("Document '{}' -> '{}' accessed in {} ms",
               event.documentId().stringValue(),
               event.documentUri().map(URI::toString).orElse("<unset>"),
               event.duration().toMillis());
   }

}
