package com.tinubu.commons.ports.document.domain.processor;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;

/**
 * General document processor.
 *
 * @see Document#process(DocumentProcessor)
 */
@FunctionalInterface
public interface DocumentProcessor {

   /**
    * Processes specified document.
    *
    * @param document document to process
    *
    * @return new processed document instance
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   Document process(Document document);

   default DocumentProcessor compose(DocumentProcessor before) {
      notNull(before, "before");
      return (Document document) -> process(before.process(document));
   }

   default DocumentProcessor andThen(DocumentProcessor after) {
      notNull(after, "after");
      return (Document document) -> after.process(process(document));
   }

}
