/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.or;
import static com.tinubu.commons.lang.util.OptionalUtils.peek;

import java.net.URI;
import java.time.Duration;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.apache.commons.lang3.time.StopWatch;

import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ddd2.domain.event.SynchronousDomainEventService;
import com.tinubu.commons.ports.document.domain.event.DocumentAccessed;
import com.tinubu.commons.ports.document.domain.event.DocumentDeleted;
import com.tinubu.commons.ports.document.domain.event.DocumentRepositoryEvent;
import com.tinubu.commons.ports.document.domain.event.DocumentSaved;

public abstract class AbstractDocumentRepository implements DocumentRepository {

   protected final SynchronousDomainEventService eventService;
   protected final StopWatch watch = new StopWatch();

   protected AbstractDocumentRepository() {
      this.eventService = new SynchronousDomainEventService();

      registerWriteEventsLoggingListener();
   }

   protected StopWatch startWatch() {
      this.watch.reset();
      this.watch.start();

      return this.watch;
   }

   @Override
   public <E extends DocumentRepositoryEvent> void registerEventListener(Class<E> eventClass,
                                                                         DomainEventListener<? super E> eventListener) {
      this.eventService.registerEventListener(eventClass, eventListener);
   }

   @Override
   public void unregisterEventListeners() {
      eventService.unregisterEventListeners();
   }

   protected <T extends Document> Optional<T> handleDocumentEvent(Supplier<Optional<T>> result,
                                                                  Function<T, DocumentRepositoryEvent> successfulEvent) {
      return peek(result.get(), entry -> publishEvent(successfulEvent.apply(entry)));
   }

   protected <T extends Document> Optional<T> handleDocumentEvent(Supplier<Optional<T>> result,
                                                                  Function<T, DocumentRepositoryEvent> successfulEvent,
                                                                  Supplier<DocumentRepositoryEvent> failureEvent) {
      return or(handleDocumentEvent(result, successfulEvent), () -> {
         publishEvent(failureEvent.get());
         return optional();
      });
   }

   protected <T extends Document> Stream<T> handleDocumentStreamEvent(Supplier<Stream<T>> result,
                                                                      Function<T, DocumentRepositoryEvent> successfulEvent) {
      return result.get().peek(document -> publishEvent(successfulEvent.apply(document)));
   }

   protected <T extends DocumentEntry> Optional<T> handleDocumentEntryEvent(Supplier<Optional<T>> result,
                                                                            Function<T, DocumentRepositoryEvent> successfulEvent) {
      return peek(result.get(), entry -> publishEvent(successfulEvent.apply(entry)));
   }

   protected <T extends DocumentEntry> Optional<T> handleDocumentEntryEvent(Supplier<Optional<T>> result,
                                                                            Function<T, DocumentRepositoryEvent> successfulEvent,
                                                                            Supplier<DocumentRepositoryEvent> failureEvent) {
      return or(handleDocumentEntryEvent(result, successfulEvent), () -> {
         publishEvent(failureEvent.get());
         return optional();
      });
   }

   protected <T extends DocumentEntry> Stream<T> handleDocumentEntryStreamEvent(Supplier<Stream<T>> result,
                                                                                Function<T, DocumentRepositoryEvent> successfulEvent) {
      return result.get().peek(document -> publishEvent(successfulEvent.apply(document)));
   }

   protected void publishEvent(DocumentRepositoryEvent event) {
      eventService.publishEvent(event);
   }

   protected DocumentSaved documentSaved(DocumentEntry document) {
      return new DocumentSaved(this,
                               document,
                               documentUri(document.documentId()).orElse(null),
                               duration(watch));
   }

   protected DocumentDeleted documentDeleted(DocumentEntry document) {
      return new DocumentDeleted(this,
                                 document,
                                 documentUri(document.documentId()).orElse(null),
                                 duration(watch));
   }

   protected DocumentAccessed documentAccessed(DocumentEntry document) {
      return new DocumentAccessed(this,
                                  document,
                                  documentUri(document.documentId()).orElse(null),
                                  duration(watch));
   }

   private Optional<URI> documentUri(DocumentPath documentId) {
      try {
         return optional(this.toUri(documentId));
      } catch (UnsupportedUriException e) {
         return optional();
      }
   }

   /**
    * Helper to extract a {@link StopWatch} current duration.
    *
    * @param stopWatch stop watch
    *
    * @return stop watch current duration
    */
   protected static Duration duration(StopWatch stopWatch) {
      return Duration.ofNanos(stopWatch.getNanoTime());
   }

}
