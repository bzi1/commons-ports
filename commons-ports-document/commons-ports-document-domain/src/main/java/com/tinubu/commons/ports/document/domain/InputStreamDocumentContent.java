/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isPositive;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Optional;
import java.util.function.Consumer;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.ReaderInputStream;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.valueformatter.HiddenValueFormatter;
import com.tinubu.commons.lang.beans.Getter;

/**
 * Document content stream implementation for read access.
 * This content is represented with an {@link InputStream}.
 */
public class InputStreamDocumentContent extends AbstractValue implements DocumentContent {

   /** Document content as {@link InputStream}. Content can be read only one time. */
   protected final InputStream content;
   /** Optional document content encoding. */
   protected final Charset contentEncoding;
   /** Optional content size indication when known. */
   protected final Long contentSize;

   protected InputStreamDocumentContent(InputStreamDocumentContentBuilder builder) {
      this.content = nullable(builder.content,
                              content -> new AutoFinalizingInputStream(content,
                                                                       nullable(builder.finalize, __ -> {})));
      this.contentEncoding = builder.contentEncoding;
      this.contentSize = builder.contentSize;
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends InputStreamDocumentContent> defineDomainFields() {
      return Fields
            .<InputStreamDocumentContent>builder()
            .superFields((Fields<InputStreamDocumentContent>) super.defineDomainFields())
            .technicalField("content",
                            v -> v.content,
                            new HiddenValueFormatter().compose(v -> v.content),
                            isNotNull())
            .field("contentEncoding", v -> v.contentEncoding)
            .field("contentSize", v -> v.contentSize, isNull().orValue(isPositive()))
            .build();
   }

   @Override
   public String stringContent(Charset defaultContentEncoding) {
      notNull(defaultContentEncoding, "defaultContentEncoding");

      try {
         return IOUtils.toString(content, contentEncoding().orElse(defaultContentEncoding));
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   @Override
   public String stringContent() {
      return stringContent(contentEncoding().orElseThrow(() -> new IllegalStateException(
            "No content encoding set, you should specify one to encode this content")));
   }

   @Override
   public InputStream inputStreamContent() {
      return content;
   }

   @Override
   public OutputStream outputStreamContent() {
      throw new IllegalStateException("Unsupported operation for this content implementation");
   }

   @Override
   public InputStreamReader readerContent(Charset defaultContentEncoding) {
      notNull(defaultContentEncoding, "defaultContentEncoding");

      return new InputStreamReader(content, contentEncoding().orElse(defaultContentEncoding));
   }

   @Override
   public Writer writerContent() {
      throw new IllegalStateException("Unsupported operation for this content implementation");
   }

   @Override
   public InputStreamReader readerContent() {
      return readerContent(contentEncoding().orElseThrow(() -> new IllegalStateException(
            "No content encoding set, you should specify one to encode this content")));
   }

   @Override
   public Optional<Long> contentSize() {
      return nullable(contentSize);
   }

   @Override
   @Getter
   public byte[] content() {
      try {
         return IOUtils.toByteArray(content);
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   @Override
   @Getter
   public Optional<Charset> contentEncoding() {
      return nullable(contentEncoding);
   }

   public static InputStreamDocumentContentBuilder reconstituteBuilder() {
      return new InputStreamDocumentContentBuilder().reconstitute();
   }

   public static class InputStreamDocumentContentBuilder extends DomainBuilder<InputStreamDocumentContent> {
      private InputStream content;
      private Charset contentEncoding;
      private Long contentSize;
      private Consumer<InputStream> finalize;

      public InputStreamDocumentContentBuilder content(InputStream content,
                                                       Charset contentEncoding,
                                                       Long contentSize) {
         this.content = content;
         this.contentEncoding = contentEncoding;
         this.contentSize = contentSize;
         return this;
      }

      public InputStreamDocumentContentBuilder content(InputStream content, Charset contentEncoding) {
         return content(content, contentEncoding, null);
      }

      public InputStreamDocumentContentBuilder content(InputStream content, Long contentSize) {
         return content(content, null, contentSize);
      }

      public InputStreamDocumentContentBuilder content(InputStream content) {
         return content(content, null, null);
      }

      public InputStreamDocumentContentBuilder content(byte[] content, Charset contentEncoding) {
         return content(new ByteArrayInputStream(notNull(content, "content")),
                        contentEncoding,
                        (long) content.length);
      }

      public InputStreamDocumentContentBuilder content(byte[] content) {
         return content(content, null);
      }

      public InputStreamDocumentContentBuilder content(String content, Charset contentEncoding) {
         return content(notNull(content, "content").getBytes(notNull(contentEncoding, "contentEncoding")),
                        contentEncoding);
      }

      public InputStreamDocumentContentBuilder content(Reader content, Charset contentEncoding) {
         return content(new ReaderInputStream(notNull(content, "content"),
                                              notNull(contentEncoding, "contentEncoding")), contentEncoding);
      }

      public InputStreamDocumentContentBuilder finalize(Consumer<InputStream> finalize) {
         this.finalize = finalize;
         return this;
      }

      @Override
      public InputStreamDocumentContent buildDomainObject() {
         return new InputStreamDocumentContent(this);
      }

   }

   /**
    * {@link InputStream} that automatically calls an arbitrary finalize operation at close.
    */
   private static class AutoFinalizingInputStream extends InputStream {

      private final InputStream inputStream;
      private final Consumer<InputStream> finalize;

      public AutoFinalizingInputStream(InputStream inputStream, Consumer<InputStream> finalize) {
         this.inputStream = notNull(inputStream, "inputStream");
         this.finalize = notNull(finalize, "finalize");
      }

      @Override
      public int read() throws IOException {
         return inputStream.read();
      }

      @Override
      public int read(byte[] b) throws IOException {
         return inputStream.read(b);
      }

      @Override
      public int read(byte[] b, int off, int len) throws IOException {
         return inputStream.read(b, off, len);
      }

      @Override
      public long skip(long n) throws IOException {
         return inputStream.skip(n);
      }

      @Override
      public int available() throws IOException {
         return inputStream.available();
      }

      @Override
      public synchronized void mark(int readlimit) {
         inputStream.mark(readlimit);
      }

      @Override
      public synchronized void reset() throws IOException {
         inputStream.reset();
      }

      @Override
      public boolean markSupported() {
         return inputStream.markSupported();
      }

      @Override
      public void close() throws IOException {
         try {
            finalize.accept(inputStream);
         } finally {
            inputStream.close();
         }
      }
   }

}

