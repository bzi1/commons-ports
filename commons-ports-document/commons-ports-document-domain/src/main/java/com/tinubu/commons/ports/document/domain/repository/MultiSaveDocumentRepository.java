/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoNullElements;
import static com.tinubu.commons.lang.util.CollectionUtils.collectionEquals;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.net.URI;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.lang.util.CollectionUtils;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;

/**
 * @implNote Immutable class implementation.
 */
public class MultiSaveDocumentRepository extends AbstractDocumentRepository {

   protected final DocumentRepository primaryRepository;
   protected final List<DocumentRepository> secondaryRepositories;

   protected MultiSaveDocumentRepository(DocumentRepository primaryRepository,
                                         List<DocumentRepository> secondaryRepositories) {
      this.primaryRepository = validate(primaryRepository, "primaryRepository", isNotNull()).orThrow();
      this.secondaryRepositories =
            validate(secondaryRepositories, "secondaryRepositories", hasNoNullElements()).orThrow();
   }

   /**
    * Creates an empty union repository.
    *
    * @return empty union repository
    */
   /*public static MultiSaveDocumentRepository ofEmpty() {
      return new MultiSaveDocumentRepository(list());
   }*/

   /**
    * Creates a union repository from layers.
    *
    * @param layers layers ordered from the lower to the upper layers. Lower layers have
    *       lesser priority.
    *
    * @return union repository with specified layers
    */
   public static MultiSaveDocumentRepository ofRepositories(DocumentRepository primaryRepository,
                                                            List<DocumentRepository> secondaryRepositories) {
      return new MultiSaveDocumentRepository(primaryRepository, secondaryRepositories);
   }

   public static MultiSaveDocumentRepository ofRepositories(DocumentRepository primaryRepository,
                                                            DocumentRepository... secondaryRepositories) {
      return new MultiSaveDocumentRepository(primaryRepository,
                                             nullable(secondaryRepositories)
                                                   .map(CollectionUtils::list)
                                                   .orElse(null));
   }

   public DocumentRepository primaryRepository() {
      return primaryRepository;
   }

   public List<DocumentRepository> secondaryRepositories() {
      return secondaryRepositories;
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      return documentRepository instanceof MultiSaveDocumentRepository
             && Objects.equals(((MultiSaveDocumentRepository) documentRepository).primaryRepository,
                               primaryRepository)
             && collectionEquals(((MultiSaveDocumentRepository) documentRepository).secondaryRepositories,
                                 secondaryRepositories,
                                 Repository::sameRepositoryAs);
   }

   @Override
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      throw new UnsupportedOperationException("This repository can't stream to multiple writers");
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      notNull(documentId, "documentId");

      return primaryRepository.findDocumentById(documentId);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      notNull(documentId, "documentId");

      return primaryRepository.findDocumentEntryById(documentId);
   }

   /**
    * {@inheritDoc}
    * Search documents on the all layers. If documents with the same identifier are found on multiple layers,
    * only the upper layer document is returned.
    *
    * @implNote We use a {@code map(..orElse(null)).filter(Objects::nonNull} scheme instead of a simpler
    *       Stream::flatMap as an optimization.
    */
   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      return primaryRepository.findDocumentsBySpecification(basePath, specification);
   }

   /**
    * {@inheritDoc}
    * Search documents on the all layers. If documents with the same identifier are found on multiple layers,
    * only the upper layer document is returned.
    */
   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      return primaryRepository.findDocumentEntriesBySpecification(basePath, specification);
   }

   /**
    * {@inheritDoc}
    * Save document on the upper layer only, if there is at least one layer, otherwise returns
    * {@link Optional#empty}.
    */
   @Override
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      notNull(document, "document");

      // FIXME async secondary repositories save + rollback/compensation logic ?
      return primaryRepository.saveDocument(document, overwrite);
   }

   /**
    * {@inheritDoc}
    * Delete document on the upper layer only, if there is at least one layer, otherwise returns
    * {@link Optional#empty}.
    */
   @Override
   public Optional<DocumentEntry> deleteDocument(DocumentPath documentId) {
      notNull(documentId, "documentId");

      // FIXME async secondary repositories delete + rollback/compensation logic ? -> pb
      return primaryRepository.deleteDocument(documentId);
   }

   @Override
   public URI toUri(DocumentPath documentId) {
      notNull(documentId, "documentId");

      throw new UnsupportedOperationException("FIXME");
   }

   @Override
   public boolean supportsUri(URI documentUri) {
      notNull(documentUri, "documentUri");

      throw new UnsupportedOperationException("FIXME");
   }

   @Override
   public Optional<Document> findDocumentByUri(URI documentUri) {
      notNull(documentUri, "documentUri");

      throw new UnsupportedOperationException("FIXME");
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
      notNull(documentUri, "documentUri");

      throw new UnsupportedOperationException("FIXME");
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", MultiSaveDocumentRepository.class.getSimpleName() + "[", "]")
            .add("primaryRepository=" + primaryRepository)
            .add("secondaryRepositories=" + secondaryRepositories)
            .toString();
   }
}
