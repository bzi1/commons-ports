/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.lazyValue;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isNotEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.isEmpty;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.optionalValue;
import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.mimeType;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.function.UnaryOperator;

import com.tinubu.commons.ddd2.domain.type.AbstractEntity;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.lang.mimetype.registry.MimeTypeRegistry;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent.LoadedDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.processor.DocumentContentProcessor;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;

/**
 * Generic document representation entity with metadata and content.
 * <p>
 * By design, document identifier is always relative to ease document portability on multiple systems. You
 * have to manage the document root logic on client side, depending on your needs. For example, {@link
 * DocumentRepository document repositories} implementations manage their own root logic to persist documents.
 * <p>
 * Document creation date and last update date are managed by document logic, but they can be overridden, or
 * deleted by {@link DocumentRepository} specific implementations on save/reconstitution.
 */
public class Document extends AbstractEntity<DocumentPath> {

   protected final DocumentPath documentId;
   protected final DocumentMetadata metadata;
   protected final DocumentContent content;

   protected Document(DocumentBuilder builder) {
      this.documentId = builder.documentId;
      this.content = builder.content;
      this.metadata = resolveMetadata(builder);
   }

   @Override
   public Fields<? extends Document> defineDomainFields() {
      return Fields
            .<Document>builder()
            .field("documentId", v -> v.documentId, isNotNull())
            .field("metadata",
                   v -> v.metadata,
                   isNotNull().andValue(isValidContentSize()).andValue(isValidContentTypeEncoding()))
            .field("content", v -> v.content, isNotNull())
            .build();
   }

   @Override
   public DocumentPath id() {
      return documentId();
   }

   @Getter
   public DocumentPath documentId() {
      return documentId;
   }

   @Getter
   public DocumentMetadata metadata() {
      return metadata;
   }

   @Getter
   public DocumentContent content() {
      return content;
   }

   /**
    * Constructs a {@link DocumentEntry} from this document.
    *
    * @return a {@link DocumentEntry} of this document
    */
   public DocumentEntry documentEntry() {
      return new DocumentEntryBuilder().documentId(documentId).metadata(metadata).build();
   }

   /**
    * Returns a "loaded content" version of this document. All document metadata are preserved excepting
    * content size if it was previously unset.
    * <p>
    * This operation is necessary if you plan to read the document content multiple times.
    * <p>
    * This operation does nothing if the document content is already loaded.
    *
    * @throws DocumentAccessException if an I/O error occurs while reading content
    * @implSpec Original content stream must be closed after being loaded.
    */
   public Document loadContent() {
      if (content instanceof LoadedDocumentContent) {
         return this;
      } else {
         try (InputStream streamContent = this.content.inputStreamContent()) {
            LoadedDocumentContent loadedContent = new LoadedDocumentContentBuilder()
                  .content(streamContent, this.content.contentEncoding().orElse(null))
                  .build();

            return DocumentBuilder
                  .from(this)
                  .content(loadedContent)
                  .metadata(DocumentMetadataBuilder
                                  .from(metadata)
                                  .contentSize(loadedContent.contentSize().orElse(null))
                                  .build())
                  .build();
         } catch (IOException e) {
            throw new DocumentAccessException(e);
         }
      }
   }

   /**
    * Updates document metadata document name.
    *
    * @param documentName document metadata path
    *
    * @return new document
    */
   public Document documentName(Path documentName) {
      return metadata(builder -> builder.documentPath(documentName));
   }

   /**
    * Updates document metadata content type. Updating content encoding using charset parameter is not
    * allowed here, it can be present, but it must match actual content encoding.
    *
    * @param contentType document metadata content type
    *
    * @return new document
    */
   public Document contentType(MimeType contentType) {
      MimeType newContentType = contentType.charset().isPresent()
                                ? contentType
                                : content
                                      .contentEncoding()
                                      .map(ce -> mimeType(contentType, ce))
                                      .orElse(contentType);

      return metadata(builder -> builder.contentType(newContentType));
   }

   /**
    * Updates document metadata attributes.
    *
    * @param attributes document metadata attributes
    *
    * @return new document
    */
   public Document attributes(Map<String, Object> attributes) {
      return metadata(builder -> builder.attributes(attributes));
   }

   /**
    * Updates document metadata attributes.
    *
    * @param attributes document metadata attributes
    *
    * @return new document
    */
   public Document addAttributes(Map<String, Object> attributes) {
      Map<String, Object> metadataAttributes = new HashMap<>(metadata.attributes());
      metadataAttributes.putAll(attributes);

      return attributes(metadataAttributes);
   }

   /**
    * Updates document metadata attributes.
    *
    * @param key document metadata attribute key
    * @param value document metadata attribute value
    *
    * @return new document
    */
   public Document addAttribute(String key, Object value) {
      Map<String, Object> metadataAttributes = new HashMap<>(metadata.attributes());
      metadataAttributes.put(key, value);

      return attributes(metadataAttributes);
   }

   /**
    * Updates document content, but not its identifier, name, content type. Metadata will be corrected
    * accordingly, including new content encoding and content size if any.
    *
    * @param content document content to set for this document
    *
    * @return new document
    */
   public Document content(DocumentContent content) {
      return DocumentBuilder
            .from(this)
            .content(content)
            .metadata(DocumentMetadataBuilder
                            .from(metadata)
                            .chain((DocumentMetadataBuilder documentMetadataBuilder) -> documentMetadataBuilder.lastUpdateDate(
                                  ApplicationClock.nowAsInstant()))
                            .contentSize(content.contentSize().orElse(null))
                            .contentType(content
                                               .contentEncoding()
                                               .map(ce -> mimeType(metadata.simpleContentType(), ce))
                                               .orElse(metadata.simpleContentType()))
                            .build())
            .build();
   }

   /**
    * Updates document content, but not its identifier, name, content type. Metadata will be corrected
    * accordingly, including new content encoding and content size if any.
    *
    * @param contentProcessor document content processor to apply to document content
    *
    * @return new document
    */
   public Document processContent(DocumentContentProcessor contentProcessor) {
      return content(contentProcessor.process(content()));
   }

   /**
    * Returns a new instance of this document processed by the specified processor.
    *
    * @param processor document processor to apply to this document
    *
    * @return new document
    */
   public Document process(DocumentProcessor processor) {
      return processor.process(this);
   }

   /**
    * Duplicates this document and identifies new document with specified identifier.
    * <p>
    * Metadata document name is not updated in the process so both documents will have the same name, but not
    * the same document id. Metadata creation date and last update date are set to {@code now}.
    *
    * @param newDocumentId new document identifier
    *
    * @return new duplicated document with new identifier
    */
   public Document duplicate(DocumentPath newDocumentId) {
      validate(newDocumentId, "newDocumentId", isNotEqualTo(value(documentId, "documentId"))).orThrow();

      return new DocumentBuilder()
            .documentId(newDocumentId)
            .content(content)
            .documentName(metadata.documentPath())
            .contentType(metadata.contentType())
            .attributes(metadata.attributes())
            .build();
   }

   /**
    * Derives {@link DocumentMetadata} from builder data.
    *
    * @param builder document builder
    *
    * @return document metadata
    *
    * @apiNote Document metadata document name use document identifier path by default.
    * @implNote If builder is in reconstitute mode, values are just passed through.
    */
   protected static DocumentMetadata resolveMetadata(DocumentBuilder builder) {
      if (builder.isReconstitute()) {
         return builder.metadata;
      } else {
         try {
            return new DocumentMetadataBuilder()
                  .mimeTypeRegistry(builder.mimeTypeRegistry)
                  .documentPath(nullable(builder.documentPath,
                                         nullable(builder.documentId).map(DocumentPath::value).orElse(null)))
                  .contentType(nullable(builder.contentType).orElse(null),
                               nullable(builder.content)
                                     .flatMap(DocumentContent::contentEncoding)
                                     .orElse(null))
                  .attributes(builder.attributes)
                  .contentSize(nullable(builder.content).flatMap(DocumentContent::contentSize).orElse(null))
                  .build();
         } catch (InvariantValidationException e) {
            if (builder.documentId != null) {
               throw e.addContext(builder.documentId);
            } else {
               throw e;
            }
         }
      }
   }

   /**
    * Internally updates metadata. Metadata last update date is always updated to current date.
    *
    * @param metadataOperator document metadata operator to use to update metadata.
    *
    * @return new document
    */
   protected Document metadata(UnaryOperator<DocumentMetadataBuilder> metadataOperator) {
      return DocumentBuilder
            .from(this)
            .metadata(DocumentMetadataBuilder
                            .from(metadata)
                            .chain(metadataOperator)
                            .chain((DocumentMetadataBuilder documentMetadataBuilder) -> documentMetadataBuilder.lastUpdateDate(
                                  ApplicationClock.nowAsInstant()))
                            .build())
            .build();
   }

   /**
    * Metadata builder chain operation to update last update date.
    *
    * @param documentMetadataBuilder metadata builder
    *
    * @return metadata builder
    */
   protected DocumentMetadataBuilder updateLastUpdateDate(DocumentMetadataBuilder documentMetadataBuilder) {
      return documentMetadataBuilder.lastUpdateDate(ApplicationClock.nowAsInstant());
   }

   /**
    * Checks if metadata content size correctly matches effective document content size, if effective document
    * content size is available.
    */
   protected InvariantRule<DocumentMetadata> isValidContentSize() {
      ParameterValue<Long> effectiveContentSize =
            lazyValue(() -> content.contentSize().orElse(null), "content.contentSize");

      return property(DocumentMetadata::contentSize,
                      "contentSize",
                      isEmpty().orValue(optionalValue(isEqualTo(effectiveContentSize).ifIsSatisfied(
                            effectiveContentSize::nonNull))));
   }

   /**
    * Checks if metadata content type charset correctly matches effective document content encoding.
    */
   protected InvariantRule<DocumentMetadata> isValidContentTypeEncoding() {
      ParameterValue<Charset> contentEncoding =
            lazyValue(() -> content.contentEncoding().orElse(null), "content.contentEncoding");

      return property(documentMetadata -> documentMetadata.contentType().charset(),
                      "contentType.charset",
                      isEmpty().orValue(optionalValue(isEqualTo(contentEncoding).ifIsSatisfied(contentEncoding::nonNull))));
   }

   public static DocumentBuilder reconstituteBuilder() {
      return new DocumentBuilder().reconstitute();
   }

   public static class DocumentBuilder extends DomainBuilder<Document> {
      private DocumentPath documentId;
      private Path documentPath;
      private MimeType contentType;
      private Map<String, Object> attributes;
      private DocumentMetadata metadata;
      private DocumentContent content;
      private MimeTypeRegistry mimeTypeRegistry;

      /**
       * Copy constructor for {@link Document}
       *
       * @param document document to copy
       *
       * @return document builder
       */
      public static DocumentBuilder from(Document document) {
         notNull(document, "document");

         return new DocumentBuilder()
               .<DocumentBuilder>reconstitute()
               .documentId(document.documentId)
               .metadata(document.metadata)
               .content(document.content);
      }

      public DocumentBuilder mimeTypeRegistry(MimeTypeRegistry mimeTypeRegistry) {
         ensureNotReconstitute();
         this.mimeTypeRegistry = mimeTypeRegistry;
         return this;
      }

      @Setter
      public DocumentBuilder documentId(DocumentPath documentId) {
         this.documentId = documentId;
         return this;
      }

      public DocumentBuilder documentPath(Path documentPath) {
         ensureNotReconstitute();
         this.documentPath = documentPath;
         return this;
      }

      public DocumentBuilder documentPath(String documentPath, String... moreDocumentPath) {
         return documentPath(Paths.get(documentPath, moreDocumentPath));
      }

      /**
       * @param documentName document path
       *
       * @return this builder
       *
       * @deprecated see {@link #documentPath(Path)} instead
       */
      @Deprecated
      public DocumentBuilder documentName(Path documentName) {
         return documentPath(documentName);
      }

      /**
       * @param documentName document path
       *
       * @return this builder
       *
       * @deprecated see {@link #documentPath(String, String[])} instead
       */
      @Deprecated
      public DocumentBuilder documentName(String documentName, String... moreDocumentName) {
         return documentPath(documentName, moreDocumentName);
      }

      public DocumentBuilder contentType(MimeType contentType) {
         ensureNotReconstitute();
         this.contentType = contentType;
         return this;
      }

      public DocumentBuilder attributes(Map<String, Object> attributes) {
         ensureNotReconstitute();
         this.attributes = attributes;
         return this;
      }

      @Setter
      public DocumentBuilder metadata(DocumentMetadata metadata) {
         ensureReconstitute();
         this.metadata = metadata;
         return this;
      }

      public DocumentBuilder metadata(UnaryOperator<DocumentMetadataBuilder> metadataOperator) {
         ensureReconstitute();
         this.metadata = DocumentMetadataBuilder.from(metadata).chain(metadataOperator).build();
         return this;
      }

      public DocumentBuilder documentEntry(DocumentEntry documentEntry) {
         ensureReconstitute();
         this.documentId = documentEntry.documentId();
         this.metadata = documentEntry.metadata();
         return this;
      }

      @Setter
      public DocumentBuilder content(DocumentContent content) {
         this.content = content;
         return this;
      }

      public DocumentBuilder loadedContent(byte[] content, Charset contentEncoding) {
         return content(new LoadedDocumentContentBuilder().content(content, contentEncoding).build());
      }

      public DocumentBuilder loadedContent(byte[] content) {
         return content(new LoadedDocumentContentBuilder().content(content).build());
      }

      public DocumentBuilder loadedContent(String content, Charset contentEncoding) {
         return content(new LoadedDocumentContentBuilder().content(content, contentEncoding).build());
      }

      public DocumentBuilder loadedContent(InputStream content, Charset contentEncoding) {
         return content(new LoadedDocumentContentBuilder().content(content, contentEncoding).build());
      }

      public DocumentBuilder loadedContent(InputStream content) {
         return content(new LoadedDocumentContentBuilder().content(content).build());
      }

      public DocumentBuilder loadedContent(Reader content, Charset contentEncoding) {
         return content(new LoadedDocumentContentBuilder().content(content, contentEncoding).build());
      }

      public DocumentBuilder streamContent(byte[] content, Charset contentEncoding) {
         return content(new InputStreamDocumentContentBuilder().content(content, contentEncoding).build());
      }

      public DocumentBuilder streamContent(byte[] content) {
         return content(new InputStreamDocumentContentBuilder().content(content).build());
      }

      public DocumentBuilder streamContent(InputStream content, Charset contentEncoding, Long contentSize) {
         return content(new InputStreamDocumentContentBuilder()
                              .content(content, contentEncoding, contentSize)
                              .build());
      }

      public DocumentBuilder streamContent(InputStream content, Charset contentEncoding) {
         return content(new InputStreamDocumentContentBuilder().content(content, contentEncoding).build());
      }

      public DocumentBuilder streamContent(InputStream content, Long contentSize) {
         return content(new InputStreamDocumentContentBuilder().content(content, contentSize).build());
      }

      public DocumentBuilder streamContent(InputStream content) {
         return content(new InputStreamDocumentContentBuilder().content(content).build());
      }

      public DocumentBuilder streamContent(String content, Charset contentEncoding) {
         return content(new InputStreamDocumentContentBuilder().content(content, contentEncoding).build());
      }

      public DocumentBuilder streamContent(Reader content, Charset contentEncoding) {
         return content(new InputStreamDocumentContentBuilder().content(content, contentEncoding).build());
      }

      @Override
      public Document buildDomainObject() {
         return new Document(this);
      }

   }

}

