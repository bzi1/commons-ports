package com.tinubu.commons.ports.document.domain.processor;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentContent;

/**
 * General document content processor.
 *
 * @see Document#processContent(DocumentContentProcessor)
 */
@FunctionalInterface
public interface DocumentContentProcessor extends DocumentProcessor {

   /**
    * Processes specified document content.
    *
    * @param documentContent document content to process
    *
    * @return new processed document content instance
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   DocumentContent process(DocumentContent documentContent);

   @Override
   default Document process(Document document) {
      return document.processContent(this);
   }

   default DocumentContentProcessor compose(DocumentContentProcessor before) {
      notNull(before, "before");
      return (DocumentContent documentContent) -> process(before.process(documentContent));
   }

   default DocumentContentProcessor andThen(DocumentContentProcessor after) {
      notNull(after, "after");
      return (DocumentContent documentContent) -> after.process(process(documentContent));
   }

}
