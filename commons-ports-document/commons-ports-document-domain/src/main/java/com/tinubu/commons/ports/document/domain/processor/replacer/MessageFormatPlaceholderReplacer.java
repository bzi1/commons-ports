package com.tinubu.commons.ports.document.domain.processor.replacer;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;

import java.text.MessageFormat;
import java.util.Map;
import java.util.function.Function;

import com.tinubu.commons.lang.util.PlaceholderReplacerReader;
import com.tinubu.commons.ports.document.domain.DocumentContent;

public class MessageFormatPlaceholderReplacer extends PlaceholderReplacer {

   public static final String DEFAULT_PLACEHOLDER_BEGIN = "${";
   public static final String DEFAULT_PLACEHOLDER_END = "}";

   public MessageFormatPlaceholderReplacer(Map<String, Object> model,
                                           String placeholderBegin,
                                           String placeholderEnd) {
      super(messageFormatReplacementFunction(model), placeholderBegin, placeholderEnd);
   }

   public MessageFormatPlaceholderReplacer(Map<String, Object> model) {
      super(messageFormatReplacementFunction(model), DEFAULT_PLACEHOLDER_BEGIN, DEFAULT_PLACEHOLDER_END);
   }

   private static Function<String, Object> messageFormatReplacementFunction(Map<String, Object> model) {
      return key -> {
         int optionsIndex = key.indexOf(',');
         String modelKey = key.substring(0, optionsIndex == -1 ? key.length() : optionsIndex);
         String options = key.substring(optionsIndex == -1 ? key.length() : optionsIndex);

         return nullable(model.get(modelKey))
               .map(value -> MessageFormat.format("{0," + options + "}", value))
               .orElse(null);
      };
   }

   @Override
   public DocumentContent process(DocumentContent documentContent) {
      return new InputStreamDocumentContentBuilder()
            .content(new PlaceholderReplacerReader(documentContent.readerContent(DEFAULT_CONTENT_ENCODING),
                                                   replacementFunction,
                                                   placeholderBegin,
                                                   placeholderEnd), DEFAULT_CONTENT_ENCODING)
            .build();
   }

}

