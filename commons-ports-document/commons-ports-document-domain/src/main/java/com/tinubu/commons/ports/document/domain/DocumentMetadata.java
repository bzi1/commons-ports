/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.lazyValue;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.string;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.allSatisfies;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isInRangeInclusive;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isLessThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.entrySet;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.hasNoNullKeyValue;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.isEmpty;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.optionalValue;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.fileName;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotEmpty;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.length;
import static com.tinubu.commons.ddd2.invariant.rules.TemporalRules.isNotBefore;
import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.mimeType;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.emptyMap;

import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Map;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.valueformatter.StringValueFormatter;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.lang.mimetype.registry.MimeTypeRegistries;
import com.tinubu.commons.lang.mimetype.registry.MimeTypeRegistry;

/**
 * Document metadata attached to a {@link Document}.
 */
// FIXME *date documentation. technical repository*Date + *date ? est-ce bien utile vu que quasi sys de stockage ne les prend en compte ?
public class DocumentMetadata extends AbstractValue {

   /** Maximum document name length. This value is aligned on POSIX filesystems limits. */
   protected static final int DOCUMENT_NAME_MAX_LENGTH = 255;
   /** Hard-cap content size. */
   protected static final long CONTENT_MAX_SIZE = 2L * 1024 * 1024 * 1024 * 1024;
   /** Default mime-type registry to use for auto-resolver. */
   protected static final MimeTypeRegistry MIME_TYPE_REGISTRY = MimeTypeRegistries.ofDefaults();

   protected final Path documentPath;
   protected final MimeType contentType;
   protected final Long contentSize;
   protected final Instant creationDate;
   protected final Instant lastUpdateDate;
   protected final Map<String, Object> attributes;

   protected DocumentMetadata(DocumentMetadataBuilder builder) {
      this.documentPath = normalize(builder.documentPath);
      if (builder.isReconstitute()) {
         this.creationDate = builder.creationDate;
         this.lastUpdateDate = builder.lastUpdateDate;
      } else {
         Instant now = ApplicationClock.nowAsInstant();
         this.creationDate = now;
         this.lastUpdateDate = now;
      }
      this.attributes = immutable(map(builder.attributes));
      this.contentSize = builder.contentSize;
      this.contentType = resolveContentType(builder);
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends DocumentMetadata> defineDomainFields() {
      ParameterValue<Instant> creationDate = lazyValue(() -> this.creationDate, "creationDate");

      return Fields
            .<DocumentMetadata>builder()
            .superFields((Fields<DocumentMetadata>) super.defineDomainFields())
            .field("documentPath",
                   v -> v.documentPath,
                   new StringValueFormatter(DOCUMENT_NAME_MAX_LENGTH).compose(DocumentMetadata::documentPath),
                   isNotEmpty()
                         .andValue(isNotAbsolute())
                         .andValue(hasNoTraversal())
                         .andValue(fileName(string(isNotBlank().andValue(length(isLessThanOrEqualTo(value(
                               DOCUMENT_NAME_MAX_LENGTH,
                               "DOCUMENT_NAME_MAX_LENGTH"))))))))
            .field("contentType", v -> v.contentType, isNotNull())
            .field("contentSize",
                   v -> v.contentSize,
                   isNull().orValue(isInRangeInclusive(value(0L),
                                                       value(CONTENT_MAX_SIZE, "CONTENT_MAX_SIZE"))))
            .field("creationDate", v -> v.creationDate)
            .field("lastUpdateDate",
                   v -> v.lastUpdateDate,
                   isNull().orValue(isNotBefore(creationDate).ifIsSatisfied(creationDate::nonNull)))
            .field("attributes", v -> v.attributes, entrySet(allSatisfies(hasNoNullKeyValue())))
            .build();
   }

   /**
    * Document logical path.
    * This path is an abstraction of a hierarchical representation, not actually linked to a filesystem
    * representation.
    * Document path must be relative.
    * <p>
    * It represents a logical name for this document, not a technical name or identifier.
    * The underlying storage system must use the {@link Document#documentId()} for storage, not this name
    * that is not guaranteed to be unique.
    *
    * @return document path
    *
    * @see #documentName()
    */
   @Getter
   public Path documentPath() {
      return documentPath;
   }

   /**
    * Returns document logical name, without relative path if any.
    *
    * @return name of document
    */
   public String documentName() {
      return documentPath.getFileName().toString();
   }

   /**
    * Checks and resolves document content-type from builder parameters.
    * Resulting {@link MimeType} includes charset parameter for content encoding if possible.
    * If charset is set in both contentType and contentEncoding, the value must match.
    *
    * @param builder builder
    *
    * @return resolved content-type, never {@code null}
    */
   protected MimeType resolveContentType(DocumentMetadataBuilder builder) {
      MimeType mimeType;

      if (builder.contentType == null) {
         MimeTypeRegistry mimeTypeRegistry = nullable(builder.mimeTypeRegistry, MIME_TYPE_REGISTRY);
         mimeType = nullable(builder.documentPath)
               .map(mimeTypeRegistry::mimeTypeOrDefault)
               .orElse(mimeTypeRegistry.defaultMimeType());
      } else {
         mimeType = builder.contentType;
      }

      if (!mimeType.charset().isPresent() && builder.contentEncoding != null) {
         mimeType = mimeType(mimeType, builder.contentEncoding);
      }

      ParameterValue<Object> contentEncoding = value(builder.contentEncoding, "contentEncoding");
      validate(mimeType.charset(),
               "contentType.charset",
               isEmpty().orValue(optionalValue(isEqualTo(contentEncoding).ifIsSatisfied(contentEncoding::nonNull)))).orThrow();

      return mimeType;
   }

   /**
    * Document content-type.
    * Never {@code null}, returns {@code application/octet-stream} if content-type is not set.
    *
    * @return document content-type
    */
   @Getter
   public MimeType contentType() {
      return contentType;
   }

   /**
    * Document content-type without parameters (charset, ...), as a simple type + subtype mime-type.
    *
    * @return document mime-type without parameters (charset, ...)
    */
   public MimeType simpleContentType() {
      return mimeType(contentType, emptyMap());
   }

   /**
    * Document content encoding if set has attribute in content type.
    *
    * @return document content encoding or {@link Optional#empty}
    */
   public Optional<Charset> contentEncoding() {
      return contentType.charset();
   }

   /**
    * Content size in bytes if available
    *
    * @return content size or {@link Optional#empty} if content size is not available
    */
   @Getter
   public Optional<Long> contentSize() {
      return nullable(contentSize);
   }

   @Getter
   public Optional<Instant> creationDate() {
      return nullable(creationDate);
   }

   @Getter
   public Optional<Instant> lastUpdateDate() {
      return nullable(lastUpdateDate);
   }

   /**
    * Document extra attributes.
    *
    * @return document extra attributes
    */
   @Getter
   public Map<String, Object> attributes() {
      return attributes;
   }

   /**
    * Document extra attribute value for specified key.
    *
    * @param key attribute key
    *
    * @return attribute value or {@link Optional#empty} if attribute not set
    */
   public Optional<Object> attribute(String key) {
      return nullable(attributes.get(key));
   }

   protected static Path normalize(Path value) {
      return value == null ? null : value.normalize();
   }

   public static DocumentMetadataBuilder reconstituteBuilder() {
      return new DocumentMetadataBuilder().reconstitute();
   }

   public static class DocumentMetadataBuilder extends DomainBuilder<DocumentMetadata> {
      private Path documentPath;
      private MimeType contentType;
      private Charset contentEncoding;
      private Long contentSize;
      private Instant creationDate;
      private Instant lastUpdateDate;
      private Map<String, Object> attributes = map();
      private MimeTypeRegistry mimeTypeRegistry;

      public static DocumentMetadataBuilder from(DocumentMetadata metadata) {
         notNull(metadata, "metadata");
         return new DocumentMetadataBuilder()
               .<DocumentMetadataBuilder>reconstitute()
               .documentPath(metadata.documentPath)
               .contentType(metadata.contentType)
               .contentSize(metadata.contentSize)
               .attributes(metadata.attributes)
               .creationDate(metadata.creationDate)
               .lastUpdateDate(metadata.lastUpdateDate);
      }

      public DocumentMetadataBuilder mimeTypeRegistry(MimeTypeRegistry mimeTypeRegistry) {
         this.mimeTypeRegistry = mimeTypeRegistry;
         return this;
      }

      @Setter
      public DocumentMetadataBuilder documentPath(Path documentPath) {
         this.documentPath = documentPath;
         return this;
      }

      public DocumentMetadataBuilder documentPath(String documentPath, String... moreDocumentPath) {
         return documentPath(Paths.get(notNull(documentPath, "documentPath"), moreDocumentPath));
      }

      @Setter
      public DocumentMetadataBuilder contentType(MimeType contentType) {
         this.contentType = contentType;
         return this;
      }

      public DocumentMetadataBuilder contentType(MimeType contentType, Charset contentEncoding) {
         this.contentType = contentType;
         this.contentEncoding = contentEncoding;
         return this;
      }

      public DocumentMetadataBuilder contentType(Charset contentEncoding) {
         this.contentEncoding = contentEncoding;
         return this;
      }

      @Setter
      public DocumentMetadataBuilder contentSize(Long contentSize) {
         this.contentSize = contentSize;
         return this;
      }

      @Setter
      public DocumentMetadataBuilder creationDate(Instant creationDate) {
         ensureReconstitute();
         this.creationDate = creationDate;
         return this;
      }

      @Setter
      public DocumentMetadataBuilder lastUpdateDate(Instant lastUpdateDate) {
         ensureReconstitute();
         this.lastUpdateDate = lastUpdateDate;
         return this;
      }

      @Setter
      public DocumentMetadataBuilder attributes(Map<String, Object> metadata) {
         this.attributes = map(metadata);
         return this;
      }

      public DocumentMetadataBuilder addAttribute(String key, Object value) {
         this.attributes.put(key, value);
         return this;
      }

      @Override
      public DocumentMetadata buildDomainObject() {
         return new DocumentMetadata(this);
      }
   }

}

