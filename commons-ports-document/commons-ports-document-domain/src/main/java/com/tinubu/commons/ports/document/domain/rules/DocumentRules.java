/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.rules;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.optionalValue;

import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.Map;
import java.util.Optional;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;

/**
 * Invariant rules for {@link Document}.
 */
public final class DocumentRules {

   private DocumentRules() {}

   public static class DocumentMainRules {

      public static <T extends Document> InvariantRule<T> documentId(final InvariantRule<? super Path> rule) {
         return isNotNull().andValue(property(d -> d.documentId().value(), "documentId", rule));
      }

      public static <T extends Document> InvariantRule<T> metadata(final InvariantRule<? super DocumentMetadata> rule) {
         return isNotNull().andValue(as(Document::metadata, rule));
      }

   }

   public static class DocumentEntryRules {

      public static <T extends DocumentEntry> InvariantRule<T> metadata(final InvariantRule<? super DocumentMetadata> rule) {
         return isNotNull().andValue(as(DocumentEntry::metadata, rule));
      }

   }

   public static class DocumentMetadataRules {

      public static <T extends DocumentMetadata> InvariantRule<T> documentName(final InvariantRule<? super String> rule) {
         return isNotNull().andValue(property(DocumentMetadata::documentName, "documentName", rule));
      }

      public static <T extends DocumentMetadata> InvariantRule<T> documentPath(final InvariantRule<? super Path> rule) {
         return isNotNull().andValue(property(DocumentMetadata::documentPath, "documentPath", rule));
      }

      public static <T extends DocumentMetadata> InvariantRule<T> contentType(final InvariantRule<? super MimeType> rule) {
         return isNotNull().andValue(property(DocumentMetadata::contentType, "contentType", rule));
      }

      public static <T extends DocumentMetadata> InvariantRule<T> optionalContentEncoding(final InvariantRule<Optional<Charset>> rule) {
         return isNotNull().andValue(property(DocumentMetadata::contentEncoding, "contentEncoding", rule));
      }

      public static <T extends DocumentMetadata> InvariantRule<T> contentEncoding(final InvariantRule<? super Charset> rule) {
         return optionalContentEncoding(optionalValue(rule));
      }

      public static <T extends DocumentMetadata> InvariantRule<T> optionalContentSize(final InvariantRule<Optional<Long>> rule) {
         return isNotNull().andValue(property(DocumentMetadata::contentSize, "contentSize", rule));
      }

      public static <T extends DocumentMetadata> InvariantRule<T> contentSize(final InvariantRule<? super Long> rule) {
         return optionalContentSize(optionalValue(rule));
      }

      public static <T extends DocumentMetadata> InvariantRule<T> attributes(final InvariantRule<Map<String, Object>> rule) {
         return isNotNull().andValue(property(DocumentMetadata::attributes, "attributes", rule));
      }
   }
}
