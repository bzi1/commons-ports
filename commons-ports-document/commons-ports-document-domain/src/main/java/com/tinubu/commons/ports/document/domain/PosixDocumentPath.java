/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd2.domain.type.support.TypeSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isPosix;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.Invariants;

/**
 * Uniquely identify a document in the repository.
 * Document identification path must be relative.
 * Document path must contain only POSIX characters ({@code [a-zA-Z0-9.-/]}).
 */
public class PosixDocumentPath extends DocumentPath {
   protected PosixDocumentPath(Path documentId, boolean newObject) {
      super(documentId, newObject);
   }

   protected PosixDocumentPath(Path documentId) {
      super(documentId);
   }

   public static PosixDocumentPath of(Path value) {
      return checkInvariants(new PosixDocumentPath(value));
   }

   public static PosixDocumentPath of(String first, String... more) {
      return of(Paths.get(first, more));
   }

   public static DocumentPath of(URI urn) {
      return of(nullable(urn).map(u -> Paths.get(documentUrnValue(u, URN_ID_TYPE))).orElse(null));
   }

   public static PosixDocumentPath ofNewObject(Path value) {
      return checkInvariants(new PosixDocumentPath(value, true));
   }

   public static PosixDocumentPath ofNewObject(String first, String... more) {
      return ofNewObject(Paths.get(first, more));
   }

   public static DocumentPath ofNewObject(URI urn) {
      return ofNewObject(nullable(urn).map(u -> Paths.get(documentUrnValue(u, URN_ID_TYPE))).orElse(null));
   }

   @Override
   public Invariants domainInvariants() {
      return Invariants.of(Invariant.of(() -> this, property(d -> d.value, "value", isPosix())));
   }

}
