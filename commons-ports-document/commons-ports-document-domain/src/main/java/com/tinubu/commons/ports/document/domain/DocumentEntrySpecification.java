/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.optionalInstanceOf;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.specification.AndSpecification;
import com.tinubu.commons.ddd2.domain.specification.CompositeSpecification;
import com.tinubu.commons.ddd2.domain.specification.CompositeSpecificationVisitor;
import com.tinubu.commons.ddd2.domain.specification.NotSpecification;
import com.tinubu.commons.ddd2.domain.specification.OrSpecification;
import com.tinubu.commons.ddd2.domain.specification.Specification;

/**
 * {@link DocumentEntry} specification.
 * <p>
 */
@FunctionalInterface
public interface DocumentEntrySpecification extends CompositeSpecification<DocumentEntry> {

   /**
    * Introduces a {@link Path document sub path} matcher to efficiently filter document paths
    * while recursively searching for documents in backends. If specification is not compatible with this
    * feature, just return {@code true}, but document searches based this specification will suffer from
    * performance.
    *
    * @param documentSubPath document sub path, without filename, relative, non-empty
    *
    * @return {@code true} if specified document sub path is compatible with current specification
    */
   default boolean satisfiedBySubPath(Path documentSubPath) {
      return true;
   }

   /**
    * A pre-set specification to return all documents from a repository.
    *
    * @return document entry specification to match all documents
    */
   static DocumentEntrySpecification allDocuments() {
      return __ -> true;
   }

   @Override
   default DocumentEntrySpecification and(List<? extends Specification<DocumentEntry>> specifications) {
      CompositeSpecification<DocumentEntry> andSpecification =
            CompositeSpecification.super.and(specifications);

      return new DocumentEntrySpecification() {
         @Override
         public boolean satisfiedBySubPath(Path documentSubPath) {
            return andSpecification.accept(new SatisfiedBySubPathVisitor(documentSubPath)).orElse(true);
         }

         @Override
         public boolean satisfiedBy(DocumentEntry documentEntry) {
            return andSpecification.satisfiedBy(documentEntry);
         }
      };
   }

   @Override
   default DocumentEntrySpecification and(Specification<DocumentEntry> specification) {
      return and(list(specification));
   }

   @Override
   default DocumentEntrySpecification or(List<? extends Specification<DocumentEntry>> specifications) {
      CompositeSpecification<DocumentEntry> orSpecification = CompositeSpecification.super.or(specifications);

      return new DocumentEntrySpecification() {
         @Override
         public boolean satisfiedBySubPath(Path documentSubPath) {
            return orSpecification.accept(new SatisfiedBySubPathVisitor(documentSubPath)).orElse(true);
         }

         @Override
         public boolean satisfiedBy(DocumentEntry documentEntry) {
            return orSpecification.satisfiedBy(documentEntry);
         }
      };
   }

   @Override
   default DocumentEntrySpecification or(Specification<DocumentEntry> specification) {
      return or(list(specification));
   }

   @Override
   default DocumentEntrySpecification not() {
      CompositeSpecification<DocumentEntry> notSpecification = CompositeSpecification.super.not();

      return new DocumentEntrySpecification() {
         @Override
         public boolean satisfiedBySubPath(Path documentSubPath) {
            return notSpecification.accept(new SatisfiedBySubPathVisitor(documentSubPath)).orElse(true);
         }

         @Override
         public boolean satisfiedBy(DocumentEntry documentEntry) {
            return notSpecification.satisfiedBy(documentEntry);
         }
      };
   }
}

/**
 * Visitor to apply {@link DocumentEntrySpecification#satisfiedBySubPath(Path)} to any composite
 * specification.
 * {@link DocumentEntrySpecification}-pure composite specifications, will return an optional boolean with
 * value set to the composed value.
 * Otherwise, visitor will always return {@link Optional#empty}.
 *
 * @see CompositeSpecification#isPure(Class)
 */
class SatisfiedBySubPathVisitor implements CompositeSpecificationVisitor<DocumentEntry, Optional<Boolean>> {

   /** Special pointer to compare to using pointer (==) equality. */
   @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
   private static final Optional<Boolean> INIT = optional();

   private final Path documentSubPath;

   public SatisfiedBySubPathVisitor(Path documentSubPath) {
      this.documentSubPath = notNull(documentSubPath, "documentSubPath");
   }

   @Override
   public Optional<Boolean> visit(Specification<DocumentEntry> specification) {
      return optionalInstanceOf(specification,
                                DocumentEntrySpecification.class).map(s -> s.satisfiedBySubPath(
            documentSubPath));
   }

   @Override
   public Optional<Boolean> visit(NotSpecification<DocumentEntry> specification) {
      return specification.specification().accept(this).map(s -> !s);
   }

   @Override
   public Optional<Boolean> visit(AndSpecification<DocumentEntry> specification) {
      return stream(specification.specifications()).reduce(INIT, (satisfiedBySubpath, spec) -> {
         if (satisfiedBySubpath == INIT) {
            return spec.accept(this);
         } else {
            return satisfiedBySubpath.flatMap(s1 -> spec.accept(this).map(s2 -> s1 && s2));
         }
      }, (o1, o2) -> o1.flatMap(s1 -> o2.map(s2 -> s1 && s2)));
   }

   @Override
   public Optional<Boolean> visit(OrSpecification<DocumentEntry> specification) {
      return stream(specification.specifications()).reduce(INIT, (satisfiedBySubpath, spec) -> {
         if (satisfiedBySubpath == INIT) {
            return spec.accept(this);
         } else {
            return satisfiedBySubpath.flatMap(s1 -> spec.accept(this).map(s2 -> s1 || s2));
         }
      }, (o1, o2) -> o1.flatMap(s1 -> o2.map(s2 -> s1 || s2)));
   }
}
