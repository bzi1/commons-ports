/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoNullElements;
import static com.tinubu.commons.lang.util.CollectionUtils.collectionEquals;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.nonNull;
import static com.tinubu.commons.lang.util.StreamUtils.reversedStream;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.net.URI;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.lang.mapper.SchwartzianTransformer;
import com.tinubu.commons.lang.util.CollectionUtils;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.UnsupportedUriException;

/**
 * Union {@link DocumentRepository} adapter implementation.
 * Zero or more repositories can be registered as "layers" in this meta-repository. Documents are searched
 * from upper layers to lower layers until one matches.
 * Documents are saved on the upper layer only.
 *
 * @implNote Immutable class implementation.
 */
public class UnionDocumentRepository extends AbstractDocumentRepository {

   /**
    * Union repository layers, ordered from the upper to the lower layer. Lower layers have lesser priority.
    */
   protected final List<DocumentRepository> layers;

   protected UnionDocumentRepository(List<DocumentRepository> layers) {
      layers = validate(layers, "layers", hasNoNullElements()).orThrow();

      this.layers =
            immutable(list(reversedStream(layers).filter(layer -> !(layer instanceof NoopDocumentRepository))));
   }

   /**
    * Creates an empty union repository.
    *
    * @return empty union repository
    */
   public static UnionDocumentRepository ofEmpty() {
      return new UnionDocumentRepository(list());
   }

   /**
    * Creates a union repository from layers.
    *
    * @param layers layers ordered from the lower to the upper layers. Lower layers have
    *       lesser priority.
    *
    * @return union repository with specified layers
    */
   public static UnionDocumentRepository ofLayers(DocumentRepository... layers) {
      return new UnionDocumentRepository(nullable(layers).map(CollectionUtils::list).orElse(null));
   }

   /**
    * Creates a union repository from layers, ordered from the lower to the upper layers. Lower layers have
    * lesser priority.
    *
    * @param layers layers ordered from the lower to the upper layers. Lower layers have
    *       lesser priority.
    *
    * @return union repository with specified layers
    */
   public static UnionDocumentRepository ofLayers(List<DocumentRepository> layers) {
      return new UnionDocumentRepository(layers);
   }

   public Optional<DocumentRepository> upperLayer() {
      return layers.stream().findFirst();
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      return documentRepository instanceof UnionDocumentRepository
             && collectionEquals(((UnionDocumentRepository) documentRepository).layers,
                                 layers,
                                 Repository::sameRepositoryAs);
   }

   @Override
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      return upperLayer().flatMap(upperLayer -> upperLayer.openDocument(documentId,
                                                                        overwrite,
                                                                        append,
                                                                        metadata));
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      notNull(documentId, "documentId");

      return stream(layers).flatMap(layer -> stream(layer.findDocumentById(documentId))).findFirst();
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      notNull(documentId, "documentId");

      return stream(layers).flatMap(layer -> stream(layer.findDocumentEntryById(documentId))).findFirst();
   }

   /**
    * {@inheritDoc}
    * Search documents on the all layers. If documents with the same identifier are found on multiple layers,
    * only the upper layer document is returned.
    *
    * @implNote We use a {@code map(..orElse(null)).filter(Objects::nonNull} scheme instead of a simpler
    *       Stream::flatMap as an optimization.
    */
   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      return findDocumentEntriesBySpecification(basePath, specification)
            .map(entry -> findDocumentById(entry.documentId()).orElse(null))
            .filter(nonNull());
   }

   /**
    * {@inheritDoc}
    * Search documents on the all layers. If documents with the same identifier are found on multiple layers,
    * only the upper layer document is returned.
    */
   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      return stream(layers)
            .flatMap(layer -> layer.findDocumentEntriesBySpecification(basePath, specification))
            .map(e -> SchwartzianTransformer.wrap(e, DocumentEntry::documentId))
            .distinct()
            .map(SchwartzianTransformer::unwrap);
   }

   /**
    * {@inheritDoc}
    * Save document on the upper layer only, if there is at least one layer, otherwise returns
    * {@link Optional#empty}.
    */
   @Override
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      notNull(document, "document");

      return upperLayer().flatMap(upperLayer -> upperLayer.saveDocument(document, overwrite));
   }

   /**
    * {@inheritDoc}
    * Delete document on the upper layer only, if there is at least one layer, otherwise returns
    * {@link Optional#empty}.
    */
   @Override
   public Optional<DocumentEntry> deleteDocument(DocumentPath documentId) {
      notNull(documentId, "documentId");

      return upperLayer().flatMap(upperLayer -> upperLayer.deleteDocument(documentId));
   }

   @Override
   public URI toUri(DocumentPath documentId) {
      notNull(documentId, "documentId");

      return upperLayer()
            .map(layer -> layer.toUri(documentId))
            .orElseThrow(() -> new UnsupportedUriException("No layer available"));
   }

   @Override
   public boolean supportsUri(URI documentUri) {
      notNull(documentUri, "documentUri");

      return stream(layers).reduce(false,
                                   (support, layer) -> support || layer.supportsUri(documentUri),
                                   (s1, s2) -> s1 || s2);
   }

   @Override
   public Optional<Document> findDocumentByUri(URI documentUri) {
      notNull(documentUri, "documentUri");

      return stream(layers).flatMap(layer -> {
         Stream<Document> layerDocument = stream();
         if (layer.supportsUri(documentUri)) {
            layerDocument = stream(layer.findDocumentByUri(documentUri));
         }
         return layerDocument;
      }).findFirst();
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
      notNull(documentUri, "documentUri");

      return stream(layers).flatMap(layer -> {
         Stream<DocumentEntry> layerDocument = stream();
         if (layer.supportsUri(documentUri)) {
            layerDocument = stream(layer.findDocumentEntryByUri(documentUri));
         }
         return layerDocument;
      }).findFirst();
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", UnionDocumentRepository.class.getSimpleName() + "[", "]")
            .add("layers=" + layers)
            .toString();
   }
}
