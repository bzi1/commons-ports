/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;

import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.lang.validation.CheckReturnValue;
import com.tinubu.commons.ports.document.domain.event.DocumentAccessed;
import com.tinubu.commons.ports.document.domain.event.DocumentDeleted;
import com.tinubu.commons.ports.document.domain.event.DocumentRepositoryEvent;
import com.tinubu.commons.ports.document.domain.event.DocumentSaved;
import com.tinubu.commons.ports.document.domain.event.LoggingDocumentRepositoryListener;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.commons.ports.document.domain.processor.common.DocumentRenamer;
import com.tinubu.commons.ports.document.domain.repository.NoopDocumentRepository;
import com.tinubu.commons.ports.document.domain.repository.UnionDocumentRepository;

/**
 * Domain repository to access {@link Document} documents. This abstraction enable to store or retrieve
 * documents.
 * <p>
 * A {@link DocumentPath} uniquely identify a document in the repository backend. It must be a relative path.
 *
 * @implSpec Repository implementations can implement {@link #close()} to release resources on
 *       repository release.
 */
// FIXME resilience4j .. timeouts + retrier for some backends
public interface DocumentRepository extends Repository<Document, DocumentPath>, UriAdapter, AutoCloseable {

   /**
    * Opens a new {@link Document} for creation. The document content will provide an {@link OutputStream}
    * for direct write access to the underlying repository. The provided document does not require to be
    * saved using {@link DocumentRepository#saveDocument(Document, boolean)}, content is directly written to
    * underlying repository storage.
    * <p>
    * Overwrite/Append option combinations results :
    * <table>
    * <tr><th></th><th colspan="2"> overwrite </th><th colspan="2"> !overwrite </th></tr>
    * <tr><td></td><td>append</td><td>!append</td><td>append</td><td>!append</td></tr>
    * <tr><td>exists</td><td> APPEND </td><td> TRUNCATE </td><td> APPEND </td><td> return {@link Optional#empty() EMPTY} </td></tr>
    * <tr><td>not exists</td><td> CREATE </td><td> CREATE </td><td> CREATE </td><td> CREATE </td></tr>
    * </table>
    *
    * @param documentId document id to create
    * @param overwrite whether to overwrite an existing document with the same name
    * @param append whether to append content to an existing document with the same name
    * @param metadata document metadata (content-type, document path, attributes, ...)
    *
    * @return opened document, or {@link Optional#empty()} if document already exists and both overwrite and
    *       append are not set
    */
   Optional<Document> openDocument(DocumentPath documentId,
                                   boolean overwrite,
                                   boolean append,
                                   OpenDocumentMetadata metadata);

   /**
    * Generates a new {@link UnionDocumentRepository} with this repository on the lowest layer and specified
    * repositories on upper layers.
    * {@link NoopDocumentRepository} repositories are ignored
    *
    * @param documentRepositories repositories to stack, from the lower to the upper layer
    *
    * @return new union document repository
    */
   default DocumentRepository stackRepositories(DocumentRepository... documentRepositories) {
      notNull(documentRepositories, "documentRepositories");

      return UnionDocumentRepository.ofLayers(list(streamConcat(stream(this), stream(documentRepositories))));
   }

   /**
    * Finds document by id.
    *
    * @param documentId document id
    *
    * @return found document or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   Optional<Document> findDocumentById(DocumentPath documentId);

   /**
    * Finds document by document entry specification.
    *
    * @param basePath starting document base path for all documents to search. Must be relative. Using
    *       this parameter may be used by implementation to optimally list documents from there instead of
    *       recursing from root
    * @param specification document entry specification
    *       Use empty directory to search for all documents.
    *
    * @return found documents
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @implSpec Implementation should optimize repository crawling, ignoring whole navigation tree
    *       branches, using dedicated {@link DocumentEntrySpecification#satisfiedBySubPath(Path)}.
    * @see DocumentEntryCriteria
    */
   Stream<Document> findDocumentsBySpecification(Path basePath, Specification<DocumentEntry> specification);

   /**
    * Finds document by document entry specification from backend default directory. Using {@link
    * #findDocumentsBySpecification(Path, Specification)} with a start path is more efficient.
    *
    * @param specification document entry specification
    *
    * @return found documents
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @implSpec Implementation should optimize repository crawling, ignoring whole navigation tree
    *       branches, using dedicated {@link DocumentEntrySpecification#satisfiedBySubPath(Path)}.
    * @see DocumentEntryCriteria
    */
   default Stream<Document> findDocumentsBySpecification(Specification<DocumentEntry> specification) {
      return findDocumentsBySpecification(Paths.get(""), specification);
   }

   /**
    * Finds document entry by id. This enables to only retrieve metadata and not document content.
    * If document exists, a document metadata is always returned with available data.
    *
    * @param documentId document id
    *
    * @return found document entry or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId);

   /**
    * Finds document entries by document entry specification.
    *
    * @param basePath starting document base path for all documents to search. Must be relative. Using
    *       this parameter may be used by implementation to optimally list documents from there instead of
    *       recursing from root
    * @param specification document entry specification
    *       Use empty directory to search for all documents.
    *
    * @return found document entries
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @implSpec Implementation should optimize repository crawling, ignoring whole navigation tree
    *       branches, using dedicated {@link DocumentEntrySpecification#satisfiedBySubPath(Path)}.
    * @see DocumentEntryCriteria
    */
   Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                            Specification<DocumentEntry> specification);

   /**
    * Finds document entries by document entry specification from backend default directory. Using {@link
    * #findDocumentEntriesBySpecification(Path, Specification)} with a start path is more efficient.
    *
    * @param specification document entry specification
    *
    * @return found document entries
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @implSpec Implementation should optimize repository crawling, ignoring whole navigation tree
    *       branches, using dedicated {@link DocumentEntrySpecification#satisfiedBySubPath(Path)}.
    * @see DocumentEntryCriteria
    */
   default Stream<DocumentEntry> findDocumentEntriesBySpecification(Specification<DocumentEntry> specification) {
      return findDocumentEntriesBySpecification(Paths.get(""), specification);
   }

   /**
    * Saves specified document.
    *
    * @param document document to save
    * @param overwrite accept to overwrite pre-existing document with same if {@code true}
    *
    * @return document entry, potentially different from original document, if document has been saved or
    *       {@link Optional#empty} otherwise : document with same id already exists and overwrite is not set,
    *       saved document has been externally deleted just before returning
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   @CheckReturnValue
   Optional<DocumentEntry> saveDocument(Document document, boolean overwrite);

   /**
    * Deletes specified document.
    *
    * @param documentId document to delete
    *
    * @return document entry of deleted file, if document has been deleted or {@link Optional#empty} otherwise
    *       : document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   @CheckReturnValue
   Optional<DocumentEntry> deleteDocument(DocumentPath documentId);

   /**
    * Transfers specified document to another repository.
    * Document is copied to target repository and not deleted from source repository.
    *
    * @param documentId document to transfer
    * @param targetDocumentRepository target repository to transfer document to
    * @param overwrite accept to overwrite pre-existing document with same identifier
    * @param processor document processor to apply on transferred document
    *
    * @return new document entry in target repository or {@link Optional#empty} if document not
    *       transferred. Document is considered not transferred if not found, or already existing and
    *       overwrite is not set
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   @CheckReturnValue
   default Optional<DocumentEntry> transferDocument(DocumentPath documentId,
                                                    DocumentRepository targetDocumentRepository,
                                                    boolean overwrite,
                                                    DocumentProcessor processor) {
      notNull(documentId, "documentId");
      notNull(targetDocumentRepository, "targetDocumentRepository");
      notNull(processor, "processor");
      satisfies(targetDocumentRepository,
                targetRepository -> !sameRepositoryAs(targetRepository) || !overwrite,
                "targetDocumentRepository",
                "Overwrite can't be set when source and target repositories are the same");

      return findDocumentById(documentId).flatMap(document -> {
         Document newDocument = processor.process(document);

         return targetDocumentRepository.saveDocument(newDocument, overwrite);
      });
   }

   /**
    * Transfers specified document to another repository.
    * Document is copied to target repository and not deleted from source repository.
    *
    * @param documentId document to transfer
    * @param targetDocumentRepository target repository to transfer document to
    * @param overwrite accept to overwrite pre-existing document with same identifier
    * @param renamer document identifier mapper for target documents
    *
    * @return new document entry in target repository or {@link Optional#empty} if document not
    *       transferred. Document is considered not transferred if not found, or already existing and
    *       overwrite is not set
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   @CheckReturnValue
   default Optional<DocumentEntry> transferDocument(DocumentPath documentId,
                                                    DocumentRepository targetDocumentRepository,
                                                    boolean overwrite,
                                                    DocumentRenamer renamer) {
      notNull(documentId, "documentId");
      notNull(targetDocumentRepository, "targetDocumentRepository");
      notNull(renamer, "renamer");
      satisfies(targetDocumentRepository,
                targetRepository -> !sameRepositoryAs(targetRepository) || !overwrite,
                "targetDocumentRepository",
                "Overwrite can't be set when source and target repositories are the same");

      return transferDocument(documentId, targetDocumentRepository, overwrite, (DocumentProcessor) renamer);
   }

   /**
    * Transfers specified document to another repository, with default document identifier mapper for target
    * document. Default mapper reuse source document identifier.
    * Document is copied to target repository and not deleted from source repository.
    *
    * @param documentId document to transfer
    * @param targetDocumentRepository target repository to transfer document to
    * @param overwrite accept to overwrite pre-existing document with same identifier
    *
    * @return new document entry in target repository or {@link Optional#empty} if document not
    *       transferred. Document is considered not transferred if not found, or already existing and
    *       overwrite is not set
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   @CheckReturnValue
   default Optional<DocumentEntry> transferDocument(DocumentPath documentId,
                                                    DocumentRepository targetDocumentRepository,
                                                    boolean overwrite) {
      return transferDocument(documentId, targetDocumentRepository, overwrite, DocumentEntry::documentId);
   }

   /**
    * Transfers documents satisfied by specification to another repository.
    * Document is copied to target repository and not deleted from source repository.
    *
    * @param documentSpecification document specification to identify documents to transfer
    * @param targetDocumentRepository target repository to transfer documents to
    * @param overwrite accept to overwrite pre-existing document with same identifier
    * @param processor document processor to apply on transferred document
    *
    * @return transferred document entries in target repository. Documents are considered not transferred
    *       if already existing and overwrite is not set
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   default List<DocumentEntry> transferDocuments(Specification<DocumentEntry> documentSpecification,
                                                 DocumentRepository targetDocumentRepository,
                                                 boolean overwrite,
                                                 DocumentProcessor processor) {
      notNull(documentSpecification, "documentSpecification");
      notNull(targetDocumentRepository, "targetDocumentRepository");
      notNull(processor, "processor");
      satisfies(targetDocumentRepository,
                targetRepository -> !sameRepositoryAs(targetRepository) || !overwrite,
                "targetDocumentRepository",
                "Overwrite can't be set when source and target repositories are the same");

      return list(findDocumentsBySpecification(documentSpecification).flatMap(document -> {
         Document newDocument = processor.process(document);

         return targetDocumentRepository
               .saveDocument(newDocument, overwrite)
               .map(Stream::of)
               .orElse(Stream.empty());
      }));
   }

   /**
    * Transfers documents satisfied by specification to another repository.
    * Document is copied to target repository and not deleted from source repository.
    *
    * @param documentSpecification document specification to identify documents to transfer
    * @param targetDocumentRepository target repository to transfer documents to
    * @param overwrite accept to overwrite pre-existing document with same identifier
    * @param renamer document identifier mapper for target documents
    *
    * @return transferred document entries in target repository. Documents are considered not transferred
    *       if already existing and overwrite is not set
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   default List<DocumentEntry> transferDocuments(Specification<DocumentEntry> documentSpecification,
                                                 DocumentRepository targetDocumentRepository,
                                                 boolean overwrite,
                                                 DocumentRenamer renamer) {
      notNull(documentSpecification, "documentSpecification");
      notNull(targetDocumentRepository, "targetDocumentRepository");
      notNull(renamer, "renamer");
      satisfies(targetDocumentRepository,
                targetRepository -> !sameRepositoryAs(targetRepository) || !overwrite,
                "targetDocumentRepository",
                "Overwrite can't be set when source and target repositories are the same");

      return transferDocuments(documentSpecification,
                               targetDocumentRepository,
                               overwrite,
                               (DocumentProcessor) renamer);
   }

   /**
    * Transfers documents satisfied by specification to another repository,with default document identifier
    * mapper for target document. Default mapper reuse source document identifier.
    * Document is copied to target repository and not deleted from source repository.
    *
    * @param documentSpecification document specification to identify documents to transfer
    * @param targetDocumentRepository target repository to transfer documents to
    * @param overwrite accept to overwrite pre-existing document with same identifier
    *
    * @return transferred document entries in target repository. Documents are considered not transferred
    *       if already existing and overwrite is not set
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   default List<DocumentEntry> transferDocuments(Specification<DocumentEntry> documentSpecification,
                                                 DocumentRepository targetDocumentRepository,
                                                 boolean overwrite) {
      return transferDocuments(documentSpecification,
                               targetDocumentRepository,
                               overwrite,
                               DocumentEntry::documentId);
   }

   /**
    * Registers event listener for this repository.
    *
    * @param eventClass event type class
    * @param eventListener event listener
    * @param <E> event type
    */
   <E extends DocumentRepositoryEvent> void registerEventListener(Class<E> eventClass,
                                                                  DomainEventListener<? super E> eventListener);

   /**
    * Registers event listener for all "write" events for this repository.
    *
    * @param eventListener event listener
    */
   default void registerWriteEventsListener(DomainEventListener<? super DocumentRepositoryEvent> eventListener) {
      list(DocumentSaved.class, DocumentDeleted.class).forEach(event -> registerEventListener(event,
                                                                                              eventListener));
   }

   /**
    * Registers event listener for all "access" events for this repository.
    *
    * @param eventListener event listener
    */
   default void registerAccessEventsListener(DomainEventListener<? super DocumentRepositoryEvent> eventListener) {
      list(DocumentAccessed.class).forEach(event -> registerEventListener(event, eventListener));
   }

   /**
    * Registers logging event listener for this repository.
    *
    * @param eventClass event type class
    * @param <E> event type
    */
   default <E extends DocumentRepositoryEvent> void registerEventLoggingListener(Class<E> eventClass) {
      registerEventListener(eventClass, new LoggingDocumentRepositoryListener<>());
   }

   /**
    * Registers logging event listener for all "write" events for this repository.
    */
   default void registerWriteEventsLoggingListener() {
      list(DocumentSaved.class, DocumentDeleted.class).forEach(this::registerEventLoggingListener);
   }

   /**
    * Registers logging event listener for all "access" events for this repository.
    */
   default void registerAccessEventsLoggingListener() {
      list(DocumentAccessed.class).forEach(this::registerEventLoggingListener);
   }

   /**
    * Unregisters all event listeners.
    */
   void unregisterEventListeners();

   /**
    * Default no-op close operation.
    */
   @Override
   default void close() {}

}
