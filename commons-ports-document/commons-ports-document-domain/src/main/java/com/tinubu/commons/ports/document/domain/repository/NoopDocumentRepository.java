/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.StreamUtils.stream;

import java.net.URI;
import java.nio.file.Path;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.UnsupportedUriException;

/**
 * No-op {@link DocumentRepository}. No operation fails, but returns only empty optionals and streams.
 */
public class NoopDocumentRepository extends AbstractDocumentRepository {

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      return false;
   }

   @Override
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      return optional();
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      return optional();
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      return stream();
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      return optional();
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      return stream();
   }

   @Override
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      return optional();
   }

   @Override
   public Optional<DocumentEntry> deleteDocument(DocumentPath documentId) {
      return optional();
   }

   @Override
   public URI toUri(DocumentPath documentId) {
      throw new UnsupportedUriException("Repository has no URI representation");
   }

   @Override
   public boolean supportsUri(URI documentUri) {
      return false;
   }

   @Override
   public Optional<Document> findDocumentByUri(URI documentUri) {
      return optional();
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
      return optional();
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", NoopDocumentRepository.class.getSimpleName() + "[", "]").toString();
   }
}
