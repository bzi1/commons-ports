package com.tinubu.commons.ports.document.domain.processor.replacer;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isStrictlyPositive;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.function.Function;

import com.tinubu.commons.lang.util.PlaceholderReplacerReader;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.processor.DocumentContentProcessor;

/**
 * Document content processor to replace simple placeholders {@code {key}} with specified model values.
 * Document must not be in binary format.
 * {@link #DEFAULT_CONTENT_ENCODING} is assumed for document without explicit content-type.
 */
public class PlaceholderReplacer implements DocumentContentProcessor {

   protected static final Charset DEFAULT_CONTENT_ENCODING = StandardCharsets.UTF_8;
   protected static final int DEFAULT_BUFFER_SIZE = 4096 * 16;
   protected static final String DEFAULT_PLACEHOLDER_BEGIN = "${";
   protected static final String DEFAULT_PLACEHOLDER_END = "}";

   protected final Function<String, Object> replacementFunction;
   protected final String placeholderBegin;
   protected final String placeholderEnd;
   protected final int bufferSize;

   public PlaceholderReplacer(Function<String, Object> replacementFunction,
                              String placeholderBegin,
                              String placeholderEnd,
                              int bufferSize) {
      this.replacementFunction = validate(replacementFunction, "replacementFunction", isNotNull()).orThrow();
      this.placeholderBegin = validate(placeholderBegin, "placeholderBegin", isNotBlank()).orThrow();
      this.placeholderEnd = validate(placeholderEnd, "placeholderEnd", isNotBlank()).orThrow();
      this.bufferSize = validate(bufferSize, "bufferSize", isStrictlyPositive()).orThrow();
   }

   public PlaceholderReplacer(Function<String, Object> replacementFunction,
                              String placeholderBegin,
                              String placeholderEnd) {
      this(replacementFunction, placeholderBegin, placeholderEnd, DEFAULT_BUFFER_SIZE);
   }

   public PlaceholderReplacer(Function<String, Object> replacementFunction, int bufferSize) {
      this(replacementFunction, DEFAULT_PLACEHOLDER_BEGIN, DEFAULT_PLACEHOLDER_END, bufferSize);
   }

   public PlaceholderReplacer(Function<String, Object> replacementFunction) {
      this(replacementFunction, DEFAULT_PLACEHOLDER_BEGIN, DEFAULT_PLACEHOLDER_END, DEFAULT_BUFFER_SIZE);
   }

   @Override
   public DocumentContent process(DocumentContent documentContent) {
      return new InputStreamDocumentContentBuilder()
            .content(new PlaceholderReplacerReader(documentContent.readerContent(DEFAULT_CONTENT_ENCODING),
                                                   replacementFunction,
                                                   placeholderBegin,
                                                   placeholderEnd), DEFAULT_CONTENT_ENCODING)
            .build();
   }

}

