/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Optional;

/**
 * Document content attached to a {@link Document}.
 */
public interface DocumentContent {

   /**
    * Returns content as a byte array.
    *
    * @return document content
    */
   byte[] content();

   /**
    * Returns content as a string. String is encoded using {@link #contentEncoding()} or specified encoding.
    *
    * @param defaultContentEncoding default encoding to apply to content if no content encoding set
    *
    * @return document content
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   String stringContent(Charset defaultContentEncoding);

   /**
    * Returns content as a string. String is encoded using {@link #contentEncoding()} or throw an error if no
    * encoding set.
    *
    * @return document content
    *
    * @throws IllegalStateException if no content encoding set
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   String stringContent();

   /**
    * Returns content as an {@link InputStream}.
    * The stream must be closed by the client code.
    *
    * @return document content as input stream
    */
   InputStream inputStreamContent();

   /**
    * Returns an {@link OutputStream} to generate new content.
    * The stream must be closed by the client code.
    *
    * @return document content generator
    */
   OutputStream outputStreamContent();

   /**
    * Returns content as a string {@link Reader}. Reader is encoded using {@link #contentEncoding()} or
    * specified encoding.
    * The reader must be closed by the client code.
    *
    * @param defaultContentEncoding default encoding to apply to content if no content encoding set
    *
    * @return document content as string reader
    */
   Reader readerContent(Charset defaultContentEncoding);

   /**
    * Returns an {@link Writer} to generate new content.
    * The stream must be closed by the client code.
    *
    * @return document content generator
    */
   Writer writerContent();

   /**
    * Returns content as a string {@link Reader}. Reader is encoded using {@link #contentEncoding()} or throw
    * an error if no encoding set.
    * The reader must be closed by the client code.
    *
    * @return document content as string reader
    *
    * @throws IllegalStateException if no content encoding set
    */
   Reader readerContent();

   /**
    * Returns content size in bytes.
    *
    * @return document content size, or {@link Optional#empty} if loader is dynamic and content size is not
    *       known
    */
   Optional<Long> contentSize();

   /**
    * Returns document content encoding {@link Charset}.
    *
    * @return document content encoding, or {@link Optional#empty} if encoding is not known
    */
   Optional<Charset> contentEncoding();
}

