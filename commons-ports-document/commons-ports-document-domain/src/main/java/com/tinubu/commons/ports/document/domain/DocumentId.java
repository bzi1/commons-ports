/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd2.domain.type.support.TypeSupport.checkInvariants;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Uniquely identify a document in the repository.
 * Document identification path must be relative.
 *
 * @deprecated Use directly {@link DocumentPath} instead
 */
@Deprecated
public class DocumentId extends DocumentPath {
   protected DocumentId(Path documentId, boolean newObject) {
      super(documentId, newObject);
   }

   protected DocumentId(Path documentId) {
      super(documentId);
   }

   public static DocumentId of(Path value) {
      return checkInvariants(new DocumentId(value));
   }

   public static DocumentId of(String first, String... more) {
      return of(Paths.get(first, more));
   }

   public static DocumentId ofNewObject(Path value) {
      return checkInvariants(new DocumentId(value, true));
   }

   public static DocumentId ofNewObject(String first, String... more) {
      return ofNewObject(Paths.get(first, more));
   }
}
