/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.string;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.allSatisfies;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isLessThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.entrySet;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.hasNoNullKeyValue;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.fileName;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotEmpty;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.length;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;
import java.util.function.UnaryOperator;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.valueformatter.StringValueFormatter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;

/**
 * Optional metadata for created documents by
 * {@link DocumentRepository#openDocument(DocumentPath, boolean, boolean, OpenDocumentMetadata)}.
 */
public class OpenDocumentMetadata extends AbstractValue {
   /** Maximum document name length. This value is aligned on POSIX filesystems limits. */
   protected static final int DOCUMENT_NAME_MAX_LENGTH = 255;

   private final Path documentPath;
   private final MimeType contentType;
   private final Map<String, Object> attributes;

   private OpenDocumentMetadata(OpenDocumentMetadataBuilder builder) {
      this.documentPath = builder.documentPath;
      this.contentType = builder.contentType;
      this.attributes = builder.attributes;
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends OpenDocumentMetadata> defineDomainFields() {
      return Fields
            .<OpenDocumentMetadata>builder()
            .field("documentPath",
                   v -> v.documentPath,
                   new StringValueFormatter(DOCUMENT_NAME_MAX_LENGTH).compose(OpenDocumentMetadata::documentPath),
                   isNull().orValue(isNotEmpty()
                                          .andValue(isNotAbsolute())
                                          .andValue(hasNoTraversal())
                                          .andValue(fileName(string(isNotBlank().andValue(length(
                                                isLessThanOrEqualTo(value(DOCUMENT_NAME_MAX_LENGTH,
                                                                          "DOCUMENT_NAME_MAX_LENGTH")))))))))
            .field("contentType", v -> v.contentType)
            .field("attributes", v -> v.attributes, entrySet(allSatisfies(hasNoNullKeyValue())))
            .build();
   }

   /**
    * Optional content encoding from content-type charset parameter if any.
    *
    * @return content encoding
    */
   public Optional<Charset> contentEncoding() {
      return contentType().flatMap(MimeType::charset);
   }

   /**
    * Converts this metadata information to {@link DocumentBuilder} chain function.
    *
    * @return chainable document builder from this metadata
    */
   public UnaryOperator<DocumentBuilder> chainDocumentBuilder() {
      return b -> b
            .<DocumentBuilder, Path>optionalChain(documentPath(), DocumentBuilder::documentPath)
            .optionalChain(contentType(), DocumentBuilder::contentType)
            .attributes(attributes());
   }

   /**
    * Optional document logical path.
    *
    * @return document path
    */
   public Optional<Path> documentPath() {
      return nullable(documentPath);
   }

   /**
    * Optional document content-type.
    * If content type contains a charset parameters, it will be used for content encoding (e.g.:
    * {@code text/plain;charset=UTF-8}).
    *
    * @return document content-type
    */
   public Optional<MimeType> contentType() {
      return nullable(contentType);
   }

   /**
    * Document extra attributes.
    *
    * @return document extra attributes, or empty list
    */
   public Map<String, Object> attributes() {
      return attributes;
   }

   public static class OpenDocumentMetadataBuilder extends DomainBuilder<OpenDocumentMetadata> {
      private Path documentPath;
      private MimeType contentType;
      private Map<String, Object> attributes = map();

      public static OpenDocumentMetadata empty() {
         return new OpenDocumentMetadataBuilder().build();
      }

      public static OpenDocumentMetadataBuilder from(OpenDocumentMetadata metadata) {
         notNull(metadata, "metadata");
         return new OpenDocumentMetadataBuilder()
               .<OpenDocumentMetadataBuilder>reconstitute()
               .documentPath(metadata.documentPath)
               .contentType(metadata.contentType)
               .attributes(metadata.attributes);
      }

      @Setter
      public OpenDocumentMetadataBuilder documentPath(Path documentPath) {
         this.documentPath = documentPath;
         return this;
      }

      public OpenDocumentMetadataBuilder documentPath(String documentPath, String... moreDocumentPath) {
         return documentPath(Paths.get(notNull(documentPath, "documentPath"), moreDocumentPath));
      }

      @Setter
      public OpenDocumentMetadataBuilder contentType(MimeType contentType) {
         this.contentType = contentType;
         return this;
      }

      @Setter
      public OpenDocumentMetadataBuilder attributes(Map<String, Object> metadata) {
         this.attributes = map(metadata);
         return this;
      }

      public OpenDocumentMetadataBuilder addAttribute(String key, Object value) {
         this.attributes.put(key, value);
         return this;
      }

      @Override
      public OpenDocumentMetadata buildDomainObject() {
         return new OpenDocumentMetadata(this);
      }
   }

}
