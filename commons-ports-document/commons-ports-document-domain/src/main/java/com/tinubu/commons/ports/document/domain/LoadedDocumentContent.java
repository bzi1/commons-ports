/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Optional;

import org.apache.commons.io.IOUtils;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.valueformatter.HiddenValueFormatter;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;

/**
 * Document content implementation for memory-stored content.
 * This content is represented with a byte array.
 */
public class LoadedDocumentContent extends AbstractValue implements DocumentContent {

   /** Document content as byte array. */
   protected final byte[] content;
   /** Optional document content encoding. */
   protected final Charset contentEncoding;

   protected LoadedDocumentContent(LoadedDocumentContentBuilder builder) {
      this.content = builder.content;
      this.contentEncoding = builder.contentEncoding;
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends LoadedDocumentContent> defineDomainFields() {
      return Fields
            .<LoadedDocumentContent>builder()
            .superFields((Fields<LoadedDocumentContent>) super.defineDomainFields())
            .field("content", v -> v.content, new HiddenValueFormatter().compose(v -> v.content), isNotNull())
            .field("contentEncoding", v -> v.contentEncoding)
            .build();
   }

   @Override
   public String stringContent(Charset defaultContentEncoding) {
      notNull(defaultContentEncoding, "defaultContentEncoding");

      return new String(content, contentEncoding().orElse(defaultContentEncoding));
   }

   @Override
   public String stringContent() {
      return stringContent(contentEncoding().orElseThrow(() -> new IllegalStateException(
            "No content encoding set, you should specify one to encode this content")));
   }

   @Override
   public ByteArrayInputStream inputStreamContent() {
      return new ByteArrayInputStream(content);
   }

   @Override
   public ByteArrayOutputStream outputStreamContent() {
      throw new IllegalStateException("Unsupported operation for this content implementation");
   }

   @Override
   public InputStreamReader readerContent(Charset defaultContentEncoding) {
      notNull(defaultContentEncoding, "defaultContentEncoding");

      return new InputStreamReader(inputStreamContent(), contentEncoding().orElse(defaultContentEncoding));
   }

   @Override
   public Writer writerContent() {
      throw new IllegalStateException("Unsupported operation for this content implementation");
   }

   @Override
   public InputStreamReader readerContent() {
      return readerContent(contentEncoding().orElseThrow(() -> new IllegalStateException(
            "No content encoding set, you should specify one to encode this content")));
   }

   @Override
   public Optional<Long> contentSize() {
      return optional((long) content.length);
   }

   @Override
   @Getter
   public byte[] content() {
      return content;
   }

   @Override
   @Getter
   public Optional<Charset> contentEncoding() {
      return nullable(contentEncoding);
   }

   public static LoadedDocumentContentBuilder reconstituteBuilder() {
      return new LoadedDocumentContentBuilder().reconstitute();
   }

   public static class LoadedDocumentContentBuilder extends DomainBuilder<LoadedDocumentContent> {
      private byte[] content;
      private Charset contentEncoding;

      public LoadedDocumentContentBuilder content(byte[] content, Charset contentEncoding) {
         this.content = content;
         this.contentEncoding = contentEncoding;
         return this;
      }

      public LoadedDocumentContentBuilder content(String content, Charset contentEncoding) {
         return content(notNull(content, "content").getBytes(notNull(contentEncoding, "contentEncoding")),
                        contentEncoding);
      }

      public LoadedDocumentContentBuilder content(InputStream content, Charset contentEncoding) {
         try {
            return content(IOUtils.toByteArray(notNull(content, "content")), contentEncoding);
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
      }

      public LoadedDocumentContentBuilder content(InputStream content) {
         return content(content, null);
      }

      public LoadedDocumentContentBuilder content(Reader content, Charset contentEncoding) {
         try {
            return content(IOUtils.toByteArray(notNull(content, "content"),
                                               notNull(contentEncoding, "contentEncoding")), contentEncoding);
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
      }

      @Setter
      public LoadedDocumentContentBuilder content(byte[] content) {
         this.content = content;
         return this;
      }

      @Setter
      public LoadedDocumentContentBuilder contentEncoding(Charset contentEncoding) {
         this.contentEncoding = contentEncoding;
         return this;
      }

      @Override
      public LoadedDocumentContent buildDomainObject() {
         return new LoadedDocumentContent(this);
      }

   }

}

