/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd2.domain.type.support.TypeSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notBlank;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.tinubu.commons.ddd2.domain.ids.PathId;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.invariant.Invariants;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;

/**
 * Uniquely identify a document in the repository.
 * Document identification path must be relative.
 * <ul>
 *    <li>value cannot have more than {@value #MAX_VALUE_LENGTH} bytes total</li>
 *    <li>each value path element, including last element, cannot have more than {@value #MAX_PATH_ELEMENT_LENGTH} bytes</li>
 * </ul>
 */
public class DocumentPath extends PathId {

   protected static final String URN_ID_TYPE = "path";

   protected DocumentPath(Path documentId, boolean newObject) {
      super(documentId, true, newObject);
   }

   protected DocumentPath(Path documentId) {
      super(documentId, true);
   }

   public static DocumentPath of(Path value) {
      return checkInvariants(new DocumentPath(value));
   }

   public static DocumentPath of(String first, String... more) {
      return of(Paths.get(first, more));
   }

   public static DocumentPath of(URI urn) {
      return of(nullable(urn).map(u -> Paths.get(documentUrnValue(u, URN_ID_TYPE))).orElse(null));
   }

   public static DocumentPath ofNewObject(Path value) {
      return checkInvariants(new DocumentPath(value, true));
   }

   public static DocumentPath ofNewObject(String first, String... more) {
      return ofNewObject(Paths.get(first, more));
   }

   public static DocumentPath ofNewObject(URI urn) {
      return ofNewObject(nullable(urn).map(u -> Paths.get(documentUrnValue(u, URN_ID_TYPE))).orElse(null));
   }

   @Override
   public Invariants domainInvariants() {
      return Invariants.of(Invariant.of(() -> this, property(d -> d.value, "value", isNotNull())));
   }

   /**
    * Creates a URN from this document path.
    * URN format is {@code urn:document:path:<relative-path>}.
    *
    * @return URN of this document path
    */
   @Override
   public URI urnValue() {
      return documentUrn(URN_ID_TYPE, stringValue());
   }

   protected URI documentUrn(String idType, String idValue) {
      notBlank(idType, "idType");
      notBlank(idValue, "idValue");

      try {
         return new URI("urn", "document:" + idType + ":" + idValue, null);
      } catch (URISyntaxException e) {
         throw new IllegalStateException(e);
      }
   }

   /**
    * Parses URN with format {@code urn:document:<id-type>:<id-value>}.
    *
    * @param urn URN
    * @param idType id type to check
    *
    * @return id URN value part
    *
    * @throws InvariantValidationException if URN syntax is invalid
    */
   protected static String documentUrnValue(URI urn, String idType) {
      validate(urn, "urn", isValidDocumentUrnSyntax(idType))
            .and(validate(idType, "idType", isNotBlank()))
            .orThrow();

      return urn.getSchemeSpecificPart().split(":", 3)[2];
   }

   protected static InvariantRule<URI> isValidDocumentUrnSyntax(String idType) {
      return isNotNull().andValue(satisfiesValue((URI urn) -> {
                                                    if (urn.isAbsolute() && urn.getScheme().equals("urn") && urn.isOpaque()) {
                                                       String[] urnParts = urn.getSchemeSpecificPart().split(":", 3);

                                                       if (urnParts.length == 3 && urnParts[0].equals("document") && urnParts[2].length() > 0) {
                                                          return idType == null || urnParts[1].equals(idType);
                                                       }
                                                    }
                                                    return false;
                                                 },
                                                 FastStringFormat.of("'",
                                                                     validatingObject(),
                                                                     "' must be valid URN : 'urn:document:",
                                                                     idType != null ? idType : "<id-type>",
                                                                     ":<id-value>'")).ruleContext(
            "DocumentPath.isValidDocumentUrnSyntax"));
   }

}
