/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.processor.common;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.List;

import org.apache.commons.io.FilenameUtils;

import com.tinubu.commons.lang.mimetype.registry.MimeTypeRegistries;
import com.tinubu.commons.lang.mimetype.registry.MimeTypeRegistry;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;

/**
 * Renames document extension based on its content-type, if any, otherwise, does not change the name.
 * If the document already has an extension, the extension is kept if it matches one of registry's primary or
 * secondary extensions, otherwise, the extension is replaced.
 * <p>
 * Default mime-type registry is used but can be changed. Does not change the name if no extension is
 * registered for content-type.
 */
public class ContentTypeDocumentRenamer implements DocumentRenamer {

   protected static final MimeTypeRegistry MIME_TYPE_REGISTRY = MimeTypeRegistries.ofDefaults();

   protected final MimeTypeRegistry mimeTypeRegistry;
   /**
    * Set to {@code true} if the extension should be added to current one instead of replacing it, e.g.:
    * .txt.gz. Anyway, extension won't be added if the same extension is already present.
    */
   protected final boolean wrapExtension;

   public ContentTypeDocumentRenamer(MimeTypeRegistry mimeTypeRegistry, boolean wrapExtension) {
      this.mimeTypeRegistry = notNull(mimeTypeRegistry, "mimeTypeRegistry");
      this.wrapExtension = wrapExtension;
   }

   public ContentTypeDocumentRenamer(MimeTypeRegistry mimeTypeRegistry) {
      this(mimeTypeRegistry, false);
   }

   public ContentTypeDocumentRenamer(boolean wrapExtension) {
      this(MIME_TYPE_REGISTRY, wrapExtension);
   }

   public ContentTypeDocumentRenamer() {
      this(MIME_TYPE_REGISTRY, false);
   }

   @Override
   public DocumentPath rename(DocumentEntry documentEntry) {
      List<String> presetExtensions = mimeTypeRegistry.extensions(documentEntry.metadata().contentType());

      if (presetExtensions.isEmpty()) {
         return documentEntry.documentId();
      } else {
         String currentExtension =
               FilenameUtils.getExtension(documentEntry.documentId().stringValue()).toLowerCase();

         if (presetExtensions.contains(currentExtension)) {
            return documentEntry.documentId();
         } else {
            String primaryPresetExtension = presetExtensions.get(0);
            String currentPath = documentEntry.documentId().stringValue();
            if (!wrapExtension) {
               currentPath = FilenameUtils.removeExtension(currentPath);
            }
            return DocumentPath.of(currentPath + "." + primaryPresetExtension);
         }

      }
   }
}
