/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalStateException;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent.LoadedDocumentContentBuilder;

public class DocumentTest {

   private static final ZonedDateTime FIXED_DATE =
         ZonedDateTime.of(LocalDateTime.of(2021, 11, 10, 14, 0, 0), ZoneId.of("UTC"));

   @BeforeAll
   public static void initializeApplicationClock() {
      ApplicationClock.setFixedClock(FIXED_DATE);
   }

   @Test
   public void testDocumentWhenNominal() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("", StandardCharsets.UTF_8)
            .build();

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("path/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo("document.pdf");
      assertThat(document.metadata().contentType()).isEqualTo(parseMimeType("application/pdf;charset=UTF-8"));
      assertThat(document.metadata().contentSize()).hasValue(0L);
      assertThat(document.content().contentSize()).hasValue(0L);
      assertThat(document.content().contentEncoding()).hasValue(StandardCharsets.UTF_8);
      assertThat(document.metadata().creationDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(document.metadata().lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
   }

   @Test
   public void testDocumentWhenBadParameters() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new DocumentBuilder().build())
            .withMessage(
                  "Invariant validation error in [DocumentMetadata[documentPath=<null>,contentType=application/octet-stream,contentSize=<null>,creationDate=%1$s,lastUpdateDate=%1$s,attributes={}]] context : "
                  + "{documentPath} 'documentPath' must not be null",
                  "2021-11-10T14:00:00Z");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new DocumentBuilder().documentId(DocumentPath.of("/")).build())
            .withMessage(
                  "Invariant validation error in [DocumentPath[value=/,newObject=false,relativePath=true]] context : "
                  + "{value} 'value=/' must have a filename component");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new DocumentBuilder().documentId(DocumentPath.of(" ")).build())
            .withMessage(
                  "Invariant validation error in [DocumentPath[value= ,newObject=false,relativePath=true]] context : "
                  + "{value} 'value.fileName' must not be blank");
   }

   @Test
   public void testDocumentMetadataWhenPath() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf"))
            .documentName("document2.pdf")
            .loadedContent("", StandardCharsets.UTF_8)
            .build();

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("document2.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo("document2.pdf");
      assertThat(document.metadata().contentType()).isEqualTo(parseMimeType("application/pdf;charset=UTF-8"));
   }

   @Test
   public void testDocumentWhenLoadedContentWithNoEncoding() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent(new ByteArrayInputStream("new content".getBytes(StandardCharsets.UTF_8)))
            .build();

      assertThat(document.metadata().contentType()).isEqualTo(parseMimeType("application/pdf"));
      assertThat(document.content().stringContent(StandardCharsets.UTF_8)).isEqualTo("new content");
      assertThat(document.content().contentEncoding()).isEmpty();

      assertThatIllegalStateException()
            .isThrownBy(() -> document.content().stringContent())
            .withMessage("No content encoding set, you should specify one to encode this content");
      assertThatIllegalStateException()
            .isThrownBy(() -> document.content().readerContent())
            .withMessage("No content encoding set, you should specify one to encode this content");
   }

   @Test
   public void testDocumentWhenStreamContentWithNoEncoding() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf"))
            .streamContent(new ByteArrayInputStream("new content".getBytes(StandardCharsets.UTF_8)))
            .build();

      assertThat(document.metadata().contentType()).isEqualTo(parseMimeType("application/pdf"));
      assertThat(document.content().stringContent(StandardCharsets.UTF_8)).isEqualTo("new content");
      assertThat(document.content().contentEncoding()).isEmpty();

      assertThatIllegalStateException()
            .isThrownBy(() -> document.content().stringContent())
            .withMessage("No content encoding set, you should specify one to encode this content");
      assertThatIllegalStateException()
            .isThrownBy(() -> document.content().readerContent())
            .withMessage("No content encoding set, you should specify one to encode this content");
   }

   @Test
   public void testDocumentSetLogicalName() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("", StandardCharsets.UTF_8)
            .build();

      document = document.documentName(Paths.get("document.txt"));

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("document.txt"));
      assertThat(document.metadata().documentName()).isEqualTo("document.txt");
      assertThat(document.metadata().contentType()).isEqualTo(parseMimeType("application/pdf;charset=UTF-8"));
   }

   @Test
   public void testDocumentSetContentType() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("", StandardCharsets.UTF_8)
            .build();

      document = document.contentType(parseMimeType("application/text"));

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("path/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo("document.pdf");
      assertThat(document
                       .metadata()
                       .contentType()).isEqualTo(parseMimeType("application/text;charset=UTF-8"));

      document = document.contentType(parseMimeType("application/text;charset=UTF-8"));

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("path/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo("document.pdf");
      assertThat(document
                       .metadata()
                       .contentType()).isEqualTo(parseMimeType("application/text;charset=UTF-8"));
   }

   @Test
   public void testDocumentSetContentTypeWhenUnmatchingContentType() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("", StandardCharsets.UTF_8)
            .build();

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> document.contentType(parseMimeType("application/text;charset=US-ASCII")))
            .withMessage(
                  "Invariant validation error in [DocumentPath[value=path/document.pdf,newObject=false,relativePath=true]] context : "
                  + "{metadata} 'metadata.contentType.charset=US-ASCII' must be equal to 'content.contentEncoding=UTF-8'");
   }

   @Test
   public void testDocumentSetAttributes() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("", StandardCharsets.UTF_8)
            .build();

      document = document.attributes(new HashMap<String, Object>() {{
         put("k1", 43);
         put("k2", "value2");
      }});

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().attributes()).containsOnly(Pair.of("k1", 43), Pair.of("k2", "value2"));

   }

   @Test
   public void testDocumentAddAttributes() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("", StandardCharsets.UTF_8)
            .build()
            .attributes(new HashMap<String, Object>() {{
               put("k1", 43);
               put("k2", "value2");
            }});

      document = document.addAttributes(new HashMap<String, Object>() {{put("k3", true);}});

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().attributes()).containsOnly(Pair.of("k1", 43),
                                                                Pair.of("k2", "value2"),
                                                                Pair.of("k3", true));

   }

   @Test
   public void testDocumentAddAttribute() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("", StandardCharsets.UTF_8)
            .build()
            .attributes(new HashMap<String, Object>() {{
               put("k1", 43);
               put("k2", "value2");
            }});

      document = document.addAttribute("k3", true);

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().attributes()).containsOnly(Pair.of("k1", 43),
                                                                Pair.of("k2", "value2"),
                                                                Pair.of("k3", true));

   }

   @Test
   public void testDocumentMetadataWhenUnmatchingContentSize() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new DocumentBuilder()
                  .<DocumentBuilder>reconstitute()
                  .documentId(DocumentPath.of("path/document.pdf"))
                  .loadedContent("content", StandardCharsets.UTF_8)
                  .metadata(new DocumentMetadataBuilder()
                                  .documentPath(Paths.get("document.pdf"))
                                  .contentSize(2L)
                                  .build())
                  .build())
            .withMessage(
                  "Invariant validation error in [DocumentPath[value=path/document.pdf,newObject=false,relativePath=true]] context : "
                  + "{metadata} 'metadata.contentSize=2' must be equal to 'content.contentSize=7'");
   }

   @Test
   public void testDocumentMetadataWhenUnmatchingContentTypeCharset() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf"))
                  .documentName(Paths.get("document.pdf"))
                  .loadedContent("content", StandardCharsets.UTF_8)
                  .contentType(parseMimeType("application/pdf;charset=US-ASCII"))
                  .build())
            .withMessage(
                  "Invariant validation error in [DocumentPath[value=path/document.pdf,newObject=false,relativePath=true]] context : "
                  + "'contentType.charset=US-ASCII' must be equal to 'contentEncoding=UTF-8'");
   }

   @Test
   public void testDocumentMetadataContentSizeWhenUnavailable() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("path", "document.pdf"))
            .streamContent(new ByteArrayInputStream("content".getBytes(StandardCharsets.UTF_8)))
            .build();

      assertThat(document.metadata().contentSize()).isEmpty();
   }

   @Test
   public void testDocumentMetadataContentSizeWhenAvailable() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf"))
            .streamContent(new ByteArrayInputStream("content".getBytes(StandardCharsets.UTF_8)), 42L)
            .documentName("document2.pdf")
            .build();

      assertThat(document.metadata().contentSize()).hasValue(42L);
   }

   @Test
   public void testDocumentWhenSetContent() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("content", StandardCharsets.UTF_8)
            .build();

      document = document.content(new LoadedDocumentContentBuilder()
                                        .content("new content", StandardCharsets.UTF_8)
                                        .build());

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("path/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo("document.pdf");
      assertThat(document.metadata().contentType()).isEqualTo(parseMimeType("application/pdf;charset=UTF-8"));
      assertThat(document.metadata().contentSize()).hasValue(11L);
      assertThat(document.content().stringContent()).isEqualTo("new content");
      assertThat(document.content().contentEncoding()).hasValue(StandardCharsets.UTF_8);
   }

   @Test
   public void testDocumentWhenSetContentWithDifferentEncoding() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf"))
            .content(new LoadedDocumentContentBuilder()
                           .content(new ByteArrayInputStream("new content".getBytes(StandardCharsets.US_ASCII)),
                                    StandardCharsets.US_ASCII)
                           .build())
            .build();

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("path/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo("document.pdf");
      assertThat(document
                       .metadata()
                       .contentType()).isEqualTo(parseMimeType("application/pdf;charset=US-ASCII"));
      assertThat(document.metadata().contentSize()).hasValue(11L);
      assertThat(document.content().stringContent()).isEqualTo("new content");
      assertThat(document.content().contentEncoding()).hasValue(StandardCharsets.US_ASCII);
   }

   @Test
   public void testDocumentDuplicateWhenNominal() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf"))
            .documentName("logicalPath/document.pdf")
            .loadedContent("content1", StandardCharsets.UTF_8)
            .build();

      ZonedDateTime pit2 = ZonedDateTime.now();
      ApplicationClock.setFixedClock(pit2);

      document = document.duplicate(DocumentPath.of("path/document2.pdf"));

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document2.pdf"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("logicalPath/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo("document.pdf");
      assertThat(document.metadata().contentType()).isEqualTo(parseMimeType("application/pdf;charset=UTF-8"));
      assertThat(document.metadata().contentSize()).hasValue(8L);
      assertThat(document.content().stringContent()).isEqualTo("content1");
      assertThat(document.content().contentEncoding()).hasValue(StandardCharsets.UTF_8);

      assertThat(document.metadata().creationDate()).hasValue(pit2.toInstant());
      assertThat(document.metadata().lastUpdateDate()).hasValue(pit2.toInstant());
   }

   @Test
   public void testDocumentDuplicateWhenSameDocumentId() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("content1", StandardCharsets.UTF_8)
            .build();

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> document.duplicate(DocumentPath.of("path/document.pdf")))
            .withMessage("Invariant validation error : "
                         + "'newDocumentId=DocumentPath[value=path/document.pdf,newObject=false,relativePath=true]' must not be equal to 'documentId=DocumentPath[value=path/document.pdf,newObject=false,relativePath=true]'");
   }

   @Test
   public void testDocumentToStringWhenHiddenContent() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("path", "document.pdf"))
            .content(new LoadedDocumentContentBuilder()
                           .content("A long or secret content", StandardCharsets.UTF_8)
                           .build())
            .build();

      assertThat(document.toString()).contains("DocumentContent[content=<hidden-value>");
   }

   @Test
   public void testDocumentStreamContentWhenReadMultiTimes() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("path", "document.pdf"))
            .streamContent("content", StandardCharsets.UTF_8)
            .build();

      assertThat(document.content().stringContent()).contains("content");
      assertThat(document.content().inputStreamContent()).hasContent("");
      assertThat(document.content().stringContent()).contains("");
      assertThat(document.content().inputStreamContent()).hasContent("");
   }

   @Test
   public void testDocumentLoadedContentWhenReadMultiTimes() {
      Document document = new DocumentBuilder().documentId(DocumentPath.of("path", "document.pdf"))
            .loadedContent("content", StandardCharsets.UTF_8)
            .build();

      assertThat(document.content().stringContent()).contains("content");
      assertThat(document.content().inputStreamContent()).hasContent("content");
      assertThat(document.content().stringContent()).contains("content");
      assertThat(document.content().inputStreamContent()).hasContent("content");
   }

   @Test
   public void testDocumentLoadContentWhenNominal() {
      Document preDocument = new DocumentBuilder().documentId(DocumentPath.of("path", "document.pdf"))
            .streamContent("content", StandardCharsets.UTF_8)
            .build();
      Document document = preDocument.loadContent();

      assertThat(document).isNotSameAs(preDocument);
      assertThat(document.content().stringContent()).contains("content");
      assertThat(document.content().inputStreamContent()).hasContent("content");
      assertThat(document.content().stringContent()).contains("content");
      assertThat(document.content().inputStreamContent()).hasContent("content");
   }

   @Test
   public void testDocumentLoadContentWhenAlreadyLoaded() {
      Document preDocument = new DocumentBuilder().documentId(DocumentPath.of("path", "document.pdf"))
            .loadedContent("content", StandardCharsets.UTF_8)
            .build();
      Document document = preDocument.loadContent();

      assertThat(document).isSameAs(preDocument);
      assertThat(document.content().stringContent()).contains("content");
      assertThat(document.content().inputStreamContent()).hasContent("content");
      assertThat(document.content().stringContent()).contains("content");
      assertThat(document.content().inputStreamContent()).hasContent("content");
   }

}