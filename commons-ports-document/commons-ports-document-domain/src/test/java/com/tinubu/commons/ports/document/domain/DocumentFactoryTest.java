/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.temporal.ChronoUnit;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.lang.datetime.ApplicationClock;

class DocumentFactoryTest {

   private static Path TEST_STORAGE_PATH;

   @BeforeAll
   public static void createStoragePath() {
      try {
         TEST_STORAGE_PATH = Files.createTempDirectory("chassis-document-factory-test");

         System.out.printf("Creating '%s' test storage path%n", TEST_STORAGE_PATH);
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   @AfterAll
   public static void deleteStoragePath() {
      try {
         System.out.printf("Deleting '%s' test storage path%n", TEST_STORAGE_PATH);

         FileUtils.deleteDirectory(TEST_STORAGE_PATH.toFile());
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   @Test
   public void testCreateFromFileWhenNominal() throws IOException {
      File testFile = TEST_STORAGE_PATH.resolve("file.txt").toFile();

      FileUtils.writeStringToFile(testFile, "content", StandardCharsets.UTF_8);

      Document document = new DocumentFactory().document(testFile,
                                                         DocumentPath.of("path/document-file.txt"),
                                                         StandardCharsets.UTF_8);

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document-file.txt"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("file.txt"));
      assertThat(document.metadata().documentName()).isEqualTo("file.txt");
      assertThat(document.metadata().contentType()).isEqualTo(parseMimeType("text/plain;charset=UTF-8"));
      assertThat(document.metadata().contentSize()).hasValue(7L);
      assertThat(document.content().contentSize()).hasValue(7L);
      assertThat(document.content().contentEncoding()).hasValue(StandardCharsets.UTF_8);
      assertThat(document.metadata().creationDate()).hasValueSatisfying(cd -> assertThat(cd).isAfter(
            ApplicationClock.nowAsInstant().minusSeconds(10)));
      assertThat(document.metadata().lastUpdateDate())
            .map(lud -> lud.truncatedTo(ChronoUnit.SECONDS))
            .hasValue(document
                            .metadata()
                            .creationDate()
                            .orElseThrow(() -> new IllegalStateException("No creation date"))
                            .truncatedTo(ChronoUnit.SECONDS));
   }

   @Test
   public void testCreateFromFileWhenNoDocumentEncoding() throws IOException {
      File testFile = TEST_STORAGE_PATH.resolve("file.txt").toFile();

      FileUtils.writeStringToFile(testFile, "content", StandardCharsets.UTF_8);

      Document document = new DocumentFactory().document(testFile, DocumentPath.of("path/document-file.txt"));

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document-file.txt"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("file.txt"));
      assertThat(document.metadata().documentName()).isEqualTo("file.txt");
      assertThat(document.metadata().contentType()).isEqualTo(parseMimeType("text/plain;charset=UTF-8"));
      assertThat(document.metadata().contentSize()).hasValue(7L);
      assertThat(document.content().contentSize()).hasValue(7L);
      assertThat(document.content().contentEncoding()).hasValue(StandardCharsets.UTF_8);
      assertThat(document.metadata().creationDate()).hasValueSatisfying(cd -> assertThat(cd).isAfter(
            ApplicationClock.nowAsInstant().minusSeconds(10)));
      assertThat(document.metadata().lastUpdateDate()).hasValueSatisfying(lud -> assertThat(lud).isAfter(
            ApplicationClock.nowAsInstant().minusSeconds(10)));
   }

   @Test
   public void testCreateFromFileWhenNotExistingFile() {
      File missingFile = TEST_STORAGE_PATH.resolve("missing").toFile();

      assertThatExceptionOfType(NoSuchFileException.class)
            .isThrownBy(() -> new DocumentFactory().document(missingFile,
                                                             DocumentPath.of("path/file.txt"),
                                                             StandardCharsets.UTF_8))
            .withMessage(missingFile.toString());
   }

   @Test
   public void testCreateFromFileWhenBadParameters() {
      File testFile = TEST_STORAGE_PATH.resolve("file.txt").toFile();

      assertThatNullPointerException()
            .isThrownBy(() -> new DocumentFactory().document(null,
                                                             DocumentPath.of("path/file.txt"),
                                                             StandardCharsets.UTF_8))
            .withMessage("'file' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> new DocumentFactory().document(testFile, null, StandardCharsets.UTF_8))
            .withMessage("'documentId' must not be null");
   }

}