/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toConcurrentMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;

class UnionDocumentRepositoryTest {

   @Test
   public void testOfEmpty() {
      assertThat(UnionDocumentRepository.ofEmpty()).isNotNull();
   }

   @Test
   public void testOfLayersWhenNominal() {
      TestDocumentRepository layer1 =
            new TestDocumentRepository("layer1", documentId("file1"), documentId("file2"));
      TestDocumentRepository layer2 =
            new TestDocumentRepository("layer2", documentId("file1"), documentId("file3"));

      assertThat(UnionDocumentRepository.ofLayers(layer1, layer2)).isNotNull();
      assertThat(UnionDocumentRepository.ofLayers(list(layer1, layer2))).isNotNull();
   }

   @Test
   public void testOfLayersWhenEmpty() {
      assertThat(UnionDocumentRepository.ofLayers()).isNotNull();
      assertThat(UnionDocumentRepository.ofLayers(list())).isNotNull();
   }

   @Test
   public void testOfLayersWhenNull() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> UnionDocumentRepository.ofLayers((DocumentRepository[]) null))
            .withMessage("Invariant validation error : 'layers' must not be null");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> UnionDocumentRepository.ofLayers((List<DocumentRepository>) null))
            .withMessage("Invariant validation error : 'layers' must not be null");
   }

   @Test
   public void testOfLayersWhenNullElement() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> UnionDocumentRepository.ofLayers((DocumentRepository) null))
            .withMessage("Invariant validation error : 'layers=[null]' > 'layers[0]' must not be null");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> UnionDocumentRepository.ofLayers(list((DocumentRepository) null)))
            .withMessage("Invariant validation error : 'layers=[null]' > 'layers[0]' must not be null");
   }

   @Test
   public void testFindDocumentByIdWhenNominal() {
      TestDocumentRepository layer1 =
            new TestDocumentRepository("layer1", documentId("file1"), documentId("file2"));
      TestDocumentRepository layer2 =
            new TestDocumentRepository("layer2", documentId("file1"), documentId("file3"));

      UnionDocumentRepository repository = UnionDocumentRepository.ofLayers(layer1, layer2);

      assertThat(repository.findDocumentById(documentId("file1"))).hasValueSatisfying(d -> assertThatDocumentEqualTo(
            d,
            "layer2",
            "file1"));
      assertThat(repository.findDocumentById(documentId("file2"))).hasValueSatisfying(d -> assertThatDocumentEqualTo(
            d,
            "layer1",
            "file2"));
      assertThat(repository.findDocumentById(documentId("file3"))).hasValueSatisfying(d -> assertThatDocumentEqualTo(
            d,
            "layer2",
            "file3"));
      assertThat(repository.findDocumentById(documentId("file4"))).isEmpty();
   }

   @Test
   public void testFindDocumentByIdWhenEmpty() {
      UnionDocumentRepository repository = UnionDocumentRepository.ofLayers();

      assertThat(repository.findDocumentById(documentId("file1"))).isEmpty();
   }

   @Test
   public void testFindDocumentEntryByIdWhenNominal() {
      TestDocumentRepository layer1 =
            new TestDocumentRepository("layer1", documentId("file1"), documentId("file2"));
      TestDocumentRepository layer2 =
            new TestDocumentRepository("layer2", documentId("file1"), documentId("file3"));

      UnionDocumentRepository repository = UnionDocumentRepository.ofLayers(layer1, layer2);

      assertThat(repository.findDocumentEntryById(documentId("file1"))).hasValueSatisfying(d -> assertThatDocumentEntryEqualTo(
            d,
            "file1"));
      assertThat(repository.findDocumentEntryById(documentId("file2"))).hasValueSatisfying(d -> assertThatDocumentEntryEqualTo(
            d,
            "file2"));
      assertThat(repository.findDocumentEntryById(documentId("file3"))).hasValueSatisfying(d -> assertThatDocumentEntryEqualTo(
            d,
            "file3"));
      assertThat(repository.findDocumentEntryById(documentId("file4"))).isEmpty();
   }

   @Test
   public void testFindDocumentEntryByIdWhenEmpty() {
      UnionDocumentRepository repository = UnionDocumentRepository.ofLayers();

      assertThat(repository.findDocumentEntryById(documentId("file1"))).isEmpty();
   }

   private void assertThatDocumentEqualTo(Document document, String expectedLayer, String expectedDocument) {
      assertThat(document.documentId().stringValue()).isEqualTo(expectedDocument);
      assertThat(document.content().stringContent()).isEqualTo(expectedLayer + "." + expectedDocument);
   }

   private void assertThatDocumentEntryEqualTo(DocumentEntry documentEntry, String expectedDocument) {
      assertThat(documentEntry.documentId().stringValue()).isEqualTo(expectedDocument);
   }

   private DocumentPath documentId(String documentPath) {
      return DocumentPath.of(documentPath);
   }

   public static class TestDocumentRepository extends AbstractDocumentRepository {

      private final String name;
      private final Map<DocumentPath, Document> documentStorage;

      public TestDocumentRepository(String name, DocumentPath... documentIds) {
         this.name = name;
         this.documentStorage = immutable(stream(documentIds)
                                                .map(this::document)
                                                .collect(toConcurrentMap(Document::documentId, identity())));
      }

      @Override
      public Optional<Document> openDocument(DocumentPath documentId,
                                             boolean overwrite,
                                             boolean append,
                                             OpenDocumentMetadata metadata) {
         throw new UnsupportedOperationException();
      }

      @Override
      public Optional<Document> findDocumentById(DocumentPath documentId) {
         return nullable(documentStorage.get(documentId));
      }

      @Override
      public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
         return nullable(documentStorage.get(documentId)).map(Document::documentEntry);
      }

      private Document document(DocumentPath documentId) {
         return new DocumentBuilder()
               .documentId(documentId)
               .streamContent(name + "." + documentId.stringValue(), StandardCharsets.UTF_8)
               .build();
      }

      @Override
      public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                           Specification<DocumentEntry> specification) {
         validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();
         notNull(specification, "specification");

         return stream(documentStorage.values()).filter(d -> specification.satisfiedBy(d.documentEntry()));
      }

      @Override
      public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                      Specification<DocumentEntry> specification) {
         validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();
         notNull(specification, "specification");

         return stream(documentStorage.values())
               .map(Document::documentEntry)
               .filter(specification::satisfiedBy);
      }

      @Override
      public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
         throw new UnsupportedOperationException();
      }

      @Override
      public Optional<DocumentEntry> deleteDocument(DocumentPath documentId) {
         throw new UnsupportedOperationException();
      }

      /**
       * URI format : {@code test:<relative-path>}
       */
      @Override
      public boolean supportsUri(URI documentUri) {
         return documentUri.isAbsolute() && documentUri.getScheme().equals("test") && documentUri.isOpaque();
      }

      @Override
      public URI toUri(DocumentPath documentId) {
         try {
            return new URI("test", documentId.stringValue(), null);
         } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
         }
      }

      @Override
      public Optional<Document> findDocumentByUri(URI documentUri) {
         satisfies(documentUri, this::supportsUri, "documentUri", "must be supported : " + documentUri);

         return findDocumentById(documentId(documentUri));
      }

      @Override
      public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
         satisfies(documentUri, this::supportsUri, "documentUri", "must be supported : " + documentUri);

         return findDocumentEntryById(documentId(documentUri));
      }

      private DocumentPath documentId(URI documentUri) {
         return DocumentPath.of(documentUri.getSchemeSpecificPart());
      }
   }
}