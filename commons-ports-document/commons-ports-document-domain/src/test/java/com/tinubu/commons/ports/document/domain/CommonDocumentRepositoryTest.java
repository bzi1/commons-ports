/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_PDF;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.peek;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.time.temporal.ChronoUnit.MILLIS;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.clearInvocations;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import org.assertj.core.api.InstanceOfAssertFactories;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder;
import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ddd2.domain.specification.CompositeSpecification;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.domain.type.Entity;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.domain.type.Value;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.lang.mimetype.MimeTypeFactory;
import com.tinubu.commons.lang.util.FileUtils;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntryCriteria.DocumentEntryCriteriaBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent.LoadedDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata.OpenDocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.event.DocumentAccessed;
import com.tinubu.commons.ports.document.domain.event.DocumentDeleted;
import com.tinubu.commons.ports.document.domain.event.DocumentRepositoryEvent;
import com.tinubu.commons.ports.document.domain.event.DocumentSaved;

// FIXME add sameRepositoryAs tests
// FIXME add attributes tests
// FIXME add creation/lastUpdateDate tests
// FIXME test DocumentAccessException (simulate IO errors ?)
// FIXME test DocumentAccessException -> events not sent
public abstract class CommonDocumentRepositoryTest {

   private static final ZonedDateTime FIXED_DATE =
         ZonedDateTime.of(LocalDateTime.of(2021, 11, 10, 14, 0, 0), ZoneId.of("UTC"));

   @BeforeAll
   public static void setApplicationClock() {
      ApplicationClock.setFixedClock(FIXED_DATE);
   }

   protected abstract DocumentRepository documentRepository();

   /**
    * Instruments document repository under test with a mocked listener for all events.
    * This function should not be called several times in a given test method, instead use
    * {@link Mockito#clearInvocations(Object[])} to reset verify invocation counts.
    *
    * @return mocked listener for further test verification
    */
   @SuppressWarnings("unchecked")
   protected DomainEventListener<DocumentRepositoryEvent> instrumentDocumentRepositoryListeners() {
      DomainEventListener<DocumentRepositoryEvent> domainEventListener =
            (DomainEventListener<DocumentRepositoryEvent>) mock(DomainEventListener.class);
      for (Class<? extends DocumentRepositoryEvent> event : list(DocumentSaved.class,
                                                                 DocumentDeleted.class,
                                                                 DocumentAccessed.class)) {
         documentRepository().registerEventListener(event, domainEventListener);
      }

      return domainEventListener;
   }

   /**
    * Abstracted ZipTransformer from implementing modules, because of dependency cycle issue on transformer
    * module.
    */
   protected abstract Document zipTransformer(DocumentPath zipPath, List<Document> documents);

   protected static Path path(String path) {
      return nullable(path).map(Paths::get).orElse(null);
   }

   protected static URI uri(String uri) {
      try {
         return new URI(uri);
      } catch (URISyntaxException e) {
         throw new IllegalArgumentException(e);
      }
   }

   /**
    * Returns {@code true} if repository updates the creation date when overwriting files, which is not the
    * expected behavior.
    *
    * @return {@code true} if repository updates the creation date when overwriting files
    */
   public boolean isUpdatingCreationDateOnOverwrite() {
      return false;
   }

   /**
    * Returns {@code true} if repository can store and restore content length independently of raw content.
    *
    * @return {@code true} if repository can read content length
    */
   public boolean isSupportingContentLength() {
      return true;
   }

   /**
    * Returns {@code true} if repository supports URI.
    *
    * @return {@code true} if repository supports URI
    */
   public boolean isSupportingUri() {
      return true;
   }

   @Test
   public void testFindDocumentByIdWhenNominal() {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DocumentRepositoryEvent> domainEventListener =
            instrumentDocumentRepositoryListeners();

      try {
         createDocuments(documentPath);

         clearInvocations(domainEventListener);

         assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(document -> {
            assertThat(document)
                  .usingComparator(contentAgnosticDocumentComparator())
                  .isEqualTo(new DocumentBuilder()
                                   .<DocumentBuilder>reconstitute()
                                   .documentId(testDocument)
                                   .streamContent(new ByteArrayInputStream("path/pathfile1.pdf".getBytes(UTF_8)),
                                                  document.metadata().contentEncoding().orElse(null),
                                                  isSupportingContentLength() ? 18L : null)
                                   .metadata(new DocumentMetadataBuilder()
                                                   .<DocumentMetadataBuilder>reconstitute()
                                                   .documentPath(documentPath)
                                                   .creationDate(stubCreationDate())
                                                   .lastUpdateDate(stubLastUpdateDate())
                                                   .contentSize(isSupportingContentLength() ? 18L : null)
                                                   .contentType(parseMimeType("application/pdf"), UTF_8)
                                                   .chain(synchronizeExpectedMetadata(document.metadata()))
                                                   .build())
                                   .build());

         });

         verify(domainEventListener).accept(any(DocumentAccessed.class));
         verifyNoMoreInteractions(domainEventListener);

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testFindDocumentByIdWhenNotFound() {
      DomainEventListener<DocumentRepositoryEvent> domainEventListener =
            instrumentDocumentRepositoryListeners();

      assertThat(documentRepository().findDocumentById(DocumentPath.of("unknown.pdf"))).isEmpty();
      assertThat(documentRepository().findDocumentById(DocumentPath.of("path/unknown.pdf"))).isEmpty();
      assertThat(documentRepository().findDocumentById(DocumentPath.of("nonexistentpath/unknown.pdf"))).isEmpty();

      verifyNoMoreInteractions(domainEventListener);

   }

   @Test
   public void testFindDocumentsBySpecificationWhenNominal() {
      DomainEventListener<DocumentRepositoryEvent> domainEventListener =
            instrumentDocumentRepositoryListeners();

      String[] createDocuments = new String[] {
            "rootfile1.pdf",
            "rootfile2.txt",
            "path/pathfile1.pdf",
            "path/pathfile2.txt",
            "path/subpath/subpathfile1.pdf",
            "path/subpath/subpathfile2.txt" };

      try {
         createDocuments(createDocuments);

         clearInvocations(domainEventListener);

         List<Document> documents = documentRepository()
               .findDocumentsBySpecification(new DocumentEntryCriteriaBuilder()
                                                   .documentParentPath(CriterionBuilder.equal(path("path")))
                                                   .build())
               .collect(toList());

         verify(domainEventListener, times(2)).accept(any(DocumentAccessed.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(documents)
               .extracting(entry -> entry.documentId().stringValue())
               .containsExactlyInAnyOrder("path/pathfile1.pdf", "path/pathfile2.txt");
      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testSaveDocumentWhenNominal() {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DocumentRepositoryEvent> domainEventListener =
            instrumentDocumentRepositoryListeners();

      deleteDocuments(documentPath);

      try {
         clearInvocations(domainEventListener);

         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument)
                                                            .streamContent("content", UTF_8)
                                                            .build(), true)).isPresent();

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(document -> {
            assertThat(document)
                  .usingComparator(contentAgnosticDocumentComparator())
                  .isEqualTo(new DocumentBuilder()
                                   .<DocumentBuilder>reconstitute()
                                   .documentId(testDocument)
                                   .streamContent("content".getBytes(UTF_8),
                                                  document.metadata().contentEncoding().orElse(null))
                                   .metadata(new DocumentMetadataBuilder()
                                                   .<DocumentMetadataBuilder>reconstitute()
                                                   .documentPath(documentPath)
                                                   .contentType(parseMimeType("application/pdf"), UTF_8)
                                                   .contentSize(7L)
                                                   .chain(synchronizeExpectedMetadata(document.metadata()))
                                                   .build())
                                   .build());
         });

      } finally {
         deleteDocuments(documentPath);
      }

   }

   @Test
   public void testSaveDocumentWhenOverwrite() throws InterruptedException {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DocumentRepositoryEvent> domainEventListener =
            instrumentDocumentRepositoryListeners();

      deleteDocuments(documentPath);

      assertThat(documentRepository().findDocumentById(testDocument)).isEmpty();

      try {
         Document document =
               new DocumentBuilder().documentId(testDocument).loadedContent("content", UTF_8).build();

         clearInvocations(domainEventListener);

         Optional<DocumentEntry> initialSave = documentRepository().saveDocument(document, true);
         assertThat(initialSave).isPresent();

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         long waitTimeMs = 1200; // >ait at least 1s to ensure overridden file's lastUpdateDate > creationDate
         Thread.sleep(waitTimeMs);
         ApplicationClock.setFixedClock(FIXED_DATE.plus(waitTimeMs, MILLIS));

         clearInvocations(domainEventListener);

         Optional<DocumentEntry> overwriteSave = documentRepository().saveDocument(document, true);

         assertThat(overwriteSave)
               .as("document save with overwrite flag must return value even if file already exist")
               .isPresent();

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         if (!isUpdatingCreationDateOnOverwrite()) {
            assertThat(overwriteSave
                             .get()
                             .metadata()
                             .creationDate()).satisfiesAnyOf(cd -> assertThat(cd).isEmpty(),
                                                             cd -> assertThat(cd)
                                                                   .as("overridden file creationDate must not be updated")
                                                                   .hasValue(initialSave
                                                                                   .get()
                                                                                   .metadata()
                                                                                   .creationDate()
                                                                                   .get()));
            assertThat(overwriteSave
                             .get()
                             .metadata()
                             .lastUpdateDate()).satisfiesAnyOf(cd -> assertThat(cd).isEmpty(),
                                                               cd -> assertThat(cd)
                                                                     .as("overridden file lastUpdateDate must be > creationDate")
                                                                     .get(InstanceOfAssertFactories.INSTANT)
                                                                     .isAfter(initialSave
                                                                                    .get()
                                                                                    .metadata()
                                                                                    .lastUpdateDate()
                                                                                    .get()));
         }

         clearInvocations(domainEventListener);

         assertThat(documentRepository().saveDocument(document, false))
               .as("document save without overwrite flag must return empty value if file already exist")
               .isEmpty();

         verifyNoMoreInteractions(domainEventListener);

      } finally {
         deleteDocuments(documentPath);
      }

   }

   @Test
   public void testDeleteDocumentWhenNominal() {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DocumentRepositoryEvent> domainEventListener =
            instrumentDocumentRepositoryListeners();

      DocumentEntry document = createDocument(documentPath);

      try {
         clearInvocations(domainEventListener);

         assertThat(documentRepository().deleteDocument(testDocument)).hasValueSatisfying(documentEntry -> {
            assertThat(documentEntry).isEqualTo(new DocumentEntryBuilder()
                                                      .<DocumentEntryBuilder>reconstitute()
                                                      .documentId(testDocument)
                                                      .metadata(new DocumentMetadataBuilder()
                                                                      .<DocumentMetadataBuilder>reconstitute()
                                                                      .documentPath(documentPath)
                                                                      .contentSize(18L)
                                                                      .contentType(parseMimeType(
                                                                            "application/pdf"), UTF_8)
                                                                      .chain(synchronizeExpectedMetadata(
                                                                            document.metadata()))
                                                                      .build())
                                                      .build());
         });

         verify(domainEventListener).accept(any(DocumentDeleted.class));
         verifyNoMoreInteractions(domainEventListener);

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testDeleteDocumentWhenNotExist() {
      Path documentPath = path("path/subpath/missing.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DocumentRepositoryEvent> domainEventListener =
            instrumentDocumentRepositoryListeners();

      assertThat(documentRepository().deleteDocument(testDocument)).isEmpty();

      verifyNoMoreInteractions(domainEventListener);
   }

   @Test
   public void testFindDocumentEntryByIdWhenNominal() {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DocumentRepositoryEvent> domainEventListener =
            instrumentDocumentRepositoryListeners();

      try {
         createDocuments(documentPath);

         clearInvocations(domainEventListener);

         Optional<DocumentEntry> document = documentRepository().findDocumentEntryById(testDocument);

         verify(domainEventListener).accept(any(DocumentAccessed.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(document).hasValue(new DocumentEntryBuilder()
                                             .<DocumentEntryBuilder>reconstitute()
                                             .documentId(testDocument)
                                             .metadata(new DocumentMetadataBuilder()
                                                             .<DocumentMetadataBuilder>reconstitute()
                                                             .documentPath(documentPath)
                                                             .contentSize(isSupportingContentLength()
                                                                          ? 18L
                                                                          : null)
                                                             .contentType(parseMimeType("application/pdf"),
                                                                          UTF_8)
                                                             .chain(synchronizeExpectedMetadata(document
                                                                                                      .map(DocumentEntry::metadata)
                                                                                                      .orElse(
                                                                                                            null)))
                                                             .build())
                                             .build());
      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testFindDocumentEntryByIdWhenNotFound() {
      DomainEventListener<DocumentRepositoryEvent> domainEventListener =
            instrumentDocumentRepositoryListeners();

      DocumentPath missingDocument = DocumentPath.of(path("missing.pdf"));
      DocumentPath missingDocumentInPath = DocumentPath.of(path("path/missing.pdf"));
      DocumentPath missingDocumentInMissingPath = DocumentPath.of(path("notexist/missing.pdf"));

      deleteDocuments(missingDocument, missingDocumentInPath, missingDocumentInMissingPath);

      clearInvocations(domainEventListener);

      assertThat(documentRepository().findDocumentEntryById(missingDocument)).isEmpty();
      assertThat(documentRepository().findDocumentEntryById(missingDocumentInPath)).isEmpty();
      assertThat(documentRepository().findDocumentEntryById(missingDocumentInMissingPath)).isEmpty();

      verifyNoMoreInteractions(domainEventListener);
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenNominal() {
      DomainEventListener<DocumentRepositoryEvent> domainEventListener =
            instrumentDocumentRepositoryListeners();

      String[] createDocuments = new String[] {
            "rootfile1.pdf",
            "rootfile2.txt",
            "path/pathfile1.pdf",
            "path/pathfile2.txt",
            "path/subpath/subpathfile1.pdf",
            "path/subpath/subpathfile2.txt" };

      try {
         createDocuments(createDocuments);

         clearInvocations(domainEventListener);

         List<DocumentEntry> documentEntries = documentRepository()
               .findDocumentEntriesBySpecification(new DocumentEntryCriteriaBuilder()
                                                         .documentParentPath(CriterionBuilder.equal(path(
                                                               "path")))
                                                         .build())
               .collect(toList());

         verify(domainEventListener, times(2)).accept(any(DocumentAccessed.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(documentEntries)
               .extracting(entry -> entry.documentId().stringValue())
               .containsExactlyInAnyOrder("path/pathfile1.pdf", "path/pathfile2.txt");
      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenDocumentEntrySpecificationWithNoDirectory() {
      String[] createDocuments = new String[] {
            "path/pathfile1.pdf",
            "path/pathfile2.txt",
            "path/subpath/subpathfile1.pdf",
            "path/subpath/subpathfile2.txt" };

      try {
         createDocuments(createDocuments);

         Stream<DocumentEntry> documentEntries =
               documentRepository().findDocumentEntriesBySpecification(path("path"),
                                                                       new DocumentEntryCriteriaBuilder().build());

         assertThat(documentEntries)
               .extracting(entry -> entry.documentId().stringValue())
               .containsExactlyInAnyOrder("path/pathfile1.pdf",
                                          "path/pathfile2.txt",
                                          "path/subpath/subpathfile1.pdf",
                                          "path/subpath/subpathfile2.txt");

      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenDocumentEntrySpecificationWithDirectoriesAndExtensions() {
      String[] createDocuments = new String[] {
            "rootfile1.pdf",
            "rootfile2.txt",
            "path/pathfile1.pdf",
            "path/pathfile2.txt",
            "path/subpath/subpathfile1.pdf",
            "path/subpath/subpathfile2.txt",
            "otherpath/subpath/subpathfile1.pdf",
            "otherpath/subpath/subpathfile2.txt" };

      try {
         createDocuments(createDocuments);

         Stream<DocumentEntry> documentEntries =
               documentRepository().findDocumentEntriesBySpecification(new DocumentEntryCriteriaBuilder()
                                                                             .documentParentPath(
                                                                                   CriterionBuilder.in(path(
                                                                                                             "path"),
                                                                                                       path("otherpath/subpath")))
                                                                             .documentExtension(
                                                                                   CriterionBuilder.equal(
                                                                                         "pdf"))
                                                                             .build());

         assertThat(documentEntries)
               .extracting(entry -> entry.documentId().stringValue())
               .containsExactlyInAnyOrder("path/pathfile1.pdf", "otherpath/subpath/subpathfile1.pdf");

      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenBaseDirectory() {
      String[] createDocuments = new String[] {
            "path/pathfile1.pdf",
            "path/pathfile2.txt",
            "path/subpath/subpathfile1.pdf",
            "path/subpath/subpathfile2.txt" };

      try {
         createDocuments(createDocuments);

         Stream<DocumentEntry> documentEntries =
               documentRepository().findDocumentEntriesBySpecification(path("path"),
                                                                       new DocumentEntryCriteriaBuilder().build());

         assertThat(documentEntries)
               .extracting(entry -> entry.documentId().stringValue())
               .containsExactlyInAnyOrder("path/pathfile1.pdf",
                                          "path/pathfile2.txt",
                                          "path/subpath/subpathfile1.pdf",
                                          "path/subpath/subpathfile2.txt");

      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenAbsoluteBaseDirectory() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> documentRepository()
                  .findDocumentEntriesBySpecification(path("/path"),
                                                      new DocumentEntryCriteriaBuilder().build())
                  .count())
            .withMessage("Invariant validation error : 'basePath=/path' must not be absolute path");
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenTraversalBaseDirectory() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> documentRepository()
                  .findDocumentEntriesBySpecification(path("path/.."),
                                                      new DocumentEntryCriteriaBuilder().build())
                  .count())
            .withMessage("Invariant validation error : 'basePath=path/..' must not have traversal paths");
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenArbitrarySpecification() {
      String[] createDocuments = new String[] {
            "rootfile1.pdf",
            "rootfile2.txt",
            "path/pathfile1.pdf",
            "path/pathfile2.txt",
            "path/subpath/subpathfile1.pdf",
            "path/subpath/subpathfile2.txt" };

      try {
         createDocuments(createDocuments);

         Specification<DocumentEntry> arbitrarySpecification =
               entry -> (entry.documentId().sameValueAs(DocumentPath.of("path/pathfile1.pdf")) || entry
                     .documentId()
                     .stringValue()
                     .contains("/subpath/") || entry.metadata().contentSize().orElse(0L) < 16) && entry
                              .documentId()
                              .stringValue()
                              .endsWith(".pdf") && !entry.documentId().stringValue().startsWith("otherpath/");
         Stream<DocumentEntry> documentEntries =
               documentRepository().findDocumentEntriesBySpecification(arbitrarySpecification);

         assertThat(documentEntries)
               .extracting(entry -> entry.documentId().stringValue())
               .containsExactlyInAnyOrder("rootfile1.pdf",
                                          "path/pathfile1.pdf",
                                          "path/subpath/subpathfile1.pdf");

      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenCompositeSpecification() {
      String[] createDocuments = new String[] {
            "rootfile1.pdf",
            "rootfile2.txt",
            "path/pathfile1.pdf",
            "path/pathfile2.txt",
            "path/subpath/subpathfile1.pdf",
            "path/subpath/subpathfile2.txt" };

      try {
         createDocuments(createDocuments);

         CompositeSpecification<DocumentEntry> documentIdSpecification =
               entry -> entry.documentId().sameValueAs(DocumentPath.of("path/pathfile1.pdf"));
         CompositeSpecification<DocumentEntry> subpathSpecification =
               entry -> entry.documentId().stringValue().contains("/subpath/");
         CompositeSpecification<DocumentEntry> smallContentSpecification =
               entry -> entry.metadata().contentSize().orElse(0L) < 16;
         CompositeSpecification<DocumentEntry> pdfSpecification =
               entry -> entry.documentId().stringValue().endsWith(".pdf");
         CompositeSpecification<DocumentEntry> otherpathSpecification =
               entry -> entry.documentId().stringValue().startsWith("otherpath/");

         CompositeSpecification<DocumentEntry> compositeSpecification = documentIdSpecification
               .or(subpathSpecification)
               .or(smallContentSpecification)
               .and(pdfSpecification)
               .and(otherpathSpecification.not());

         Stream<DocumentEntry> documentEntries =
               documentRepository().findDocumentEntriesBySpecification(compositeSpecification);

         assertThat(documentEntries)
               .extracting(entry -> entry.documentId().stringValue())
               .containsExactlyInAnyOrder("rootfile1.pdf",
                                          "path/pathfile1.pdf",
                                          "path/subpath/subpathfile1.pdf");

      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testToUriWhenNullParameter() {
      if (!isSupportingUri()) return;

      assertThatNullPointerException()
            .isThrownBy(() -> documentRepository().toUri(null))
            .withMessage("'documentId' must not be null");
   }

   @Test
   public void testSupportsUriWhenUnsupportedScheme() {
      if (!isSupportingUri()) return;

      assertThat(documentRepository().supportsUri(uri("notexists:/path/test.txt"))).isFalse();
      assertThat(documentRepository().supportsUri(uri("notexists:path/test.txt"))).isFalse();
   }

   @Test
   public void testFindDocumentByUriWhenNominal() {
      if (!isSupportingUri()) return;

      DomainEventListener<DocumentRepositoryEvent> domainEventListener =
            instrumentDocumentRepositoryListeners();

      DocumentPath documentPath = DocumentPath.of("path/pathfile2.txt");

      try {
         createDocuments(documentPath);

         URI documentUri = documentRepository().toUri(documentPath);

         clearInvocations(domainEventListener);

         assertThat(documentRepository().findDocumentByUri(documentUri)).hasValueSatisfying(document -> {
            assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/pathfile2.txt"));
            assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("path/pathfile2.txt"));
         });

         verify(domainEventListener).accept(any(DocumentAccessed.class));
         verifyNoMoreInteractions(domainEventListener);

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testFindDocumentByUriWhenNullParameter() {
      if (!isSupportingUri()) return;

      DomainEventListener<DocumentRepositoryEvent> domainEventListener =
            instrumentDocumentRepositoryListeners();

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> documentRepository().findDocumentByUri(null))
            .withMessage("Invariant validation error : 'documentUri' must not be null");

      verifyNoMoreInteractions(domainEventListener);
   }

   @Test
   public void testFindDocumentByUriWhenUnsupportedUri() {
      if (!isSupportingUri()) return;

      DomainEventListener<DocumentRepositoryEvent> domainEventListener =
            instrumentDocumentRepositoryListeners();

      assertThatIllegalArgumentException()
            .isThrownBy(() -> documentRepository().findDocumentByUri(uri("notexists:/path/test.txt")))
            .withMessage("'documentUri' must be supported : notexists:/path/test.txt");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> documentRepository().findDocumentByUri(uri("notexists:path/test.txt")))
            .withMessage("'documentUri' must be supported : notexists:path/test.txt");

      verifyNoMoreInteractions(domainEventListener);
   }

   @Test
   public void testFindDocumentByUriWhenZipTransformerUriAdapter() {
      if (!isSupportingUri()) return;

      DocumentPath documentPath = DocumentPath.of("path/pathfile2.txt");
      DocumentPath zipPath = DocumentPath.of("test.zip");

      try {
         createDocument(path("path/pathfile2.txt"));
         DocumentEntry zipDocument = createZipDocument(zipPath, documentPath);
         URI zipUri = documentRepository().toUri(zipDocument.documentId());

         Optional<Document> entryDocument =
               documentRepository().findDocumentByUri(uri("zip:" + zipUri + "!/path/pathfile2.txt"));

         assertThat(entryDocument).hasValueSatisfying(d -> {
            assertThat(d.documentId()).isEqualTo(DocumentPath.of("path/pathfile2.txt"));
            assertThat(d.metadata().documentPath()).isEqualTo(Paths.get("path/pathfile2.txt"));
         });

         assertThatIllegalArgumentException()
               .as("Unsupported delegated URI must not validate, because you have to ensure URI is supported before calling find*")
               .isThrownBy(() -> documentRepository().findDocumentByUri(uri("zip:file:/not/exists.jar!/")))
               .withMessage("'documentUri' must be supported : zip:file:/not/exists.jar!/");

         assertThat(documentRepository().findDocumentByUri(uri("zip:" + zipUri + ".notexist" + "!/")))
               .as("Supported delegated URI, but nonexistent document must return empty optional")
               .isEmpty();

         Optional<Document> document = documentRepository().findDocumentByUri(uri("zip:" + zipUri + "!/"));

         assertThat(document).hasValueSatisfying(d -> {
            assertThat(d.documentId()).isEqualTo(DocumentPath.of("test.zip"));
            assertThat(d.metadata().documentPath()).isEqualTo(Paths.get("test.zip"));
         });

         assertThatExceptionOfType(DocumentAccessException.class)
               .as("Non existent entry in archive must throw an access exception")
               .isThrownBy(() -> documentRepository().findDocumentByUri(uri("zip:"
                                                                            + zipUri
                                                                            + "!/not/exists")))
               .withMessage("'not/exists' entry not found in 'test.zip'");
      } finally {
         deleteDocuments(documentPath, zipPath);
      }
   }

   @Test
   public void testFindDocumentByUriWhenJarTransformerUriAdapter() {
      if (!isSupportingUri()) return;

      DocumentPath documentPath = DocumentPath.of("path/pathfile2.txt");
      DocumentPath jarPath = DocumentPath.of("test.jar");

      try {
         createDocument(documentPath);
         DocumentEntry jarDocument = createZipDocument(jarPath, documentPath);
         URI jarUri = documentRepository().toUri(jarDocument.documentId());

         Optional<Document> entryDocument =
               documentRepository().findDocumentByUri(uri("jar:" + jarUri + "!/path/pathfile2.txt"));

         assertThat(entryDocument).hasValueSatisfying(d -> {
            assertThat(d.documentId()).isEqualTo(DocumentPath.of("path/pathfile2.txt"));
            assertThat(d.metadata().documentPath()).isEqualTo(Paths.get("path/pathfile2.txt"));
         });

         assertThatIllegalArgumentException()
               .as("Unsupported delegated URI must not validate, because you have to ensure URI is supported before calling find*")
               .isThrownBy(() -> documentRepository().findDocumentByUri(uri("jar:file:/not/exists.jar!/")))
               .withMessage("'documentUri' must be supported : jar:file:/not/exists.jar!/");

         assertThat(documentRepository().findDocumentByUri(uri("jar:" + jarUri + ".notexist" + "!/")))
               .as("Supported delegated URI, but nonexistent document must return empty optional")
               .isEmpty();

         Optional<Document> document = documentRepository().findDocumentByUri(uri("jar:" + jarUri + "!/"));

         assertThat(document).hasValueSatisfying(d -> {
            assertThat(d.documentId()).isEqualTo(DocumentPath.of("test.jar"));
            assertThat(d.metadata().documentPath()).isEqualTo(Paths.get("test.jar"));
         });

         assertThatExceptionOfType(DocumentAccessException.class)
               .as("Non existent entry in archive must throw an access exception")
               .isThrownBy(() -> documentRepository().findDocumentByUri(uri("jar:"
                                                                            + jarUri
                                                                            + "!/not/exists")))
               .withMessage("'not/exists' entry not found in 'test.jar'");

      } finally {
         deleteDocuments(documentPath, jarPath);
      }
   }

   @Test
   public void testFindDocumentByUriWhenJarTransformerUriAdapterWithWarUrl() {
      if (!isSupportingUri()) return;

      DocumentPath documentPath = DocumentPath.of("path/pathfile2.txt");
      DocumentPath warPath = DocumentPath.of("test.war");

      try {
         createDocument(documentPath);
         DocumentEntry warDocument = createZipDocument(warPath, documentPath);
         URI jarUri = documentRepository().toUri(warDocument.documentId());

         Optional<Document> entryDocument =
               documentRepository().findDocumentByUri(uri("jar:" + jarUri + "!/path/pathfile2.txt"));

         assertThat(entryDocument).hasValueSatisfying(d -> {
            assertThat(d.documentId()).isEqualTo(DocumentPath.of("path/pathfile2.txt"));
            assertThat(d.metadata().documentPath()).isEqualTo(Paths.get("path/pathfile2.txt"));
         });

         Optional<Document> document = documentRepository().findDocumentByUri(uri("jar:" + jarUri + "!/"));

         assertThat(document).hasValueSatisfying(d -> {
            assertThat(d.documentId()).isEqualTo(DocumentPath.of("test.war"));
            assertThat(d.metadata().documentPath()).isEqualTo(Paths.get("test.war"));
         });

         assertThatExceptionOfType(DocumentAccessException.class)
               .as("Non existent entry in archive must throw an access exception")
               .isThrownBy(() -> documentRepository().findDocumentByUri(uri("jar:"
                                                                            + jarUri
                                                                            + "!/not/exists")))
               .withMessage("'not/exists' entry not found in 'test.war'");

      } finally {
         deleteDocuments(documentPath, warPath);
      }
   }

   @Test
   public void testFindDocumentByUriWhenJarTransformerUriAdapterWithEarUrl() {
      if (!isSupportingUri()) return;

      DocumentPath documentPath = DocumentPath.of("path/pathfile2.txt");
      DocumentPath earPath = DocumentPath.of("test.ear");

      try {
         createDocument(documentPath);
         DocumentEntry earDocument = createZipDocument(earPath, documentPath);
         URI jarUri = documentRepository().toUri(earDocument.documentId());

         Optional<Document> entryDocument =
               documentRepository().findDocumentByUri(uri("jar:" + jarUri + "!/path/pathfile2.txt"));

         assertThat(entryDocument).hasValueSatisfying(d -> {
            assertThat(d.documentId()).isEqualTo(DocumentPath.of("path/pathfile2.txt"));
            assertThat(d.metadata().documentPath()).isEqualTo(Paths.get("path/pathfile2.txt"));
         });

         Optional<Document> document = documentRepository().findDocumentByUri(uri("jar:" + jarUri + "!/"));

         assertThat(document).hasValueSatisfying(d -> {
            assertThat(d.documentId()).isEqualTo(DocumentPath.of("test.ear"));
            assertThat(d.metadata().documentPath()).isEqualTo(Paths.get("test.ear"));
         });

         assertThatExceptionOfType(DocumentAccessException.class)
               .as("Non existent entry in archive must throw an access exception")
               .isThrownBy(() -> documentRepository().findDocumentByUri(uri("jar:"
                                                                            + jarUri
                                                                            + "!/not/exists")))
               .withMessage("'not/exists' entry not found in 'test.ear'");

      } finally {
         deleteDocuments(documentPath, earPath);
      }
   }

   @Test
   public void testFindDocumentByUriWhenJarTransformerUriAdapterWithBadUrl() {
      if (!isSupportingUri()) return;

      DocumentPath documentPath = DocumentPath.of("path/pathfile2.txt");
      DocumentPath badPath = DocumentPath.of("test.bad");

      try {
         createDocument(documentPath);
         DocumentEntry badDocument = createZipDocument(badPath, documentPath);
         URI badUri = documentRepository().toUri(badDocument.documentId());

         assertThatExceptionOfType(InvariantValidationException.class)
               .isThrownBy(() -> documentRepository().findDocumentByUri(uri("jar:"
                                                                            + badUri
                                                                            + "!/path/pathfile2.txt")))
               .withMessage(
                     "Invariant validation error : 'document.contentType=application/octet-stream' must be in [application/zip,application/java-archive]");
      } finally {
         deleteDocuments(documentPath, badPath);
      }
   }

   @Test
   public void testOpenDocumentWhenNominal() {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DocumentRepositoryEvent> domainEventListener =
            instrumentDocumentRepositoryListeners();

      try {
         OpenDocumentMetadata metadata = new OpenDocumentMetadataBuilder()
               .documentPath(documentPath)
               .contentType(MimeTypeFactory.mimeType(APPLICATION_PDF, UTF_8))
               .build();

         Optional<Document> newDocument =
               peek(documentRepository().openDocument(testDocument, true, false, metadata),
                    writeContent("content"));

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(newDocument).isPresent();

         Optional<Document> document = documentRepository().findDocumentById(testDocument);

         assertThat(document)
               .usingValueComparator(contentAgnosticDocumentComparator())
               .hasValue(new DocumentBuilder().<DocumentBuilder>reconstitute().documentId(testDocument)
                               .metadata(new DocumentMetadataBuilder()
                                               .<DocumentMetadataBuilder>reconstitute()
                                               .documentPath(documentPath)
                                               .contentSize(isSupportingContentLength() ? 7L : null)
                                               .contentType(parseMimeType("application/pdf"), UTF_8)
                                               .chain(synchronizeExpectedMetadata(document
                                                                                        .map(Document::metadata)
                                                                                        .orElse(null)))
                                               .build())
                               .content(new LoadedDocumentContentBuilder()
                                              .content("content", UTF_8)
                                              .chain(synchronizeExpectedContent(document
                                                                                      .map(Document::content)
                                                                                      .orElse(null)))
                                              .build())
                               .build());
      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testOpenDocumentWhenOverwriteAndAppend() {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DocumentRepositoryEvent> domainEventListener =
            instrumentDocumentRepositoryListeners();

      try {
         OpenDocumentMetadata metadata =
               new OpenDocumentMetadataBuilder().contentType(APPLICATION_PDF).build();

         Optional<Document> newDocument =
               peek(documentRepository().openDocument(testDocument, true, true, metadata),
                    writeContent("content"));

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(newDocument).isPresent();
         assertThatDocumentContent(testDocument, "content");

         clearInvocations(domainEventListener);

         Optional<Document> existingDocument =
               peek(documentRepository().openDocument(testDocument, true, true, metadata),
                    writeContent("append"));

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(existingDocument).isPresent();
         assertThatDocumentContent(testDocument, "contentappend");

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testOpenDocumentWhenOverwriteAndNotAppend() {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DocumentRepositoryEvent> domainEventListener =
            instrumentDocumentRepositoryListeners();

      try {
         OpenDocumentMetadata metadata =
               new OpenDocumentMetadataBuilder().contentType(APPLICATION_PDF).build();

         Optional<Document> newDocument =
               peek(documentRepository().openDocument(testDocument, true, false, metadata),
                    writeContent("content"));

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(newDocument).isPresent();
         assertThatDocumentContent(testDocument, "content");

         clearInvocations(domainEventListener);

         Optional<Document> existingDocument =
               peek(documentRepository().openDocument(testDocument, true, false, metadata),
                    writeContent("overwrite"));

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(existingDocument).isPresent();
         assertThatDocumentContent(testDocument, "overwrite");

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testOpenDocumentWhenNotOverwriteAndAppend() {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DocumentRepositoryEvent> domainEventListener =
            instrumentDocumentRepositoryListeners();

      try {
         OpenDocumentMetadata metadata =
               new OpenDocumentMetadataBuilder().contentType(APPLICATION_PDF).build();

         Optional<Document> newDocument =
               peek(documentRepository().openDocument(testDocument, false, true, metadata),
                    writeContent("content"));

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(newDocument).isPresent();
         assertThatDocumentContent(testDocument, "content");

         clearInvocations(domainEventListener);

         Optional<Document> existingDocument =
               peek(documentRepository().openDocument(testDocument, false, true, metadata),
                    writeContent("append"));

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(existingDocument).isPresent();
         assertThatDocumentContent(testDocument, "contentappend");

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testOpenDocumentWhenNotOverwriteAndNotAppend() {
      Path documentPath = path("path/pathfile1.pdf");
      DocumentPath testDocument = DocumentPath.of(documentPath);

      DomainEventListener<DocumentRepositoryEvent> domainEventListener =
            instrumentDocumentRepositoryListeners();

      try {
         OpenDocumentMetadata metadata =
               new OpenDocumentMetadataBuilder().contentType(APPLICATION_PDF).build();

         Optional<Document> newDocument =
               peek(documentRepository().openDocument(testDocument, false, false, metadata),
                    writeContent("content"));

         verify(domainEventListener).accept(any(DocumentSaved.class));
         verifyNoMoreInteractions(domainEventListener);

         assertThat(newDocument).isPresent();
         assertThatDocumentContent(testDocument, "content");

         clearInvocations(domainEventListener);

         Optional<Document> existingDocument =
               peek(documentRepository().openDocument(testDocument, false, false, metadata),
                    writeContent("overwrite"));

         verifyNoMoreInteractions(domainEventListener);

         assertThat(existingDocument).isEmpty();

      } finally {
         deleteDocuments(documentPath);
      }
   }

   protected Consumer<Document> writeContent(String content) {
      return d -> {
         try (BufferedWriter writer = new BufferedWriter(d.content().writerContent())) {
            writer.write(content);
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
      };
   }

   protected Consumer<Document> writeZeroContent(int length) {
      return d -> {
         try (BufferedWriter writer = new BufferedWriter(d.content().writerContent())) {
            int bufferLength = 16 * 1024;
            char[] buffer = new char[bufferLength];
            Arrays.fill(buffer, '0');

            for (int i = 0; i < length / bufferLength; i++) {
               writer.write(buffer, 0, bufferLength);
            }
            writer.write(buffer, 0, length % bufferLength);
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
      };
   }

   protected void assertThatDocumentContent(DocumentPath documentPath, String expected) {
      Optional<Document> readDocument = documentRepository().findDocumentById(documentPath);

      assertThat(readDocument).hasValueSatisfying(rd -> {
         assertThat(rd.content().stringContent(UTF_8)).isEqualTo(expected);
      });
   }

   /**
    * Creates a zip archive from specified files.
    *
    * @param zipPath zip archive path
    * @param documentPaths documents to archive
    *
    * @return zip archive document entry
    *
    * @implSpec for testing purpose, if zip path extension is {@code bad}, zip content-type is reset to
    *       test unzipping archive with bad content-type.
    */
   protected DocumentEntry createZipDocument(DocumentPath zipPath, DocumentPath... documentPaths) {
      List<Document> documents = list(stream(documentPaths).map(documentPath -> documentRepository()
            .findDocumentById(documentPath)
            .orElseThrow(IllegalStateException::new)));

      Document zipDocument = zipTransformer(zipPath, documents);

      if ("bad".equals(FileUtils.fileExtension(zipPath.value()))) {
         zipDocument = DocumentBuilder
               .from(zipDocument)
               .metadata(DocumentMetadataBuilder.from(zipDocument.metadata()).contentType(null, null).build())
               .build();
      }

      return createDocument(zipDocument);
   }

   protected DocumentBuilder stubDocument(DocumentPath documentId,
                                          UnaryOperator<DocumentMetadataBuilder> metadataBuilderOperation,
                                          UnaryOperator<InputStreamDocumentContentBuilder> contentBuilderOperation) {
      String content = documentId.stringValue();

      return new DocumentBuilder()
            .<DocumentBuilder>reconstitute()
            .documentId(documentId)
            .metadata(new DocumentMetadataBuilder()
                            .<DocumentMetadataBuilder>reconstitute()
                            .documentPath(documentId.value())
                            .contentType(UTF_8)
                            .contentSize((long) content.length())
                            .creationDate(stubCreationDate())
                            .lastUpdateDate(stubLastUpdateDate())
                            .chain(metadataBuilderOperation)
                            .build())
            .content(new InputStreamDocumentContentBuilder()
                           .<InputStreamDocumentContentBuilder>reconstitute()
                           .content(content, UTF_8)
                           .chain(contentBuilderOperation)
                           .build());
   }

   private Instant stubLastUpdateDate() {
      return ApplicationClock.nowAsInstant().plusSeconds(60);
   }

   private Instant stubCreationDate() {
      return ApplicationClock.nowAsInstant();
   }

   protected DocumentBuilder stubDocument(DocumentPath documentId,
                                          UnaryOperator<DocumentMetadataBuilder> metadataBuilderOperation) {
      return stubDocument(documentId, metadataBuilderOperation, UnaryOperator.identity());
   }

   protected DocumentBuilder stubDocument(DocumentPath documentId) {
      return stubDocument(documentId, UnaryOperator.identity(), UnaryOperator.identity());
   }

   /**
    * Copy some metadata from actual metadata when metadata are not supported.
    */
   protected UnaryOperator<DocumentMetadataBuilder> synchronizeExpectedMetadata(DocumentMetadata actual) {
      return builder -> builder
            .creationDate(actual.creationDate().orElse(null))
            .lastUpdateDate(actual.lastUpdateDate().orElse(null));
   }

   /**
    * Copy some content metadata from actual content when metadata are not supported.
    */
   protected UnaryOperator<LoadedDocumentContentBuilder> synchronizeExpectedContent(DocumentContent actual) {
      return builder -> builder;
   }

   protected Optional<Document> filterDocument(Stream<Document> documents, DocumentPath documentId) {
      return documents.filter(d -> d.documentId().equals(documentId)).findAny();
   }

   protected Optional<DocumentEntry> filterDocumentEntry(Stream<DocumentEntry> documents,
                                                         DocumentPath documentId) {
      return documents.filter(d -> d.documentId().equals(documentId)).findAny();
   }

   protected DocumentEntry createDocument(Document document) {
      return documentRepository()
            .saveDocument(document, true)
            .orElseThrow(() -> new IllegalStateException(String.format("Can't create '%s' document",
                                                                       document.documentId().stringValue())));
   }

   protected DocumentEntry createDocument(Path documentPath) {
      return createDocument(stubDocument(DocumentPath.of(documentPath)).build());
   }

   protected DocumentEntry createDocument(String documentPath) {
      return createDocument(path(documentPath));
   }

   protected DocumentEntry createDocument(DocumentPath documentPath) {
      return createDocument(documentPath.value());
   }

   protected List<DocumentEntry> createDocuments(Path... paths) {
      return createDocuments(list(paths));
   }

   protected List<DocumentEntry> createDocuments(DocumentPath... paths) {
      return createDocuments(list(stream(paths).map(DocumentPath::value)));
   }

   protected List<DocumentEntry> createDocuments(String... paths) {
      return createDocuments(list(stream(paths).map(Paths::get)));
   }

   protected List<DocumentEntry> createDocuments(List<Path> paths) {
      return list(stream(paths).map(this::createDocument));
   }

   protected void deleteDocument(DocumentPath documentId) {
      documentRepository().deleteDocument(documentId);
   }

   protected void deleteDocuments(DocumentPath... documentIds) {
      stream(documentIds).forEach(this::deleteDocument);
   }

   protected void deleteDocuments(List<Path> paths) {
      stream(paths).map(DocumentPath::of).forEach(this::deleteDocument);
   }

   protected void deleteDocuments(String... paths) {
      stream(paths).map(DocumentPath::of).forEach(this::deleteDocument);
   }

   protected void deleteDocuments(Path... paths) {
      stream(paths).map(DocumentPath::of).forEach(this::deleteDocument);
   }

   protected <T extends Value> Comparator<? super T> valueComparator() {
      return (d1, d2) -> d1.sameValueAs(d2) ? 0 : -1;
   }

   protected <T extends Entity<ID>, ID extends Id> Comparator<? super T> entityComparator() {
      return (d1, d2) -> d1.sameIdentityAs(d2) ? 0 : -1;
   }

   protected <T extends Entity<ID>, ID extends Id> Comparator<? super T> entityComparatorByValue() {
      return (d1, d2) -> d1.sameValueAs(d2) ? 0 : -1;
   }

   /**
    * Specialized document comparator that ignores if content is a stream or loaded content.
    *
    * @param <T> document type
    *
    * @return document comparator
    */
   protected <T extends Document> Comparator<? super T> contentAgnosticDocumentComparator() {
      return (d1, d2) -> d1.documentId.sameValueAs(d2.documentId)
                         && d1.metadata.sameValueAs(d2.metadata)
                         && d1.content.contentEncoding().equals(d2.content.contentEncoding())
                         && d1.content.contentSize().equals(d2.content.contentSize())
                         && Arrays.equals(d1.content.content(), d2.content.content()) ? 0 : -1;
   }

}