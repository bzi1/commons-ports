/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;

public class DocumentMetadataTest {

   private static final ZonedDateTime FIXED_DATE =
         ZonedDateTime.of(LocalDateTime.of(2021, 11, 10, 14, 0, 0), ZoneId.of("UTC"));

   @BeforeAll
   public static void setApplicationClock() {
      ApplicationClock.setFixedClock(FIXED_DATE);
   }

   @Test
   public void testDocumentMetadataWhenNewDocument() {
      DocumentMetadata metadata =
            stubMetadata().documentPath("document.pdf").contentType(StandardCharsets.UTF_8).build();

      assertThat(metadata.documentPath()).isEqualTo(Paths.get("document.pdf"));
      assertThat(metadata.documentName()).isEqualTo("document.pdf");
      assertThat(metadata.contentType()).isEqualTo(parseMimeType("application/pdf;charset=UTF-8"));
      assertThat(metadata.contentSize()).isEmpty();
      assertThat(metadata.creationDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(metadata.lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
   }

   private DocumentMetadataBuilder stubMetadata() {
      Instant now = ApplicationClock.nowAsInstant();
      return new DocumentMetadataBuilder()
            .<DocumentMetadataBuilder>reconstitute()
            .creationDate(now)
            .lastUpdateDate(now);
   }

   @Test
   public void testDocumentMetadataWhenInvalidDocumentPath() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> stubMetadata().documentPath(null).build())
            .withMessage(
                  "Invariant validation error in [DocumentMetadata[documentPath=<null>,contentType=application/octet-stream,contentSize=<null>,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}]] context : {documentPath} 'documentPath' must not be null");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> stubMetadata().documentPath("/").build())
            .withMessage(
                  "Invariant validation error in [DocumentMetadata[documentPath=/,contentType=application/octet-stream,contentSize=<null>,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}]] context : {documentPath} 'documentPath=/' must not be absolute path");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> stubMetadata().documentPath("").build())
            .withMessage(
                  "Invariant validation error in [DocumentMetadata[documentPath=,contentType=application/octet-stream,contentSize=<null>,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}]] context : {documentPath} 'documentPath' must not be empty");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> stubMetadata().documentPath("path/ ").build())
            .withMessage(
                  "Invariant validation error in [DocumentMetadata[documentPath=path/ ,contentType=application/octet-stream,contentSize=<null>,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}]] context : {documentPath} 'documentPath.fileName' must not be blank");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> stubMetadata().documentPath(StringUtils.repeat("x", 260)).build())
            .withMessage(
                  "Invariant validation error in [DocumentMetadata[documentPath=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx...,contentType=application/octet-stream,contentSize=<null>,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}]] context : {documentPath} 'documentPath.fileName.length=260' must be less than or equal to 'DOCUMENT_NAME_MAX_LENGTH=255'");
   }

   @Test
   public void testDocumentMetadataWhenInvalidDates() {
      assertThat(new DocumentMetadataBuilder()
                       .<DocumentMetadataBuilder>reconstitute()
                       .documentPath("document.pdf")
                       .contentType(StandardCharsets.UTF_8)
                       .creationDate(null)
                       .lastUpdateDate(null)
                       .build()).isNotNull();

      assertThat(new DocumentMetadataBuilder()
                       .<DocumentMetadataBuilder>reconstitute()
                       .documentPath("document.pdf")
                       .contentType(StandardCharsets.UTF_8)
                       .creationDate(null)
                       .lastUpdateDate(LocalDateTime.of(2021, 11, 10, 14, 0, 0).toInstant(ZoneOffset.UTC))
                       .build()).isNotNull();

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new DocumentMetadataBuilder()
                  .<DocumentMetadataBuilder>reconstitute()
                  .documentPath("document.pdf")
                  .contentType(StandardCharsets.UTF_8)
                  .creationDate(LocalDateTime.of(2021, 11, 10, 14, 0, 0).toInstant(ZoneOffset.UTC))
                  .lastUpdateDate(LocalDateTime.of(2021, 10, 10, 14, 0, 0).toInstant(ZoneOffset.UTC))
                  .build())
            .withMessage(
                  "Invariant validation error in [DocumentMetadata[documentPath=document.pdf,contentType=application/pdf;charset=UTF-8,contentSize=<null>,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-10-10T14:00:00Z,attributes={}]] context : {lastUpdateDate} 'lastUpdateDate=2021-10-10T14:00:00Z' must not be before 'creationDate=2021-11-10T14:00:00Z'");
   }

   @Test
   public void testDocumentMetadataWhenAttributes() {
      DocumentMetadata metadata = stubMetadata()
            .documentPath("document.pdf")
            .contentType(StandardCharsets.UTF_8)
            .attributes(new HashMap<String, Object>() {{
               put("k1", 43);
               put("k2", "value2");
            }})
            .addAttribute("k3", true)
            .build();

      assertThat(metadata.attributes()).containsOnly(Pair.of("k1", 43), Pair.of("k2", "value2"),
                                                     Pair.of("k3", true));
   }

   @Test
   public void testDocumentMetadataWhenContentType() {
      DocumentMetadata metadata = stubMetadata()
            .documentPath("document.pdf")
            .contentType(parseMimeType("application/pdf;charset=UTF-8"))
            .build();

      assertThat(metadata.contentType()).isEqualTo(parseMimeType("application/pdf;charset=UTF-8"));
   }

   @Test
   public void testDocumentMetadataWhenContentTypeAndCharset() {
      DocumentMetadata metadata = stubMetadata()
            .documentPath("document.pdf")
            .contentType(parseMimeType("application/pdf"), StandardCharsets.UTF_8)
            .build();

      assertThat(metadata.contentType()).isEqualTo(parseMimeType("application/pdf;charset=UTF-8"));
   }

   @Test
   public void testDocumentMetadataWhenCharsetOnly() {
      DocumentMetadata metadata =
            stubMetadata().documentPath("document.pdf").contentType(StandardCharsets.UTF_8).build();

      assertThat(metadata.contentType()).isEqualTo(parseMimeType("application/pdf;charset=UTF-8"));
   }

   @Test
   public void testDocumentMetadataWhenMismatchingContentTypeCharset() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> stubMetadata()
                  .documentPath("document.pdf")
                  .contentType(parseMimeType("application/pdf;charset=UTF-8"), StandardCharsets.US_ASCII)
                  .build())
            .withMessage(
                  "Invariant validation error : 'contentType.charset=UTF-8' must be equal to 'contentEncoding=US-ASCII'");
   }

}