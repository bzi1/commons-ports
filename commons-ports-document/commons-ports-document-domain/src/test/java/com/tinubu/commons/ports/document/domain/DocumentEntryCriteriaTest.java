/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;

import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntryCriteria.DocumentEntryCriteriaBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;

// FIXME remaining criterion tests
public class DocumentEntryCriteriaTest {

   @BeforeAll
   public static void setToStringStyle() {
      ToStringBuilder.setDefaultStyle(ToStringStyle.SHORT_PREFIX_STYLE);
   }

   @Test
   public void testSatisfiedByPathCriterionWhenNominal() {
      DocumentEntryCriteria specification =
            new DocumentEntryCriteriaBuilder().documentPath(CriterionBuilder.equal(Paths.get("path/file")))
            .build();

      assertThat(specification.satisfiedBy(new DocumentEntryBuilder().documentId(DocumentPath.of("path/file"))
                                                 .metadata(documentMetadata("path/file"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(DocumentPath.of("path/subpath/file"))
                                                 .metadata(documentMetadata("path/subpath/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(DocumentPath.of("otherpath/file"))
                                                 .metadata(documentMetadata("otherpath/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder().documentId(DocumentPath.of("file"))
                                                 .metadata(documentMetadata("file"))
                                                 .build())).isFalse();
   }

   @Test
   public void testSatisfiedByPathCriterionWhenBadParameters() {
      assertThatNoException().isThrownBy(() -> new DocumentEntryCriteriaBuilder()
            .documentPath(CriterionBuilder.equal(Paths.get("")))
            .build());
      assertThatNoException().isThrownBy(() -> new DocumentEntryCriteriaBuilder()
            .documentPath(CriterionBuilder.equal(Paths.get(".")))
            .build());
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new DocumentEntryCriteriaBuilder()
                  .documentPath(CriterionBuilder.equal(Paths.get("/path")))
                  .build())
            .withMessage(
                  "Invariant validation error in [DocumentEntryCriteria[documentPath=PathCriterion[operator=EQUAL,operands=[/path],flags=Flags[]],documentPathDepth=<null>,documentParentPath=<null>,documentName=<null>,documentExtension=<null>,contentType=<null>,contentSize=<null>,creationDate=<null>,lastUpdateDate=<null>]] context : "
                  + "{documentPath} 'documentPath.operands=[/path]' > 'documentPath.operands[0]=/path' must not be absolute path");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new DocumentEntryCriteriaBuilder()
                  .documentPath(CriterionBuilder.equal(Paths.get("../path")))
                  .build())
            .withMessage(
                  "Invariant validation error in [PathCriterion[operator=EQUAL,operands=[../path],flags=Flags[]]] context : {operands} 'operands=[../path]' > 'operands[0]=../path' must not have traversal paths");
   }

   @Test
   public void testSatisfiedByPathCriterionWhenAbsolutePath() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new DocumentEntryCriteriaBuilder()
                  .documentPath(CriterionBuilder.equal(Paths.get("/path")))
                  .build())
            .withMessage(
                  "Invariant validation error in [DocumentEntryCriteria[documentPath=PathCriterion[operator=EQUAL,operands=[/path],flags=Flags[]],documentPathDepth=<null>,documentParentPath=<null>,documentName=<null>,documentExtension=<null>,contentType=<null>,contentSize=<null>,creationDate=<null>,lastUpdateDate=<null>]] context : {documentPath} 'documentPath.operands=[/path]' > 'documentPath.operands[0]=/path' must not be absolute path");
   }

   @Test
   public void testSatisfiedByPathCriterionWhenSlashTerminatedPath() {
      DocumentEntryCriteria specification = new DocumentEntryCriteriaBuilder()
            .documentPath(CriterionBuilder.equal(Paths.get("path/subpath/file/")))
            .build();

      assertThat(specification.satisfiedBy(new DocumentEntryBuilder().documentId(DocumentPath.of("path/file"))
                                                 .metadata(documentMetadata("path/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(DocumentPath.of("path/subpath/file"))
                                                 .metadata(documentMetadata("path/subpath/file"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(DocumentPath.of("otherpath/file"))
                                                 .metadata(documentMetadata("otherpath/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder().documentId(DocumentPath.of("file"))
                                                 .metadata(documentMetadata("file"))
                                                 .build())).isFalse();
   }

   @Test
   public void testSatisfiedByPathCriterionWhenNoDirectory() {
      DocumentEntryCriteria specification = new DocumentEntryCriteriaBuilder().build();

      assertThat(specification.satisfiedBy(new DocumentEntryBuilder().documentId(DocumentPath.of("path/file"))
                                                 .metadata(documentMetadata("path/file"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(DocumentPath.of("path/subpath/file"))
                                                 .metadata(documentMetadata("path/subpath/file"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(DocumentPath.of("otherpath/file"))
                                                 .metadata(documentMetadata("otherpath/file"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder().documentId(DocumentPath.of("file"))
                                                 .metadata(documentMetadata("file"))
                                                 .build())).isTrue();
   }

   @Test
   public void testSatisfiedByPathCriterionWhenMultipleDirectories() {
      DocumentEntryCriteria specification = new DocumentEntryCriteriaBuilder()
            .documentPath(CriterionBuilder.in(Paths.get("path/file"), Paths.get("otherpath/file")))
            .build();

      assertThat(specification.satisfiedBy(new DocumentEntryBuilder().documentId(DocumentPath.of("path/file"))
                                                 .metadata(documentMetadata("path/file"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(DocumentPath.of("path/subpath/file"))
                                                 .metadata(documentMetadata("path/subpath/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(DocumentPath.of("otherpath/file"))
                                                 .metadata(documentMetadata("otherpath/file"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder().documentId(DocumentPath.of("file"))
                                                 .metadata(documentMetadata("file"))
                                                 .build())).isFalse();
   }

   @Test
   public void testSatisfiedByPathCriterionWhenSubDirectories() {
      DocumentEntryCriteria specification = new DocumentEntryCriteriaBuilder()
            .documentPath(CriterionBuilder.equal(Paths.get("path/subpath/file")))
            .build();

      assertThat(specification.satisfiedBy(new DocumentEntryBuilder().documentId(DocumentPath.of("path/file"))
                                                 .metadata(documentMetadata("path/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(DocumentPath.of("path/subpath/file"))
                                                 .metadata(documentMetadata("path/subpath/file"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(DocumentPath.of("otherpath/path/subpath/file"))
                                                 .metadata(documentMetadata("otherpath/path/subpath/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(DocumentPath.of("otherpath/file"))
                                                 .metadata(documentMetadata("otherpath/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder().documentId(DocumentPath.of("file"))
                                                 .metadata(documentMetadata("file"))
                                                 .build())).isFalse();
   }

   @Test
   public void testSatisfiedByPathDepthCriterionWhenNominal() {
      DocumentEntryCriteria specification =
            new DocumentEntryCriteriaBuilder().documentPathDepth(CriterionBuilder.equal(1)).build();

      assertThat(specification.satisfiedBy(new DocumentEntryBuilder().documentId(DocumentPath.of("path/file"))
                                                 .metadata(documentMetadata("path/file"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(DocumentPath.of("path/subpath/file"))
                                                 .metadata(documentMetadata("path/subpath/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder().documentId(DocumentPath.of("file"))
                                                 .metadata(documentMetadata("file"))
                                                 .build())).isFalse();
   }

   @Test
   public void testSatisfiedByPathDepthCriterionWhenZero() {
      DocumentEntryCriteria specification =
            new DocumentEntryCriteriaBuilder().documentPathDepth(CriterionBuilder.equal(0)).build();

      assertThat(specification.satisfiedBy(new DocumentEntryBuilder().documentId(DocumentPath.of("path/file"))
                                                 .metadata(documentMetadata("path/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(DocumentPath.of("path/subpath/file"))
                                                 .metadata(documentMetadata("path/subpath/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder().documentId(DocumentPath.of("file"))
                                                 .metadata(documentMetadata("file"))
                                                 .build())).isTrue();
   }

   @Test
   public void testSatisfiedByExtensionCriterionWhenNominal() {
      DocumentEntryCriteria specification =
            new DocumentEntryCriteriaBuilder().documentExtension(CriterionBuilder.equal("ext")).build();

      assertThat(specification.satisfiedBy(new DocumentEntryBuilder().documentId(DocumentPath.of("path/file"))
                                                 .metadata(documentMetadata("path/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(DocumentPath.of("path/file.ext"))
                                                 .metadata(documentMetadata("path/file.ext"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(DocumentPath.of("path/file.otherext"))
                                                 .metadata(documentMetadata("path/file.otherext"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder().documentId(DocumentPath.of("file"))
                                                 .metadata(documentMetadata("file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder().documentId(DocumentPath.of("file.ext"))
                                                 .metadata(documentMetadata("file.ext"))
                                                 .build())).isTrue();
   }

   @Test
   public void testSatisfiedByExtensionCriterionWhenEmptyExtension() {
      DocumentEntryCriteria specification =
            new DocumentEntryCriteriaBuilder().documentExtension(CriterionBuilder.equal("")).build();

      assertThat(specification.satisfiedBy(new DocumentEntryBuilder().documentId(DocumentPath.of("path/file"))
                                                 .metadata(documentMetadata("path/file"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(DocumentPath.of("path/file.ext"))
                                                 .metadata(documentMetadata("path/file.ext"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder().documentId(DocumentPath.of("file"))
                                                 .metadata(documentMetadata("file"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder().documentId(DocumentPath.of("file.ext"))
                                                 .metadata(documentMetadata("file.ext"))
                                                 .build())).isFalse();
   }

   @Nested
   public class SatisfiedBySubPath {

      @Test
      public void testSatisfiedBySubPathWhenNominal() {
         DocumentEntryCriteria specification = new DocumentEntryCriteriaBuilder()
               .documentPath(CriterionBuilder.equal(Paths.get("subpath/path")))
               .build();

         assertThat(specification.satisfiedBySubPath(Paths.get("path"))).isFalse();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path/otherpath"))).isFalse();
      }

      @Test
      public void testSatisfiedBySubPathWhenBadParameters() {
         DocumentEntryCriteria specification = new DocumentEntryCriteriaBuilder()
               .documentPath(CriterionBuilder.equal(Paths.get("subpath/path")))
               .build();

         assertThatExceptionOfType(InvariantValidationException.class)
               .isThrownBy(() -> specification.satisfiedBySubPath(null))
               .withMessage("Invariant validation error : 'documentSubPath' must not be null");
         assertThatNoException().isThrownBy(() -> specification.satisfiedBySubPath(Paths.get("")));
         assertThatNoException().isThrownBy(() -> specification.satisfiedBySubPath(Paths.get(".")));
         assertThatExceptionOfType(InvariantValidationException.class)
               .isThrownBy(() -> specification.satisfiedBySubPath(Paths.get("/")))
               .withMessage("Invariant validation error : 'documentSubPath=/' must not be absolute path");
      }

      @Test
      public void testSatisfiedBySubPathWhenSlashTerminatedPath() {
         DocumentEntryCriteria specification = new DocumentEntryCriteriaBuilder()
               .documentPath(CriterionBuilder.equal(Paths.get("subpath/path/")))
               .build();

         assertThat(specification.satisfiedBySubPath(Paths.get("path"))).isFalse();
         assertThat(specification.satisfiedBySubPath(Paths.get("path/"))).isFalse();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path/"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path/otherpath"))).isFalse();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path/otherpath/"))).isFalse();
      }

      @Test
      public void testSatisfiedBySubPathWhenNoDocumentPathCriterion() {
         DocumentEntryCriteria specification =
               new DocumentEntryCriteriaBuilder().documentExtension(CriterionBuilder.equal("pdf")).build();

         assertThat(specification.satisfiedBySubPath(Paths.get("path"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path/otherpath"))).isTrue();
      }

      @Test
      public void testSatisfiedBySubPathWhenEmptyCriteria() {
         DocumentEntryCriteria specification = new DocumentEntryCriteriaBuilder().build();

         assertThat(specification.satisfiedBySubPath(Paths.get("path"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path/otherpath"))).isTrue();
      }

      @Test
      public void testSatisfiedBySubPathWhenEqualCriterion() {
         DocumentEntryCriteria specification = new DocumentEntryCriteriaBuilder()
               .documentPath(CriterionBuilder.equal(Paths.get("subpath/path")))
               .build();

         assertThat(specification.satisfiedBySubPath(Paths.get("path"))).isFalse();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path/otherpath"))).isFalse();
      }

      @Test
      public void testSatisfiedBySubPathWhenNotEqualCriterion() {
         DocumentEntryCriteria specification = new DocumentEntryCriteriaBuilder()
               .documentPath(CriterionBuilder.notEqual(Paths.get("subpath/path")))
               .build();

         assertThat(specification.satisfiedBySubPath(Paths.get("path"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path"))).isFalse();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path/otherpath"))).isTrue();
      }

      @Test
      public void testSatisfiedBySubPathWhenInCriterion() {
         DocumentEntryCriteria specification = new DocumentEntryCriteriaBuilder()
               .documentPath(CriterionBuilder.in(Paths.get("subpath/path"), Paths.get("subpath/otherpath")))
               .build();

         assertThat(specification.satisfiedBySubPath(Paths.get("path"))).isFalse();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/otherpath"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path/otherpath"))).isFalse();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/otherpath/otherpath"))).isFalse();
      }

      @Test
      public void testSatisfiedBySubPathWhenNotInCriterion() {
         DocumentEntryCriteria specification = new DocumentEntryCriteriaBuilder()
               .documentPath(CriterionBuilder.notIn(Paths.get("subpath/path"),
                                                    Paths.get("subpath/otherpath")))
               .build();

         assertThat(specification.satisfiedBySubPath(Paths.get("path"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path"))).isFalse();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/otherpath"))).isFalse();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path/otherpath"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/otherpath/otherpath"))).isTrue();
      }

      @Test
      public void testSatisfiedBySubPathWhenDefinedCriterion() {
         DocumentEntryCriteria specification =
               new DocumentEntryCriteriaBuilder().documentPath(CriterionBuilder.defined()).build();

         assertThat(specification.satisfiedBySubPath(Paths.get("path"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path/otherpath"))).isTrue();
      }

      @Test
      public void testSatisfiedBySubPathWhenUndefinedCriterion() {
         DocumentEntryCriteria specification =
               new DocumentEntryCriteriaBuilder().documentPath(CriterionBuilder.undefined()).build();

         assertThat(specification.satisfiedBySubPath(Paths.get("path"))).isFalse();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path"))).isFalse();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path/otherpath"))).isFalse();
      }

      @Test
      public void testSatisfiedBySubPathWhenFalseCriterion() {
         DocumentEntryCriteria specification =
               new DocumentEntryCriteriaBuilder().documentPath(CriterionBuilder.alwaysFalse()).build();

         assertThat(specification.satisfiedBySubPath(Paths.get("path"))).isFalse();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path"))).isFalse();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path/otherpath"))).isFalse();
      }

      @Test
      public void testSatisfiedBySubPathWhenUnsupportedCriterion() {
         assertThat(new DocumentEntryCriteriaBuilder()
                          .documentPath(CriterionBuilder.between(Paths.get("from"), Paths.get("to")))
                          .build()
                          .satisfiedBySubPath(Paths.get("path"))).isTrue();
         assertThat(new DocumentEntryCriteriaBuilder()
                          .documentPath(CriterionBuilder.betweenInclusive(Paths.get("from"), Paths.get("to")))
                          .build()
                          .satisfiedBySubPath(Paths.get("path"))).isTrue();
         assertThat(new DocumentEntryCriteriaBuilder()
                          .documentPath(CriterionBuilder.betweenExclusive(Paths.get("from"), Paths.get("to")))
                          .build()
                          .satisfiedBySubPath(Paths.get("path"))).isTrue();
         assertThat(new DocumentEntryCriteriaBuilder()
                          .documentPath(CriterionBuilder.greater(Paths.get("filterpath")))
                          .build()
                          .satisfiedBySubPath(Paths.get("path"))).isTrue();
         assertThat(new DocumentEntryCriteriaBuilder()
                          .documentPath(CriterionBuilder.greaterOrEqual(Paths.get("filterpath")))
                          .build()
                          .satisfiedBySubPath(Paths.get("path"))).isTrue();
         assertThat(new DocumentEntryCriteriaBuilder()
                          .documentPath(CriterionBuilder.lesser(Paths.get("filterpath")))
                          .build()
                          .satisfiedBySubPath(Paths.get("path"))).isTrue();
         assertThat(new DocumentEntryCriteriaBuilder()
                          .documentPath(CriterionBuilder.lesserOrEqual(Paths.get("filterpath")))
                          .build()
                          .satisfiedBySubPath(Paths.get("path"))).isTrue();
      }

      @Test
      public void testSatisfiedBySubPathWhenCompositeSpecificationWhenNominal() {
         DocumentEntrySpecification specification = new DocumentEntryCriteriaBuilder()
               .documentPath(CriterionBuilder.equal(Paths.get("subpath/path/file")))
               .build()
               .or(new DocumentEntryCriteriaBuilder()
                         .documentPath(CriterionBuilder.equal(Paths.get("otherpath/path/file")))
                         .build());

         assertThat(specification.satisfiedBySubPath(Paths.get("path"))).isFalse();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("otherpath/path"))).isTrue();
      }

      @Test
      public void testSatisfiedBySubPathWhenCompositeSpecificationWhenNotPure() {
         DocumentEntrySpecification specification = new DocumentEntryCriteriaBuilder()
               .documentPath(CriterionBuilder.equal(Paths.get("subpath/path/file")))
               .build()
               .or(new DocumentEntryCriteriaBuilder()
                         .documentPath(CriterionBuilder.equal(Paths.get("otherpath/path/file")))
                         .build())
               .or(__ -> true);

         assertThat(specification.satisfiedBySubPath(Paths.get("path"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("otherpath/path"))).isTrue();
      }

      @Test
      public void testSatisfiedBySubPathWhenCompositeSpecificationWhenNotSpecification() {
         DocumentEntrySpecification specification = new DocumentEntryCriteriaBuilder()
               .documentPath(CriterionBuilder.equal(Paths.get("subpath/path/file")))
               .build()
               .or(new DocumentEntryCriteriaBuilder()
                         .documentPath(CriterionBuilder.equal(Paths.get("otherpath/path/file")))
                         .build()
                         .not());

         assertThat(specification.satisfiedBySubPath(Paths.get("path"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("otherpath/path"))).isFalse();
      }

      @Test
      public void testSatisfiedBySubPathWhenCompositeSpecificationWhenOrSpecification() {
         DocumentEntrySpecification specification = new DocumentEntryCriteriaBuilder()
               .documentPath(CriterionBuilder.equal(Paths.get("subpath/path/file")))
               .build()
               .or(new DocumentEntryCriteriaBuilder()
                         .documentPath(CriterionBuilder.equal(Paths.get("otherpath/path/file")))
                         .build());

         assertThat(specification.satisfiedBySubPath(Paths.get("path"))).isFalse();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("otherpath/path"))).isTrue();
      }

      @Test
      public void testSatisfiedBySubPathWhenCompositeSpecificationWhenAndSpecification() {
         DocumentEntrySpecification specification = new DocumentEntryCriteriaBuilder()
               .documentPath(CriterionBuilder.equal(Paths.get("subpath/path/file")))
               .build()
               .and(new DocumentEntryCriteriaBuilder()
                          .documentPath(CriterionBuilder.equal(Paths.get("otherpath/path/file")))
                          .build()
                          .not());

         assertThat(specification.satisfiedBySubPath(Paths.get("path"))).isFalse();
         assertThat(specification.satisfiedBySubPath(Paths.get("subpath/path"))).isTrue();
         assertThat(specification.satisfiedBySubPath(Paths.get("otherpath/path"))).isFalse();
      }

   }

   private DocumentMetadata documentMetadata(String documentName) {
      return new DocumentMetadataBuilder()
            .documentPath(documentName)
            .contentType(StandardCharsets.UTF_8)
            .build();
   }

}