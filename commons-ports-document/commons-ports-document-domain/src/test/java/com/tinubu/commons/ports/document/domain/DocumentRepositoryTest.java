/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.spy;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.commons.ports.document.domain.processor.common.DocumentRenamer;

public class DocumentRepositoryTest {

   private static final ZonedDateTime now = ZonedDateTime.now();

   @BeforeAll
   public static void initializeApplicationClock() {
      ApplicationClock.setFixedClock(now);
   }

   @Test
   public void testTransferDocumentWhenNominal() {
      DocumentRepository source = mockSourceRepository();
      DocumentRepository target = mockTargetRepository();

      Assertions.assertThat(source.transferDocument(DocumentPath.of("path/file1"), target, false))
            .isEqualTo(optional(newDocumentEntry("path/file1")));
   }

   @Test
   public void testTransferDocumentWhenBadParameters() {
      DocumentRepository source = mockSourceRepository();
      DocumentRepository target = mockTargetRepository();

      assertThatNullPointerException()
            .isThrownBy(() -> source.transferDocument(null, target, false))
            .withMessage("'documentId' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> source.transferDocument(DocumentPath.of("path/file1"), null, false))
            .withMessage("'targetDocumentRepository' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> source.transferDocument(DocumentPath.of("path/file1"),
                                                      target,
                                                      false,
                                                      (DocumentProcessor) null))
            .withMessage("'processor' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> source.transferDocument(DocumentPath.of("path/file1"),
                                                      target,
                                                      false,
                                                      (DocumentRenamer) null))
            .withMessage("'renamer' must not be null");
   }

   @Test
   public void testTransferDocumentWhenDocumentNotFound() {
      DocumentRepository source = mockSourceRepository();
      DocumentRepository target = mockTargetRepository();

      Assertions
            .assertThat(source.transferDocument(DocumentPath.of("path/notexist"), target, false))
            .isEmpty();
   }

   @Test
   public void testTransferDocumentWhenExistingTarget() {
      DocumentRepository source = mockSourceRepository();
      DocumentRepository target = mockTargetRepository();

      Assertions
            .assertThat(source.transferDocument(DocumentPath.of("path/exist"), target, true))
            .isEqualTo(optional(newDocumentEntry("path/exist")));
      Assertions.assertThat(source.transferDocument(DocumentPath.of("path/exist"), target, false)).isEmpty();
   }

   @Test
   public void testTransferDocumentWhenCustomTargetDocumentId() {
      DocumentRepository source = mockSourceRepository();
      DocumentRepository target = mockTargetRepository();

      Assertions
            .assertThat(source.transferDocument(DocumentPath.of("path/exist"),
                                                target,
                                                true,
                                                (DocumentEntry entry) -> DocumentPath.of(entry
                                                                                               .documentId()
                                                                                               .value()
                                                                                         + "-transferred")))
            .isEqualTo(optional(newTransferredDocumentEntry("path/exist", "path/exist-transferred")));
   }

   @Test
   public void testTransferDocumentsWhenNominal() {
      DocumentRepository source = mockSourceRepository();
      DocumentRepository target = mockTargetRepository();

      Assertions
            .assertThat(source.transferDocuments(__ -> true, target, false))
            .containsExactly(newDocumentEntry("path/file1"), newDocumentEntry("path/file2"));
   }

   @Test
   public void testTransferDocumentsWhenBadParameters() {
      DocumentRepository source = mockSourceRepository();
      DocumentRepository target = mockTargetRepository();

      assertThatNullPointerException()
            .isThrownBy(() -> source.transferDocuments(null, target, false))
            .withMessage("'documentSpecification' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> source.transferDocuments(__ -> true, null, false))
            .withMessage("'targetDocumentRepository' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> source.transferDocuments(__ -> true, target, false, (DocumentProcessor) null))
            .withMessage("'processor' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> source.transferDocuments(__ -> true, target, false, (DocumentRenamer) null))
            .withMessage("'renamer' must not be null");
   }

   @Test
   public void testTransferDocumentsWhenCustomTargetDocumentId() {
      DocumentRepository source = mockSourceRepository();
      DocumentRepository target = mockTargetRepository();

      Assertions
            .assertThat(source.transferDocuments(__ -> true,
                                                 target,
                                                 false,
                                                 (DocumentEntry entry) -> DocumentPath.of(entry
                                                                                                .documentId()
                                                                                                .value()
                                                                                          + "-transferred")))
            .containsExactly(newTransferredDocumentEntry("path/file1", "path/file1-transferred"),
                             newTransferredDocumentEntry("path/file2", "path/file2-transferred"));
   }

   private DocumentRepository mockSourceRepository() {
      DocumentRepository sourceRepository = spy(DocumentRepository.class);

      List<DocumentEntry> documents = Arrays.asList(newDocumentEntry("path/exist"),
                                                    newDocumentEntry("path/file1"),
                                                    newDocumentEntry("path/file2"));

      doAnswer(invocation -> documents
            .stream()
            .filter(entry -> entry.documentId().equals(invocation.<DocumentPath>getArgument(0)))
            .findAny()
            .map(this::newDocument)).when(sourceRepository).findDocumentById(any());

      doAnswer(invocation -> documents
            .stream()
            .filter(invocation.<Specification<DocumentEntry>>getArgument(0))
            .map(this::newDocument)).when(sourceRepository).findDocumentsBySpecification(any());

      doAnswer(invocation -> optional())
            .when(sourceRepository)
            .findDocumentById(DocumentPath.of("path/notexist"));

      return sourceRepository;
   }

   private DocumentRepository mockTargetRepository() {
      DocumentRepository targetRepository = spy(DocumentRepository.class);

      doAnswer(iom -> optional(iom.getArgument(0, Document.class).documentEntry()))
            .when(targetRepository)
            .saveDocument(any(), anyBoolean());
      doAnswer(iom -> optional(iom.getArgument(0, Document.class).documentEntry()))
            .when(targetRepository)
            .saveDocument(argThat(d -> d.documentId().equals(DocumentPath.of("path/exist")) || d
                  .documentId()
                  .equals(DocumentPath.of("path/exist-transferred"))), eq(true));
      doAnswer(__ -> optional())
            .when(targetRepository)
            .saveDocument(argThat(d -> d.documentId().equals(DocumentPath.of("path/exist")) || d
                  .documentId()
                  .equals(DocumentPath.of("path/exist-transferred"))), eq(false));

      return targetRepository;
   }

   private DocumentEntry newDocumentEntry(DocumentPath documentId) {
      Instant now = ApplicationClock.nowAsInstant();

      return new DocumentEntryBuilder()
            .<DocumentEntryBuilder>reconstitute()
            .documentId(documentId)
            .metadata(new DocumentMetadataBuilder()
                            .<DocumentMetadataBuilder>reconstitute()
                            .creationDate(now)
                            .lastUpdateDate(now)
                            .documentPath(documentId.value())
                            .contentSize((long) documentId.stringValue().length())
                            .contentType(parseMimeType("application/octet-stream;charset=UTF-8"))
                            .build())
            .build();
   }

   private DocumentEntry newDocumentEntry(String documentId) {
      return newDocumentEntry(DocumentPath.of(documentId));
   }

   private DocumentEntry newTransferredDocumentEntry(DocumentPath documentId, DocumentPath newDocumentId) {
      return DocumentEntryBuilder.from(newDocumentEntry(documentId)).documentId(newDocumentId).build();
   }

   private DocumentEntry newTransferredDocumentEntry(String documentId, String newDocumentId) {
      return newTransferredDocumentEntry(DocumentPath.of(documentId), DocumentPath.of(newDocumentId));
   }

   private Document newDocument(DocumentPath documentId) {
      return newDocument(newDocumentEntry(documentId));
   }

   private Document newDocument(String documentId) {
      return newDocument(DocumentPath.of(documentId));
   }

   private Document newDocument(DocumentEntry documentEntry) {
      return new DocumentBuilder()
            .<DocumentBuilder>reconstitute()
            .documentEntry(documentEntry)
            .loadedContent(documentEntry.documentId().stringValue(), StandardCharsets.UTF_8)
            .build();
   }

}
