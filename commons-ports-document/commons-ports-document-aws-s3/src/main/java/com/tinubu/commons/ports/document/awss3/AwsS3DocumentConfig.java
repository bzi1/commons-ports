/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isGreaterThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isLessThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isStrictlyPositive;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.net.URL;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ports.document.domain.DocumentEntry;

/**
 * Azure Blob document repository configuration.
 */
public class AwsS3DocumentConfig extends AbstractValue {
   /** Absolute maximum number of keys that can be retrieved in a single call when listing objects. */
   private static final int MAX_KEYS_MAX_VALUE = 1000;
   /** Default part upload chunk size value. */
   private static final int DEFAULT_UPLOAD_CHUNK_SIZE = 10 * 1024 * 1024;
   /** Minimum supported part upload chunk size value. */
   private static final int MIN_UPLOAD_CHUNK_SIZE = 5 * 1024 * 1024;

   private final String region;
   private final URL endpoint;
   private final String bucketName;
   private final boolean createIfMissingBucket;
   private final boolean failFastIfMissingBucket;
   private final boolean pathStyleAccess;
   private final int listingMaxKeys;
   private final ListingContentMode listingContentMode;
   private final int uploadChunkSize;

   private AwsS3DocumentConfig(AwsS3DocumentConfigBuilder builder) {
      this.region = builder.region;
      this.endpoint = builder.endpoint;
      this.bucketName = builder.bucketName;
      this.createIfMissingBucket = nullable(builder.createIfMissingBucket, false);
      this.failFastIfMissingBucket = nullable(builder.failFastIfMissingBucket, true);
      this.pathStyleAccess = nullable(builder.pathStyleAccess, false);
      this.listingMaxKeys = nullable(builder.listingMaxKeys, MAX_KEYS_MAX_VALUE);
      this.listingContentMode = nullable(builder.listingContentMode, ListingContentMode.FAST);
      this.uploadChunkSize = nullable(builder.uploadChunkSize, DEFAULT_UPLOAD_CHUNK_SIZE);
   }

   @Override
   public Fields<? extends AwsS3DocumentConfig> defineDomainFields() {
      return Fields
            .<AwsS3DocumentConfig>builder()
            .field("region", v -> v.region, isNotBlank())
            .field("endpoint", v -> v.endpoint)
            .field("bucketName", v -> v.bucketName, isNotBlank())
            .field("createIfMissingBucket", v -> v.createIfMissingBucket)
            .field("failFastIfMissingBucket", v -> v.failFastIfMissingBucket)
            .field("pathStyleAccess", v -> v.pathStyleAccess)
            .field("listingMaxKeys",
                   v -> v.listingMaxKeys,
                   isStrictlyPositive().andValue(isLessThanOrEqualTo(value(MAX_KEYS_MAX_VALUE))))
            .field("listingContentMode", v -> v.listingContentMode, isNotNull())
            .field("uploadChunkSize",
                   v -> v.uploadChunkSize,
                   isGreaterThanOrEqualTo(value(MIN_UPLOAD_CHUNK_SIZE)))
            .build();
   }

   /**
    * S3 region.
    */
   public String region() {
      return region;
   }

   /**
    * Optional S3 endpoint override.
    */
   public Optional<URL> endpoint() {
      return nullable(endpoint);
   }

   /**
    * S3 bucket name.
    */
   public String bucketName() {
      return bucketName;
   }

   /**
    * Optional flag to automatically create missing bucket. Default to {@code false}.
    */
   public boolean createIfMissingBucket() {
      return createIfMissingBucket;
   }

   /**
    * Optional flag to fail-fast if missing bucket. Default to {@code true}.
    */
   public boolean failFastIfMissingBucket() {
      return failFastIfMissingBucket;
   }

   /**
    * Optional flag to use path-style access.
    */
   public boolean pathStyleAccess() {
      return pathStyleAccess;
   }

   /**
    * Optional max keys (i.e. page size) in objects listing operations. Default to
    * {@link #MAX_KEYS_MAX_VALUE}.
    */
   public int listingMaxKeys() {
      return listingMaxKeys;
   }

   /**
    * Optional listing content mode to workaround S3 limitations.
    * Default to {@link ListingContentMode#FAST} : S3 does not return all metadata for objects, so that
    * {@link DocumentEntry} metadata are  incomplete for both filtering and returned entries. The library can
    * complete the metadata at the cost of one extra API call per object.
    */
   public ListingContentMode listingContentMode() {
      return listingContentMode;
   }

   /**
    * Optional multipart upload chunk size. Default to {@link #DEFAULT_UPLOAD_CHUNK_SIZE}.
    */
   public int uploadChunkSize() {
      return uploadChunkSize;
   }

   /**
    * S3 listing operation metadata completing mode.
    */
   public enum ListingContentMode {
      /** Fast mode : no metadata are completed for both filtering and returned entries. */
      FAST,
      /**
       * Low performance mode : metadata are completed after filtering, for returned entries. Use this
       * mode only if you do not need complete metadata for specification filtering, but only for returned
       * entries, and there is a small number of returned objects after filtering.
       */
      RETURN_COMPLETE,
      /**
       * Very low performance mode : metadata are completed for both filtering and returned entries. Use this
       * mode only if you need complete metadata and there is a small number of objects in the bucket.
       */
      FILTERING_COMPLETE
   }

   public static class AwsS3DocumentConfigBuilder extends DomainBuilder<AwsS3DocumentConfig> {

      private String region;
      private URL endpoint;
      private String bucketName;
      private Boolean createIfMissingBucket;
      private Boolean failFastIfMissingBucket;
      private Boolean pathStyleAccess;
      private Integer listingMaxKeys;
      private ListingContentMode listingContentMode;
      private Integer uploadChunkSize;

      public AwsS3DocumentConfigBuilder region(String region) {
         this.region = region;
         return this;
      }

      public AwsS3DocumentConfigBuilder endpoint(URL endpoint) {
         this.endpoint = endpoint;
         return this;
      }

      public AwsS3DocumentConfigBuilder bucketName(String bucketName) {
         this.bucketName = bucketName;
         return this;
      }

      public AwsS3DocumentConfigBuilder createIfMissingBucket(boolean createIfMissingBucket) {
         this.createIfMissingBucket = createIfMissingBucket;
         return this;
      }

      public AwsS3DocumentConfigBuilder failFastIfMissingBucket(boolean failFastIfMissingBucket) {
         this.failFastIfMissingBucket = failFastIfMissingBucket;
         return this;
      }

      public AwsS3DocumentConfigBuilder pathStyleAccess(boolean pathStyleAccess) {
         this.pathStyleAccess = pathStyleAccess;
         return this;
      }

      public AwsS3DocumentConfigBuilder listingMaxKeys(int listingMaxKeys) {
         this.listingMaxKeys = listingMaxKeys;
         return this;
      }

      public AwsS3DocumentConfigBuilder listingContentMode(ListingContentMode listingContentMode) {
         this.listingContentMode = listingContentMode;
         return this;
      }

      public AwsS3DocumentConfigBuilder uploadChunkSize(Integer uploadChunkSize) {
         this.uploadChunkSize = uploadChunkSize;
         return this;
      }

      @Override
      protected AwsS3DocumentConfig buildDomainObject() {
         return new AwsS3DocumentConfig(this);
      }
   }
}
