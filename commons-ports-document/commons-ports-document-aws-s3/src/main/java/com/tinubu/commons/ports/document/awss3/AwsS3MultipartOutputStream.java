/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static com.tinubu.commons.ports.document.awss3.S3Utils.contentDisposition;
import static com.tinubu.commons.ports.document.awss3.S3Utils.tryCreateBucket;
import static com.tinubu.commons.ports.document.awss3.S3Utils.userMetadata;

import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;

import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.AbortMultipartUploadRequest;
import software.amazon.awssdk.services.s3.model.CompleteMultipartUploadRequest;
import software.amazon.awssdk.services.s3.model.CompletedMultipartUpload;
import software.amazon.awssdk.services.s3.model.CompletedPart;
import software.amazon.awssdk.services.s3.model.CreateMultipartUploadRequest;
import software.amazon.awssdk.services.s3.model.CreateMultipartUploadResponse;
import software.amazon.awssdk.services.s3.model.NoSuchBucketException;
import software.amazon.awssdk.services.s3.model.NoSuchKeyException;
import software.amazon.awssdk.services.s3.model.NoSuchUploadException;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.UploadPartRequest;
import software.amazon.awssdk.services.s3.model.UploadPartResponse;

/**
 * {@link OutputStream} implementation using S3 multipart upload under the hood.
 * Maximum memory consumption for this stream has the size of a chunk.
 * A regular upload is used while stream size is less than chunk size.
 */
public class AwsS3MultipartOutputStream extends OutputStream {
   private static final Logger log = LoggerFactory.getLogger(AwsS3MultipartOutputStream.class);

   /** Default chunk size. */
   private static final int DEFAULT_UPLOAD_CHUNK_SIZE = 10 * 1024 * 1024;
   /** Minimum chunk size supported by S3 (last chunk has no minimum limit). */
   private static final int MIN_UPLOAD_CHUNK_SIZE = 5 * 1024 * 1024;

   /** The S3 bucket name. */
   private final String bucketName;
   /** Create bucket if missing. */
   private final boolean createIfMissingBucket;
   /** The object key name within the bucket. */
   private final String key;
   /** The temporary buffer used for storing the chunks. */
   private final byte[] buf;
   /** S3 client. */
   private final S3Client client;
   /** Collection of the etags for the parts that have been uploaded. */
   private final List<String> etags;
   /** Optional metadata for created object. */
   private final OpenDocumentMetadata metadata;

   /** Position in the buffer. */
   private int position;
   /** Unique id for this upload. */
   private String uploadId;
   /** Indicates whether the stream is still open. */
   private boolean open;

   /**
    * Creates a new instance.
    *
    * @param client the AmazonS3 client
    * @param bucketName name of the bucket
    * @param key key within the bucket
    * @param metadata document metadata
    * @param uploadChunkSize upload chunk size
    */
   public AwsS3MultipartOutputStream(S3Client client,
                                     String bucketName,
                                     boolean createIfMissingBucket,
                                     String key,
                                     OpenDocumentMetadata metadata,
                                     int uploadChunkSize) {
      notNull(client, "client");
      notBlank(bucketName, "bucketName");
      notBlank(key, "key");
      notNull(metadata, "metadata");
      satisfies(uploadChunkSize,
                bs -> bs >= MIN_UPLOAD_CHUNK_SIZE,
                "uploadChunkSize",
                "must be >= " + MIN_UPLOAD_CHUNK_SIZE);

      this.client = client;
      this.bucketName = bucketName;
      this.createIfMissingBucket = createIfMissingBucket;
      this.key = key;
      this.metadata = metadata;

      this.buf = new byte[uploadChunkSize];
      this.position = 0;
      this.etags = new ArrayList<>();
      this.open = true;
   }

   /**
    * Creates a new instance with default buffer size ({@value #DEFAULT_UPLOAD_CHUNK_SIZE}).
    *
    * @param client the AmazonS3 client
    * @param bucketName name of the bucket
    * @param key key within the bucket
    * @param metadata document metadata
    */
   public AwsS3MultipartOutputStream(S3Client client,
                                     String bucketName,
                                     boolean createIfMissingBucket,
                                     String key,
                                     OpenDocumentMetadata metadata) {
      this(client, bucketName, createIfMissingBucket, key, metadata, DEFAULT_UPLOAD_CHUNK_SIZE);
   }

   public void cancel() {
      open = false;
      if (uploadId != null) {
         try {
            client.abortMultipartUpload(AbortMultipartUploadRequest
                                              .builder()
                                              .bucket(bucketName)
                                              .key(key)
                                              .uploadId(uploadId)
                                              .build());
         } catch (NoSuchUploadException | NoSuchBucketException | NoSuchKeyException e) {
            /* Do nothing. */
         } catch (SdkException e) {
            throw new DocumentAccessException(e);
         }
      }
   }

   @Override
   public void write(int b) {
      assertOpen();
      if (position >= buf.length) {
         flushBufferAndRewind();
      }
      buf[position++] = (byte) b;
   }

   /**
    * Write an array to the stream.
    *
    * @param byteArray the byte-array to append
    */
   @Override
   public void write(byte[] byteArray) {
      write(byteArray, 0, byteArray.length);
   }

   /**
    * Writes an array to the stream.
    *
    * @param byteArray the array to write
    * @param position the offset into the array
    * @param length the number of bytes to write
    */
   @Override
   public void write(byte[] byteArray, int position, int length) {
      assertOpen();
      int ofs = position;
      int len = length;
      int size;
      while (len > (size = buf.length - this.position)) {
         System.arraycopy(byteArray, ofs, buf, this.position, size);
         this.position += size;
         flushBufferAndRewind();
         ofs += size;
         len -= size;
      }
      System.arraycopy(byteArray, ofs, buf, this.position, len);
      this.position += len;
   }

   @Override
   public synchronized void flush() {
      assertOpen();
   }

   @Override
   public void close() {
      if (open) {
         open = false;
         if (uploadId != null) {
            if (position > 0) {
               uploadPart();
            }

            CompletedPart[] completedParts = new CompletedPart[etags.size()];
            for (int i = 0; i < etags.size(); i++) {
               completedParts[i] = CompletedPart.builder().eTag(etags.get(i)).partNumber(i + 1).build();
            }

            CompletedMultipartUpload completedMultipartUpload =
                  CompletedMultipartUpload.builder().parts(completedParts).build();
            CompleteMultipartUploadRequest completeMultipartUploadRequest = CompleteMultipartUploadRequest
                  .builder()
                  .bucket(bucketName)
                  .key(key)
                  .uploadId(uploadId)
                  .multipartUpload(completedMultipartUpload)
                  .build();
            try {
               client.completeMultipartUpload(completeMultipartUploadRequest);
            } catch (SdkException e) {
               throw new DocumentAccessException(e);
            }

         } else {
            PutObjectRequest putRequest = PutObjectRequest
                  .builder()
                  .bucket(bucketName)
                  .key(key)
                  .contentLength((long) position)
                  .contentType(metadata.contentType().map(MimeType::toString).orElse(null))
                  .metadata(userMetadata(metadata))
                  .contentEncoding(metadata.contentEncoding().map(Charset::name).orElse(null))
                  .contentDisposition(contentDisposition(metadata))
                  .build();

            RequestBody requestBody =
                  RequestBody.fromInputStream(new ByteArrayInputStream(buf, 0, position), position);

            tryCreateBucket(client, bucketName, createIfMissingBucket);

            try {
               client.putObject(putRequest, requestBody);
            } catch (NoSuchBucketException e) {
               throw new DocumentAccessException(String.format("Unknown '%s' bucket", bucketName), e);
            } catch (SdkException e) {
               throw new DocumentAccessException(e);
            }
         }
      }
   }

   protected void flushBufferAndRewind() {
      if (uploadId == null) {
         CreateMultipartUploadRequest uploadRequest = CreateMultipartUploadRequest
               .builder()
               .bucket(bucketName)
               .key(key)
               .contentType(metadata.contentType().map(MimeType::toString).orElse(null))
               .metadata(userMetadata(metadata))
               .contentEncoding(metadata.contentEncoding().map(Charset::name).orElse(null))
               .contentDisposition(contentDisposition(metadata))
               .build();

         boolean retryWithCreatedBucket = false;
         do {
            try {
               CreateMultipartUploadResponse multipartUpload = client.createMultipartUpload(uploadRequest);
               uploadId = multipartUpload.uploadId();
               retryWithCreatedBucket = false;
            } catch (NoSuchBucketException e) {
               if (retryWithCreatedBucket) {
                  throw new DocumentAccessException(String.format("Unknown '%s' bucket", bucketName), e);
               } else {
                  tryCreateBucket(client, bucketName, createIfMissingBucket);
                  retryWithCreatedBucket = true;
               }
            } catch (SdkException e) {
               throw new DocumentAccessException(e);
            }

         } while (retryWithCreatedBucket);

      }
      uploadPart();
      position = 0;
   }

   protected void uploadPart() {
      UploadPartRequest uploadRequest = UploadPartRequest
            .builder()
            .bucket(bucketName)
            .key(key)
            .uploadId(uploadId)
            .partNumber(etags.size() + 1)
            .contentLength((long) position)
            .build();
      RequestBody requestBody =
            RequestBody.fromInputStream(new ByteArrayInputStream(buf, 0, position), position);
      try {
         UploadPartResponse uploadPartResponse = client.uploadPart(uploadRequest, requestBody);
         etags.add(uploadPartResponse.eTag());
      } catch (SdkException e) {
         throw new DocumentAccessException(e);
      }
   }

   private void assertOpen() {
      if (!open) {
         throw new IllegalStateException("Stream is closed");
      }
   }

}
