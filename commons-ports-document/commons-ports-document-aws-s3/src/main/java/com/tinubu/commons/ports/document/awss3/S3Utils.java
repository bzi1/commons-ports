/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static java.util.stream.Collectors.toMap;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;

import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.BucketAlreadyExistsException;
import software.amazon.awssdk.services.s3.model.BucketAlreadyOwnedByYouException;
import software.amazon.awssdk.services.s3.model.CreateBucketRequest;
import software.amazon.awssdk.services.s3.model.HeadBucketRequest;
import software.amazon.awssdk.services.s3.model.NoSuchBucketException;

public final class S3Utils {
   private static final Logger log = LoggerFactory.getLogger(S3Utils.class);

   private static final Pattern CONTENT_DISPOSITION_PATTERN =
         Pattern.compile("Content-Disposition:\\s*attachment;\\s*filename=\"([^\"]+)\"");

   private S3Utils() {}

   public static void tryCreateBucket(S3Client client, String bucketName, boolean createIfMissingBucket) {
      if (createIfMissingBucket) {
         try {
            client.createBucket(CreateBucketRequest.builder().bucket(bucketName).build());
            log.debug("Created missing '{}' bucket", bucketName);
         } catch (BucketAlreadyExistsException | BucketAlreadyOwnedByYouException e) {
            /* Does nothing. */
         } catch (SdkException e) {
            throw new DocumentAccessException(e);
         }
      } else {
         try {
            client.headBucket(HeadBucketRequest.builder().bucket(bucketName).build());
         } catch (NoSuchBucketException e) {
            throw new DocumentAccessException(String.format("Can't create '%s' missing bucket", bucketName),
                                              e);
         } catch (SdkException e) {
            throw new DocumentAccessException(e);
         }
      }
   }

   public static Map<String, String> userMetadata(OpenDocumentMetadata metadata) {
      return map(LinkedHashMap::new,
                 metadata
                       .attributes()
                       .entrySet()
                       .stream()
                       .collect(toMap(Entry::getKey, e -> e.getValue().toString())));
   }

   public static String contentDisposition(OpenDocumentMetadata metadata) {
      return metadata
            .documentPath()
            .map(documentPath -> "Content-Disposition: attachment; filename=\""
                                 + documentPath.toString()
                                 + "\"")
            .orElse("Content-Disposition: attachment");
   }

   /**
    * Extract filename from a Content-Disposition header.
    *
    * @param contentDisposition Content-Disposition header
    *
    * @return filename path
    */
   public static Optional<Path> documentPath(String contentDisposition) {
      if (contentDisposition == null) {
         return optional();
      }

      Matcher matcher = CONTENT_DISPOSITION_PATTERN.matcher(contentDisposition);
      if (matcher.matches()) {
         return optional(Paths.get(matcher.group(1)));
      } else {
         return optional();
      }
   }

   /** Java < 10 InputStream::transferTo implementation. */
   public static long transferTo(InputStream in, OutputStream out) throws IOException {
      Objects.requireNonNull(in, "in");
      Objects.requireNonNull(out, "out");

      final int TRANSFER_BUFFER_SIZE = 4096 * 16;

      long transferred = 0;
      byte[] buffer = new byte[TRANSFER_BUFFER_SIZE];
      int read;
      while ((read = in.read(buffer, 0, TRANSFER_BUFFER_SIZE)) >= 0) {
         out.write(buffer, 0, read);
         transferred += read;
      }
      return transferred;
   }

}
