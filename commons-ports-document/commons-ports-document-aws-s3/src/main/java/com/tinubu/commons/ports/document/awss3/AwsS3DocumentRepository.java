/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.ListingContentMode.FILTERING_COMPLETE;
import static com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.ListingContentMode.RETURN_COMPLETE;
import static com.tinubu.commons.ports.document.awss3.S3Utils.documentPath;
import static com.tinubu.commons.ports.document.awss3.S3Utils.transferTo;
import static com.tinubu.commons.ports.document.awss3.S3Utils.tryCreateBucket;
import static com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.lang.mimetype.MimeTypeFactory;
import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.ListingContentMode;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata.OpenDocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.UnsupportedUriException;
import com.tinubu.commons.ports.document.domain.UriAdapter;
import com.tinubu.commons.ports.document.transformer.DocumentTransformerUriAdapter;

import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.S3ClientBuilder;
import software.amazon.awssdk.services.s3.model.DeleteBucketRequest;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.HeadObjectRequest;
import software.amazon.awssdk.services.s3.model.HeadObjectResponse;
import software.amazon.awssdk.services.s3.model.InvalidObjectStateException;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;
import software.amazon.awssdk.services.s3.model.NoSuchBucketException;
import software.amazon.awssdk.services.s3.model.NoSuchKeyException;
import software.amazon.awssdk.services.s3.model.S3Object;

/**
 * Azure Blob {@link DocumentRepository} adapter implementation.
 * <p>
 * If configured bucket is missing, the bucket will be created only at write time, or the write
 * operation will fail if repository configuration does not instruct to do so.
 */
// FIXME Create bucket ACLs
public class AwsS3DocumentRepository extends AbstractDocumentRepository {

   private final int sameRepositoryHash;
   private final DocumentTransformerUriAdapter transformerUriAdapter;
   private final String bucketName;
   private final S3Client client;
   private final int listingMaxKeys;
   private final ListingContentMode listingContentMode;
   private final int uploadChunkSize;
   private final boolean createIfMissingBucket;

   public AwsS3DocumentRepository(AwsS3DocumentConfig awsS3DocumentConfig) {
      this.sameRepositoryHash = sameRepositoryHash(awsS3DocumentConfig);
      this.transformerUriAdapter = new DocumentTransformerUriAdapter(new AwsS3UriAdapter());
      this.bucketName = awsS3DocumentConfig.bucketName();
      this.client = buildS3Client(awsS3DocumentConfig);
      this.listingMaxKeys = awsS3DocumentConfig.listingMaxKeys();
      this.listingContentMode = awsS3DocumentConfig.listingContentMode();
      this.uploadChunkSize = awsS3DocumentConfig.uploadChunkSize();
      this.createIfMissingBucket = awsS3DocumentConfig.createIfMissingBucket();

      if (awsS3DocumentConfig.failFastIfMissingBucket()) {
         tryCreateBucket(client, bucketName, createIfMissingBucket);
      }
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      return documentRepository instanceof AwsS3DocumentRepository
             && Objects.equals(((AwsS3DocumentRepository) documentRepository).sameRepositoryHash,
                               sameRepositoryHash);
   }

   /**
    * {@inheritDoc}
    *
    * @implNote Overwrite check is not atomic, this can lead to overwriting a document created
    *       concurrently by another client, even if overwrite flag is set to {@code false}.
    */
   @Override
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      notNull(documentId, "documentId");
      notNull(metadata, "metadata");

      if (append) {
         throw new IllegalArgumentException("Append mode not supported with output streams");
      }

      startWatch();

      return handleDocumentEvent(() -> {
         if (!(overwrite || append) && documentEntry(documentId).isPresent()) {
            return optional();
         }

         return optional(new DocumentBuilder()
                               .documentId(documentId)
                               .content(new OutputStreamDocumentContentBuilder()
                                              .content(new AwsS3MultipartOutputStream(client,
                                                                                      bucketName,
                                                                                      createIfMissingBucket,
                                                                                      key(documentId),
                                                                                      metadata,
                                                                                      uploadChunkSize))
                                              .build())
                               .chain(metadata.chainDocumentBuilder())
                               .build());
      }, d -> documentSaved(d.documentEntry()));
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      notNull(documentId, "documentId");

      startWatch();

      return handleDocumentEvent(() -> {
         try {
            ResponseInputStream<GetObjectResponse> object = client.getObject(GetObjectRequest
                                                                                   .builder()
                                                                                   .bucket(bucketName)
                                                                                   .key(key(documentId))
                                                                                   .build());

            return optional(new DocumentBuilder()
                                  .<DocumentBuilder>reconstitute()
                                  .documentId(documentId)
                                  .metadata(documentMetadata(documentId, object.response()))
                                  .content(inputStreamContent(object))
                                  .build());
         } catch (NoSuchKeyException | NoSuchBucketException | InvalidObjectStateException e) {
            return optional();
         } catch (SdkException e) {
            throw new DocumentAccessException(e);
         }
      }, d -> documentAccessed(d.documentEntry()));
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      startWatch();

      return handleDocumentStreamEvent(() -> documentEntries(basePath, specification).flatMap(entry -> {
         Optional<DocumentContent> documentContent = awsS3DocumentContent(entry.documentId());
         return stream(documentContent.map(content -> new DocumentBuilder()
               .<DocumentBuilder>reconstitute()
               .documentEntry(entry)
               .content(content)
               .build()));
      }), d -> documentAccessed(d.documentEntry()));
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      notNull(documentId, "documentId");

      startWatch();

      return handleDocumentEntryEvent(() -> documentEntry(documentId), this::documentAccessed);
   }

   /**
    * {@inheritDoc}
    *
    * @implSpec Metadata returned by S3 are very limited, and can't be completed because of the
    *       performance impact. The following Metadata are not managed by default in returned document entry
    *       and specification filtering :
    *       <ul>
    *          <li>creation date : unset</li>
    *          <li>custom attributes : unset</li>
    *          <li>content type : unset </li>
    *          <li>document path/name : replaced with {@link DocumentPath document identifier} value</li>
    *       </ul>
    */
   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();
      notNull(specification, "specification");

      startWatch();

      return handleDocumentEntryStreamEvent(() -> documentEntries(basePath, specification),
                                            this::documentAccessed);
   }

   /**
    * {@inheritDoc}
    *
    * @implNote Overwrite check is not atomic, this can lead to overwriting a document created
    *       concurrently by another client, even if overwrite flag is set to {@code false}.
    */
   @Override
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      notNull(document, "document");

      if (document.content() instanceof OutputStreamDocumentContent) {
         throw new IllegalArgumentException(
               "Unsupported operation for this document content implementation : " + document
                     .content()
                     .getClass()
                     .getName());
      }

      startWatch();

      return handleDocumentEntryEvent(() -> {

         String key = key(document.documentId());

         if (overwrite || !documentEntry(document.documentId()).isPresent()) {
            try (AwsS3MultipartOutputStream outputStream = new AwsS3MultipartOutputStream(client,
                                                                                          bucketName,
                                                                                          createIfMissingBucket,
                                                                                          key,
                                                                                          openDocumentMetadata(
                                                                                                document.metadata()),
                                                                                          uploadChunkSize)) {
               try {
                  transferTo(document.content().inputStreamContent(), outputStream);
               } catch (IOException e) {
                  throw new DocumentAccessException(e);
               }
            }
            return documentEntry(document.documentId());
         }
         return optional();
      }, this::documentSaved);
   }

   @Override
   public Optional<DocumentEntry> deleteDocument(DocumentPath documentId) {
      notNull(documentId, "documentId");

      startWatch();

      return handleDocumentEntryEvent(() -> {

         String key = key(documentId);

         return documentEntry(documentId).flatMap(entry -> {
            try {
               client.deleteObject(DeleteObjectRequest.builder().bucket(bucketName).key(key).build());
            } catch (NoSuchBucketException e) {
               return optional();
            } catch (SdkException e) {
               throw new DocumentAccessException(e);
            }
            return optional(entry);
         });
      }, this::documentDeleted);
   }

   @Override
   public boolean supportsUri(URI documentUri) {
      return transformerUriAdapter.supportsUri(documentUri);
   }

   @Override
   public URI toUri(DocumentPath documentId) {
      return transformerUriAdapter.toUri(documentId);
   }

   @Override
   public Optional<Document> findDocumentByUri(URI documentUri) {
      return transformerUriAdapter.findDocumentByUri(documentUri);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
      return transformerUriAdapter.findDocumentEntryByUri(documentUri);
   }

   /**
    * Real Azure Blob URI adapter to be used by {@link DocumentTransformerUriAdapter}.
    */
   public class AwsS3UriAdapter implements UriAdapter {
      @Override
      public boolean supportsUri(URI documentUri) {
         return false;
      }

      @Override
      public URI toUri(DocumentPath documentId) {
         throw new UnsupportedUriException("Repository has no URI representation");
      }

      @Override
      public Optional<Document> findDocumentByUri(URI documentUri) {
         satisfies(documentUri, this::supportsUri, "documentUri", "must be supported : " + documentUri);

         return findDocumentById(documentId(documentUri));
      }

      @Override
      public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
         satisfies(documentUri, this::supportsUri, "documentUri", "must be supported : " + documentUri);

         return findDocumentEntryById(documentId(documentUri));
      }

      /**
       * Extracts document identifier from specified document URI.
       *
       * @param documentUri document URI
       *
       * @return document identifier
       */
      protected DocumentPath documentId(URI documentUri) {
         throw new UnsupportedOperationException();
      }

   }

   /** Generates a hash unique for a given server storage path to identify a similar repository. */
   private int sameRepositoryHash(AwsS3DocumentConfig awsS3DocumentConfig) {
      return Objects.hash(awsS3DocumentConfig.region(), awsS3DocumentConfig.bucketName());
   }

   private static S3Client buildS3Client(AwsS3DocumentConfig awsS3DocumentConfig) {
      S3ClientBuilder s3ClientBuilder =
            S3Client.builder().forcePathStyle(awsS3DocumentConfig.pathStyleAccess());

      s3ClientBuilder.region(Region.of(awsS3DocumentConfig.region()));
      awsS3DocumentConfig.endpoint().ifPresent(endpoint -> {
         try {
            s3ClientBuilder.endpointOverride(endpoint.toURI());
         } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
         }
      });

      return s3ClientBuilder.build();
   }

   private OpenDocumentMetadata openDocumentMetadata(DocumentMetadata metadata) {
      return new OpenDocumentMetadataBuilder()
            .documentPath(metadata.documentPath())
            .contentType(metadata.contentType())
            .attributes(metadata.attributes())
            .build();
   }

   private Optional<DocumentEntry> documentEntry(DocumentPath documentId) {
      try {
         HeadObjectResponse headObjectResponse =
               client.headObject(HeadObjectRequest.builder().bucket(bucketName).key(key(documentId)).build());

         return optional(new DocumentEntryBuilder()
                               .<DocumentEntryBuilder>reconstitute()
                               .documentId(documentId)
                               .metadata(documentMetadata(documentId, headObjectResponse))
                               .build());
      } catch (NoSuchKeyException | NoSuchBucketException e) {
         return optional();
      } catch (SdkException e) {
         throw new DocumentAccessException(e);
      }
   }

   private Stream<DocumentEntry> documentEntries(Path basePath, Specification<DocumentEntry> specification) {
      Stream<DocumentEntry> documentEntries =
            listPrefixDocumentEntries(basePath, directoryFilter(specification));

      if (listingContentMode == FILTERING_COMPLETE) {
         documentEntries = documentEntries.flatMap(e -> stream(findDocumentEntryById(e.documentId())));
      }

      documentEntries = documentEntries.filter(specification::satisfiedBy);

      if (listingContentMode == RETURN_COMPLETE) {
         documentEntries = documentEntries.flatMap(e -> stream(findDocumentEntryById(e.documentId())));
      }

      return documentEntries;
   }

   /**
    * Delete bucket for testing purpose.
    */
   void deleteBucket(String bucketName) {
      try {
         client.deleteBucket(DeleteBucketRequest.builder().bucket(bucketName).build());
      } catch (NoSuchBucketException e) {
         /* Does nothing. */
      } catch (SdkException e) {
         throw new DocumentAccessException(e);
      }
   }

   private Optional<DocumentContent> awsS3DocumentContent(DocumentPath documentId) {
      try {
         ResponseInputStream<GetObjectResponse> object =
               client.getObject(GetObjectRequest.builder().bucket(bucketName).key(key(documentId)).build());

         return optional(inputStreamContent(object));
      } catch (NoSuchKeyException | InvalidObjectStateException e) {
         return optional();
      } catch (SdkException e) {
         throw new DocumentAccessException(e);
      }
   }

   private RequestBody requestBody(Document document) {
      return RequestBody.fromInputStream(document.content().inputStreamContent(),
                                         document
                                               .content()
                                               .contentSize()
                                               .orElseThrow(() -> new IllegalArgumentException(String.format(
                                                     "Unknown '%s' content size",
                                                     document.documentId()))));
   }

   private DocumentEntry documentEntry(S3Object object) {
      DocumentPath documentId = DocumentPath.of(object.key());

      return new DocumentEntryBuilder()
            .<DocumentEntryBuilder>reconstitute()
            .documentId(documentId)
            .metadata(documentMetadata(documentId, object))
            .build();
   }

   private Predicate<Path> directoryFilter(Specification<DocumentEntry> specification) {
      if (specification instanceof DocumentEntrySpecification) {
         DocumentEntrySpecification documentEntrySpecification = (DocumentEntrySpecification) specification;
         return documentEntrySpecification::satisfiedBySubPath;
      } else {
         return __ -> true;
      }
   }

   private Stream<DocumentEntry> listPrefixDocumentEntries(Path prefix, Predicate<Path> prefixFilter) {
      notNull(prefix, "prefix");
      notNull(prefixFilter, "prefixFilter");

      String s3Prefix = prefix.toString().isEmpty() ? "" : prefix + "/";

      try {
         return client
               .listObjectsV2Paginator(ListObjectsV2Request
                                             .builder()
                                             .bucket(bucketName)
                                             .delimiter("/")
                                             .prefix(s3Prefix)
                                             .maxKeys(listingMaxKeys)
                                             .build())
               .stream()
               .flatMap(response -> {
                  Stream<DocumentEntry> entries = stream();
                  if (response.hasContents()) {
                     entries = streamConcat(entries, response.contents().stream().map(this::documentEntry));
                  }

                  if (response.hasCommonPrefixes()) {
                     entries = streamConcat(entries,
                                            response
                                                  .commonPrefixes()
                                                  .stream()
                                                  .map(commonPrefix -> Paths.get(commonPrefix.prefix()))
                                                  .filter(prefixFilter)
                                                  .flatMap(commonPrefix -> listPrefixDocumentEntries(
                                                        commonPrefix,
                                                        prefixFilter)));
                  }

                  return entries;
               });
      } catch (NoSuchBucketException e) {
         return stream();
      } catch (SdkException e) {
         throw new DocumentAccessException(e);
      }
   }

   /**
    * @implSpec incomplete Metadata.
    */
   private DocumentMetadata documentMetadata(DocumentPath documentId, S3Object objectMetadata) {
      Instant lastUpdateDate = objectMetadata.lastModified();
      Long contentSize = objectMetadata.size();

      return new DocumentMetadataBuilder()
            .<DocumentMetadataBuilder>reconstitute()
            .documentPath(documentId.value())
            .lastUpdateDate(lastUpdateDate)
            .contentSize(contentSize)
            .build();
   }

   private DocumentMetadata documentMetadata(DocumentPath documentId, GetObjectResponse objectMetadata) {
      Instant lastUpdateDate = objectMetadata.lastModified();
      MimeType contentType =
            nullable(objectMetadata.contentType()).map(MimeTypeFactory::parseMimeType).orElse(null);
      Path documentPath = documentPath(objectMetadata.contentDisposition()).orElse(documentId.value());
      Long contentSize = objectMetadata.contentLength();
      Map<String, Object> attributes = map(objectMetadata.metadata());

      return new DocumentMetadataBuilder()
            .<DocumentMetadataBuilder>reconstitute()
            .documentPath(documentPath)
            .contentType(contentType)
            .lastUpdateDate(lastUpdateDate)
            .contentSize(contentSize)
            .attributes(attributes)
            .build();
   }

   private DocumentMetadata documentMetadata(DocumentPath documentId, HeadObjectResponse objectMetadata) {
      Instant lastUpdateDate = objectMetadata.lastModified();
      MimeType contentType =
            nullable(objectMetadata.contentType()).map(MimeTypeFactory::parseMimeType).orElse(null);
      Path documentPath = documentPath(objectMetadata.contentDisposition()).orElse(documentId.value());
      Long contentSize = objectMetadata.contentLength();
      Map<String, Object> attributes = map(objectMetadata.metadata());

      return new DocumentMetadataBuilder()
            .<DocumentMetadataBuilder>reconstitute()
            .documentPath(documentPath)
            .contentType(contentType)
            .lastUpdateDate(lastUpdateDate)
            .contentSize(contentSize)
            .attributes(attributes)
            .build();
   }

   private InputStreamDocumentContent inputStreamContent(ResponseInputStream<GetObjectResponse> object) {
      Long contentSize = object.response().contentLength();
      Charset contentEncoding = nullable(object.response().contentEncoding())
            .map(Charset::forName)
            .orElseGet(() -> nullable(object.response().contentType())
                  .map(MimeTypeFactory::parseMimeType)
                  .flatMap(MimeType::charset)
                  .orElse(null));

      return new InputStreamDocumentContentBuilder().content(object, contentEncoding, contentSize).build();
   }

   private String key(DocumentPath documentId) {
      return documentId.stringValue();
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", AwsS3DocumentRepository.class.getSimpleName() + "[", "]").toString();
   }
}
