/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_PDF;
import static com.tinubu.commons.lang.util.OptionalUtils.peek;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.io.IOException;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.function.UnaryOperator;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.images.PullPolicy;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import com.tinubu.commons.lang.mimetype.MimeTypeFactory;
import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.AwsS3DocumentConfigBuilder;
import com.tinubu.commons.ports.document.domain.CommonDocumentRepositoryTest;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata.OpenDocumentMetadataBuilder;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ZipTransformer;

@Testcontainers(disabledWithoutDocker = true)
public class AwsS3DocumentRepositoryTest {
   private static final Logger log = LoggerFactory.getLogger(AwsS3DocumentRepositoryTest.class);

   public static final DockerImageName LOCALSTACK_DOCKER_IMAGE = DockerImageName.parse(
         "localstack/localstack@sha256:990e2c67f6e5baafd63b13f263f2ff257fd85689a7eea3cbb661f39b4a3cdbed");
   //.withTag("1.3.0");
   public static final String TEST_BUCKET = "test-bucket";
   /** @implNote Region must be set to {@code us-east-1} when using custom endpoint with S3. */
   private static final String TEST_REGION = "us-east-1";
   private static final int CHUNK_SIZE = 5 * 1024 * 1024;

   @Container
   private static final GenericContainer<?> localstack = new GenericContainer<>(LOCALSTACK_DOCKER_IMAGE)
         .withImagePullPolicy(PullPolicy.alwaysPull())
         .withExposedPorts(4566)
         .withLogConsumer(new Slf4jLogConsumer(log));

   @AfterAll
   public static void afterAll() {
      localstack.close();
   }

   private static URL s3Url() {
      try {
         return URI
               .create("http://" + localstack.getContainerIpAddress() + ":" + localstack.getMappedPort(4566))
               .toURL();
      } catch (MalformedURLException e) {
         throw new IllegalStateException(e);
      }
   }

   @Nested
   public class WhenCreateIfMissingBucket extends CommonDocumentRepositoryTest {

      private AwsS3DocumentRepository documentRepository;

      /**
       * Bucket is deleted after each test to test missing bucket lazy creation in all operations.
       * failFastIfMissingBucket should be set to false for tests to be complete.
       */
      @BeforeEach
      public void configureDocumentRepository() {
         assertThat(localstack.isRunning()).isTrue();

         documentRepository = new AwsS3DocumentRepository(awsS3Config());
         documentRepository.deleteBucket(TEST_BUCKET);
      }

      private AwsS3DocumentConfig awsS3Config() {
         return new AwsS3DocumentConfigBuilder()
               .region(TEST_REGION)
               .endpoint(s3Url())
               .bucketName(TEST_BUCKET)
               .pathStyleAccess(true)
               .createIfMissingBucket(true)
               .failFastIfMissingBucket(false)
               .uploadChunkSize(CHUNK_SIZE)
               .build();
      }

      @Override
      protected DocumentRepository documentRepository() {
         return documentRepository;
      }

      @Override
      public boolean isSupportingUri() {
         return false;
      }

      @Override
      protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
         return new ZipTransformer(zipPath).compress(documents);
      }

      @Override
      @Disabled("Not supported")
      public void testOpenDocumentWhenOverwriteAndAppend() { }

      @Override
      @Disabled("Not supported")
      public void testOpenDocumentWhenNotOverwriteAndAppend() { }

      @Override
      protected UnaryOperator<DocumentMetadataBuilder> synchronizeExpectedMetadata(DocumentMetadata actual) {
         return builder -> builder
               .chain(super.synchronizeExpectedMetadata(actual))
               .attributes(actual.attributes())
               .contentType(actual.contentEncoding().orElse(null));
      }

      /**
       * Document must be > chunk size for multipart upload to be enabled.
       */
      @Test
      public void testOpenDocumentWhenNoMultipart() {
         Path documentPath = path("path/pathfile1.pdf");
         DocumentPath testDocument = DocumentPath.of(documentPath);

         try {
            OpenDocumentMetadata metadata = new OpenDocumentMetadataBuilder()
                  .documentPath("path/alternative.pdf")
                  .contentType(MimeTypeFactory.mimeType(APPLICATION_PDF, UTF_8))
                  .build();

            String content = StringUtils.repeat('0', 10);
            Optional<Document> newDocument =
                  peek(documentRepository().openDocument(testDocument, true, false, metadata),
                       writeContent(content));

            assertThat(newDocument).isPresent();

            Optional<Document> document = documentRepository().findDocumentById(testDocument);

            assertThat(document)
                  .usingValueComparator(contentAgnosticDocumentComparator())
                  .hasValue(new DocumentBuilder()
                                  .<DocumentBuilder>reconstitute()
                                  .documentId(testDocument)
                                  .metadata(new DocumentMetadataBuilder()
                                                  .<DocumentMetadataBuilder>reconstitute()
                                                  .documentPath(Paths.get("path/alternative.pdf"))
                                                  .contentSize(isSupportingContentLength() ? 10L : null)
                                                  .contentType(parseMimeType("application/pdf;charset=UTF-8"))
                                                  .chain(synchronizeExpectedMetadata(document
                                                                                           .map(Document::metadata)
                                                                                           .orElse(null)))
                                                  .build())
                                  .loadedContent(content, UTF_8)
                                  .build());
         } finally {
            deleteDocuments(documentPath);
         }
      }

      /**
       * Document must be > chunk size for multipart upload to be enabled.
       */
      @Test
      public void testOpenDocumentWhenMultipart() {
         Path documentPath = path("path/pathfile1.pdf");
         DocumentPath testDocument = DocumentPath.of(documentPath);

         try {
            OpenDocumentMetadata metadata = new OpenDocumentMetadataBuilder()
                  .documentPath("path/alternative.pdf")
                  .contentType(MimeTypeFactory.mimeType(APPLICATION_PDF, UTF_8))
                  .build();

            String content = StringUtils.repeat('0', 2 * CHUNK_SIZE + 1);
            Optional<Document> newDocument =
                  peek(documentRepository().openDocument(testDocument, true, false, metadata),
                       writeContent(content));

            assertThat(newDocument).isPresent();

            Optional<Document> document = documentRepository().findDocumentById(testDocument);

            assertThat(document)
                  .usingValueComparator(contentAgnosticDocumentComparator())
                  .hasValue(new DocumentBuilder()
                                  .<DocumentBuilder>reconstitute()
                                  .documentId(testDocument)
                                  .metadata(new DocumentMetadataBuilder()
                                                  .<DocumentMetadataBuilder>reconstitute()
                                                  .documentPath(Paths.get("path/alternative.pdf"))
                                                  .contentSize(isSupportingContentLength() ? 2 * CHUNK_SIZE
                                                                                             + 1L : null)
                                                  .contentType(parseMimeType("application/pdf;charset=UTF-8"))
                                                  .chain(synchronizeExpectedMetadata(document
                                                                                           .map(Document::metadata)
                                                                                           .orElse(null)))
                                                  .build())
                                  .loadedContent(content, UTF_8)
                                  .build());
         } finally {
            deleteDocuments(documentPath);
         }
      }
   }

   @Nested
   public class WhenFailFast {

      private AwsS3DocumentRepository documentRepository;

      @BeforeEach
      public void configureDocumentRepository() {
         assertThat(localstack.isRunning()).isTrue();

         AwsS3DocumentConfig config = new AwsS3DocumentConfigBuilder()
               .region(TEST_REGION)
               .endpoint(s3Url())
               .bucketName(TEST_BUCKET)
               .pathStyleAccess(true)
               .createIfMissingBucket(true)
               .failFastIfMissingBucket(true)
               .build();

         documentRepository = new AwsS3DocumentRepository(config);
         documentRepository.deleteBucket(TEST_BUCKET);
      }

      @Test
      public void testFailFastWhenMissingBucket() {
         AwsS3DocumentConfigBuilder config = new AwsS3DocumentConfigBuilder()
               .region(TEST_REGION)
               .endpoint(s3Url())
               .bucketName(TEST_BUCKET)
               .pathStyleAccess(true)
               .failFastIfMissingBucket(true)
               .uploadChunkSize(CHUNK_SIZE);

         assertThatExceptionOfType(DocumentAccessException.class)
               .isThrownBy(() -> new AwsS3DocumentRepository(config.createIfMissingBucket(false).build()))
               .withMessage("Can't create 'test-bucket' missing bucket");

         new AwsS3DocumentRepository(config.createIfMissingBucket(true).build());
      }

   }

   @Nested
   public class WhenMissingBucket {

      private AwsS3DocumentRepository documentRepository;

      @BeforeEach
      public void configureDocumentRepository() {
         assertThat(localstack.isRunning()).isTrue();

         documentRepository = new AwsS3DocumentRepository(awsS3Config());
         documentRepository.deleteBucket(TEST_BUCKET);
      }

      private AwsS3DocumentConfig awsS3Config() {
         return new AwsS3DocumentConfigBuilder()
               .region(TEST_REGION)
               .endpoint(s3Url())
               .bucketName(TEST_BUCKET)
               .pathStyleAccess(true)
               .createIfMissingBucket(false)
               .failFastIfMissingBucket(false)
               .uploadChunkSize(CHUNK_SIZE)
               .build();
      }

      @Test
      public void testOpenDocumentWithNoMultipartWhenMissingBucket() {
         assertThatExceptionOfType(DocumentAccessException.class)
               .isThrownBy(() -> documentRepository
                     .openDocument(DocumentPath.of("file.txt"),
                                   false,
                                   false,
                                   OpenDocumentMetadataBuilder.empty())
                     .ifPresent(document -> {
                        try (Writer writer = document.content().writerContent()) {
                           writer.write("content");
                        } catch (IOException e) {
                           throw new IllegalStateException(e);
                        }
                     }))
               .withMessage("Can't create 'test-bucket' missing bucket");
      }

      /**
       * Document must be > chunk size for multipart upload to be enabled.
       */
      @Test
      public void testOpenDocumentWithMultipartWhenMissingBucket() {
         assertThatExceptionOfType(DocumentAccessException.class)
               .isThrownBy(() -> documentRepository
                     .openDocument(DocumentPath.of("file.txt"),
                                   false,
                                   false,
                                   OpenDocumentMetadataBuilder.empty())
                     .ifPresent(document -> {
                        try (Writer writer = document.content().writerContent()) {
                           String content = StringUtils.repeat('0', 2 * CHUNK_SIZE + 1);
                           writer.write(content);
                        } catch (IOException e) {
                           throw new IllegalStateException(e);
                        }
                     }))
               .withMessage("Can't create 'test-bucket' missing bucket");
      }

      @Test
      public void testSaveDocumentWithNoMultipartWhenMissingBucket() {
         assertThatExceptionOfType(DocumentAccessException.class)
               .isThrownBy(() -> documentRepository.saveDocument(new DocumentBuilder()
                                                                       .documentId(DocumentPath.of("file.txt"))
                                                                       .loadedContent("content", UTF_8)
                                                                       .build(), false))
               .withMessage("Can't create 'test-bucket' missing bucket");
      }

      /**
       * Document must be > chunk size for multipart upload to be enabled.
       */
      @Test
      public void testSaveDocumentWithMultipartWhenMissingBucket() {
         String content = StringUtils.repeat('0', 2 * CHUNK_SIZE + 1);
         assertThatExceptionOfType(DocumentAccessException.class)
               .isThrownBy(() -> documentRepository.saveDocument(new DocumentBuilder()
                                                                       .documentId(DocumentPath.of("file.txt"))
                                                                       .streamContent(content, UTF_8)
                                                                       .build(), false))
               .withMessage("Can't create 'test-bucket' missing bucket");
      }
   }

}
