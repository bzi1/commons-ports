/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.azureblob;

import static com.tinubu.commons.ddd2.domain.type.support.TypeSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.valueformatter.HiddenValueFormatter;
import com.tinubu.commons.ports.document.azureblob.AzureBlobDocumentRepository.BlobType;

/**
 * Azure Blob document repository configuration.
 */
public class AzureBlobDocumentConfig extends AbstractValue {
   /** Default Blob type when unspecified. */
   private static final BlobType DEFAULT_BLOB_TYPE = BlobType.BLOCK;

   private final String containerName;
   private final String connectionString;
   private final String endpoint;
   private final AzureAuthentication authentication;
   private final boolean createIfMissingContainer;
   private final BlobType defaultBlobType;

   private AzureBlobDocumentConfig(AzureBlobDocumentConfigBuilder builder) {
      this.containerName = builder.containerName;
      this.connectionString = builder.connectionString;
      this.endpoint = builder.endpoint;
      this.authentication = builder.authentication;
      this.createIfMissingContainer = nullable(builder.createIfMissingContainer, false);
      this.defaultBlobType = nullable(builder.defaultBlobType, DEFAULT_BLOB_TYPE);
   }

   @Override
   public Fields<? extends AzureBlobDocumentConfig> defineDomainFields() {
      return Fields
            .<AzureBlobDocumentConfig>builder()
            .field("containerName", v -> v.containerName, isNotBlank())
            .field("connectionString",
                   v -> v.connectionString,
                   v -> protectedConnectionString(v.connectionString),
                   isNull().orValue(isNotBlank()))
            .field("endpoint", v -> v.endpoint, isNotBlank().ifIsSatisfied(() -> connectionString == null))
            .field("authentication",
                   v -> v.authentication,
                   isNotNull().ifIsSatisfied(() -> connectionString == null))
            .field("createIfMissingContainer", v -> v.createIfMissingContainer)
            .field("defaultBlobType", v -> v.defaultBlobType)
            .build();
   }

   /**
    * Azure Blob endpoint. e.g.: {@code http://127.0.0.1:10000/devstoreaccount1}.
    * Must be set if {@link #connectionString} is not set.
    */
   public String endpoint() {
      return endpoint;
   }

   /**
    * Azure Blob connection string. If set, {@link #endpoint} and {@link #authentication} will be ignored.
    * e.g.: {@code
    * DefaultEndpointsProtocol=http;AccountName=devstoreaccount1;AccountKey=Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==;BlobEndpoint=http://127.0.0.1:10000/devstoreaccount1;}
    */
   public String connectionString() {
      return connectionString;
   }

   /**
    * Azure Blob container name.
    */
   public String containerName() {
      return containerName;
   }

   /**
    * Whether to automatically create missing container. Default to {@code false}.
    */
   public boolean createIfMissingContainer() {
      return createIfMissingContainer;
   }

   /**
    * Default blob type to use for blob creation. Can be overridden by document metadata (`azure.blob.type`).
    * Default to {@link #DEFAULT_BLOB_TYPE}.
    */
   public BlobType defaultBlobType() {
      return defaultBlobType;
   }

   /**
    * Authentication account name/key.
    * Must be set if {@link #connectionString} is not set.
    */
   public AzureAuthentication authentication() {
      return authentication;
   }

   /**
    * Protects possible credentials in connection string.
    */
   private static String protectedConnectionString(String connectionString) {
      if (connectionString != null) {
         return connectionString
               .replaceAll("AccountName=[^;]+", "AccountName=<protected>")
               .replaceAll("AccountKey=[^;]+", "AccountKey=<protected>");
      } else {
         return null;
      }
   }

   public static class AzureAuthentication extends AbstractValue {
      /**
       * Azure account name.
       */
      private final String storageAccountName;

      /**
       * Azure authentication key.
       */
      private final String storageAccountKey;

      private AzureAuthentication(String storageAccountName, String storageAccountKey) {
         this.storageAccountName = storageAccountName;
         this.storageAccountKey = storageAccountKey;
      }

      public static AzureAuthentication of(String storageAccountName, String storageAccountKey) {
         return checkInvariants(new AzureAuthentication(storageAccountName, storageAccountKey));
      }

      @Override
      public Fields<? extends AzureAuthentication> defineDomainFields() {
         return Fields
               .<AzureAuthentication>builder()
               .field("storageAccountName",
                      v -> v.storageAccountName,
                      v -> new HiddenValueFormatter().compose(AzureAuthentication::storageAccountName),
                      isNotBlank())
               .field("storageAccountKey",
                      v -> v.storageAccountKey,
                      v -> new HiddenValueFormatter().compose(AzureAuthentication::storageAccountKey),
                      isNotBlank())
               .build();
      }

      public String storageAccountName() {
         return storageAccountName;
      }

      public String storageAccountKey() {
         return storageAccountKey;
      }

   }

   public static class AzureBlobDocumentConfigBuilder extends DomainBuilder<AzureBlobDocumentConfig> {
      private String containerName;
      private String connectionString;
      private String endpoint;
      private AzureAuthentication authentication;
      private Boolean createIfMissingContainer;
      private BlobType defaultBlobType;

      public AzureBlobDocumentConfigBuilder containerName(String containerName) {
         this.containerName = containerName;
         return this;
      }

      public AzureBlobDocumentConfigBuilder connectionString(String connectionString) {
         this.connectionString = connectionString;
         return this;
      }

      public AzureBlobDocumentConfigBuilder endpoint(String endpoint) {
         this.endpoint = endpoint;
         return this;
      }

      public AzureBlobDocumentConfigBuilder authentication(AzureAuthentication authentication) {
         this.authentication = authentication;
         return this;
      }

      public AzureBlobDocumentConfigBuilder createIfMissingContainer(boolean createIfMissingContainer) {
         this.createIfMissingContainer = createIfMissingContainer;
         return this;
      }

      public AzureBlobDocumentConfigBuilder defaultBlobType(BlobType defaultBlobType) {
         this.defaultBlobType = defaultBlobType;
         return this;
      }

      @Override
      protected AzureBlobDocumentConfig buildDomainObject() {
         return new AzureBlobDocumentConfig(this);
      }
   }
}
