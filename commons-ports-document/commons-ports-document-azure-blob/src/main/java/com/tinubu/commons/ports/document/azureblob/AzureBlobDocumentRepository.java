/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.azureblob;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.optionalInstanceOf;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static com.tinubu.commons.ports.document.azureblob.AzureBlobDocumentRepository.BlobType.APPEND;
import static com.tinubu.commons.ports.document.azureblob.AzureBlobDocumentRepository.BlobType.BLOCK;
import static com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import static com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import static com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import static com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import static java.util.stream.Collectors.toList;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azure.core.util.Context;
import com.azure.storage.blob.BlobClient;
import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobServiceClient;
import com.azure.storage.blob.BlobServiceClientBuilder;
import com.azure.storage.blob.models.AppendBlobRequestConditions;
import com.azure.storage.blob.models.BlobHttpHeaders;
import com.azure.storage.blob.models.BlobItem;
import com.azure.storage.blob.models.BlobProperties;
import com.azure.storage.blob.models.BlobRequestConditions;
import com.azure.storage.blob.models.BlobStorageException;
import com.azure.storage.blob.models.ParallelTransferOptions;
import com.azure.storage.blob.options.AppendBlobCreateOptions;
import com.azure.storage.blob.options.BlobParallelUploadOptions;
import com.azure.storage.blob.options.BlockBlobOutputStreamOptions;
import com.azure.storage.blob.specialized.AppendBlobClient;
import com.azure.storage.blob.specialized.BlobOutputStream;
import com.azure.storage.blob.specialized.BlockBlobAsyncClient;
import com.azure.storage.blob.specialized.BlockBlobClient;
import com.azure.storage.common.StorageSharedKeyCredential;
import com.azure.storage.common.implementation.Constants;
import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.lang.mapper.EnumMapper;
import com.tinubu.commons.lang.mimetype.MimeTypeFactory;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.UnsupportedUriException;
import com.tinubu.commons.ports.document.domain.UriAdapter;
import com.tinubu.commons.ports.document.transformer.DocumentTransformerUriAdapter;

/**
 * Azure Blob {@link DocumentRepository} adapter implementation.
 * <p>
 * If configured container is missing, the container will be created only at write time, or the write
 * operation will fail if repository configuration does not instruct to do so.
 */
public class AzureBlobDocumentRepository extends AbstractDocumentRepository {
   private static final Logger log = LoggerFactory.getLogger(AzureBlobDocumentRepository.class);

   /**
    * Document attribute to specify the Blob type on Azure storage.
    * Supported values are {@link BlobType#attributeValue()}.
    *
    * @see <a
    *       href="https://learn.microsoft.com/fr-fr/rest/api/storageservices/understanding-block-blobs--append-blobs--and-page-blobs">Azure
    *       blob types</a>
    */
   private static final String BLOB_TYPE_ATTRIBUTE = "azure.blob.type";

   /**
    * Azure block (chunk) size to transfer at a time (max supported is
    * {@value BlockBlobAsyncClient#MAX_STAGE_BLOCK_BYTES_LONG}).
    * Memory consumption at any instant can be up to {@value #BLOCK_SIZE} * {@value MAX_CONCURRENCY} *
    * {@code number of concurrent upload operations}.
    */
   private static final long BLOCK_SIZE = 2L * 1024 * 1024;

   /**
    * The maximum number of workers that will upload chunks concurrently for a single upload operation. Note
    * that there's no shared thread pools between operations, so that saving multiple documents in concurrency
    * can lead to an indefinite number of working threads.
    */
   private static final int MAX_CONCURRENCY = 5;

   private final BlobContainerClient blobContainerClient;
   /** Hash of a set of server connection parameters identifying a similar repository. */
   private final int sameRepositoryHash;
   private final DocumentTransformerUriAdapter transformerUriAdapter;
   /** Default {@link BlobType} when {@link #BLOB_TYPE_ATTRIBUTE} is not set in document metadata. */
   private final BlobType defaultBlobType;

   public AzureBlobDocumentRepository(AzureBlobDocumentConfig azureBlobDocumentConfig,
                                      BlobServiceClient blobServiceClient) {
      this.blobContainerClient = blobContainerClient(blobServiceClient, azureBlobDocumentConfig);
      this.sameRepositoryHash = sameRepositoryHash(azureBlobDocumentConfig);
      this.transformerUriAdapter = new DocumentTransformerUriAdapter(new AzureBlobUriAdapter());
      this.defaultBlobType = azureBlobDocumentConfig.defaultBlobType();
   }

   public AzureBlobDocumentRepository(AzureBlobDocumentConfig azureBlobDocumentConfig) {
      this(azureBlobDocumentConfig, blobServiceClient(azureBlobDocumentConfig));
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      return documentRepository instanceof AzureBlobDocumentRepository
             && Objects.equals(((AzureBlobDocumentRepository) documentRepository).sameRepositoryHash,
                               sameRepositoryHash);
   }

   @Override
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      notNull(documentId, "documentId");
      notNull(metadata, "metadata");

      startWatch();

      return handleDocumentEvent(() -> {
         Path blobFile = blobFile(documentId);

         Document document = new DocumentBuilder()
               .documentId(documentId)
               .content(new OutputStreamDocumentContentBuilder()
                              .content(new ByteArrayOutputStream(), metadata.contentEncoding().orElse(null))
                              .build())
               .chain(metadata.chainDocumentBuilder())
               .build();

         try {
            BlobType blobType = blobType(document.metadata()).orElse(append ? APPEND : defaultBlobType);

            BlobOutputStream blobOutputStream;
            switch (blobType) {
               case APPEND:
                  if (append) {
                     throw new IllegalArgumentException("Append mode not supported with output streams");
                  }

                  AppendBlobClient appendBlobClient = appendBlobClient(blobFile);
                  appendBlobClient.createWithResponse(appendBlobCreateOptions(document.metadata(),
                                                                              overwrite || append),
                                                      null,
                                                      Context.NONE);
                  blobOutputStream =
                        appendBlobClient.getBlobOutputStream(appendBlobRequestConditions(overwrite));

                  break;

               case BLOCK:
                  if (append) {
                     throw new IllegalArgumentException("Append mode not supported for Block Blobs");
                  }

                  BlockBlobClient blockBlobClient = blockBlobClient(blobFile);

                  if (!overwrite && blockBlobClient.exists()) {
                     return optional();
                  }

                  blobOutputStream =
                        blockBlobClient.getBlobOutputStream(blockBlobOutputStreamOptions(document.metadata(),
                                                                                         overwrite));
                  break;
               default:
                  throw new IllegalStateException("Unknown Blob type");
            }

            return optional(DocumentBuilder
                                  .from(document)
                                  .content(new OutputStreamDocumentContentBuilder()
                                                 .content(new BufferedOutputStream(blobOutputStream))
                                                 .build())
                                  .build());
         } catch (UncheckedIOException e) {
            throw new DocumentAccessException(e);
         } catch (BlobStorageException e) {
            if (blobAlreadyExists(e)) {
               return optional();
            } else {
               throw new DocumentAccessException(e);
            }
         }
      }, d -> documentSaved(d.documentEntry()));
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      notNull(documentId, "documentId");

      startWatch();

      return handleDocumentEvent(() -> {
         Path blobFile = blobFile(documentId);

         return azureBlobDocumentEntry(blobFile, documentId).flatMap(entry -> azureBlobDocumentContent(
               blobFile,
               blobType(entry.metadata()).orElse(defaultBlobType)).map(content -> new DocumentBuilder()
               .<DocumentBuilder>reconstitute()
               .documentEntry(entry)
               .content(content)
               .build()));
      }, d -> documentAccessed(d.documentEntry()));
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      return findDocumentEntriesBySpecification(basePath, specification).flatMap(entry -> stream(
            azureBlobDocumentContent(blobFile(entry.documentId()),
                                     blobType(entry.metadata()).orElse(defaultBlobType)).map(content -> new DocumentBuilder()
                  .<DocumentBuilder>reconstitute()
                  .documentEntry(entry)
                  .content(content)
                  .build())));
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      notNull(documentId, "documentId");

      startWatch();

      return handleDocumentEntryEvent(() -> {
         Path blobFile = blobFile(documentId);

         return azureBlobDocumentEntry(blobFile, documentId);
      }, this::documentAccessed);
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();
      notNull(specification, "specification");

      startWatch();

      return handleDocumentEntryStreamEvent(() -> listDocumentEntries(basePath,
                                                                      directoryFilter(specification)).filter(
            specification::satisfiedBy), this::documentAccessed);
   }

   @Override
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      notNull(document, "document");

      if (document.content() instanceof OutputStreamDocumentContent) {
         throw new IllegalArgumentException(
               "Unsupported operation for this document content implementation : " + document
                     .content()
                     .getClass()
                     .getName());
      }

      startWatch();

      return handleDocumentEntryEvent(() -> {

         Path blobFile = blobFile(document.documentId());

         long contentLength = document
               .content()
               .contentSize()
               .orElseThrow(() -> new IllegalStateException(String.format(
                     "Unknown '%s' document content size. Content size is required to upload content to Azure blob",
                     document.documentId().stringValue())));

         Optional<DocumentEntry> savedDocument;
         try (InputStream inputStream = new BufferedInputStream(document.content().inputStreamContent())) {

            switch (blobType(document.metadata()).orElse(defaultBlobType)) {
               case APPEND:
                  AppendBlobClient appendBlobClient = appendBlobClient(blobFile);

                  appendBlobClient.createWithResponse(appendBlobCreateOptions(document.metadata(), overwrite),
                                                      null,
                                                      Context.NONE);
                  appendBlobClient.appendBlock(inputStream, contentLength);
                  break;
               case BLOCK:
                  BlobClient blobClient = blobClient(blobFile);

                  blobClient
                        .uploadWithResponse(blockBlobParallelUploadOptions(document.metadata(),
                                                                           inputStream,
                                                                           overwrite), null, Context.NONE)
                        .getValue();
                  break;
               default:
                  throw new IllegalStateException("Unknown Blob type");
            }
            savedDocument = azureBlobDocumentEntry(blobFile, document.documentId());
         } catch (IOException | UncheckedIOException e) {
            throw new DocumentAccessException(e);
         } catch (BlobStorageException e) {
            if (blobAlreadyExists(e)) {
               savedDocument = optional();
            } else {
               throw new DocumentAccessException(e);
            }
         }

         return savedDocument;
      }, this::documentSaved);
   }

   @Override
   public Optional<DocumentEntry> deleteDocument(DocumentPath documentId) {
      notNull(documentId, "documentId");

      startWatch();

      return handleDocumentEntryEvent(() -> {

         Path blobFile = blobFile(documentId);

         Optional<DocumentEntry> documentEntry = azureBlobDocumentEntry(blobFile, documentId);

         Optional<DocumentEntry> deletedDocument = documentEntry;

         if (documentEntry.isPresent()) {

            try {
               BlobClient blobClient;
               switch (blobType(blobProperties(blobFile).getBlobType())) {
                  case APPEND:
                  case BLOCK:
                     blobClient = blobClient(blobFile);
                     break;
                  default:
                     throw new IllegalStateException("Unknown Blob type");
               }

               blobClient.delete();
            } catch (BlobStorageException e) {
               if (blobNotFound(e)) {
                  deletedDocument = optional();
               } else {
                  throw new DocumentAccessException(e);
               }
            }
         }

         return deletedDocument;
      }, this::documentDeleted);
   }

   @Override
   public boolean supportsUri(URI documentUri) {
      return transformerUriAdapter.supportsUri(documentUri);
   }

   @Override
   public URI toUri(DocumentPath documentId) {
      return transformerUriAdapter.toUri(documentId);
   }

   @Override
   public Optional<Document> findDocumentByUri(URI documentUri) {
      return transformerUriAdapter.findDocumentByUri(documentUri);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
      return transformerUriAdapter.findDocumentEntryByUri(documentUri);
   }

   /**
    * Real Azure Blob URI adapter to be used by {@link DocumentTransformerUriAdapter}.
    */
   public class AzureBlobUriAdapter implements UriAdapter {
      @Override
      public boolean supportsUri(URI documentUri) {
         return false;
      }

      @Override
      public URI toUri(DocumentPath documentId) {
         throw new UnsupportedUriException("Repository has no URI representation");
      }

      @Override
      public Optional<Document> findDocumentByUri(URI documentUri) {
         satisfies(documentUri, this::supportsUri, "documentUri", "must be supported : " + documentUri);

         return findDocumentById(documentId(documentUri));
      }

      @Override
      public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
         satisfies(documentUri, this::supportsUri, "documentUri", "must be supported : " + documentUri);

         return findDocumentEntryById(documentId(documentUri));
      }

      /**
       * Extracts document identifier from specified document URI.
       *
       * @param documentUri document URI
       *
       * @return document identifier
       */
      protected DocumentPath documentId(URI documentUri) {
         throw new UnsupportedOperationException();
      }
   }

   public enum BlobType {
      BLOCK("block"), APPEND("append");

      private static final EnumMapper<BlobType, String> mapper =
            new EnumMapper<>(BlobType.class, BlobType::attributeValue);

      private final String attributeValue;

      BlobType(String attributeValue) {
         this.attributeValue = attributeValue;
      }

      public String attributeValue() {
         return attributeValue;
      }

      public static BlobType ofAttributeValue(String attributeValue) {
         notNull(attributeValue, "attributeValue");

         return mapper.map(attributeValue.toLowerCase());
      }
   }

   /** Generates a hash unique for a given server storage path to identify a similar repository. */
   private int sameRepositoryHash(AzureBlobDocumentConfig azureBlobDocumentConfig) {
      return Objects.hash(azureBlobDocumentConfig.endpoint(),
                          azureBlobDocumentConfig.connectionString(),
                          azureBlobDocumentConfig.containerName());
   }

   private Predicate<Path> directoryFilter(Specification<DocumentEntry> specification) {
      if (specification instanceof DocumentEntrySpecification) {
         DocumentEntrySpecification documentEntrySpecification = (DocumentEntrySpecification) specification;
         return documentEntrySpecification::satisfiedBySubPath;
      } else {
         return __ -> true;
      }
   }

   private Map<String, String> metadata(DocumentMetadata documentMetadata) {
      return map(documentMetadata
                       .attributes()
                       .entrySet()
                       .stream()
                       .map(e -> Pair.of(e.getKey(), e.getValue().toString())));
   }

   private BlockBlobOutputStreamOptions blockBlobOutputStreamOptions(DocumentMetadata documentMetadata,
                                                                     boolean overwrite) {
      BlobHttpHeaders headers = blobHttpHeaders(documentMetadata);
      BlobRequestConditions blobRequestConditions = blobRequestConditions(overwrite);

      return new BlockBlobOutputStreamOptions()
            .setParallelTransferOptions(parallelTransferOptions())
            .setHeaders(headers)
            .setMetadata(metadata(documentMetadata))
            .setRequestConditions(blobRequestConditions);
   }

   private BlobParallelUploadOptions blockBlobParallelUploadOptions(DocumentMetadata documentMetadata,
                                                                    InputStream inputStream,
                                                                    boolean overwrite) {
      BlobHttpHeaders headers = blobHttpHeaders(documentMetadata);
      BlobRequestConditions blobRequestConditions = blobRequestConditions(overwrite);

      return new BlobParallelUploadOptions(inputStream)
            .setParallelTransferOptions(parallelTransferOptions())
            .setHeaders(headers)
            .setMetadata(metadata(documentMetadata))
            .setRequestConditions(blobRequestConditions);
   }

   /**
    * Configuration used to parallelize data transfer operations.
    *
    * @return parallel transfer options configuration
    */
   private ParallelTransferOptions parallelTransferOptions() {
      return new ParallelTransferOptions().setBlockSizeLong(BLOCK_SIZE).setMaxConcurrency(MAX_CONCURRENCY);
   }

   private AppendBlobCreateOptions appendBlobCreateOptions(DocumentMetadata documentMetadata,
                                                           boolean overwrite) {
      BlobHttpHeaders headers = blobHttpHeaders(documentMetadata);
      BlobRequestConditions blobRequestConditions = blobRequestConditions(overwrite);

      return new AppendBlobCreateOptions()
            .setMetadata(metadata(documentMetadata))
            .setHeaders(headers)
            .setRequestConditions(blobRequestConditions);
   }

   private BlobRequestConditions blobRequestConditions(boolean overwrite) {
      BlobRequestConditions blobRequestConditions = new BlobRequestConditions();
      if (!overwrite) {
         blobRequestConditions.setIfNoneMatch(Constants.HeaderConstants.ETAG_WILDCARD);
      }
      return blobRequestConditions;
   }

   private AppendBlobRequestConditions appendBlobRequestConditions(boolean overwrite) {
      AppendBlobRequestConditions blobRequestConditions = new AppendBlobRequestConditions();
      if (!overwrite) {
         blobRequestConditions.setIfNoneMatch(Constants.HeaderConstants.ETAG_WILDCARD);
      }
      return blobRequestConditions;
   }

   private BlobHttpHeaders blobHttpHeaders(DocumentMetadata documentMetadata) {
      return new BlobHttpHeaders()
            .setContentType(documentMetadata.contentType().toString())
            .setContentEncoding(documentMetadata.contentEncoding().map(Charset::name).orElse(null));
   }

   /**
    * List document entries from specified base directory. Base directory represents a physical path in the
    * container. It can be relative or absolute, empty or not.
    *
    * @param baseDirectory base directory to search from
    * @param directoryFilter user-space directory filter.Use {@code __ -> false} to disable recursive
    *       search
    *
    * @return filtered document entries
    */
   private Stream<DocumentEntry> listDocumentEntries(Path baseDirectory, Predicate<Path> directoryFilter) {
      notNull(baseDirectory, "baseDirectory");
      notNull(directoryFilter, "directoryFilter");

      String azureBaseDirectory = baseDirectory.toString().isEmpty() ? "" : baseDirectory + "/";

      List<BlobItem> blobItems =
            blobContainerClient.listBlobsByHierarchy(azureBaseDirectory).stream().collect(toList());

      return blobItems.stream().flatMap(blobItem -> {
         Path blobPath = Paths.get(blobItem.getName());

         if (isBlobDir(blobItem)) {
            if (directoryFilter.test(blobPath)) {
               return listDocumentEntries(blobPath, directoryFilter);
            } else {
               return stream();
            }
         } else {
            return stream(azureBlobDocumentEntry(Paths.get(blobItem.getName()), null));
         }
      });

   }

   private boolean blobAlreadyExists(BlobStorageException e) {
      return e.getErrorCode().toString().equals("BlobAlreadyExists");
   }

   private boolean blobNotFound(BlobStorageException e) {
      return e.getErrorCode().toString().equals("BlobNotFound");
   }

   /**
    * Low-level document entry generation from Azure Blob file.
    *
    * @param blobFile Azure Blob file
    * @param documentId optional document identifier, when known, to optimize reverse document id
    *       mapping
    *
    * @return document entry or {@link Optional#empty} if blob file not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   private Optional<DocumentEntry> azureBlobDocumentEntry(Path blobFile, DocumentPath documentId) {
      return azureBlobMetadata(blobFile).map(metadata -> new DocumentEntryBuilder()
            .<DocumentEntryBuilder>reconstitute()
            .documentId(documentId != null ? documentId : documentId(blobFile))
            .metadata(metadata)
            .build());
   }

   /**
    * Low-level document content generation from Azure Blob file.
    *
    * @param blobFile Azure Blob file
    * @param blobType optional Blob type if already known
    *
    * @return document content or {@link Optional#empty} if blob file not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   private Optional<InputStreamDocumentContent> azureBlobDocumentContent(Path blobFile, BlobType blobType) {
      try {
         if (blobType == null) {
            blobType = blobType(blobProperties(blobFile).getBlobType());
         }

         BlobClient blobClient;
         switch (blobType) {
            case APPEND:
            case BLOCK:
               blobClient = blobClient(blobFile);
               break;
            default:
               throw new IllegalStateException("Unknown Blob type");
         }

         InputStream dataStream = blobClient.downloadContent().toStream();

         return optional(new InputStreamDocumentContentBuilder()
                               .<InputStreamDocumentContentBuilder>reconstitute()
                               .content(dataStream,
                                        nullable(blobClient.getProperties().getContentEncoding())
                                              .map(Charset::forName)
                                              .orElse(null),
                                        blobClient.getProperties().getBlobSize())
                               .build());
      } catch (BlobStorageException e) {
         if (blobNotFound(e)) {
            return optional();
         } else {
            throw new DocumentAccessException(e);
         }
      }

   }

   /**
    * Low-level document metadata generation from Azure Blob file.
    *
    * @param blobFile Azure Blob file
    *
    * @return document metadata or {@link Optional#empty} if blob file not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   private Optional<DocumentMetadata> azureBlobMetadata(Path blobFile) {
      try {
         BlobProperties properties = blobProperties(blobFile);

         return optional(new DocumentMetadataBuilder()
                               .<DocumentMetadataBuilder>reconstitute()
                               .documentPath(documentId(blobFile).value())
                               .contentSize(properties.getBlobSize())
                               .contentType(nullable(properties.getContentType())
                                                  .map(MimeTypeFactory::parseMimeType)
                                                  .orElse(null),
                                            nullable(properties.getContentEncoding())
                                                  .map(Charset::forName)
                                                  .orElse(null))
                               .attributes(map(properties.getMetadata()))
                               .addAttribute(BLOB_TYPE_ATTRIBUTE,
                                             blobType(properties.getBlobType()).attributeValue())
                               .<DocumentMetadataBuilder, OffsetDateTime>optionalChain(nullable(properties.getCreationTime()),
                                                                                       (b, creationDate) -> b.creationDate(
                                                                                             creationDate.toInstant()))
                               .<DocumentMetadataBuilder, OffsetDateTime>optionalChain(nullable(properties.getLastModified()),
                                                                                       (b, lastUpdateDate) -> b.lastUpdateDate(
                                                                                             lastUpdateDate.toInstant()))
                               .build());
      } catch (BlobStorageException e) {
         if (blobNotFound(e)) {
            return optional();
         } else {
            throw new DocumentAccessException(e);
         }
      }
   }

   private Optional<BlobType> blobType(DocumentMetadata documentMetadata) {
      return documentMetadata
            .attribute(BLOB_TYPE_ATTRIBUTE)
            .flatMap(bt -> optionalInstanceOf(bt, String.class))
            .map(BlobType::ofAttributeValue);
   }

   private BlobType blobType(com.azure.storage.blob.models.BlobType azureBlobType) {
      BlobType blobType;

      switch (azureBlobType) {
         case APPEND_BLOB:
            blobType = APPEND;
            break;
         case BLOCK_BLOB:
            blobType = BLOCK;
            break;
         default:
            throw new IllegalStateException(String.format("Unsupported '%s' Blob type", azureBlobType));
      }

      return blobType;
   }

   private BlobProperties blobProperties(Path blobFile) {
      return blobContainerClient.getBlobClient(blobFile.toString()).getProperties();
   }

   private BlockBlobClient blockBlobClient(Path blobFile) {
      return blobContainerClient.getBlobClient(blobFile.toString()).getBlockBlobClient();
   }

   private BlobClient blobClient(Path blobFile) {
      return blobContainerClient.getBlobClient(blobFile.toString());
   }

   private AppendBlobClient appendBlobClient(Path blobFile) {
      return blobContainerClient.getBlobClient(blobFile.toString()).getAppendBlobClient();
   }

   private boolean isBlobDir(BlobItem blobItem) {
      return Boolean.TRUE.equals(blobItem.isPrefix());
   }

   /**
    * Generates real Azure Blob path for specified document.
    *
    * @param documentId document id
    *
    * @return real Azure Blob path
    */
   public Path blobFile(DocumentPath documentId) {
      return documentId.value();
   }

   /**
    * Returns document identifier from Azure Blob file. Reverses real path to generate a
    * logical document path.
    *
    * @param blobFile Azure Blob file
    *
    * @return document identifier from Azure Blob file
    */
   private DocumentPath documentId(Path blobFile) {
      return DocumentPath.of(blobFile);
   }

   /**
    * Returns the authentication to connect to Azure Blob Service.
    *
    * @param azureBlobDocumentConfig adapter configuration
    *
    * @return Azure authentication
    */
   private static StorageSharedKeyCredential storageCredential(AzureBlobDocumentConfig azureBlobDocumentConfig) {
      return new StorageSharedKeyCredential(azureBlobDocumentConfig.authentication().storageAccountName(),
                                            azureBlobDocumentConfig.authentication().storageAccountKey());
   }

   private static BlobServiceClient blobServiceClient(AzureBlobDocumentConfig azureBlobDocumentConfig) {
      BlobServiceClientBuilder blobServiceClientBuilder = new BlobServiceClientBuilder();

      if (!StringUtils.isBlank(azureBlobDocumentConfig.connectionString())) {
         blobServiceClientBuilder.connectionString(azureBlobDocumentConfig.connectionString());
      } else {
         blobServiceClientBuilder.endpoint(azureBlobDocumentConfig.endpoint());
         blobServiceClientBuilder.credential(storageCredential(azureBlobDocumentConfig)).buildClient();
      }

      return blobServiceClientBuilder.buildClient();
   }

   private static BlobContainerClient blobContainerClient(BlobServiceClient blobServiceClient,
                                                          AzureBlobDocumentConfig azureBlobDocumentConfig) {
      String containerName = azureBlobDocumentConfig.containerName();
      BlobContainerClient blobContainerClient = blobServiceClient.getBlobContainerClient(containerName);

      if (!blobContainerClient.exists()) {
         if (azureBlobDocumentConfig.createIfMissingContainer()) {
            blobContainerClient.create();
            log.debug("Created missing '{}' container", containerName);
         } else {
            throw new DocumentAccessException(String.format("Unknown '%s' container", containerName));
         }
      }

      return blobContainerClient;
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", AzureBlobDocumentRepository.class.getSimpleName() + "[", "]").toString();
   }
}
