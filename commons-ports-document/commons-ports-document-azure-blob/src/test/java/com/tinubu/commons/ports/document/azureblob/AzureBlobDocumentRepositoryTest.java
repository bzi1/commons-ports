/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.azureblob;

import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.ports.document.azureblob.AzureBlobDocumentRepository.BlobType.APPEND;
import static com.tinubu.commons.ports.document.azureblob.AzureBlobDocumentRepository.BlobType.BLOCK;
import static org.assertj.core.api.Assertions.assertThat;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.function.UnaryOperator;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.images.PullPolicy;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import com.tinubu.commons.ports.document.azureblob.AzureBlobDocumentConfig.AzureAuthentication;
import com.tinubu.commons.ports.document.azureblob.AzureBlobDocumentConfig.AzureBlobDocumentConfigBuilder;
import com.tinubu.commons.ports.document.azureblob.AzureBlobDocumentRepository.BlobType;
import com.tinubu.commons.ports.document.domain.CommonDocumentRepositoryTest;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ZipTransformer;

@Testcontainers(disabledWithoutDocker = true)
public class AzureBlobDocumentRepositoryTest {
   private static final Logger log = LoggerFactory.getLogger(AzureBlobDocumentRepositoryTest.class);

   public static final DockerImageName AZURITE_DOCKER_IMAGE =
         DockerImageName.parse("mcr.microsoft.com/azure-storage/azurite").withTag("latest");
   private static final String ACCOUNT_NAME = "devstoreaccount1";
   private static final String ACCOUNT_KEY =
         "Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==";
   public static final String CONTAINER_NAME = "test-container";

   private AzureBlobDocumentRepository documentRepository;

   @Container
   private static final GenericContainer<?> azurite = new GenericContainer<>(AZURITE_DOCKER_IMAGE)
         .withImagePullPolicy(PullPolicy.alwaysPull())
         .withExposedPorts(10000)
         .withLogConsumer(new Slf4jLogConsumer(log));

   private static AzureBlobDocumentConfig azureBlobConfig(BlobType defaultBlobType) {
      return new AzureBlobDocumentConfigBuilder()
            .endpoint(azureBlobEndpoint("http",
                                        azurite.getHost(),
                                        azurite.getFirstMappedPort(),
                                        ACCOUNT_NAME).toString())
            .authentication(AzureAuthentication.of(ACCOUNT_NAME, ACCOUNT_KEY))
            .containerName(CONTAINER_NAME)
            .createIfMissingContainer(true)
            .defaultBlobType(defaultBlobType)
            .build();
   }

   @AfterAll
   public static void afterAll() {
      azurite.close();
   }

   public abstract class AbstractAzureDocumentRepositoryTest extends CommonDocumentRepositoryTest {

      @Override
      protected DocumentRepository documentRepository() {
         return documentRepository;
      }

      @Override
      protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
         return new ZipTransformer(zipPath).compress(documents);
      }

      @Override
      public boolean isUpdatingCreationDateOnOverwrite() {
         return true;
      }

      @Override
      public boolean isSupportingUri() {
         return false;
      }

   }

   @Nested
   public class WhenBlockBlob extends AbstractAzureDocumentRepositoryTest {

      @BeforeEach
      public void configureDocumentRepository() {
         assertThat(azurite.isRunning()).isTrue();

         AzureBlobDocumentRepositoryTest.this.documentRepository =
               new AzureBlobDocumentRepository(azureBlobConfig(BLOCK));
      }

      @Override
      protected UnaryOperator<DocumentMetadataBuilder> synchronizeExpectedMetadata(DocumentMetadata actual) {
         return builder -> builder.chain(super.synchronizeExpectedMetadata(actual))
               .attributes(actual.attributes());
      }

      @Override
      @Disabled("Not supported")
      public void testOpenDocumentWhenOverwriteAndAppend() {}

      @Override
      @Disabled("Not supported")
      public void testOpenDocumentWhenNotOverwriteAndAppend() {}
   }

   @Nested
   public class WhenAppendBlob extends AbstractAzureDocumentRepositoryTest {

      @BeforeEach
      public void configureDocumentRepository() {
         assertThat(azurite.isRunning()).isTrue();

         AzureBlobDocumentRepositoryTest.this.documentRepository =
               new AzureBlobDocumentRepository(azureBlobConfig(APPEND));
      }

      @Override
      protected UnaryOperator<DocumentMetadataBuilder> synchronizeExpectedMetadata(DocumentMetadata actual) {
         return builder -> builder.chain(super.synchronizeExpectedMetadata(actual))
               .attributes(actual.attributes());
      }

      @Override
      @Disabled("Not supported")
      public void testOpenDocumentWhenOverwriteAndAppend() {}

      @Override
      @Disabled("Not supported")
      public void testOpenDocumentWhenNotOverwriteAndAppend() {}
   }

   private static URI azureBlobEndpoint(String protocol, String host, int port, String accountName) {
      notBlank(protocol, "protocol");
      notBlank(host, "host");
      notBlank(accountName, "accountName");

      return uri(protocol, host, port).resolve(accountName);
   }

   private static URI uri(String protocol, String host, int port) {
      try {
         return new URI(protocol, null, host, port, "/", null, null);
      } catch (URISyntaxException e) {
         throw new IllegalStateException(e);
      }
   }
}
