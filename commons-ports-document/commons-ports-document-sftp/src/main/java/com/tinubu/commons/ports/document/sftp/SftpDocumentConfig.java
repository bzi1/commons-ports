/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp;

import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.isNotEmptyBytes;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isStrictlyPositive;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.IOUtils;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.valueformatter.HiddenValueFormatter;

/**
 * SFTP document repository configuration.
 */
// FIXME createIfMissingBasePath + failFastIfMissingBasePath ?
public class SftpDocumentConfig extends AbstractValue {
   private static final String DEFAULT_HOST = "localhost";
   private static final int DEFAULT_PORT = 22;
   private static final String DEFAULT_USERNAME = System.getProperty("user.name");
   private static final byte[] DEFAULT_PRIVATE_KEY =
         fileContent(Paths.get(System.getProperty("user.home")).resolve(".ssh/id_rsa"), true);
   /**
    * Default SFTP caching session pool size. Value must be > 0.
    */
   private static final int DEFAULT_SESSION_POOL_SIZE = 10;

   private final String host;
   private final int port;
   private final String username;
   private final String password;
   private final byte[] privateKey;
   private final Path basePath;
   private final int sessionPoolSize;

   public SftpDocumentConfig(SftpDocumentConfigBuilder builder) {
      this.host = nullable(builder.host, DEFAULT_HOST);
      this.port = nullable(builder.port, DEFAULT_PORT);
      this.username = nullable(builder.username, DEFAULT_USERNAME);
      this.password = builder.password;
      this.privateKey = nullable(builder.privateKey, DEFAULT_PRIVATE_KEY);
      this.basePath = nullable(builder.basePath, Paths.get(""));
      this.sessionPoolSize = nullable(builder.sessionPoolSize, DEFAULT_SESSION_POOL_SIZE);
   }

   @Override
   public Fields<? extends SftpDocumentConfig> defineDomainFields() {
      return Fields
            .<SftpDocumentConfig>builder()
            .field("host", v -> v.host, isNotBlank())
            .field("port", v -> v.port, isStrictlyPositive())
            .field("username", v -> v.username, isNotBlank())
            .field("password",
                   v -> v.password,
                   new HiddenValueFormatter().compose(SftpDocumentConfig::password),
                   isNotBlank().ifIsSatisfied(() -> privateKey == null))
            .field("privateKey",
                   v -> v.privateKey,
                   new HiddenValueFormatter().compose(SftpDocumentConfig::privateKey),
                   isNull().orValue(isNotEmptyBytes()))
            .field("basePath", v -> v.basePath, isNotNull())
            .field("sessionPoolSize", v -> v.sessionPoolSize, isStrictlyPositive())
            .build();
   }

   /**
    * SFTP server host. Default to {@link #DEFAULT_HOST}.
    */
   public String host() {
      return host;
   }

   /**
    * SFTP server port. Default to {@link #DEFAULT_PORT}.
    */
   public Integer port() {
      return port;
   }

   /**
    * SFTP server authentication username.
    * Default to {@code ${user.name}}.
    */
   public String username() {
      return username;
   }

   /**
    * Optional SFTP server authentication password.
    * Use either privateKey or password for authentication. Use password authentication if both set.
    */
   public String password() {
      return password;
   }

   /**
    * Optional SFTP server authentication private key.
    * Use either privateKey or password for authentication. Use password authentication if both set.
    * Default to {@code ${user.home}/.ssh/id_rsa}.
    */
   public byte[] privateKey() {
      return privateKey;
   }

   /**
    * Optional base path to store documents in SFTP server. If omitted, the SFTP server default directory will
    * be used.
    */
   public Path basePath() {
      return basePath;
   }

   /**
    * Optional SFTP caching session pool size. Value must be > 0. Default to
    * {@link #DEFAULT_SESSION_POOL_SIZE}.
    *
    * @return SFTP caching session pool size
    */
   public int sessionPoolSize() {
      return sessionPoolSize;
   }

   private static byte[] fileContent(Path path, boolean ignoreIfNotFound) {
      try (FileInputStream fis = new FileInputStream(path.toString())) {
         return IOUtils.toByteArray(fis);
      } catch (FileNotFoundException e) {
         if (!ignoreIfNotFound) {
            throw new IllegalStateException(String.format("Unknown '%s' path", path));
         } else {
            return null;
         }
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   public static class SftpDocumentConfigBuilder extends DomainBuilder<SftpDocumentConfig> {
      private String host;
      private Integer port;
      private String username;
      private String password;
      private byte[] privateKey;
      private Path basePath;
      private Integer sessionPoolSize;

      public SftpDocumentConfigBuilder host(String host) {
         this.host = host;
         return this;
      }

      public SftpDocumentConfigBuilder port(Integer port) {
         this.port = port;
         return this;
      }

      public SftpDocumentConfigBuilder username(String username) {
         this.username = username;
         return this;
      }

      public SftpDocumentConfigBuilder password(String password) {
         this.password = password;
         return this;
      }

      public SftpDocumentConfigBuilder privateKey(byte[] privateKey) {
         this.privateKey = privateKey;
         return this;
      }

      public SftpDocumentConfigBuilder basePath(Path basePath) {
         this.basePath = basePath;
         return this;
      }

      public SftpDocumentConfigBuilder sessionPoolSize(Integer sessionPoolSize) {
         this.sessionPoolSize = sessionPoolSize;
         return this;
      }

      @Override
      protected SftpDocumentConfig buildDomainObject() {
         return new SftpDocumentConfig(this);
      }
   }
}
