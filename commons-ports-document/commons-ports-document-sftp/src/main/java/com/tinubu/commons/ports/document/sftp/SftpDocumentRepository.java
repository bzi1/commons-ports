/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.integration.file.remote.session.CachingSessionFactory;
import org.springframework.integration.file.remote.session.Session;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;

import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.invariant.rules.UriRules;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.UnsupportedUriException;
import com.tinubu.commons.ports.document.domain.UriAdapter;
import com.tinubu.commons.ports.document.transformer.DocumentTransformerUriAdapter;

/**
 * SFTP {@link DocumentRepository} adapter implementation.
 * <p>
 * This implementation uses a {@link CachingSessionFactory SFTP connection pool}, that must be closed before
 * leaving the application. This is done transparently by {@link AutoCloseable#close()} method implementation.
 */
public class SftpDocumentRepository extends AbstractDocumentRepository {
   private static final Logger log = LoggerFactory.getLogger(SftpDocumentRepository.class);

   /** Whether to allow unknown SFTP server keys. */
   public static final boolean SESSION_ALLOW_UNKNOWN_KEYS = true;
   /** Whether to test connections in caching pool. */
   public static final boolean CACHING_SESSION_TEST_SESSION = true;
   /** Caching pool connection wait timeout in milliseconds. */
   public static final int CACHING_SESSION_WAIT_TIMEOUT = 5000;

   private final SessionFactory<LsEntry> sftpSessionFactory;
   private final Path basePath;
   private final SftpDocumentConfig sftpDocumentConfig;

   /** Hash of a set of server connection parameters identifying a similar repository. */
   private final int sameRepositoryHash;
   private final DocumentTransformerUriAdapter transformerUriAdapter;

   SftpDocumentRepository(SftpDocumentConfig sftpDocumentConfig, SessionFactory<LsEntry> sftpSessionFactory) {
      this.sftpDocumentConfig = notNull(sftpDocumentConfig, "sftpDocumentConfig");
      this.sftpSessionFactory = notNull(sftpSessionFactory, "sftpSessionFactory");
      this.basePath = sftpDocumentConfig.basePath();
      this.sameRepositoryHash = sameRepositoryHash(sftpDocumentConfig);
      this.transformerUriAdapter = new DocumentTransformerUriAdapter(new SftpUriAdapter());
   }

   public SftpDocumentRepository(SftpDocumentConfig sftpDocumentConfig) {
      this(sftpDocumentConfig, sftpSessionFactory(sftpDocumentConfig));
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      return documentRepository instanceof SftpDocumentRepository
             && Objects.equals(((SftpDocumentRepository) documentRepository).sameRepositoryHash,
                               sameRepositoryHash);
   }

   @Override
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      throw new UnsupportedOperationException("Unsupported operation for this repository");
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      notNull(documentId, "documentId");

      startWatch();

      return handleDocumentEvent(() -> {
         try (Session<LsEntry> session = sftpSessionFactory.getSession()) {
            Path sftpFile = sftpFile(documentId);

            return documentEntry(session, sftpFile).map(entry -> new DocumentBuilder()
                  .<DocumentBuilder>reconstitute()
                  .documentEntry(entry)
                  .content(sftpDocumentContent(session,
                                               sftpFile,
                                               entry.metadata().contentSize().orElse(null)))
                  .build());
         } catch (UncheckedIOException e) {
            throw new DocumentAccessException(e);
         }
      }, d -> documentAccessed(d.documentEntry()));
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();
      notNull(specification, "specification");

      startWatch();

      return handleDocumentStreamEvent(() -> {
         try (Session<LsEntry> session = sftpSessionFactory.getSession()) {
            return listDocumentEntries(session, sftpDirectory(basePath), directoryFilter(specification))
                  .filter(specification)
                  .map(entry -> new DocumentBuilder()
                        .<DocumentBuilder>reconstitute()
                        .documentEntry(entry)
                        .content(sftpDocumentContent(session,
                                                     sftpFile(entry.documentId()),
                                                     entry.metadata().contentSize().orElse(null)))
                        .build());
         } catch (UncheckedIOException e) {
            throw new DocumentAccessException(e);
         }
      }, d -> documentAccessed(d.documentEntry()));
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      notNull(documentId, "documentId");

      startWatch();

      return handleDocumentEntryEvent(() -> {
         try (Session<LsEntry> session = sftpSessionFactory.getSession()) {
            Path sftpFile = sftpFile(documentId);

            return documentEntry(session, sftpFile);
         } catch (UncheckedIOException e) {
            throw new DocumentAccessException(e);
         }
      }, this::documentAccessed);
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();
      notNull(specification, "specification");

      startWatch();

      return handleDocumentEntryStreamEvent(() -> {
         try (Session<LsEntry> session = sftpSessionFactory.getSession()) {
            return listDocumentEntries(session,
                                       sftpDirectory(basePath),
                                       directoryFilter(specification)).filter(specification);
         } catch (UncheckedIOException e) {
            throw new DocumentAccessException(e);
         }
      }, this::documentAccessed);
   }

   @Override
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      notNull(document, "document");

      if (document.content() instanceof OutputStreamDocumentContent) {
         throw new IllegalArgumentException(
               "Unsupported operation for this document content implementation : " + document
                     .content()
                     .getClass()
                     .getName());
      }

      startWatch();

      return handleDocumentEntryEvent(() -> {

         try (Session<LsEntry> session = sftpSessionFactory.getSession()) {
            Path sftpFile = sftpFile(document.documentId());
            if (overwrite || !session.exists(sftpFile.toString())) {

               if (sftpFile.getParent() != null) {
                  sftpMkdirs(session, sftpFile.getParent());
               }

               try (InputStream inputStream = document.content().inputStreamContent()) {
                  session.write(inputStream, sftpFile.toString());
               }

               return documentEntry(session, sftpFile);
            }

            return optional();
         } catch (IOException | UncheckedIOException e) {
            throw new DocumentAccessException(e);
         }
      }, this::documentSaved);
   }

   @Override
   public Optional<DocumentEntry> deleteDocument(DocumentPath documentId) {
      notNull(documentId, "documentId");

      startWatch();

      return handleDocumentEntryEvent(() -> {

         try (Session<LsEntry> session = sftpSessionFactory.getSession()) {
            Path sftpFile = sftpFile(documentId);
            Optional<DocumentEntry> documentEntry = documentEntry(session, sftpFile);

            boolean removed = documentEntry.isPresent() && session.remove(sftpFile.toString());

            return documentEntry.filter(__ -> removed);
         } catch (IOException | UncheckedIOException e) {
            throw new DocumentAccessException(e);
         }
      }, this::documentDeleted);
   }

   @Override
   public boolean supportsUri(URI documentUri) {
      return transformerUriAdapter.supportsUri(documentUri);
   }

   @Override
   public URI toUri(DocumentPath documentId) {
      return transformerUriAdapter.toUri(documentId);
   }

   @Override
   public Optional<Document> findDocumentByUri(URI documentUri) {
      return transformerUriAdapter.findDocumentByUri(documentUri);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
      return transformerUriAdapter.findDocumentEntryByUri(documentUri);
   }

   /**
    * Closes {@link CachingSessionFactory} pool sessions if needed.
    */
   @Override
   public void close() {
      if (this.sftpSessionFactory instanceof CachingSessionFactory) {
         ((CachingSessionFactory<LsEntry>) this.sftpSessionFactory).destroy();
      }
   }

   /**
    * Real SFTP URI adapter to be used by {@link DocumentTransformerUriAdapter}.
    * <p>
    * For security purpose, user password is never part of the URI (and it does not help to uniquely identify
    * a resource).
    *
    * @see <a href="https://datatracker.ietf.org/doc/html/draft-ietf-secsh-scp-sftp-ssh-uri-04">SFTP URI
    *       RFC</a>
    */
   public class SftpUriAdapter implements UriAdapter {
      private static final String URI_SCHEME = "sftp";

      @Override
      public boolean supportsUri(URI documentUri) {
         validate(documentUri, "documentUri", UriRules.hasNoTraversal()).orThrow();

         return documentUri.isAbsolute()
                && documentUri.getScheme().equals(URI_SCHEME)
                && (documentUri.getAuthority() != null
                    && sftpDocumentConfig
                          .host()
                          .equals(documentUri.getHost())
                    && sftpDocumentConfig.port().equals(documentUri.getPort())
                    && sftpDocumentConfig.username().equals(documentUri.getUserInfo()))
                && documentUri.getFragment() == null
                && documentUri.getQuery() == null
                && documentUri.getPath() != null;
      }

      @Override
      public URI toUri(DocumentPath documentId) {
         notNull(documentId, "documentId");

         try {
            return new URI(URI_SCHEME,
                           sftpDocumentConfig.username(),
                           sftpDocumentConfig.host(),
                           sftpDocumentConfig.port(),
                           "/" + documentId.stringValue(),
                           null,
                           null);
         } catch (URISyntaxException e) {
            throw new UnsupportedUriException(e);
         }

      }

      @Override
      public Optional<Document> findDocumentByUri(URI documentUri) {
         satisfies(documentUri, this::supportsUri, "documentUri", "must be supported : " + documentUri);

         return findDocumentById(documentId(documentUri));
      }

      @Override
      public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
         satisfies(documentUri, this::supportsUri, "documentUri", "must be supported : " + documentUri);

         return findDocumentEntryById(documentId(documentUri));
      }

      /**
       * Extracts document identifier from specified document URI.
       *
       * @param documentUri document URI
       *
       * @return document identifier
       */
      protected DocumentPath documentId(URI documentUri) {
         return DocumentPath.of(Paths.get("/").relativize(Paths.get(documentUri.getPath())));
      }
   }

   /** Generates a hash unique for a given server storage path to identify a similar repository. */
   private int sameRepositoryHash(SftpDocumentConfig sftpDocumentConfig) {
      return Objects.hash(sftpDocumentConfig.host(),
                          sftpDocumentConfig.port(),
                          sftpDocumentConfig.username(),
                          sftpDocumentConfig.basePath());
   }

   private static SessionFactory<LsEntry> sftpSessionFactory(SftpDocumentConfig sftpDocumentConfig) {
      notNull(sftpDocumentConfig, "sftpDocumentConfig");

      DefaultSftpSessionFactory factory = new DefaultSftpSessionFactory(true);
      factory.setHost(sftpDocumentConfig.host());
      factory.setPort(sftpDocumentConfig.port());
      factory.setUser(sftpDocumentConfig.username());
      factory.setPassword(sftpDocumentConfig.password());
      factory.setEnableDaemonThread(false);
      if (sftpDocumentConfig.password() == null) {
         factory.setPrivateKey(new ByteArrayResource(sftpDocumentConfig.privateKey()));
      }
      factory.setAllowUnknownKeys(SESSION_ALLOW_UNKNOWN_KEYS);

      CachingSessionFactory<LsEntry> cachingSessionFactory =
            new CachingSessionFactory<>(factory, sftpDocumentConfig.sessionPoolSize());
      cachingSessionFactory.setTestSession(CACHING_SESSION_TEST_SESSION);
      cachingSessionFactory.setSessionWaitTimeout(CACHING_SESSION_WAIT_TIMEOUT);

      return cachingSessionFactory;
   }

   private Predicate<Path> directoryFilter(Specification<DocumentEntry> specification) {
      if (specification instanceof DocumentEntrySpecification) {
         DocumentEntrySpecification documentEntrySpecification = (DocumentEntrySpecification) specification;
         return documentEntrySpecification::satisfiedBySubPath;
      } else {
         return __ -> true;
      }
   }

   /**
    * Gets document entry for backend-space file.
    *
    * @param session SFTP session
    * @param sftpFile backend-space file
    */
   private Optional<DocumentEntry> documentEntry(Session<LsEntry> session, Path sftpFile) {
      notNull(session, "session");
      notNull(sftpFile, "sftpFile");

      Path sftpFileParent = nullable(sftpFile.getParent(), () -> Paths.get(""));

      return listDocumentEntries(session, sftpFileParent, __ -> false)
            .filter(entry -> entry.documentId().value().getFileName().equals(sftpFile.getFileName()))
            .findAny();
   }

   /**
    * Lists recursively all document entries from backend.
    *
    * @param session SFTP session
    */
   private Stream<DocumentEntry> listAllDocumentEntries(Session<LsEntry> session) {
      return listDocumentEntries(session, basePath, __ -> true);
   }

   /**
    * Lists document entries from specified backend-space directory.
    *
    * @param session SFTP session
    * @param baseDirectory backend-space directory to search from
    * @param directoryFilter user-space directory filter.Use {@code __ -> false} to disable recursive
    *       search
    *
    * @return filtered document entries
    */
   private Stream<DocumentEntry> listDocumentEntries(Session<LsEntry> session,
                                                     Path baseDirectory,
                                                     Predicate<Path> directoryFilter) {
      notNull(session, "session");
      notNull(baseDirectory, "baseDirectory");
      notNull(directoryFilter, "directoryFilter");

      String sessionBaseDirectory = baseDirectory.toString().isEmpty() ? "." : baseDirectory.toString();

      try {
         if (session.exists(sessionBaseDirectory)) {
            return Stream
                  .of(session.list(sessionBaseDirectory))
                  .filter(traversalEntry().negate())
                  .flatMap(lsEntry -> {
                     Path sftpPath = baseDirectory.resolve(lsEntry.getFilename());

                     if (lsEntry.getAttrs().isDir()) {
                        if (directoryFilter.test(documentDirectory(sftpPath))) {
                           return listDocumentEntries(session, sftpPath, directoryFilter);
                        } else {
                           return Stream.empty();
                        }
                     } else if (lsEntry.getAttrs().isReg()) {
                        return Stream.of(lsEntry).map(entry -> sftpDocumentEntry(sftpPath, entry));
                     } else {
                        return Stream.empty();
                     }
                  });
         }
      } catch (IOException | UncheckedIOException e) {
         log.warn("Error while accessing '{}' directory : {}", sessionBaseDirectory, e.getMessage());
      }

      return Stream.empty();
   }

   private Predicate<LsEntry> traversalEntry() {
      return lsEntry -> {
         String filename = lsEntry.getFilename();

         return filename.equals(".") || filename.equals("..");
      };
   }

   private DocumentEntry sftpDocumentEntry(Path sftpFile, LsEntry entry) {
      return new DocumentEntryBuilder()
            .<DocumentEntryBuilder>reconstitute()
            .documentId(documentId(sftpFile))
            .metadata(sftpDocumentMetadata(sftpFile, entry))
            .build();
   }

   private DocumentMetadata sftpDocumentMetadata(Path sftpFile, LsEntry entry) {
      return new DocumentMetadataBuilder()
            .<DocumentMetadataBuilder>reconstitute()
            .documentPath(documentId(sftpFile).value())
            .contentSize(entry.getAttrs().getSize())
            .creationDate(null)
            .lastUpdateDate(Instant.ofEpochSecond(entry.getAttrs().getMTime()))
            .build();
   }

   private InputStreamDocumentContent sftpDocumentContent(Session<LsEntry> session,
                                                          Path sftpFile,
                                                          Long contentSize) {
      try {
         return new InputStreamDocumentContentBuilder()
               .<InputStreamDocumentContentBuilder>reconstitute()
               .content(session.readRaw(sftpFile.toString()), contentSize)
               .build();
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   /**
    * Generates real SFTP file path for specified document.
    *
    * @param documentId document id
    *
    * @return document real SFTP file path
    */
   private Path sftpFile(DocumentPath documentId) {
      return basePath.resolve(documentId.value());
   }

   /**
    * Generates real SFTP directory path for specified document directory.
    *
    * @param documentDirectory document directory
    *
    * @return document real SFTP directory path
    */
   private Path sftpDirectory(Path documentDirectory) {
      return basePath.resolve(documentDirectory);
   }

   /**
    * Returns document identifier from SFTP document file. Reverses real path to generate a
    * logical document path.
    *
    * @param sftpFile SFTP document file
    *
    * @return document identifier from SFTP file
    */
   private DocumentPath documentId(Path sftpFile) {
      return DocumentPath.of(basePath.relativize(sftpFile));
   }

   /**
    * Returns document directory from SFTP document directory. Reverses real path to generate a
    * logical document directory path.
    *
    * @param sftpDirectory SFTP document directory
    *
    * @return document directory
    */
   private Path documentDirectory(Path sftpDirectory) {
      return basePath.relativize(sftpDirectory);
   }

   /**
    * Create specified directory recursively. Path represents a backend-space directory.
    *
    * @param session SFTP session
    * @param directory directory to create
    *
    * @throws IOException if an I/O error occurs
    */
   private void sftpMkdirs(Session<LsEntry> session, Path directory) throws IOException {
      notNull(session, "session");
      notNull(directory, "directory");

      _sftpMkdirs(session, directory);
   }

   private void _sftpMkdirs(Session<LsEntry> session, Path path) throws IOException {
      if (path != null) {
         _sftpMkdirs(session, path.getParent());
         if (!session.exists(path.toString())) {
            session.mkdir(path.toString());
         }
      }
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", SftpDocumentRepository.class.getSimpleName() + "[", "]")
            .add("basePath=" + basePath)
            .toString();
   }
}
