/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp;

import static java.util.Collections.singletonList;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.function.UnaryOperator;

import org.apache.commons.io.FileUtils;
import org.apache.sshd.common.file.virtualfs.VirtualFileSystemFactory;
import org.apache.sshd.server.SshServer;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.apache.sshd.sftp.server.SftpSubsystemFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.springframework.integration.file.remote.session.SessionFactory;

import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.tinubu.commons.ports.document.domain.CommonDocumentRepositoryTest;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ZipTransformer;

public abstract class AbstractSftpDocumentRepositoryTest extends CommonDocumentRepositoryTest {

   /** SFTP server storage path. */
   private static Path TEST_STORAGE_PATH;
   protected static final String SSH_SERVER_USERNAME = "user";
   protected static final String SSH_SERVER_PASSWORD = "password";

   private static SshServer server;
   private static SessionFactory<LsEntry> sftpSession;
   private SftpDocumentRepository documentRepository;

   public abstract SftpDocumentConfig sftpDocumentConfig(SshServer server);

   @BeforeAll
   public static void startSftpServer() throws IOException {
      try {
         TEST_STORAGE_PATH = Files.createTempDirectory("chassis-document-sftp-test");

         System.out.printf("Creating %s as SFTP storage directory%n", TEST_STORAGE_PATH);

      } catch (IOException e) {
         throw new IllegalStateException(e);
      }

      server = SshServer.setUpDefaultServer();
      server.setPasswordAuthenticator((username, password, session) -> true);
      server.setPort(0);

      Path hostKey = Files.createTempFile("hostkey", ".ser");
      hostKey.toFile().deleteOnExit();

      server.setKeyPairProvider(new SimpleGeneratorHostKeyProvider(hostKey));
      server.setSubsystemFactories(singletonList(new SftpSubsystemFactory()));
      server.setFileSystemFactory(new VirtualFileSystemFactory(TEST_STORAGE_PATH));

      server.start();
   }

   @AfterAll
   public static void stopSftpServer() throws IOException {
      server.stop(true);

      try {
         System.out.printf("Deleting test storage path %s%n", TEST_STORAGE_PATH);

         FileUtils.deleteDirectory(TEST_STORAGE_PATH.toFile());
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   @BeforeEach
   public void configureDocumentRepository() {
      this.documentRepository = newSftpDocumentRepository(server);
   }

   @Override
   public boolean isSupportingUri() {
      return true;
   }

   @Override
   protected DocumentRepository documentRepository() {
      return documentRepository;
   }

   @Override
   protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
      return new ZipTransformer(zipPath).compress(documents);
   }

   private SftpDocumentRepository newSftpDocumentRepository(SshServer server) {
      return new SftpDocumentRepository(sftpDocumentConfig(server));
   }

   @Override
   protected UnaryOperator<DocumentMetadataBuilder> synchronizeExpectedMetadata(DocumentMetadata actual) {
      return builder -> builder
            .chain(super.synchronizeExpectedMetadata(actual))
            .attributes(actual.attributes())
            .contentType(actual.contentEncoding().orElse(null));
   }

   @Override
   @Disabled("Not supported")
   public void testOpenDocumentWhenNominal() {}

   @Override
   @Disabled("Not supported")
   public void testOpenDocumentWhenOverwriteAndAppend() {}

   @Override
   @Disabled("Not supported")
   public void testOpenDocumentWhenOverwriteAndNotAppend() {}

   @Override
   @Disabled("Not supported")
   public void testOpenDocumentWhenNotOverwriteAndAppend() {}

   @Override
   @Disabled("Not supported")
   public void testOpenDocumentWhenNotOverwriteAndNotAppend() {}

}