/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp;

import org.apache.sshd.server.SshServer;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;

import com.tinubu.commons.ports.document.sftp.SftpDocumentConfig.SftpDocumentConfigBuilder;

public class SftpDocumentRepositoryTest {

   @Nested
   public class WhenSessionPoolSizeOne extends AbstractSftpDocumentRepositoryTest {

      @Override
      public SftpDocumentConfig sftpDocumentConfig(SshServer server) {
         return new SftpDocumentConfigBuilder()
               .host(server.getHost())
               .port(server.getPort())
               .username(SSH_SERVER_USERNAME)
               .password(SSH_SERVER_PASSWORD)
               .sessionPoolSize(1)
               .build();
      }
   }

   @Nested
   public class WhenServerBasePath extends AbstractSftpDocumentRepositoryTest {

      @Override
      public SftpDocumentConfig sftpDocumentConfig(SshServer server) {
         return new SftpDocumentConfigBuilder()
               .basePath(path("/"))
               .host(server.getHost())
               .port(server.getPort())
               .username(SSH_SERVER_USERNAME)
               .password(SSH_SERVER_PASSWORD)
               .sessionPoolSize(1)
               .build();
      }

      @Override
      @Disabled("Not supported")
      public void testOpenDocumentWhenNominal() {}

      @Override
      @Disabled("Not supported")
      public void testOpenDocumentWhenOverwriteAndAppend() {}

      @Override
      @Disabled("Not supported")
      public void testOpenDocumentWhenOverwriteAndNotAppend() {}

      @Override
      @Disabled("Not supported")
      public void testOpenDocumentWhenNotOverwriteAndAppend() {}

      @Override
      @Disabled("Not supported")
      public void testOpenDocumentWhenNotOverwriteAndNotAppend() {}

   }

}