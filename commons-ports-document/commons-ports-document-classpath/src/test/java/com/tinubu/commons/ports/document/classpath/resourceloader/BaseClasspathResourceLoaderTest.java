/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.classpath.resourceloader;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.io.IOException;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.lang.util.Pair;

public abstract class BaseClasspathResourceLoaderTest {

   protected abstract ClasspathResourceLoader resourceLoader();

   @Test
   public void testResourceWhenNominal() throws IOException {
      assertThat(resourceLoader().resource(Paths.get(
            "/com/tinubu/commons/ports/document/classpath/rootfile2.txt"))).hasValueSatisfying(resource -> {
         assertThat(resource.name()).isEqualTo("rootfile2.txt");
         try {
            assertThat(resource.isDirectory()).isFalse();
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
         try {
            assertThat(resource.content()).hasContent("rootfile2.txt");
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
      });
   }

   @Test
   public void testResourceWhenBadParameters() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> resourceLoader().resource(null))
            .withMessage("Invariant validation error : 'resourcePath' must not be null");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> resourceLoader().resource(Paths.get("")))
            .withMessage("Invariant validation error : 'resourcePath' must not be empty");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> resourceLoader().resource(Paths.get("path")))
            .withMessage("Invariant validation error : 'resourcePath=path' must be absolute path");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> resourceLoader().resource(Paths.get(" ")))
            .withMessage("Invariant validation error : 'resourcePath= ' must be absolute path");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> resourceLoader().resource(Paths.get("/")))
            .withMessage("Invariant validation error : 'resourcePath=/' must have a filename component");
   }

   @Test
   public void testResourceWhenLocalClasspathDirectory() throws IOException {
      assertThat(resourceLoader().resource(Paths.get("/com/tinubu/commons/ports/document/classpath"))).hasValueSatisfying(
            resource -> {
               assertThat(resource.name()).isEqualTo("classpath");
               try {
                  assertThat(resource.isDirectory()).isTrue();
               } catch (IOException e) {
                  throw new IllegalStateException(e);
               }
               try {
                  assertThat(resource.content()).isNotNull();
               } catch (IOException e) {
                  throw new IllegalStateException(e);
               }
            });
   }

   @Test
   public void testResourceWhenJarClasspath() throws IOException {
      assertThat(resourceLoader().resource(Paths.get("/com/tinubu/commons/lang/validation/Validate.class"))).hasValueSatisfying(
            resource -> {
               assertThat(resource.name()).isEqualTo("Validate.class");
               try {
                  assertThat(resource.isDirectory()).isFalse();
               } catch (IOException e) {
                  throw new IllegalStateException(e);
               }
               try {
                  assertThat(resource.content()).isNotEmpty();
               } catch (IOException e) {
                  throw new IllegalStateException(e);
               }
            });
   }

   @Test
   public void testResourceWhenJarClasspathDirectory() throws IOException {
      assertThat(resourceLoader().resource(Paths.get("/com/tinubu/commons/lang/validation"))).hasValueSatisfying(
            resource -> {
               assertThat(resource.name()).isEqualTo("validation");
               try {
                  assertThat(resource.isDirectory())
                        .as("JAR classpath entries has no directory support")
                        .isFalse();
               } catch (IOException e) {
                  throw new IllegalStateException(e);
               }
               try {
                  assertThat(resource.content()).isNotNull();
               } catch (IOException e) {
                  throw new IllegalStateException(e);
               }
            });
   }

   @Test
   public void testResourcesWhenNominal() throws IOException {
      assertThat(resourceLoader().resources(Paths.get("/com/tinubu/commons/ports/document/classpath/path")))
            .extracting(r -> Pair.of(r.name(), r.isDirectory()))
            .containsExactlyInAnyOrder(Pair.of("subpath", true),
                                       Pair.of("pathfile1.pdf", false),
                                       Pair.of("pathfile2.txt", false));
   }

   @Test
   public void testResourcesWhenBadParameters() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> resourceLoader().resources(null))
            .withMessage("Invariant validation error : 'directoryPath' must not be null");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> resourceLoader().resources(Paths.get("")))
            .withMessage("Invariant validation error : 'directoryPath' must not be empty");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> resourceLoader().resources(Paths.get("path")))
            .withMessage("Invariant validation error : 'directoryPath=path' must be absolute path");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> resourceLoader().resources(Paths.get(" ")))
            .withMessage("Invariant validation error : 'directoryPath= ' must be absolute path");
   }

}