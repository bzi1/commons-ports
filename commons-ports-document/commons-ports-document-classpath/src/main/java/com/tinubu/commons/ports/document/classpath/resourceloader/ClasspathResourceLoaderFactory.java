/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.classpath.resourceloader;

import java.nio.file.Path;

/**
 * Creates an instance of {@link ClasspathResourceLoader}.
 * Multiple implementations are supported if the following dependencies are on the classpath, and in this
 * order :
 * <ul>
 *    <li>org.springframework:spring-core</li>
 * </ul>
 * <p>
 * If no implementation is found, a default {@link SystemClasspathResourceLoader system resource loader} is
 * returned, that does not support {@link ClasspathResourceLoader#resources(Path)}).
 */
public class ClasspathResourceLoaderFactory {

   /**
    * Creates a resource loader from available implementations.
    *
    * @return new {@link ClasspathResourceLoader} instance
    *
    * @throws IllegalStateException if no supported implementation found
    */
   public static ClasspathResourceLoader resourceLoader() {
      if (detectSpringCore()) {
         return new SpringCoreClasspathResourceLoader();
      } else {
         return new SystemClasspathResourceLoader();
      }
   }

   private static boolean detectSpringCore() {
      return detect("org.springframework.core.io.support.PathMatchingResourcePatternResolver");
   }

   private static boolean detect(String className) {
      try {
         Class.forName(className);
         return true;
      } catch (Throwable ex) {
         return false;
      }
   }

}
