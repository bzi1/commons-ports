/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.classpath.resourceloader;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasFileName;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotEmpty;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;

/**
 * {@link ClasspathResourceLoader} implementation using default JDK.
 *
 * @implSpec {@link #resources(Path)} is not supported.
 */
public class SystemClasspathResourceLoader implements ClasspathResourceLoader {

   private static ClassLoader defaultClassLoader = defaultClassLoader();

   @Override
   public Optional<ClasspathResource> resource(Path resourcePath) {
      validate(resourcePath,
               "resourcePath",
               isNotEmpty().andValue(isAbsolute().andValue(hasFileName()))).orThrow();

      Path relativeResourcePath = Paths.get("/").relativize(resourcePath.normalize());

      return nullable(defaultClassLoader.getResourceAsStream(relativeResourcePath.toString())).map(inputStream -> new ClasspathResource() {

         @Override
         public String name() {
            return resourcePath.getFileName().toString();
         }

         @Override
         public boolean isDirectory() {
            File resourceFile =
                  FileUtils.toFile(defaultClassLoader.getResource(relativeResourcePath.toString()));

            return resourceFile != null && resourceFile.isDirectory();
         }

         @Override
         public InputStream content() {
            return inputStream;
         }
      });
   }

   @Override
   public Stream<ClasspathResource> resources(Path directoryPath) {
      validate(directoryPath, "directoryPath", isNotEmpty().andValue(isAbsolute())).orThrow();

      throw new UnsupportedOperationException(
            "Resources listing in classpath directory is not supported by system loader");
   }

   /**
    * Retrieves a {@link ClassLoader} from local context.
    *
    * @return class loader, never {@code null}
    */
   protected static ClassLoader defaultClassLoader() {
      ClassLoader cl = null;
      try {
         cl = Thread.currentThread().getContextClassLoader();
      } catch (Throwable ex) {
         /* Security exception or other internal exception */
      }

      if (cl == null) {
         cl = SystemClasspathResourceLoader.class.getClassLoader();
      }

      if (cl == null) {
         cl = ClassLoader.getSystemClassLoader();
      }

      return cl;
   }

}
