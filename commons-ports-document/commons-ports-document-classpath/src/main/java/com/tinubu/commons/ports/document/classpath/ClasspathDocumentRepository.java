/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.classpath;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.BiFunction;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.invariant.rules.UriRules;
import com.tinubu.commons.ports.document.classpath.resourceloader.ClasspathResource;
import com.tinubu.commons.ports.document.classpath.resourceloader.ClasspathResourceLoader;
import com.tinubu.commons.ports.document.classpath.resourceloader.ClasspathResourceLoaderFactory;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.UnsupportedUriException;
import com.tinubu.commons.ports.document.domain.UriAdapter;
import com.tinubu.commons.ports.document.transformer.DocumentTransformerUriAdapter;

/**
 * Classpath {@link DocumentRepository} adapter implementation.
 */
public class ClasspathDocumentRepository extends AbstractDocumentRepository {

   private final Path classpathPrefix;
   private final DocumentTransformerUriAdapter transformerUriAdapter;
   private final ClasspathResourceLoader resourceLoader;

   public ClasspathDocumentRepository(Path classpathPrefix) {
      this.transformerUriAdapter = new DocumentTransformerUriAdapter(new ClasspathUriAdapter());
      this.classpathPrefix = validate(classpathPrefix,
                                      "classpathPrefix",
                                      isNotNull().andValue(isAbsolute().andValue(hasNoTraversal())))
            .orThrow()
            .normalize();
      resourceLoader = ClasspathResourceLoaderFactory.resourceLoader();
   }

   public ClasspathDocumentRepository() {
      this(Paths.get("/"));
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      return documentRepository instanceof ClasspathDocumentRepository
             && Objects.equals(((ClasspathDocumentRepository) documentRepository).classpathPrefix,
                               classpathPrefix);
   }

   @Override
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      throw new UnsupportedOperationException("Classpath repository is read-only");
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      notNull(documentId, "documentId");

      startWatch();

      return handleDocumentEvent(() -> resource(documentId).map(r -> document(documentEntry(documentId), r)),
                                 d -> documentAccessed(d.documentEntry()));
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();
      notNull(specification, "specification");

      startWatch();

      return handleDocumentStreamEvent(() -> listResources(basePath,
                                                           specification,
                                                           (resource, documentEntry) -> document(documentEntry,
                                                                                                 resource)),
                                       d -> documentAccessed(d.documentEntry()));
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      notNull(documentId, "documentId");

      startWatch();

      return handleDocumentEntryEvent(() -> resource(documentId).map(r -> documentEntry(documentId)),
                                      this::documentAccessed);
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();
      notNull(specification, "specification");

      startWatch();

      return handleDocumentEntryStreamEvent(() -> listResources(basePath,
                                                                specification,
                                                                (__, documentEntry) -> documentEntry),
                                            this::documentAccessed);
   }

   @Override
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      throw new UnsupportedOperationException("Classpath repository is read-only");
   }

   @Override
   public Optional<DocumentEntry> deleteDocument(DocumentPath documentId) {
      throw new UnsupportedOperationException("Classpath repository is read-only");
   }

   @Override
   public boolean supportsUri(URI documentUri) {
      return transformerUriAdapter.supportsUri(documentUri);
   }

   @Override
   public URI toUri(DocumentPath documentId) {
      return transformerUriAdapter.toUri(documentId);
   }

   @Override
   public Optional<Document> findDocumentByUri(URI documentUri) {
      return transformerUriAdapter.findDocumentByUri(documentUri);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
      return transformerUriAdapter.findDocumentEntryByUri(documentUri);
   }

   /**
    * Real classpath URI adapter to be used by {@link DocumentTransformerUriAdapter}.
    */
   public class ClasspathUriAdapter implements UriAdapter {
      private static final String URI_SCHEME = "classpath";

      @Override
      public boolean supportsUri(URI documentUri) {
         validate(documentUri, "documentUri", UriRules.hasNoTraversal()).orThrow();

         return documentUri.isAbsolute()
                && documentUri.getScheme().equals(URI_SCHEME)
                && documentUri.getPath() != null
                && documentUri.getAuthority() == null
                && documentUri.getFragment() == null
                && documentUri.getQuery() == null
                && Paths.get(documentUri.getPath()).startsWith(classpathPrefix);
      }

      @Override
      public URI toUri(DocumentPath documentId) {
         notNull(documentId, "documentId");

         try {
            return new URI(URI_SCHEME, null, classpathPrefix.resolve(documentId.value()).toString(), null);
         } catch (URISyntaxException e) {
            throw new UnsupportedUriException(e);
         }
      }

      @Override
      public Optional<Document> findDocumentByUri(URI documentUri) {
         satisfies(documentUri, this::supportsUri, "documentUri", "must be supported : " + documentUri);

         return findDocumentById(documentId(documentUri));
      }

      @Override
      public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
         satisfies(documentUri, this::supportsUri, "documentUri", "must be supported : " + documentUri);

         return findDocumentEntryById(documentId(documentUri));
      }

      /**
       * Extracts document identifier from specified document URI.
       *
       * @param documentUri document URI
       *
       * @return document identifier
       */
      protected DocumentPath documentId(URI documentUri) {
         return DocumentPath.of(classpathPrefix.relativize(Paths.get(documentUri.getPath())));
      }
   }

   private DocumentEntry documentEntry(DocumentPath documentId) {
      return new DocumentEntryBuilder()
            .<DocumentEntryBuilder>reconstitute()
            .documentId(documentId)
            .metadata(new DocumentMetadataBuilder().documentPath(documentId.value()).build())
            .build();
   }

   private Document document(DocumentEntry documentEntry, ClasspathResource resource) {
      try {
         return new DocumentBuilder()
               .<DocumentBuilder>reconstitute()
               .documentEntry(documentEntry)
               .streamContent(resource.content())
               .build();
      } catch (IOException e) {
         throw new DocumentAccessException(String.format("Can't read '%s'",
                                                         documentEntry.documentId().stringValue()), e);
      }
   }

   private Optional<ClasspathResource> resource(DocumentPath documentId) {
      try {
         return resourceLoader.resource(classpathPrefix.resolve(documentId.value()));
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }

   }

   /**
    * Returns {@link ClasspathResource} directly present in specified path.
    *
    * @param path path to list, or {@code null}
    *
    * @return resource stream
    */
   private Stream<ClasspathResource> directResources(Path path) {
      try {
         return resourceLoader.resources(classpathPrefix.resolve(nullable(path, () -> Paths.get(""))));
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   /**
    * Searches for resources in specified base path and matching specification.
    *
    * @param basePath search base path
    * @param specification search specification, should be a {@link DocumentEntrySpecification} for
    *       search optimizations
    * @param mapper mapper for found resources
    *
    * @return found resources as document entries
    */
   private <T> Stream<T> listResources(Path basePath,
                                       Specification<DocumentEntry> specification,
                                       BiFunction<ClasspathResource, DocumentEntry, T> mapper) {
      if (specification instanceof DocumentEntrySpecification) {
         if (!((DocumentEntrySpecification) specification).satisfiedBySubPath(basePath)) {
            return stream();
         }
      }

      return directResources(basePath).flatMap(resource -> {
         Path currentPath = basePath.resolve(requireNonNull(resource.name()));

         try {
            if (resource.isDirectory()) {
               return listResources(currentPath, specification, mapper);
            } else {
               return stream(documentEntry(DocumentPath.of(currentPath)))
                     .filter(specification::satisfiedBy)
                     .map(documentEntry -> mapper.apply(resource, documentEntry));
            }
         } catch (IOException e) {
            throw new DocumentAccessException(e);
         }
      });
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", ClasspathDocumentRepository.class.getSimpleName() + "[", "]")
            .add("classPathPrefix=" + classpathPrefix)
            .toString();
   }
}
