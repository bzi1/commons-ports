/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.smtp;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;

import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.mail.MailSenderAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.message.domain.Message;
import com.tinubu.commons.ports.message.domain.Message.MessageBuilder;
import com.tinubu.commons.ports.message.domain.MessageAddress;
import com.tinubu.commons.ports.message.domain.MessageContent.MessageContentBuilder;
import com.tinubu.commons.ports.message.domain.MessageDocument.MessageDocumentBuilder;
import com.tinubu.commons.ports.message.smtp.autoconfigure.CommonPortsMessageSmtpAutoConfiguration;

@SpringBootTest(classes = SmtpServiceTest.class)
@ImportAutoConfiguration({ CommonPortsMessageSmtpAutoConfiguration.class, MailSenderAutoConfiguration.class })
public class SmtpServiceTest {

   @Autowired
   SmtpMessageService smtp;

   @Test
   @Disabled("Manual test")
   public void test() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("noreply@domain.tld"))
            .toAddress(MessageAddress.of("test@domain.tld"))
            .subject("subject")
            .content(MessageContentBuilder.of("content", parseMimeType("text/plain")))
            .addAttachments(MessageDocumentBuilder.create((new DocumentBuilder()
                                                                 .documentId(DocumentPath.of("document.txt"))
                                                                 .loadedContent("document content",
                                                                                StandardCharsets.UTF_8)
                                                                 .build())))
            .build();

      smtp.sendMessage(message);
   }

}
