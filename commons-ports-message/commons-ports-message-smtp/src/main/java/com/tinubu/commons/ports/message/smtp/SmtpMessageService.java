/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.smtp;

import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_PLAIN;
import static com.tinubu.commons.lang.validation.Validate.satisfies;

import java.io.IOException;
import java.io.InputStream;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Repository;

import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.message.domain.AbstractMessageService;
import com.tinubu.commons.ports.message.domain.Message;
import com.tinubu.commons.ports.message.domain.MessageAddress;
import com.tinubu.commons.ports.message.domain.MessageDocument;
import com.tinubu.commons.ports.message.domain.MessageIOException;
import com.tinubu.commons.ports.message.domain.MessageService;

/**
 * SMTP {@link MessageService} adapter implementation.
 *
 * @implNote Component is {@link Lazy} and named to support customized adapter initialization and
 *       referencing.
 */
@Repository("commons-ports.message.smtpMessageService")
@Lazy
public class SmtpMessageService extends AbstractMessageService {
   /** Multipart mode to use for client compatibility. */
   private static final int MULTIPART_MODE = MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED;

   private final JavaMailSender javaMailSender;

   @Autowired
   public SmtpMessageService(JavaMailSender javaMailSender) {
      this.javaMailSender = javaMailSender;
   }

   @Override
   // FIXME close document input stream ? introduce a custom, streamed, auto-cloesable, Datasource ?
   public void sendMessage(Message message) {
      satisfies(message.content(),
                content -> content.isHtml() || content.contentType().equalsTypeAndSubtype(TEXT_PLAIN),
                "message.content",
                String.format("mime type is not supported : %s", message.content().contentType()));

      restartWatch();

      MimeMessage mimeMessage = javaMailSender.createMimeMessage();

      try {
         MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, MULTIPART_MODE);
         helper.setFrom(message.fromAddress().address());
         helper.setTo(message.toAddress().address());
         helper.setCc(message.ccAddresses().stream().map(MessageAddress::address).toArray(String[]::new));
         helper.setBcc(message.bccAddresses().stream().map(MessageAddress::address).toArray(String[]::new));
         if (message.subject().isPresent()) {
            helper.setSubject(message.subject().get());
         }
         helper.setText(message.content().content(), message.content().isHtml());

         for (MessageDocument attachment : message.attachments()) {
            helper.addAttachment(attachment.document().metadata().documentName(),
                                 documentDataSource(attachment.document()));
         }

         for (MessageDocument inline : message.inlines()) {
            helper.addInline(inline.messageDocumentId(), documentDataSource(inline.document()));
         }

         javaMailSender.send(mimeMessage);

         publishMessageSent(message);
      } catch (MessagingException | MailException | IOException e) {
         throw new MessageIOException(e.getMessage(), e);
      }
   }

   /**
    * Builds {@link DataSource} from {@link Document}. Ensures data source has correct content encoding from
    * specified document.
    *
    * @param document document to builds data source from
    *
    * @throws IOException if an I/O error occurs
    */
   private DataSource documentDataSource(Document document) throws IOException {
      try (InputStream documentContent = document.content().inputStreamContent()) {
         return new ByteArrayDataSource(documentContent, documentContentType(document));
      }
   }

   /**
    * Builds document content type MIME as a String including the charset from document encoding if any.
    *
    * @param document document to extract content type
    *
    * @return document content type including the charset if any
    */
   private String documentContentType(Document document) {
      MimeType contentType = document.metadata().contentType();

      return contentType.toString();
   }

}
