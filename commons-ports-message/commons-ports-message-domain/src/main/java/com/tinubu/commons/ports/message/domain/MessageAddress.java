package com.tinubu.commons.ports.message.domain;

import static com.tinubu.commons.ddd2.domain.type.support.TypeSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Getter;

/**
 * Generic message address.
 */
public class MessageAddress extends AbstractValue {

   protected final String address;

   protected MessageAddress(String address) {
      this.address = address;
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends MessageAddress> defineDomainFields() {
      return Fields
            .<MessageAddress>builder()
            .superFields((Fields<MessageAddress>) super.defineDomainFields())
            .field("address", v -> v.address, isNotBlank())
            .build();
   }

   public static MessageAddress of(String address) {
      return checkInvariants(new MessageAddress(address));
   }

   @Getter
   public String address() {
      return address;
   }
}
