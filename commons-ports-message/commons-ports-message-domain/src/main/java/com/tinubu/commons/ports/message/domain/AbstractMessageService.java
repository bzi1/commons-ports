/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.domain;

import static com.tinubu.commons.lang.util.CollectionUtils.list;

import org.apache.commons.lang3.time.StopWatch;

import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ddd2.domain.event.SynchronousDomainEventService;
import com.tinubu.commons.ports.message.domain.event.LoggingMessageServiceListener;
import com.tinubu.commons.ports.message.domain.event.MessageSent;
import com.tinubu.commons.ports.message.domain.event.MessageServiceEvent;

public abstract class AbstractMessageService implements MessageService {

   protected final SynchronousDomainEventService eventService;
   protected final StopWatch watch = new StopWatch();

   public AbstractMessageService() {
      this.eventService = new SynchronousDomainEventService();

      for (Class<? extends MessageServiceEvent> event : list(MessageSent.class)) {
         registerEventListener(event, new LoggingMessageServiceListener<>());
      }

   }

   protected void restartWatch() {
      this.watch.reset();
      this.watch.start();
   }

   protected void stopWatch() {
      this.watch.stop();
   }

   @Override
   public <E extends MessageServiceEvent> void registerEventListener(Class<E> eventClass,
                                                                     DomainEventListener<E> eventListener) {
      this.eventService.registerEventListener(eventClass, eventListener);
   }

   @Override
   public void unregisterEventListeners() {
      eventService.unregisterEventListeners();
   }

   public void publishEvent(MessageServiceEvent event) {
      eventService.publishEvent(event);
   }

   public void publishMessageSent(Message message, StopWatch watch) {
      publishEvent(new MessageSent(this, message, watch));
   }

   public void publishMessageSent(Message message) {
      publishEvent(new MessageSent(this, message));
   }

}
