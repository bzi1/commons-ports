/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.domain.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ports.message.domain.MessageService;

/**
 * Logging listener for {@link MessageService} events.
 * Change {@link LoggingMessageServiceListener} log level to 'info' to see these logs.
 */
public class LoggingMessageServiceListener<E extends MessageServiceEvent> implements DomainEventListener<E> {

   private static final Logger log = LoggerFactory.getLogger(LoggingMessageServiceListener.class);

   @Override
   public void accept(E event) {
      if (event instanceof MessageSent) {
         log((MessageSent) event);
      }
   }

   public void log(MessageSent event) {
      log.info("Message '{}' from {} to {} successfully sent in {}ms",
               event.message().subject(),
               event.message().fromAddress(),
               event.message().toAddress(),
               event.duration.toMillis());
   }

}
