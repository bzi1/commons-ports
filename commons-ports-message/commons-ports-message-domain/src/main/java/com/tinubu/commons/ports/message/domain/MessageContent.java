package com.tinubu.commons.ports.message.domain;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_XHTML;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_HTML;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.stream.Stream;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.valueformatter.HiddenValueFormatter;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;

/**
 * Message content.
 */
public class MessageContent extends AbstractValue {

   /** Recognized MIME types for HTML content. */
   protected static final MimeType[] HTML_MIME_TYPES = { TEXT_HTML, APPLICATION_XHTML };

   protected final String content;
   protected final MimeType contentType;

   protected MessageContent(MessageContentBuilder builder) {
      this.content = builder.content;
      this.contentType = builder.contentType;
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends MessageContent> defineDomainFields() {
      return Fields
            .<MessageContent>builder()
            .superFields((Fields<MessageContent>) super.defineDomainFields())
            .field("content", v -> v.content, v -> new HiddenValueFormatter().apply(v.content), isNotNull())
            .field("contentType", v -> v.contentType, isNotNull())
            .build();
   }

   /**
    * Returns {@code true} if content is HTML.
    *
    * @return whether content is HTML
    */
   public boolean isHtml() {
      return Stream.of(HTML_MIME_TYPES).anyMatch(contentType::equalsTypeAndSubtype);
   }

   /**
    * Message content.
    *
    * @return message content
    */
   @Getter
   public String content() {
      return content;
   }

   /**
    * Message content type.
    *
    * @return message content type
    */
   @Getter
   public MimeType contentType() {
      return contentType;
   }

   public static MessageContentBuilder reconstituteBuilder() {
      return new MessageContentBuilder().reconstitute();
   }

   public static class MessageContentBuilder extends DomainBuilder<MessageContent> {
      private String content;
      private MimeType contentType;

      public static MessageContent of(String content, MimeType contentType) {
         return new MessageContentBuilder().content(content).contentType(contentType).build();
      }

      public static MessageContentBuilder from(MessageContent messageContent) {
         notNull(messageContent, "messageContent");
         return new MessageContentBuilder()
               .<MessageContentBuilder>reconstitute()
               .content(messageContent.content)
               .contentType(messageContent.contentType);
      }

      @Setter
      public MessageContentBuilder content(String content) {
         this.content = content;
         return this;
      }

      @Setter
      public MessageContentBuilder contentType(MimeType contentType) {
         this.contentType = contentType;
         return this;
      }

      @Override
      public MessageContent buildDomainObject() {
         return new MessageContent(this);
      }
   }

}
