package com.tinubu.commons.ports.message.domain;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoDuplicates;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;

/**
 * Generic message representation.
 */
public class Message extends AbstractValue {

   protected final MessageAddress fromAddress;
   protected final MessageAddress toAddress;
   protected final List<MessageAddress> ccAddresses;
   protected final List<MessageAddress> bccAddresses;
   protected final String subject;
   protected final MessageContent content;
   protected final List<MessageDocument> attachments;
   protected final List<MessageDocument> inlines;

   protected Message(MessageBuilder builder) {
      this.fromAddress = builder.fromAddress;
      this.toAddress = builder.toAddress;
      this.ccAddresses = immutable(list(builder.ccAddresses));
      this.bccAddresses = immutable(list(builder.bccAddresses));
      this.subject = builder.subject;
      this.content = builder.content;
      this.attachments = immutable(list(builder.attachments));
      this.inlines = immutable(list(builder.inlines));
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends Message> defineDomainFields() {
      return Fields
            .<Message>builder()
            .superFields((Fields<Message>) super.defineDomainFields())
            .field("fromAddress", v -> v.fromAddress, isNotNull())
            .field("toAddress", v -> v.toAddress, isNotNull())
            .field("ccAddresses", v -> v.ccAddresses, isNotNull())
            .field("bccAddresses", v -> v.bccAddresses, isNotNull())
            .field("subject", v -> v.subject)
            .field("content", v -> v.content, isNotNull())
            .field("attachments", v -> v.attachments, v -> displayDocuments(v.attachments), isNotNull())
            .field("inlines", v -> v.inlines, v -> displayDocuments(v.inlines), isNotNull())
            .invariants(Invariant.of(() -> this,
                                     as(message -> Stream
                                              .concat(message.attachments().stream(), message.inlines().stream())
                                              .collect(toList()),
                                        hasNoDuplicates(MessageDocument::messageDocumentId))))
            .build();
   }

   protected Object displayDocuments(List<MessageDocument> documents) {
      return documents
            .stream()
            .map(d -> d.messageDocumentId() + "->" + d.document().documentId())
            .collect(joining(",", "[", "]"));
   }

   /**
    * Sender of the message.
    *
    * @return message sender
    */
   @Getter
   public MessageAddress fromAddress() {
      return fromAddress;
   }

   /**
    * Receiver of the message.
    *
    * @return message receiver
    */
   @Getter
   public MessageAddress toAddress() {
      return toAddress;
   }

   /**
    * Optional Carbon Copy (CC) receivers of the message.
    *
    * @return message CC receivers or empty list
    */
   @Getter
   public List<MessageAddress> ccAddresses() {
      return ccAddresses;
   }

   /**
    * Optional Blind Carbon Copy (BCC) receivers of the message.
    *
    * @return message BCC receivers or empty list
    */
   @Getter
   public List<MessageAddress> bccAddresses() {
      return bccAddresses;
   }

   /**
    * Optional message subject.
    * Message subject can also be blank.
    *
    * @return message subject
    */
   @Getter
   public Optional<String> subject() {
      return nullable(subject);
   }

   /**
    * Message content.
    *
    * @return message content
    */
   @Getter
   public MessageContent content() {
      return content;
   }

   /**
    * Optional document attachments.
    *
    * @return attachments or empty list
    */
   @Getter
   public List<MessageDocument> attachments() {
      return attachments;
   }

   /**
    * Optional document inlines. These documents are referenced by {@link MessageDocument#messageDocumentId()}
    * in the content.
    *
    * @return attachments or empty list
    */
   @Getter
   public List<MessageDocument> inlines() {
      return inlines;
   }

   public static MessageBuilder reconstituteBuilder() {
      return new MessageBuilder().reconstitute();
   }

   public static class MessageBuilder extends DomainBuilder<Message> {
      private MessageAddress fromAddress;
      private MessageAddress toAddress;
      private List<MessageAddress> ccAddresses = list();
      private List<MessageAddress> bccAddresses = list();
      private String subject;
      private MessageContent content;
      private List<MessageDocument> attachments = list();
      private List<MessageDocument> inlines = list();

      public static MessageBuilder from(Message message) {
         notNull(message, "message");
         return new MessageBuilder()
               .<MessageBuilder>reconstitute()
               .fromAddress(message.fromAddress)
               .toAddress(message.toAddress)
               .ccAddresses(message.ccAddresses)
               .bccAddresses(message.bccAddresses)
               .content(message.content)
               .subject(message.subject)
               .attachments(message.attachments)
               .inlines(message.inlines);
      }

      @Setter
      public MessageBuilder fromAddress(MessageAddress from) {
         this.fromAddress = from;

         return this;
      }

      @Setter
      public MessageBuilder toAddress(MessageAddress to) {
         this.toAddress = to;

         return this;
      }

      @Setter
      public MessageBuilder ccAddresses(List<MessageAddress> cc) {
         this.ccAddresses = list(cc);

         return this;
      }

      public MessageBuilder addCcAddresses(MessageAddress... cc) {
         this.ccAddresses.addAll(list(cc));

         return this;
      }

      @Setter
      public MessageBuilder bccAddresses(List<MessageAddress> bcc) {
         this.bccAddresses = list(bcc);

         return this;
      }

      public MessageBuilder addBccAddresses(MessageAddress... bcc) {
         this.bccAddresses.addAll(list(bcc));

         return this;
      }

      @Setter
      public MessageBuilder content(MessageContent content) {
         this.content = content;

         return this;
      }

      @Setter
      public MessageBuilder subject(String subject) {
         this.subject = subject;

         return this;
      }

      @Setter
      public MessageBuilder attachments(List<MessageDocument> attachments) {
         this.attachments = list(attachments);

         return this;
      }

      public MessageBuilder addAttachments(MessageDocument... attachments) {
         this.attachments.addAll(list(attachments));

         return this;
      }

      @Setter
      public MessageBuilder inlines(List<MessageDocument> inlines) {
         this.inlines = list(inlines);

         return this;
      }

      public MessageBuilder addInlines(MessageDocument... inlines) {
         this.inlines.addAll(list(inlines));

         return this;
      }

      @Override
      public Message buildDomainObject() {
         return new Message(this);
      }
   }

}
