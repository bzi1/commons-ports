package com.tinubu.commons.ports.message.domain;

import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ports.message.domain.event.MessageServiceEvent;

/**
 * Message communication service.
 *
 * @implSpec Service implementations can implement {@link #close()} to release resources on
 *       service release.
 */
public interface MessageService extends AutoCloseable {

   /**
    * Sends a message.
    *
    * @param message message to send
    *
    * @throws MessageIOException if an error occurs while sending message
    */
   void sendMessage(Message message);

   /**
    * Registers event listener for this repository.
    *
    * @param eventClass event type class
    * @param eventListener event listener
    * @param <E> event type
    */
   <E extends MessageServiceEvent> void registerEventListener(Class<E> eventClass,
                                                              DomainEventListener<E> eventListener);

   /**
    * Unregisters all event listeners.
    */
   void unregisterEventListeners();

   /**
    * Default no-op close operation.
    */
   @Override
   default void close() {}

}
