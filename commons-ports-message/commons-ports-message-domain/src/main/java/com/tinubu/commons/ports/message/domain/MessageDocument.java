package com.tinubu.commons.ports.message.domain;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.ports.document.domain.Document;

/**
 * A document to attach to {@link Message}.
 * This representation adds support for a unique, normalized, document identifier in the scope of the message,
 * that can be used to inline a document by referencing it.
 */
public class MessageDocument extends AbstractValue {
   private final Document document;
   private final String messageDocumentId;

   protected MessageDocument(MessageDocumentBuilder builder) {
      this.document = builder.document;
      this.messageDocumentId = nullable(builder.messageDocumentId,
                                        this::normalizeMessageDocumentId,
                                        defaultMessageDocumentId(document));
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends MessageDocument> defineDomainFields() {
      return Fields
            .<MessageDocument>builder()
            .superFields((Fields<MessageDocument>) super.defineDomainFields())
            .field("document", v -> v.document, isNotNull())
            .field("messageDocumentId", v -> v.messageDocumentId, isNotBlank())
            .build();
   }

   /**
    * Identifier for this document in the context of the message. By default, message document id is set to
    * document file name. Message document id is normalized (URL encoded).
    *
    * @return document identifier in message
    */
   @Getter
   public String messageDocumentId() {
      return messageDocumentId;
   }

   /**
    * Message document.
    *
    * @return message document
    */
   @Getter
   public Document document() {
      return document;
   }

   /**
    * Generates a default message document id from specified document.
    * Message document id must be normalized.
    *
    * @param document optional document
    *
    * @return message document id, or {@code null} if document not set
    */
   private String defaultMessageDocumentId(Document document) {
      if (document == null) {
         return null;
      } else {
         return normalizeMessageDocumentId(document.metadata().documentName());
      }
   }

   /**
    * URL encode message document id
    *
    * @param messageDocumentId message document id to encode
    *
    * @return URL encoded message document id
    */
   private String normalizeMessageDocumentId(String messageDocumentId) {
      notBlank(messageDocumentId, "messageDocumentId");

      try {
         return URLEncoder.encode(messageDocumentId, StandardCharsets.UTF_8.name());
      } catch (UnsupportedEncodingException e) {
         throw new IllegalStateException(e);
      }
   }

   public static MessageDocumentBuilder reconstituteBuilder() {
      return new MessageDocumentBuilder().reconstitute();
   }

   public static class MessageDocumentBuilder extends DomainBuilder<MessageDocument> {
      private String messageDocumentId;
      private Document document;

      public static MessageDocument create(Document document) {
         return new MessageDocumentBuilder().document(document).build();
      }

      public static MessageDocument create(String messageDocumentId, Document document) {
         return new MessageDocumentBuilder().messageDocumentId(messageDocumentId).document(document).build();
      }

      public static MessageDocumentBuilder from(MessageDocument messageDocument) {
         notNull(messageDocument, "messageDocument");
         return new MessageDocumentBuilder()
               .<MessageDocumentBuilder>reconstitute()
               .messageDocumentId(messageDocument.messageDocumentId)
               .document(messageDocument.document);
      }

      @Setter
      public MessageDocumentBuilder messageDocumentId(String messageDocumentId) {
         this.messageDocumentId = messageDocumentId;
         return this;
      }

      @Setter
      public MessageDocumentBuilder document(Document document) {
         this.document = document;
         return this;
      }

      @Override
      public MessageDocument buildDomainObject() {
         return new MessageDocument(this);
      }
   }

}
