/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.domain.event;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;

import java.time.Duration;

import org.apache.commons.lang3.time.StopWatch;

import com.tinubu.commons.ddd2.domain.event.AbstractDomainEventValue;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.ports.message.domain.Message;
import com.tinubu.commons.ports.message.domain.MessageService;

public class MessageServiceEvent extends AbstractDomainEventValue {

   protected final Message message;
   protected final Duration duration;

   public MessageServiceEvent(MessageService messageService, Message message, Duration duration) {
      super(messageService);
      this.message = message;
      this.duration = duration;
   }

   public MessageServiceEvent(MessageService messageService, Message message) {
      this(messageService, message, Duration.ofMillis(0));
   }

   public MessageServiceEvent(MessageService messageService, Message message, StopWatch watch) {
      this(messageService, message, duration(watch));
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends MessageServiceEvent> defineDomainFields() {
      return Fields
            .<MessageServiceEvent>builder()
            .superFields((Fields<MessageServiceEvent>) super.defineDomainFields())
            .field("message", v -> v.message, isNotNull())
            .field("duration", v -> v.duration, isNotNull())
            .build();
   }

   @Getter
   public Message message() {
      return message;
   }

   @Getter
   public Duration duration() {
      return duration;
   }

   protected static Duration duration(StopWatch stopWatch) {
      return Duration.ofNanos(stopWatch.getNanoTime());
   }

}
