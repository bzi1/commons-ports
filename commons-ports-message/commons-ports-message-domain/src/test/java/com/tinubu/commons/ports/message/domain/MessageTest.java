/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.domain;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.message.domain.Message.MessageBuilder;
import com.tinubu.commons.ports.message.domain.MessageContent.MessageContentBuilder;
import com.tinubu.commons.ports.message.domain.MessageDocument.MessageDocumentBuilder;

public class MessageTest {

   private static final ZonedDateTime FIXED_DATE =
         ZonedDateTime.of(LocalDateTime.of(2021, 11, 10, 14, 0, 0), ZoneId.of("UTC"));

   @BeforeAll
   public static void setApplicationClock() {
      ApplicationClock.setFixedClock(FIXED_DATE);
   }

   @Test
   public void testMessageWhenNominal() {
      assertThat(new MessageBuilder()
                       .fromAddress(MessageAddress.of("noreply@domain.tld"))
                       .toAddress(MessageAddress.of("test@domain.tld"))
                       .subject("subject")
                       .content(MessageContentBuilder.of("content", parseMimeType("text/plain")))
                       .addAttachments(MessageDocumentBuilder.create(new DocumentBuilder()
                                                                           .documentId(DocumentPath.of(
                                                                                 "document.txt"))
                                                                           .loadedContent("document content",
                                                                                          StandardCharsets.UTF_8)
                                                                           .build()))).isNotNull();
   }

   @Test
   public void testMessageWhenEmptyBody() {
      assertThat(new MessageBuilder()
                       .fromAddress(MessageAddress.of("noreply@domain.tld"))
                       .toAddress(MessageAddress.of("test@domain.tld"))
                       .subject("subject")
                       .content(MessageContentBuilder.of("", parseMimeType("text/plain")))).isNotNull();
   }

   @Test
   public void testMessageWhenDuplicatedMessageDocumentIds() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new MessageBuilder()
                  .fromAddress(MessageAddress.of("noreply@domain.tld"))
                  .toAddress(MessageAddress.of("test@domain.tld"))
                  .subject("subject")
                  .content(MessageContentBuilder.of("content", parseMimeType("text/plain")))
                  .addAttachments(MessageDocumentBuilder.create(new DocumentBuilder()
                                                                      .documentId(DocumentPath.of(
                                                                            "document.txt"))
                                                                      .loadedContent("document content",
                                                                                     StandardCharsets.UTF_8)
                                                                      .build()))
                  .addInlines(MessageDocumentBuilder.create(new DocumentBuilder()
                                                                  .documentId(DocumentPath.of("document.txt"))
                                                                  .loadedContent("document content",
                                                                                 StandardCharsets.UTF_8)
                                                                  .build()))
                  .addAttachments(MessageDocumentBuilder.create(new DocumentBuilder()
                                                                      .documentId(DocumentPath.of(
                                                                            "document2.txt"))
                                                                      .loadedContent("document content",
                                                                                     StandardCharsets.UTF_8)
                                                                      .build()))
                  .addInlines(MessageDocumentBuilder.create(new DocumentBuilder()
                                                                  .documentId(DocumentPath.of("document2.txt"))
                                                                  .loadedContent("document content",
                                                                                 StandardCharsets.UTF_8)
                                                                  .build()))
                  .build())
            .withMessage("Invariant validation error in "
                         + "[Message[fromAddress=MessageAddress[address=noreply@domain.tld],toAddress=MessageAddress[address=test@domain.tld],ccAddresses=[],bccAddresses=[],subject=subject,content=MessageContent[content=<hidden-value>,contentType=text/plain],attachments=[document.txt->DocumentPath[value=document.txt,newObject=false,relativePath=true],document2.txt->DocumentPath[value=document2.txt,newObject=false,relativePath=true]],inlines=[document.txt->DocumentPath[value=document.txt,newObject=false,relativePath=true],document2.txt->DocumentPath[value=document2.txt,newObject=false,relativePath=true]]]] context : "
                         + "'[MessageDocument[document=Document[documentId=DocumentPath[value=document.txt,newObject=false,relativePath=true],metadata=DocumentMetadata[documentPath=document.txt,contentType=text/plain;charset=UTF-8,contentSize=16,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8]],messageDocumentId=document.txt], MessageDocument[document=Document[documentId=DocumentPath[value=document2.txt,newObject=false,relativePath=true],metadata=DocumentMetadata[documentPath=document2.txt,contentType=text/plain;charset=UTF-8,contentSize=16,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8]],messageDocumentId=document2.txt], MessageDocument[document=Document[documentId=DocumentPath[value=document.txt,newObject=false,relativePath=true],metadata=DocumentMetadata[documentPath=document.txt,contentType=text/plain;charset=UTF-8,contentSize=16,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8]],messageDocumentId=document.txt], MessageDocument[document=Document[documentId=DocumentPath[value=document2.txt,newObject=false,relativePath=true],metadata=DocumentMetadata[documentPath=document2.txt,contentType=text/plain;charset=UTF-8,contentSize=16,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8]],messageDocumentId=document2.txt]]' has duplicates : "
                         + "{document.txt->["
                         + "MessageDocument[document=Document[documentId=DocumentPath[value=document.txt,newObject=false,relativePath=true],metadata=DocumentMetadata[documentPath=document.txt,contentType=text/plain;charset=UTF-8,contentSize=16,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8]],messageDocumentId=document.txt],"
                         + "MessageDocument[document=Document[documentId=DocumentPath[value=document.txt,newObject=false,relativePath=true],metadata=DocumentMetadata[documentPath=document.txt,contentType=text/plain;charset=UTF-8,contentSize=16,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8]],messageDocumentId=document.txt]],"
                         + "document2.txt->["
                         + "MessageDocument[document=Document[documentId=DocumentPath[value=document2.txt,newObject=false,relativePath=true],metadata=DocumentMetadata[documentPath=document2.txt,contentType=text/plain;charset=UTF-8,contentSize=16,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8]],messageDocumentId=document2.txt],"
                         + "MessageDocument[document=Document[documentId=DocumentPath[value=document2.txt,newObject=false,relativePath=true],metadata=DocumentMetadata[documentPath=document2.txt,contentType=text/plain;charset=UTF-8,contentSize=16,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8]],messageDocumentId=document2.txt]]}");
   }

}